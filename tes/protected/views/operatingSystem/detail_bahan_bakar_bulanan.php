 <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $labelBulan),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'perbulanan',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tampilBulan,

									          'min' => 0 ,
									          'max' => 2
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Kondisi'),
											 //'labels' => array('format' => '{kondisi}'),
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Tanggal {point.key}<br/>',
											//'pointFormat' => 'Pembangkit : wase<br/>',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
										  //'colors'=>array('green' , 'yellow'),
									      'series' => array(
										     array('name' => 'HSD', 'data' => $hasilBulananHsd  ,'type' => 'column'),
										     array('name' => 'MFO', 'data' => $hasilBulananMfo  ,'type' => 'column'),
										     array('name' => 'Olein', 'data' => $hasilBulananOlein  ,'type' => 'column'),
										     array('name' => 'Batu Bara', 'data' => $hasilBulananBatuBara  ,'type' => 'column'),
											
											
									      )
									   )
									));

							?>