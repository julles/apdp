

<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script>
	$(document).ready(
		function()
		{
			$("#putars").click(
				function()
				{
					$("#btn1").click();
					$("#btn2").click();
					$("#btn3").click();
					
				}
			);
		}
	);
</script>
<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
            	<div id="hearts">
      				<a id="putar"href="#" onclick="rotate()" title="Minimize and Maximize"></a>
                </div>
                <div class="judul" align="center">
                    <h2><b>Neraca Daya</b></h2>
                </div>
            	<div id="textnya">
                	
                    <div id="portlet">
                    	<p class="portlet-title"><u>Neraca Daya Harian</u></p>
                    </div>
                	<div id="beban-puncak">
                        <label style="padding-right:0px;">Bulan & Tahun</label>
                        <select class="form-control" style="width:100px;" id = 'bulanHarian'>
                            <option value="">Select</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                        <select class="form-control" style="width:100px;" id = 'tahunHarian'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2000;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn1" class = "btn btn-primary">Tampil</button>' , 
                        		 array('operatingSystem/neracaDayaHarian'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perharian',
                        		 	'data' => array('bulan' => 'js:bulanHarian.value' , 'tahun' => 'js:tahunHarian.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading2").show();
									}',
									'complete' => 'function(){
										$("#loading2").hide();
									}',
                        		 )

                        	);
                         ?>
                        <div id = 'loading2' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        <div id = 'harian'></div>
                        <div class="export2" id = 'perharian'>
                    		<!-- Bar Chart -->

                            <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $hariIniHarian),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'harian',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tampilHari,

									          'min' => 0 ,
									          'max' => 10
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Beban Puncak'),
											 //'labels' => array('format' => '{kondisi}'),
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Tanggal {point.key}<br/>',
											'pointFormat' => 'Beban Puncak : {point.y}<br/>Daya Terpasang : {point.daya_terpasang}<br/>
																	Plan Outage : {point.outage}<br/>  Force Outage : {point.force}<br/>Derating : {point.derating}<br/>
																	Beban Terlayani : {point.terlayani}<br/>Cadangan Operasi : {point.operasi}<br/>Padam : {point.padam}<br/>',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
										  
										  //'colors'=>array('green' , 'yellow'),
									      'series' => array(
									      	
									         array('name' => 'Beban Puncak', 'data' => $modelHarian  ,'type' => 'column'),
											
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									));

							?>
                            <!-- End Bar Chart -->
                    	</div>
                	</div>
                    <div id="portlet">
                    	<p class="portlet-title"><u>Neraca Daya Bulanan</u></p>
                    </div>
                    <?php
                    	echo CHtml::beginForm();
                    ?>
                    <div id="beban-puncak">
                        <label style="padding-right:0px;">Tahun</label>
                        <select class="form-control" style="width:100px;" id = 'tahunBulanan'>
                            <option value="">Select</option>
                           <?php
                        	$year = date('Y');
                        	for($a=2000;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>
                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn2" class = "btn btn-primary">Tampil</button>' , 
                        		 array('operatingSystem/neracaDayaBulanan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perbulanan',
                        		 	'data' => array('tahun' => 'js:tahunBulanan.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading3").show();
									}',
									'complete' => 'function(){
										$("#loading3").hide();
									}',
                        		 )

                        	);
                         ?>

                        <?php
                        	echo CHtml::endForm();
                        ?>
                        <div id = 'loading3' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        
                        <div id = 'bulanan'></div>
                        <div class="export2" id = 'perbulanan'>
                    		<!-- Bar Chart -->
                          <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $hariIniBulanan),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'bulanan',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tampilBulan,

									          'min' => 0 ,
									          'max' => 3
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Beban Puncak'),
											 //'labels' => array('format' => '{kondisi}'),
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Bulan {point.key}<br/>',
											'pointFormat' => 'Beban Puncak : {point.y}<br/>Daya Terpasang : {point.daya_terpasang}<br/>
																	Plan Outage : {point.outage}<br/>  Force Outage : {point.force}<br/>Derating : {point.derating}<br/>
																	Beban Terlayani : {point.terlayani}<br/>Cadangan Operasi : {point.operasi}<br/>Padam : {point.padam}<br/>',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
										  
										  //'colors'=>array('green' , 'yellow'),
									      'series' => array(
									      	
									         array('name' => 'Beban Puncak', 'data' => $modelBulanan  ,'type' => 'column'),
											
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									));

							?>
                           
                            <!-- End Bar Chart -->
                    	</div>
                	</div>
					<div id="portlet">
                    	<p class="portlet-title"><u>Neraca Daya Tahunan</u></p>
                    </div>
                    <div id="beban-puncak">
                    <?php
                    echo CHtml::beginForm();
                    ?>
                        <label style="padding-right:0px;">Tahun</label>
                        <select class="form-control" style="width:100px;" id = 'awal'>
                            <option value="">Select</option>
                            <?php
	                        	$year = date('Y');
	                        	for($a=2000;$a<=$year;$a++)
	                        	{
	                        		echo "<option value = '$a'>$a</option>";
	                        	}
                        	?>
                        </select>
                        <label style="padding-right:0px;">s.d</label>
                        <select class="form-control" style="width:100px;" id = 'akhir'>
                        	<option value="">Select</option>
                            <?php
                        	$year = date('Y');
                        	for($a=2000;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>
                        </select>
                      <?php
                        	echo CHtml::ajaxLink('<button id = "btn3" class = "btn btn-primary">Tampil</button>' , 
                        		 array('operatingSystem/neracaDayaTahunan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#pertahunan',
                        		 	'data' => array('awal' => 'js:awal.value' , 'akhir' => 'js:akhir.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading4").show();
									}',
									'complete' => 'function(){
										$("#loading4").hide();
									}',
                        		 )

                        	);
                         ?>
                        
                        <div id = 'loading4' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        
                        <div id = 'tahunan'></div>
                        <div class="export2" id = 'pertahunan'>
                    		<!-- Bar Chart -->
                            <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $hariIniTahunan),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'tahunan',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tampilTahunan,

									          'min' => 0 ,
									          'max' => 3
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Beban Puncak'),
											 //'labels' => array('format' => '{kondisi}'),
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Tahun {point.key}<br/>',
											'pointFormat' => 'Beban Puncak : {point.y}<br/>Daya Terpasang : {point.daya_terpasang}<br/>
																	Plan Outage : {point.outage}<br/>  Force Outage : {point.force}<br/>Derating : {point.derating}<br/>
																	Beban Terlayani : {point.terlayani}<br/>Cadangan Operasi : {point.operasi}<br/>Padam : {point.padam}<br/>',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
										  
										  //'colors'=>array('green' , 'yellow'),
									      'series' => array(
									      	
									         array('name' => 'Beban Puncak', 'data' => $modelTahunan  ,'type' => 'column'),
											
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									));

							?>
                           
                            <!-- End Bar Chart -->
                    	</div>
                    	<?php
                    		echo CHtml::endForm();
                    	?>
                	</div>
                </div>
            </div>
            <!-- End Pertama -->
        </div>

        <script>
	$(function() {
  		var special = ['reveal', 'top', 'boring', 'perspective', 'extra-pop'];
  		// Toggle Nav on Click
  		$('#hearts a').click(function() {
			var delay_ = 300;
			checkWidth = $('#leftContent').css( "width");
			var widthnya="594";
			heightnya = "400";
			widthbaru = "594"
			widthnya5 = "267";
			widthnya6 = "565";
			LineCharts = "564";
			if( checkWidth < '186'){
			  $('#leftContent').animate( {"width":"186", "height":"100" + "%"}, delay_, "linear");
			  $('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			  $('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			  $('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			  $('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
			}else if(checkWidth > "0"){
			  $('#leftContent').animate({ "width":"0", "height":"0"}, delay_, "linear" );	
				widthnya = "780";
				heightnya = "500";
				widthnya5 = "325";
				widthnya6 = "750";
				LineCharts = "750";
			}
			$('#rightContent').animate({ "width": widthnya}, delay_, "linear" );
			$('.judul').animate({ "width": widthnya}, delay_, "linear" );
			$('.containerKanan').animate({ "width": widthnya}, delay_, "linear" );
			$('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			$('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			$('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			$('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
  		});
	});
</script>