<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $titleBulan ,'style' => 'font-size:16px;'),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'bulanan',
						                       
						                    ),
									      /*'scrollbar' => array(
									      	'enabled' => true
									       ),
										   */
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $bulan,
											'labels' => array(
													'rotation' => 270,
													'y' => 40,
													//'style' => array('font-size' => '5px;')
												 )
									          
									      ),
										  'colors'=>array('green' , 'yellow','red'),
									      'yAxis' => array(
									         'title' => array('text' => 'Hari'),
												  
									      ),
									      
									     
									     /*'tooltip' => array(
											'headerFormat' => '{point.key}<br/>',
											'pointFormat' => '{series.name}:{point.y}<br/>Tanggal: {point.tanggal}<br/>Jam: {point.jam}',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
										  */
									      'series' => array(
									      	
									         array('name' => 'Normal', 'data' => $hasilBulanNormal  ,'type' => 'column'),
											 array('name' => 'Siaga', 'data' => $hasilBulanSiaga  ,'type' => 'column'),
											 array('name' => 'Defisit', 'data' => $hasilBulanDefisit  ,'type' => 'column'),
									        
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									)); 
									
							?>