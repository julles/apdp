<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $hariIniBulanan),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'bulanan',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tampilBulan,

									          'min' => 0 ,
									          'max' => 3
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Beban Puncak'),
											 //'labels' => array('format' => '{kondisi}'),
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Bulan {point.key}<br/>',
											'pointFormat' => 'Beban Puncak : {point.y}<br/>Daya Terpasang : {point.daya_terpasang}<br/>
																	Plan Outage : {point.outage}<br/>  Force Outage : {point.force}<br/>Derating : {point.derating}<br/>
																	Beban Terlayani : {point.terlayani}<br/>Cadangan Operasi : {point.operasi}<br/>Padam : {point.padam}<br/>',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
										  
										  //'colors'=>array('green' , 'yellow'),
									      'series' => array(
									      	
									         array('name' => 'Beban Puncak', 'data' => $modelBulanan  ,'type' => 'column'),
											
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									));

							?>