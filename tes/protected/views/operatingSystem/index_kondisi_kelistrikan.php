

<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script>
	$(document).ready(
		function()
		{
			$("#putars").click(
				function()
				{
					$("#btn1").click();
					$("#btn2").click();
					$("#btn3").click();
					
				}
			);
		}
	);
</script>
<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
            	<div id="hearts">
      				<a id="putar"href="#" onclick="rotate()" title="Minimize and Maximize"></a>
                </div>
                <div class="judul" align="center">
                    <h2><b>Kondisi Kelistrikan</b></h2>
                </div>
            	<div id="textnya">
                	
                    <div id="portlet">
                    	<p class="portlet-title"><u>Kondisi Kelistrikan Perhari</u></p>
                    </div>
                	<div id="beban-puncak">
                        <label style="padding-right:0px;">Bulan & Tahun</label>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'bulanHarian'>
                            <option value="">Select</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'tahunHarian'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn1" style = "height:27px;margin-top:1px;">Tampil</button>' , 
                        		 array('operatingSystem/kondisiKelistrikanDetailHarian'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perharian',
                        		 	'data' => array('bulan' => 'js:bulanHarian.value' , 'tahun' => 'js:tahunHarian.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading2").show();
									}',
									'complete' => 'function(){
										$("#loading2").hide();
									}',
                        		 )

                        	);
                         ?>
                        <div id = 'loading2' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        <div id = 'harian'></div>
                        <div class="export2" id = 'perharian'>
                    		<!-- Bar Chart -->

                            <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $hariIniHarian ,'style' => 'font-size:16px;'),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'harian',
						                       
						                    ),
									     /* 'scrollbar' => array(
									      	'enabled' => true
									       ),
										 */
									      
									      /*'rangeSelector' => array(
									      		'selected' => 1
									       ),
										   */
									      
									      'xAxis' => array(
									         'categories' => $tgl,

									          'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 )
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Kondisi'),
											 'labels' => array('format' => '{kondisi}'),
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Tanggal {point.key}<br/>',
											'pointFormat' => 'Kondisi : {point.kondisi}<br/>',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
										  //'colors'=>array('green' , 'yellow'),
									      'series' => array(
									      	
									         array('name' => 'Kondisi', 'data' => $modelHarian  ,'type' => 'line'),
											
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									));

							?>
                            <!-- End Bar Chart -->
                    	</div>
                	</div>
                    <div id="portlet">
                    	<p class="portlet-title"><u>Kondisi Kelistrikan Perbulan</u></p>
                    </div>
                    <?php
                    	echo CHtml::beginForm();
                    ?>
                    <div id="beban-puncak">
                        <label style="padding-right:0px;">Tahun</label>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'tahunBulanan'>
                            <option value="">Select</option>
                           <?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>
                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn2" style = "height:27px;margin-top:1px;">Tampil</button>' , 
                        		 array('operatingSystem/kondisiKelistrikanDetailBulanan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perbulanan',
                        		 	'data' => array('tahun' => 'js:tahunBulanan.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading3").show();
									}',
									'complete' => 'function(){
										$("#loading3").hide();
									}',
                        		 )

                        	);
                         ?>

                        <?php
                        	echo CHtml::endForm();
                        ?>
                        <div id = 'loading3' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        
                        <div id = 'bulanan'></div>
                        <div class="export2" id = 'perbulanan'>
                    		<!-- Bar Chart -->
                           	<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $titleBulan ,'style' => 'font-size:16px;'),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'bulanan',
						                       
						                    ),
									      /*'scrollbar' => array(
									      	'enabled' => true
									       ),
										   */
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $bulan,
											'labels' => array(
													'rotation' => 270,
													'y' => 40,
													//'style' => array('font-size' => '5px;')
												 )
									          
									      ),
										  'colors'=>array('green' , 'yellow','red'),
									      'yAxis' => array(
									         'title' => array('text' => 'Hari'),
												  
									      ),
									      
									     
									     /*'tooltip' => array(
											'headerFormat' => '{point.key}<br/>',
											'pointFormat' => '{series.name}:{point.y}<br/>Tanggal: {point.tanggal}<br/>Jam: {point.jam}',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
										  */
									      'series' => array(
									      	
									         array('name' => 'Normal', 'data' => $hasilBulanNormal  ,'type' => 'column'),
											 array('name' => 'Siaga', 'data' => $hasilBulanSiaga  ,'type' => 'column'),
											 array('name' => 'Defisit', 'data' => $hasilBulanDefisit  ,'type' => 'column'),
									        
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									)); 
									
							?>
                           
                            <!-- End Bar Chart -->
                    	</div>
                	</div>
					<div id="portlet">
                    	<p class="portlet-title"><u>Kondisi Kelistrikan Pertahun</u></p>
                    </div>
                    <div id="beban-puncak">
                    <?php
                    echo CHtml::beginForm();
                    ?>
                        <label style="padding-right:0px;">Tahun</label>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'awal'>
                            <option value="">Select</option>
                            <?php
	                        	$year = date('Y');
	                        	for($a=2010;$a<=$year;$a++)
	                        	{
	                        		echo "<option value = '$a'>$a</option>";
	                        	}
                        	?>
                        </select>
                        <label style="padding-right:0px;">s.d</label>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'akhir'>
                        	<option value="">Select</option>
                            <?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>
                        </select>
                      <?php
                        	echo CHtml::ajaxLink('<button id = "btn3" style = "height:27px;margin-top:1px;">Tampil</button>' , 
                        		 array('operatingSystem/kondisiKelistrikanDetailTahunan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#pertahunan',
                        		 	'data' => array('awal' => 'js:awal.value' , 'akhir' => 'js:akhir.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading4").show();
									}',
									'complete' => 'function(){
										$("#loading4").hide();
									}',
                        		 )

                        	);
                         ?>
                        
                        <div id = 'loading4' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        
                        <div id = 'tahunan'></div>
                        <div class="export2" id = 'pertahunan'>
                    		<!-- Bar Chart -->
                            <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => 'Pertahun' ,'style' => 'font-size:16px;'),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'tahunan',
						                       
						                    ),
									      /*'scrollbar' => array(
									      	'enabled' => true
									       ),*/
									      
									     /* 'rangeSelector' => array(
									      		'selected' => 1
									       ),
										  */
									      
									      'xAxis' => array(
									         'categories' => $tahun,
											 'labels' => array(
													'rotation' => 270,
													'y' => 40,
													//'style' => array('font-size' => '5px;')
												 )
											),
									       'colors'=>array('green' , 'yellow','red'),
											  'yAxis' => array(
												 'title' => array('text' => 'Hari'),
												 
											  ),
									     /* 'tooltip' => array(
											'headerFormat' => '{point.key}<br/>',
											'pointFormat' => '{series.name}:{point.y}<br/>Tanggal: {point.tanggal}<br/>Jam: {point.jam}',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
									      */
									     'series' => $hasilTahun
									   )
									));
									
							?>
                           
                            <!-- End Bar Chart -->
                    	</div>
                    	<?php
                    		echo CHtml::endForm();
                    	?>
                	</div>
                </div>
            </div>
            <!-- End Pertama -->
        </div>

        <script>
	$(function() {
  		var special = ['reveal', 'top', 'boring', 'perspective', 'extra-pop'];
  		// Toggle Nav on Click
  		$('#hearts a').click(function() {
			var delay_ = 300;
			checkWidth = $('#leftContent').css( "width");
			var widthnya="594";
			heightnya = "400";
			widthbaru = "594"
			widthnya5 = "267";
			widthnya6 = "565";
			LineCharts = "564";
			if( checkWidth < '186'){
			  $('#leftContent').animate( {"width":"186", "height":"100" + "%"}, delay_, "linear");
			  $('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			  $('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			  $('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			  $('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
			}else if(checkWidth > "0"){
			  $('#leftContent').animate({ "width":"0", "height":"0"}, delay_, "linear" );	
				widthnya = "780";
				heightnya = "500";
				widthnya5 = "325";
				widthnya6 = "750";
				LineCharts = "750";
			}
			$('#rightContent').animate({ "width": widthnya}, delay_, "linear" );
			$('.judul').animate({ "width": widthnya}, delay_, "linear" );
			$('.containerKanan').animate({ "width": widthnya}, delay_, "linear" );
			$('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			$('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			$('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			$('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
  		});
	});
</script>