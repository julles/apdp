
<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script>
	$(document).ready(
		function()
		{
			$("#putars").click(
				function()
				{
					$("#btn1").click();
					$("#btn2").click();
					$("#btn3").click();
					$("#btn4").click();
					
				}
			);
		}
	);
</script>
<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
            	<div id="hearts">
      				<a id="putar"href="#" onclick="rotate()" title="Minimize and Maximize"></a>
                </div>
                <div class="judul" align="center">
                    <h2><b>Pembebanan Trafo</b></h2>
                </div>
            	<div id="textnya">
                	<div id="pembebanan-trafo-top" style="height: 60px;">
                        <div class="trafo-panel-top">
                            <div class="trafo-panel-input-top">
                                <label style="padding-right:20px;">GI</label>
                                <!--select class="form-control">
                                    <option value="">All</option>
                                    <option value="1">Gardu Induk 1</option>
                                    <option value="2">Gardu Induk 2</option>
                                    <option value="3">Gardu Induk 3</option>
                                    <option value="4">Gardu Induk 4</option>
                                    <option value="5">Gardu Induk 5</option>
                                </select-->
                                <?php
                                	echo CHtml::dropDownList('GarduInduk' , '' , CHtml::listData(MasterGarduInduk::model()->findAll() , 'id' , 'gardu_induk') , 
                                		array(
                                			'class' => 'form-control',
                                			'empty' => 'All',
                                			'id' => 'gardu_induk_id',
                                			'ajax' => array(
                                				'update' => '#trafo_id',
                                				'type' => 'POST',
                                				'url' => Yii::app()->createUrl("/penyaluran/getTrafo"),
                                				'data' => array(
                                					'gardu_induk_id' => 'js:this.value'
                                				)

                                			)
                                		)
                                	);
                                ?>
                                
                            </div>
                        </div>
                        <div class="trafo-panel-top">
                            <div class="trafo-panel-input-top">
                                <label style="padding-right:10px;">Trafo</label>
                                <?php
                                	echo CHtml::dropDownList('trafo' , '' , array() , array('empty' => 'All' , 'class' => 'form-control' , 'id' =>'trafo_id'));
                                ?>
                            </div>
                        </div>
                        <!--div class="trafo-panel-top" style="text-align: left;">
                            <div class="trafo-panel-input-top">
                                <input type="submit" value="Tampil" class="btn btn-primary">
                            </div>
                        </div-->
                    </div>
                    
                    <div id="textnya">
                	<div id="portlet">
                    	<p class="portlet-title"><u>Beban Trafo Perjam</u></p>
                    </div>
                	<div id="beban-puncak">
                        <?php
                	echo CHtml::beginForm();
                	?>
                        <label style="padding-right:0px;">Tanggal</label>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(

                                'id' => 'tanggalJam',
                                'name'=>'tanggalJam',
                                // additional javascript options for the date picker plugin
                                'options' => array(
                                'showAnim' => 'fold',
                                'dateFormat'=>'yy-mm-dd',
                                 'changeMonth'=>true,
                                'changeYear'=>true,
                                'yearRange'=>'1900:2099',
                                ),
                                'htmlOptions' => array(
                                'style' => 'height:26px;',
                                 'class' => 'form-control'
                                ),
                            ));

                         ?>

                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn1" class = "btn btn-primary">Tampil</button>' , 
                        		 array('penyaluran/pembebananTrafoDetailJam'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perjam',
                        		 	'data' => array('tanggalJam' => 'js:tanggalJam.value' , 'gardu_induk_id' => 'js:gardu_induk_id.value' , 'trafo_id' => 'js:trafo_id.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading1").show();
									}',
									
									'complete' => 'function(){
										$("#loading1").hide();
									}',
                        		 )

                        	);
                         ?>
						<div id = 'loading1' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        <div class="export2" id = 'perjam'>
                            <!-- Line Chart -->
							<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),
							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $hariIni),
									      'width' => '100%',
									      'xAxis' => array(
									         'categories' => $tampil_jam,
									          
									          'min' => 0 ,
									          'max' => 10
									      ),
									      
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'yAxis' => array(
									         'title' => array('text' => 'Daya (Mw)')
									      ),


									     
									      'series' => array(
									      	
									         array('name' => 'Mw', 'data' => $hasilJam),
									         
									         
									      )
									   )
									));

							?>
                            <!-- End Line Chart -->
                    	</div>
						<?php

                    	echo CHtml::endForm();
                    	?>
                	</div>
                    <div id="portlet">
                    	<p class="portlet-title"><u>Beban Trafo Harian</u></p>
                    </div>
                	<div id="beban-puncak">
                        <label style="padding-right:0px;">Bulan & Tahun</label>
                        <select class="form-control" style="width:100px;" id = 'bulanHarian'>
                            <option value="">Select</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                        <select class="form-control" style="width:100px;" id = 'tahunHarian'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2000;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn2" class = "btn btn-primary">Tampil</button>' , 
                        		 array('penyaluran/pembebananTrafoDetailHari'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perharian',
                        		 	'data' => array('bulan' => 'js:bulanHarian.value' , 'tahun' => 'js:tahunHarian.value' , 'gardu_induk_id' => 'js:gardu_induk_id.value' , 'trafo_id' => 'js:trafo_id.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading2").show();
									}',
									'complete' => 'function(){
										$("#loading2").hide();
									}',
                        		 )

                        	);
                         ?>
                        <div id = 'loading2' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        <div id = 'harian'></div>
                        <div class="export2" id = 'perharian'>
                    		<!-- Bar Chart -->

                            <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $labelBulanTahun),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'harian',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tampilHari,

									          'min' => 0 ,
									          'max' => 10
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Daya (Mw)')
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Tanggal {point.key}<br/>',
											'pointFormat' => 'Gardu Induk : {point.gardu}<br/>Trafo : {point.trafo}<br/>{series.name}:{point.y}',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
									      'series' => array(
									      	
									         array('name' => 'Mw', 'data' => $hasilHari  ,'type' => 'column'),
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									));
									

							?>
                            <!-- End Bar Chart -->
                    	</div>
                	</div>




                	<div id="portlet">
                    	<p class="portlet-title"><u>Beban Trafo Bulanan</u></p>
                    </div>
                	<div id="beban-puncak">
                        
                        <select class="form-control" style="width:100px;" id = 'tahunBulanan'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2000;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn3" class = "btn btn-primary">Tampil</button>' , 
                        		 array('penyaluran/pembebananTrafoDetailBulan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perbulanan',
                        		 	'data' => array('tahun' => 'js:tahunBulanan.value' , 'gardu_induk_id' => 'js:gardu_induk_id.value' , 'trafo_id' => 'js:trafo_id.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading3").show();
									}',
									'complete' => 'function(){
										$("#loading3").hide();
									}',
                        		 )

                        	);
                         ?>
                        <div id = 'loading3' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        <div id = 'bulanan'></div>
                        <div class="export2" id = 'perbulanan'>
                    		<!-- Bar Chart -->

                            <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $labelBulan),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'bulanan',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tampilBulan,

									          'min' => 0 ,
									          'max' => 3
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Daya (Mw)')
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Tanggal {point.key}<br/>',
											'pointFormat' => 'Gardu Induk : {point.gardu}<br/>Trafo : {point.trafo}<br/>{series.name}:{point.y}',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
									      'series' => array(
									      	
									         array('name' => 'Mw', 'data' => $hasilBulan  ,'type' => 'column'),
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									));
									

							?>
                            <!-- End Bar Chart -->
                    	</div>
                	</div>


                	<div id="portlet">
                    	<p class="portlet-title"><u>Beban Trafo Tahunan</u></p>
                    </div>
                	<div id="beban-puncak">
                        Tahun
                        <select class="form-control" style="width:100px;" id = 'awal'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2000;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
                        S/D
                        <select class="form-control" style="width:100px;" id = 'akhir'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2000;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn4" class = "btn btn-primary">Tampil</button>' , 
                        		 array('penyaluran/pembebananTrafoDetailTahun'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#pertahunan',
                        		 	'data' => array('awal' => 'js:awal.value' , 'akhir' => 'js:akhir.value', 'gardu_induk_id' => 'js:gardu_induk_id.value' , 'trafo_id' => 'js:trafo_id.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading4").show();
									}',
									'complete' => 'function(){
										$("#loading4").hide();
									}',
                        		 )

                        	);
                         ?>
                        <div id = 'loading4' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        <div id = 'tahunan'></div>
                        <div class="export2" id = 'pertahunan'>
                    		<!-- Bar Chart -->

                            <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $labelTahun),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'tahunan',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tampilTahun,

									          'min' => 0 ,
									          'max' => 3
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Daya (Mw)')
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Tanggal {point.key}<br/>',
											'pointFormat' => 'Gardu Induk : {point.gardu}<br/>Trafo : {point.trafo}<br/>{series.name}:{point.y}',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
									      'series' => array(
									      	
									         array('name' => 'Mw', 'data' => $hasilTahunan  ,'type' => 'column'),
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									));
									

							?>
                            <!-- End Bar Chart -->
                    	</div>
                	</div>


                </div>
                    
                </div>
            </div>
            <!-- End Pertama -->
        </div>