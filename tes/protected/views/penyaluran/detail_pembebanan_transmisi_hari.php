 <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $labelBulanTahun),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'harian',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tampilHari,

									          'min' => 0 ,
									          'max' => 10
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Daya (Mw)')
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Tanggal {point.key}<br/>',
											'pointFormat' => 'Gardu Induk : {point.gardu}<br/>Transmisi : {point.transmisi}<br/>{series.name}:{point.y}',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
									      'series' => array(
									      	
									         array('name' => 'Mw', 'data' => $hasilHari  ,'type' => 'column'),
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									));
									

							?>