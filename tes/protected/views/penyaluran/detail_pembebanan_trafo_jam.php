
<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   			'gradient' => array('enabled'=> true),
								   	   		'credits' => array('enabled' => false),
								   	   		
								   	   		 'exporting' => array('enabled' => true),
									      'title' => array('text' => $hariIni),
									      'xAxis' => array(
									         'categories' => $tampil_jam,
									          
									          'min' => 0 ,
									           'max' => 10
									      ),
									      
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'yAxis' => array(
									         'title' => array('text' => 'Daya (Mw)')
									      ),
									     
									      'series' => array(
									      	
									         array('name' => 'Mw', 'data' => $hasilJam),
									         
									         
									      )
									   )
									));

