<div id = 'bulanan'></div>


							<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
								   'options'=>array(
									  'title' => array('text' => $year ,'style' => 'font-size:16px;'),
									  'credits' => false , 
									  'xAxis' => array(
										 'categories' => $listBulan,
										/*'min' => 0, 
										'max' => 3, */
										'labels' => array(
												'rotation' => 270,
												'y' => 40,)
									  ),
									   'chart' => array(
											'renderTo' => 'bulanan',
										   
										),
									  'yAxis' => array(
										 'title' => array('text' => 'MW'),
										'stackLabels' => array(
										//	'enabled' => true
										),
										),
									  'plotOptions' => array(
										'column' => array(
												'stacking' => 'normal',
												//'dataLabels' => array('enabled' => true)
										)
									  
									  ),
									  /* 'tooltip' => array(
										'headerFormat' => 'Bulan {point.key}<br/>',
										'pointFormat' => '{series.name}:{point.y}<br/>  <br/> Tanggal: {point.tanggal}',
										'footerFormat'=> '</table>',
										'shared' => true,
										'useHtml' => true
									  ),*/
									  'colors' => array('#2EF287' ,'#A3C558' , '#CD6F22','#FFFFB4','#FFFF05','#B34EE9','#3972B7' , '#050505', 'green' , 'red','black'),
									  'series' => array(
										
										 array('name' => 'PO', 'data' => $poBulan, 'type' => 'column'),
										  array('name' => 'MO', 'data' => $moBulan, 'type' => 'column'),
										 array('name' => 'FO', 'data' => $foBulan , 'type' => 'column'),
										 array('name' => 'Derating', 'data' => $deratingBulan , 'type' => 'column'),
										
										  array('name' => 'Cad.Operasi', 'data' => $cad_operasiBulan , 'type' => 'column'),
										   array('name' => 'GangFdr', 'data' => $fdrBulan, 'type' => 'column'),
										 array('name' => 'Beban', 'data' => $bebanBulan , 'type' => 'column'),
										
										array('name' => 'Padam', 'data' => $padamBulan, 'type' => 'column'),
										
										 
										 array('name' => 'DM', 'data' => $dmBulan, 'type' => 'line'),
										 array('name' => 'Beban Puncak', 'data' => $beban_puncakBulan, 'type' => 'line'),
										  array('name' => 'Kap Terpasang', 'data' => $terpasangBulan , 'type' => 'line' , 'marker' => array(
												'states' => array(
													'select' => array(
														'fillColor' => '',
													) 
												)
											)
										),
										
										
									  )
								   )
								));

							?>