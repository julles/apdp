<div id = "tahunan"></div>


							<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
								   'options'=>array(
									  'title' => array('text' => $labelTahunan ,'style' => 'font-size:16px;'),
									  'credits' => false , 
									  'xAxis' => array(
										 'categories' => $listTahun,
										/*'min' => 0, 
										'max' => 3, */
										'labels' => array(
												'rotation' => 270,
												'y' => 40,)
									  ),
									  'chart' => array(
											'renderTo' => 'tahunan',
										   
										),
									  'yAxis' => array(
										 'title' => array('text' => 'MW'),
										'stackLabels' => array(
										//	'enabled' => true
										),
										),
									  'plotOptions' => array(
										'column' => array(
												'stacking' => 'normal',
												//'dataLabels' => array('enabled' => true)
										)
									  
									  ),
									  /* 'tooltip' => array(
										'headerFormat' => 'Bulan {point.key}<br/>',
										'pointFormat' => '{series.name}:{point.y}<br/>  <br/> Tanggal: {point.tanggal}',
										'footerFormat'=> '</table>',
										'shared' => true,
										'useHtml' => true
									  ),*/
									  'colors' => array('#2EF287' ,'#A3C558' , '#CD6F22','#FFFFB4','#FFFF05','#B34EE9','#3972B7' , '#050505', 'green' , 'red','black'),
									  'series' => array(
										
										 array('name' => 'PO', 'data' => $poTahun, 'type' => 'column'),
										  array('name' => 'MO', 'data' => $moTahun, 'type' => 'column'),
										 array('name' => 'FO', 'data' => $foTahun , 'type' => 'column'),
										 array('name' => 'Derating', 'data' => $deratingTahun , 'type' => 'column'),
										
										  array('name' => 'Cad.Operasi', 'data' => $cad_operasiTahun , 'type' => 'column'),
										   array('name' => 'GangFdr', 'data' => $fdrTahun, 'type' => 'column'),
										 array('name' => 'Beban', 'data' => $bebanTahun , 'type' => 'column'),
										
										array('name' => 'Padam', 'data' => $padamTahun, 'type' => 'column'),
										
										 
										 array('name' => 'DM', 'data' => $dmTahun, 'type' => 'line'),
										 array('name' => 'Beban Puncak', 'data' => $beban_puncakTahun, 'type' => 'line'),
										  array('name' => 'Kap Terpasang', 'data' => $terpasangTahun , 'type' => 'line' , 'marker' => array(
												'states' => array(
													'select' => array(
														'fillColor' => '',
													) 
												)
											)
										),
										
										
									  )
								   )
								));

							?>