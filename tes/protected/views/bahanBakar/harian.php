<div id = 'bakar'></div>
<?php
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelHarian,'style' => 'font-size:16px;'),
										  'credits' => false ,
										  'xAxis' => array(
											 'categories' => $dataHarian,
											 
											 'labels' => array(
												'rotation' => 0,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 ),
											   'min' => 0 ,
									          'max' => 12
										  ),
										  'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
										  'yAxis' => array(
											'min' => 0,
											 'title' => array('text' => 'Liter')
										  ),
										   'chart' => array(
														'renderTo' => 'bakar',
													   
													),
										  'series' => array(
											 array('name' => 'HSD', 'data' => $hsd , 'type' => 'column'),
											 array('name' => 'MFO', 'data' => $mfo , 'type' => 'column'),
											 array('name' => 'OLEIN', 'data' => $olein , 'type' => 'column'),
											 array('name' => 'BATU BARA', 'data' => $batu_bara , 'type' => 'column'),
											
										  )
									   )
									));
								
								?>