<div id = 'render1'></div>
<?php
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelTahunan,'style' => 'font-size:16px;'),
										  'credits' => false ,
										  'xAxis' => array(
											 'categories' => $dataTahunan,
											 
											 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 ),
										//	   'min' => 0 ,
									    //      'max' => 6
										  ),
										  'chart' => array(
												'renderTo' => 'render1',
											   
											),
										  'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
										  'yAxis' => array(
											'min' => 0,
											 'title' => array('text' => 'Liter')
										  ),
										  'series' => array(
											 array('name' => 'HSD', 'data' => $hsdTahunan , 'type' => 'column'),
											 array('name' => 'MFO', 'data' => $mfoTahunan , 'type' => 'column'),
											 array('name' => 'OLEIN', 'data' => $oleinTahunan , 'type' => 'column'),
											 array('name' => 'BATU BARA', 'data' => $batu_bara_tahunan , 'type' => 'column'),
											
										  )
									   )
									));
								
								?>