 <div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
            	<div id="hearts">
      				<a id="putar"href="#" onclick="rotate()" title="Minimize and Maximize"></a>
                </div>
                <div class="judul" align="center">
                    <h2><b>Galleri Video</b></h2>
                </div>
            	<div id="gallery">
                    
            	<?php
            	$alamat = 'http://i3.ytimg.com/vi/';
            	$koneksi = Yii::app()->db;
            	foreach($model as $row)
            	{
            		$url = $koneksi->createCommand()->select('MIN(id) , url')->from('gallery_video_detail')->where('gallery_video_id=:ids' , array(':ids' => $row->id))->queryRow();
            		$exp = explode("=",$url['url']);
            		$hasil = $exp[1];
            		$hasilGambar = $alamat.$hasil."/mqdefault.jpg";
            	?>
                    <!-- Pertama -->
                    <div class="col-3">
                    	<a href="<?php echo ar::createUrl("/kegiatan/videoDetail/read/$row->seo"); ?>">
                        	<div class="box">
                                <img src="<?php echo $hasilGambar; ?>" />
                                <div class="title"><?php echo $row->nama_album; ?></div>
                            </div>
                        </a>
                    </div>
                    <!-- End Pertama -->
                <?php
                }
                ?>    
                    
                </div>
                
                <!-- Pagination -->
                <div class="pagerDB">
                   <?php
                                $this->widget('CLinkPager', array(
                                'pages'=>$pages,
                                'maxButtonCount'=>5,
                              'cssFile'=>Yii::app()->baseUrl."/web/css/pager.css",
                                //'nextPageLabel'=>'My text >',
                                'header'=>'',
                                'header'=>'',
                                'nextPageLabel'=>'Next',
                                'prevPageLabel'=>'Prev',
                                'firstPageLabel'=>'<<',
                                'lastPageLabel'=>'>>',
                                'htmlOptions'=>array(
                                    //'class'=>'mypager_class',
                                    'style'=>'',
                                    'target' => '_self',
                                    //'id'=>'mypager_id'
                                ),   
                          
                            ));
                            ?> 
                </div>
                <!-- End Pagination -->
            </div>
            <!-- End Pertama -->
        </div>

        <script>
	$(function() {
  		var special = ['reveal', 'top', 'boring', 'perspective', 'extra-pop'];
  		// Toggle Nav on Click
  		$('#hearts a').click(function() {
			var delay_ = 300;
			checkWidth = $('#leftContent').css( "width");
			var widthnya="594";
			heightnya = "400";
			widthbaru = "594"
			if( checkWidth < '186'){
			  $('#leftContent').animate( {"width":"186", "height":"100" + "%"}, delay_, "linear");
			}else if(checkWidth > "0"){
			  $('#leftContent').animate({ "width":"0", "height":"0"}, delay_, "linear" );	
				widthnya = "780";
				heightnya = "500";
			}
			$('#rightContent').animate({ "width": widthnya}, delay_, "linear" );
			$('.judul').animate({ "width": widthnya}, delay_, "linear" );
			$('#gallery').animate({ "width": widthnya}, delay_, "linear" );
			$('.containerKanan').animate({ "width": widthnya}, delay_, "linear" );
  		});
	});
</script>