<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
            	<div id="hearts">
      				<a id="putar"href="#" onclick="rotate()" title="Minimize and Maximize"></a>
                </div>
                <div class="judul" align="center">
                    <h2><b>Pengumuman</b></h2>
                </div>
            	<div id="payper">


            	<?php
            	$db = Yii::app()->db;
            	foreach($model as $row)
            	{
            		$tanggal = $db->createCommand()->select('YEAR(date_post) AS tahun , MONTHNAME(date_post) AS bulan , DAY(date_post) AS hari')
            		->from('pengumuman')
            		->where('id=:id' , array(':id' => $row->id))
            		->queryRow();


            	?>
                    <!-- Pertama -->
                    <div class="news">
                    	<div class="image-default">
                        	<div class="tahun"><?php echo $tanggal['tahun']; ?></div>
                            <div class="tanggal"><?php echo $tanggal['hari']; ?></div>
                            <div class="bulan"><?php echo $tanggal['bulan']; ?></div>
                        </div>
                        <div class="text-payper">
                        	<h2><a  href="<?php echo ar::createUrl("/kegiatan/pengumumanDetail/read/$row->seo");?>"><?php echo $row->judul; ?></a></h2>
                            <div class="subheader">
                            	<?php
                            		$trim = trim(strip_tags($row->deskripsi));
                            		$substr = substr($trim , 0 , 150);

                            		echo $substr;
                            	?>
                            	...
                            </div>
                        </div>
                    </div>
                    <!-- End Pertama -->
                <?php
            	}
                ?>    

                    
                </div>
                
                <!-- Pagination -->
                <div class="pagerDB">
                                                <?php
                                $this->widget('CLinkPager', array(
                                'pages'=>$pages,
                                'maxButtonCount'=>5,
                              'cssFile'=>Yii::app()->baseUrl."/web/css/pager.css",
                                //'nextPageLabel'=>'My text >',
                                'header'=>'',
                                'header'=>'',
                                'nextPageLabel'=>'Next',
                                'prevPageLabel'=>'Prev',
                                'firstPageLabel'=>'<<',
                                'lastPageLabel'=>'>>',
                                'htmlOptions'=>array(
                                    //'class'=>'mypager_class',
                                    'style'=>'',
                                    'target' => '_self',
                                    //'id'=>'mypager_id'
                                ),   
                          
                            ));
                            ?> 
                </div>
                <!-- End Pagination -->
            </div>
            <!-- End Pertama -->
        </div>