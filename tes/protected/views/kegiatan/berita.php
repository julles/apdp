<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
            	<div id="hearts">
      				<a id="putar"href="#" onclick="rotate()" title="Minimize and Maximize"></a>
                </div>
                <div class="judul" align="center">
                    <h2><b>Berita</b></h2>
                </div>
            	<div id="payper">


            	<?php
            	$db = Yii::app()->db;
            	foreach($model as $row)
            	{
            		$tanggal = $db->createCommand()->select('YEAR(date_post) AS tahun , MONTHNAME(date_post) AS bulan , DAY(date_post) AS hari')
            		->from('berita')
            		->where('id=:id' , array(':id' => $row->id))
            		->queryRow();


            	?>
                    <!-- Pertama -->
                    <div class="news">
                    	<div class="image-default">
                        	<div class="tahun"><?php echo $tanggal['tahun']; ?></div>
                            <div class="tanggal"><?php echo $tanggal['hari']; ?></div>
                            <div class="bulan"><?php echo $tanggal['bulan']; ?></div>
                        </div>
                        <div class="text-payper">
                        	<h2><a  href="<?php echo ar::createUrl("/kegiatan/beritaDetail/read/$row->seo");?>"><?php echo $row->judul; ?></a></h2>
                            <div class="subheader">
                            	<?php
                            		$trim = trim(strip_tags($row->deskripsi));
                            		$substr = substr($trim , 0 , 150);

                            		echo $substr;
                            	?>
                            	...
                            </div>
                        </div>
                    </div>
                    <!-- End Pertama -->
                <?php
            	}
                ?>    

                    
                </div>
                
                <!-- Pagination -->
                <div class="pagerDB">
                             <?php
                                $this->widget('CLinkPager', array(
                                'pages'=>$pages,
                                'maxButtonCount'=>5,
                              'cssFile'=>Yii::app()->baseUrl."/web/css/pager.css",
                                //'nextPageLabel'=>'My text >',
                                'header'=>'',
                                'header'=>'',
                                'nextPageLabel'=>'Next',
                                'prevPageLabel'=>'Prev',
                                'firstPageLabel'=>'<<',
                                'lastPageLabel'=>'>>',
                                'htmlOptions'=>array(
                                    //'class'=>'mypager_class',
                                    'style'=>'',
                                    'target' => '_self',
                                    //'id'=>'mypager_id'
                                ),   
                          
                            ));
                            ?> 
                </div>
                <!-- End Pagination -->
            </div>
            <!-- End Pertama -->
        </div>

         <script>
	$(function() {
  		var special = ['reveal', 'top', 'boring', 'perspective', 'extra-pop'];
  		// Toggle Nav on Click
  		$('#hearts a').click(function() {
			var delay_ = 300;
			checkWidth = $('#leftContent').css( "width");
			var widthnya="594";
			heightnya = "400";
			widthbaru = "594"
			if( checkWidth < '186'){
			  $('#leftContent').animate( {"width":"186", "height":"100" + "%"}, delay_, "linear");
			  $('.containerKanan img').animate({ "width": widthbaru , "height": heightnya}, delay_, "linear" );
			}else if(checkWidth > "0"){
			  $('#leftContent').animate({ "width":"0", "height":"0"}, delay_, "linear" );	
				widthnya = "780";
				heightnya = "500";
			}
			$('#rightContent').animate({ "width": widthnya}, delay_, "linear" );
			$('.judul').animate({ "width": widthnya}, delay_, "linear" );
			$('.containerKanan').animate({ "width": widthnya}, delay_, "linear" );
			$('.containerKanan img').animate({ "width": widthnya, "height": heightnya}, delay_, "linear" );
  		});
	});
</script>