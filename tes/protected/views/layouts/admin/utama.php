
<?php
	$base = ar::baseUrl()."/admin/";
	$token =  Yii::app()->session['tokenId'];
	//$exp = Yii::app()->session['expires_by'];
			
			
	

	if(!empty($token))
	{
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META NAME="robots" CONTENT="noarchive"> 
<title><?php echo Yii::app()->name; ?></title>
<link rel="shortcut icon" href="<?php echo $base; ?>image/pln-logo.jpg">
<!-- CSS Style -->
<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/mystyle.css">
<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/colorbox.css">
<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/main.css">
<!-- End CSS Style -->

<!-- Javascript -->
<script src="<?php echo $base; ?>js/menu.js" type="text/javascript"></script>
	<?php /*	
	<script src="<?php echo $base; ?>js/jquery-1.10.1.min.js" type="text/javascript"></script>
	*/

	Yii::app()->clientScript->registerCoreScript('jquery');
	?>
<script src="<?php echo $base; ?>js/modernizr.custom.js" type="text/javascript"></script>
<script src="<?php echo $base; ?>js/jquery.fancybox.js" type="text/javascript"></script>
<!-- End Javascript -->
<script>
	$(document).ready(function(){
		$('#pushstat').css({'display':'none'});
	});
</script>

</head>
<?php
	$alamat = Yii::app()->createUrl("/administrator/default/setSessi");
?>
<body>
	<script type="text/javascript">
	/*
			$(document).ready(function(){

				$(document).mouseover(function(){
					 $.post(<?php echo "'$alamat'"; ?>, {"sessi": "kika"});
				})
			});
	*/
</script>
	<!-- Container -->
	<div id="container" style="display: block;">
    	<!-- Header -->
		<div id="header-panel">
        	<img src="<?php echo $base; ?>image/login2.png" />
        </div>
        <!-- End Header -->
        
        <!-- Body -->
		<div id="body">
            <!-- Menu -->
			<?php $this->renderpartial('//layouts/admin/menu'); ?>
            <!-- End Menu -->
            
            <!-- Content -->
            <?php
            	$sController = ar::query(
            		"SELECT COUNT(menu.id)  FROM menu INNER JOIN hak_akses
					ON hak_akses.menu_id = menu.id
					INNER JOIN role ON
					role.id = hak_akses.role_id
					WHERE controller = '".Yii::app()->controller->id."'
					AND role_id = '".Yii::app()->session['tokenRole']."'
					"

				)
				->queryScalar();
            		
				if($sController > 0 || Yii::app()->controller->id == 'default')
				{
            		echo $content;
				}else{
					throw new CHttpException(404, "Error Processing Request");
					
				}

             ?>
            <!-- End Content -->
		</div>
        <!-- End Body -->
        
		<p class="footer">
		Control panel PLN APDP Kalimantan Barat<br>
		Powered by <strong><a style="text-decoration:none;" target="_blank" href="<?php echo $base; ?>http://pragmainf.co.id">PT Pragma Informatika</a></strong>
		</p>
        
	</div>
    <!-- End Container -->
</body>
<?php
/*
<script src="<?php echo $base; ?>js/custom.js" type="text/javascript"></script>

*/

?>
</html>
<?php
	}else{
		 $this->redirect(array('/administrator/default/login'));
	}
?>