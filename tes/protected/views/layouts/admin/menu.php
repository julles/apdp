<?php
	$base = ar::baseUrl()."/admin/";
?>
<div id="menu">
				<div id="MainMenu">
					<div id="Menu">
						<div class="suckertreemenu">
							<ul id="treemenu1">
								<?php /*<li class="panel-default"><a class="actived" href="<?php echo $base; ?>index3.html">Home</a></li>*/ ?>

								<?php
								$module = Yii::app()->controller->module->id;
								$sqlParent = ar::queryBuilder()
								->select('id , nama_menu')
								->from('menu')
								->order('pos ASC')
								->where('parent_id=:parent_id' , array(':parent_id' => 0))
								->query();

								while(($r = $sqlParent->read()) != false)
								{

									$count = ar::query("SELECT COUNT(hak_akses.id) FROM hak_akses
						                    INNER JOIN menu ON hak_akses.menu_id = menu.id
						                    WHERE menu.parent_id = '$r[id]' AND hak_akses.role_id = '".Yii::app()->session['tokenRole']."'
						                ")
									
									->queryScalar();


									if($count > 0)
									{
											echo "<li class = 'panel-sekilasAPDP'>";

												echo CHtml::link($r['nama_menu'] , '#');

												echo '<ul style="top: 34px; visibility: hidden;">';

													$sqlChild = ar::queryBuilder()
													->select('id , nama_menu , controller')
													->from('menu')
													->where('parent_id=:parents' , array(':parents' => $r['id']))
													->query();

													while(($rr = $sqlChild->read()) !== false)
													{
														$ceks = ar::queryBuilder()->select('id')->from('hak_akses')->where('menu_id=:menu_id AND role_id=:role' , array(':menu_id' => $rr['id'] , ':role' => Yii::app()->session['tokenRole']))
														->queryScalar();
														if(!empty($ceks))
														{
															echo '<li style="border: none;">';

															echo CHtml::link($rr['nama_menu'] , array("/$module/$rr[controller]"));

															echo'</li>';
												
														}
													}
												echo '</ul>';

									echo "</li>";
								

									}
								}



								?>

								
								
								<li class="panel-logout" style = 'text-align:center;'><a href="<?php echo Yii::app()->createUrl("/administrator/default/logout"); ?>"><?php echo CHtml::image(Yii::app()->baseUrl."/images/logout.png" ,'', array('width' => 20)); ?></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="cleaner_h0"></div>
			</div>		
			<div class="cleaner_h0"></div>