

<!-- Sekilas APDP -->
	<ul id="sekilas-apdp" class="ddsubmenustyle" style="z-index: 2000; left: 0px; top: -1000px; visibility: hidden; height: 1.5334694179289512px; width: auto; overflow: hidden; opacity: 0.013815039801161721;">


        <li><?php echo CHtml::link('Profil Perusahaan' , array('site/profilPerusahaan')); ?></li>
        <li><?php echo CHtml::link('Visi  Dan Misi' , array('site/visiMisi')); ?></a></li>
        <li><?php echo CHtml::link('Wilayah Kerja' , array('site/wilayahKerja')); ?></a></li>
        <li><?php echo CHtml::link('Struktur Organisasi' , array('site/strukturOrganisasi')); ?></a></li>
       <li><?php echo CHtml::link('Proses Bisnis' , array('site/prosesBisnis')); ?></a></li>
	</ul>
    <!-- End Sekilas APDP -->

    <!-- Kegiatan -->
	<ul id="kegiatan" class="ddsubmenustyle" style="z-index: 2000; left: 0px; top: -1000px; visibility: hidden; height: 1.5334694179289512px; width: auto; overflow: hidden; opacity: 0.013815039801161721;">
        <li><?php echo CHtml::link('Berita' , array('kegiatan/berita')); ?></a></li>
         <li><?php echo CHtml::link('Pengumuman' , array('kegiatan/pengumuman')); ?></a></li>
         <li><?php echo CHtml::link('Gallery Foto' , array('kegiatan/gallery')); ?></a></li>
        <li><?php echo CHtml::link('Gallery Video' , array('kegiatan/video')); ?></a></li>
	</ul>
    <!-- End Kegiatan -->

    <!-- Operating Sistem -->
	<ul id="operating" class="ddsubmenustyle" style="z-index: 2000; left: 0px; top: -1000px; visibility: hidden; height: 1.5334694179289512px; width: auto; overflow: hidden; opacity: 0.013815039801161721;">
        <li><?php echo CHtml::link('Beban Sistem' , array('/beban')); ?></li>
          <li><?php echo CHtml::link('Neraca Daya' , array('/neracaDaya')); ?></li>
        <li><?php echo CHtml::link('Kondisi Kelistrikan' , array('operatingSystem/kondisiKelistrikan')); ?></li>
        <!--<li><a href="frekuensi.html">Frekuensi</a></li>
       <li><?php echo CHtml::link('Tegangan' , array('operatingSystem/tegangan')); ?></li>
        <li><a href="#">Load Flow</a></li>-->
        <li>
		<?php echo CHtml::link('Neraca Energi' , ''); ?>
			<ul>
				<li><?php echo CHtml::link('Pembangkit' , array('/PEMBANGKIT-NERACA')); ?></li>
				<li><?php echo CHtml::link('Transmisi' , array('/TRANSMISI-NERACA')); ?></li>
				<?php /* <li><?php echo CHtml::link('Trafo' , array('/')); ?></li> */ ?>
				<li><?php echo CHtml::link('Distribusi' , array('/PEMBANGKIT-DISTRIBUSI')); ?></li>
				<li><?php echo CHtml::link('PS GI' , array('/PS-GI')); ?></li>
				<li><?php echo CHtml::link('PS GH' , array('/PS-GH')); ?></li>
				<li><?php echo CHtml::link('Stand kWh' , array('/Stand-kWh')); ?></li>
			</ul>
		</li>
		
	    <li>
		<?php echo CHtml::link('Bahan Bakar' , ''); ?>
			<ul>
				<li><?php echo CHtml::link('Pemakaian' , array('/bahanBakar')); ?></li>
				<li><?php echo CHtml::link('Stok' , array('/bahanBakarStok')); ?></li>
			</ul>
		</li>
		
          <li><?php echo CHtml::link('Kinerja Operasi' , array('/kinerjaOperasi')); ?></li>

	</ul>
    <!-- End Operating Sistem -->

    <!-- Penyaluran -->
    <?php
    	$linkTgi = Yii::app()->db->createCommand("SELECT link FROM tgi")->queryScalar();
    ?>
	<ul id="penyaluran" class="ddsubmenustyle" style="z-index: 2000; left: 0px; top: -1000px; visibility: hidden; height: 1.5334694179289512px; width: auto; overflow: hidden; opacity: 0.013815039801161721;">
        <li><a href="<?php echo $linkTgi; ?>" target="_blank">TGI Khatulistiwa</a></li>
        <li><?php echo CHtml::link('Pembebanan Trafo' , array('/ntrafo')); ?></a></li>
       <li><?php echo CHtml::link('Pembebanan Transmisi' , array('/ntransmisi')); ?></a></li>
        <!--<li><a href="#">Pemeliharaan</a></li>-->
        <li><a href="<?php echo Yii::app()->createUrl("/kinerjaPenyaluran"); ?>">Kinerja Penyaluran</a></li>
	</ul>
    <!-- End Penyaluran -->

    <!-- Scadatel -->
	<ul id="scadatel" class="ddsubmenustyle" style="z-index: 2000; left: 0px; top: -1000px; visibility: hidden; height: 1.5334694179289512px; width: auto; overflow: hidden; opacity: 0.013815039801161721;">
        <li><?php echo CHtml::link("Gangguan RTU" , array('scadatel/gangguanRtu')); ?></li>
        <li><?php echo CHtml::link("Gangguan TC" , array('scadatel/gangguanTc')); ?></li>
        <li><?php echo CHtml::link("Gangguan Telekomunikasi" , array('scadatel/gangguanTelekomunikasi')); ?></li>

        <li><a href="<?php echo Yii::app()->createUrl("/kinerjaScadatel"); ?>">Kinerja Scadatel</a></li>
	</ul>
    <!-- End Scadatel -->
    <script>
	$(function() {
  		var special = ['reveal', 'top', 'boring', 'perspective', 'extra-pop'];
  		// Toggle Nav on Click
  		$('#hearts a').click(function() {
			var delay_ = 300;
			checkWidth = $('#leftContent').css( "width");
			var widthnya="594";
			heightnya = "400";
			widthbaru = "594"
			widthnya5 = "267";
			if( checkWidth < '186'){
			  $('#leftContent').animate( {"width":"186", "height":"100" + "%"}, delay_, "linear");
			  $('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			}else if(checkWidth > "0"){
			  $('#leftContent').animate({ "width":"0", "height":"0"}, delay_, "linear" );
				widthnya = "780";
				heightnya = "500";
				widthnya5 = "325";
			}
			$('#rightContent').animate({ "width": widthnya}, delay_, "linear" );
			$('.judul').animate({ "width": widthnya}, delay_, "linear" );
			$('.containerKanan').animate({ "width": widthnya}, delay_, "linear" );
			$('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
  		});
	});
</script>
<script>
	function rotate()
	{
		document.getElementById('putar').style.margin='0px';
		document.getElementById("putar").style.WebkitTransform="rotate(-95deg)";
		document.getElementById("putar").style.MozTransform="rotate(-95deg)";
		document.getElementById('putar').setAttribute('onclick','changeid()');
		var e = document.getElementById("putar");
		e.id = "putars";
	}
	function changeid ()
	{
		document.getElementById('putars').style.margin='7px';
		document.getElementById("putars").style.WebkitTransform="rotate(95deg)";
		document.getElementById("putars").style.MozTransform="rotate(95deg)";
		document.getElementById('putars').setAttribute('onclick','rotate()')
		var e = document.getElementById("putars");
		e.id = "putar";
	}
</script>
