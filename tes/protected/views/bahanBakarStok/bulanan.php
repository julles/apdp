<div id = 'render1'></div>
<?php
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelTahunan),
										  'credits' => false ,
										  'xAxis' => array(
											 'categories' => $dataTahunan,
											 
											 'labels' => array(
												'rotation' => 0,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 ),
											   'min' => 0 ,
									          'max' => 3
										  ),
										  'chart' => array(
												'renderTo' => 'render1',
											   
											),
										  'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
										  'yAxis' => array(
											'min' => 0,
											 'title' => array('text' => 'Liter')
										  ),
										  'series' => array(
											 array('name' => 'hsd', 'data' => $hsdTahunan , 'type' => 'column'),
											 array('name' => 'mfo', 'data' => $mfoTahunan , 'type' => 'column'),
											 array('name' => 'olein', 'data' => $oleinTahunan , 'type' => 'column'),
											 array('name' => 'batu bara', 'data' => $batu_bara_tahunan , 'type' => 'column'),
											
										  )
									   )
									));
								
								?>