<style type="text/css">
	
	  th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
 
    div.container {
        width: 80%;
    }
</style>
<?php
	$base = Yii::app()->baseUrl."/web/";
	$lastDay = date('t');
	
	
	//echo "adaalh".field('10','2014','impor',1 , 5);
?>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl."/admin/"; ?>css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl."/admin/"; ?>css/dataTables.fixedColumns.css">
<script src = "<?php echo Yii::app()->baseUrl."/admin/js/jquery.dataTables.js"; ?>"></script>
<script src = "<?php echo Yii::app()->baseUrl."/admin/js/dataTables.fixedColumns.js"; ?>"></script>

<script>
$(document).ready(function() {
    var table = $('#gridYii').dataTable({
		"ordering": false,
		 "sDom": "<'row-fluid'r><'row-fluid't><'row-fluid'<'span6'i><'span6'p>>", 

        //"sScrollY": 200,
        "sScrollX": "40%",
        "sScrollXInner": "0%",
        "bScrollCollapse": true,
		aLengthMenu: [
        [25, 100],
        [25, 100]
		
		//[25, 50, 100, 200, -1],
        //[25, 50, 100, 200, "All"]
		],
		// "bProcessing": true,
		// "sAjaxSource": '<?php echo Yii::app()->createUrl('/neracaEnergi/populasi'); ?>'
	});

     new $.fn.dataTable.FixedColumns( table, {
        "iLeftColumns":3,
        "sHeightMatch" : "auto",
        //"iRightColumns": 0
    } );

} );
</script>
<?php
	$allTahun = array();
	for($a=2010;$a<=date('Y');$a++)
	{
		$allTahun[$a] = $a;
	}
	
	/*$ajax = array(
		'url' => Yii::app()->createUrl("/neracaEnergi/getItem"),
		'type' => 'POST',
		'update' => '#item_id',
		'data' => array(
			'gardu_induk_id' => 'js:gardu_induk_id.value'
		)
	);*/
?>
<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
            	<div id="hearts">
      				<a id="putar"href="#" onclick="rotate()" title="Minimize and Maximize"></a>
                </div>
                <div class="judul" align="center">
                    <h2><b>Neraca Energi Stand kWh </b></h2>
                </div>
				<div id = 'textnya'>
				
				<div id="pembebanan-trafo-top" style="height: 150;">
				
					<div class="trafo-panel-top" style = "width:237px;">
						<div class="trafo-panel-input-top">
							
							<div class = 'col-sm12'>
							<?php 
								echo CHtml::image(Yii::app()->baseUrl."/images/loading.gif" ,"" ,array('id' => 'loading','style' => 'display:none;'));
							?>
							<table>
								<tr>
									<td colspan = "2">
										<?php
											echo CHtml::dropDownList("garud_induk" ,"" , CHtml::listData(MasterGarduInduk::model()->findAll(array('order' => 'id ASC')) , 'id' , 'gardu_induk') , array('id' => 'gardu_induk_id'  , 'class' => 'form-control' ,'style' => 'width:100%'));
										?>
									</td>
									
										<?php
											//echo CHtml::dropDownList("item_id" ,"" , array() , array('empty' => 'Item' , 'id' => 'item_id' , 'class' => 'form-control'));
										?>
									
								</tr>
								
								<tr>
								
									<td>
										<?php
											echo CHtml::dropDownList('tahun' ,"" , $allTahun , array('empty' => 'Tahun' , 'id' => 'tahun',  'class' => 'form-control') );
										?>
									</td>
									<td>
										 <select  id = 'bulan' class = 'form-control'>
											<option value="">Bulan</option>
											<option value="1">Januari</option>
											<option value="2">Februari</option>
											<option value="3">Maret</option>
											<option value="4">April</option>
											<option value="5">Mei</option>
											<option value="6">Juni</option>
											<option value="7">Juli</option>
											<option value="8">Agustus</option>
											<option value="9">September</option>
											<option value="10">Oktober</option>
											<option value="11">November</option>
											<option value="12">Desember</option>
										</select>
									</td>
								</tr>
								<tr>
									<td colspan='2'>
										<?php
											echo CHtml::ajaxLink("Cari" , array('/neracaEnergi/filter') , 
												array(
													'type' => 'POST' ,
													'update' => '#divFilter',
													'data' => array(
														'gardu_induk_id' => 'js:gardu_induk_id.value',
														//'item_id' => 'js:item_id.value',
														'tahun' => 'js:tahun.value',
														'bulan' => 'js:bulan.value'
													),
													'beforeSend' => 'function(){$("#loading").show()}',
													'complete' => 'function(){$("#loading").hide()}'
												),
												array(
													'class' => 'btn btn-primary'
												)
											);
										?>
									</td>
								</tr>
							</table>
									
							</div>
				
				
				</div>
				</div>
				</div>
				</div>
				
				<p>&nbsp;</p>
            	<div id="textnya">
				<div id = 'divFilter'>
                	<table id="gridYii" class="display" cellspacing="0" width="99%" style = 'font-size:10px;'>
						    <thead>
						         <tr>
											
									<th>Gardu Induk</th>
									<th>Item</th>
									<th>&nbsp;</th>
									<?php
										for($a=1;$a<=$lastDay;$a++)
										{
											echo "<th>$a</th>";
										}
									?>
									
								</tr>
						    </thead>
						 
						    <tbody>
							<?php
								while(($row = $model->read())!==false)
								{
									
								
									echo "
									<tr>
											<td>$row[gardu_induk]</td>
											<td>$row[item]</td>
											<td>EXP</td>
									";
												for($a=1;$a<=$lastDay;$a++)
												{
													echo "<td>".floatval($this->field($bln , $tahun , 'expor' ,$a , $row['item_id']))."</td>";
												}
											
									echo"
									</tr>
									<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>IMP</td>
									
											";
												for($a=1;$a<=$lastDay;$a++)
												{
													echo "<td>".floatval($this->field($bln , $tahun , 'impor' ,$a,$row['item_id']))."</td>";
												}
											
									echo"
									</tr>
									";
								}
							?>
									
						     </tbody>
						     </table>
						     <p>
						     	&nbsp;
						     </p>
            	
				</div>
                </div>
				
				
            </div>
            <!-- End Pertama -->
        </div>

        <script>
	$(function() {
  		var special = ['reveal', 'top', 'boring', 'perspective', 'extra-pop'];
  		// Toggle Nav on Click
  		$('#hearts a').click(function() {
			var delay_ = 300;
			checkWidth = $('#leftContent').css( "width");
			var widthnya="594";
			heightnya = "400";
			widthbaru = "594"
			widthnya5 = "267";
			widthnya6 = "565";
			LineCharts = "564";
			if( checkWidth < '186'){
			  $('#leftContent').animate( {"width":"186", "height":"100" + "%"}, delay_, "linear");
			  $('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			  $('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			  $('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			  $('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
			}else if(checkWidth > "0"){
			  $('#leftContent').animate({ "width":"0", "height":"0"}, delay_, "linear" );	
				widthnya = "780";
				heightnya = "500";
				widthnya5 = "325";
				widthnya6 = "750";
				LineCharts = "750";
			}
			$('#rightContent').animate({ "width": widthnya}, delay_, "linear" );
			$('.judul').animate({ "width": widthnya}, delay_, "linear" );
			$('.containerKanan').animate({ "width": widthnya}, delay_, "linear" );
			$('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			$('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			$('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			$('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
  		});
	});
</script>