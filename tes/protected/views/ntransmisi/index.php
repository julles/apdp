<?php
$db = Yii::app()->db;
/*
<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
*/
?>
<script>
	$(document).ready(
		function()
		{
			$("#btn_utama").click(
				function()
				{
					$("#btn_jam").click();
					$("#btn_bulan").click();
					$("#btn_hari").click();
					$("#btn_tahun").click();
				
				}
			);
		}
	);
</script>

<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
            	<div id="hearts">
      				<a id="putar"href="#" onclick="rotate()" title="Minimize and Maximize"></a>
                </div>
                <div class="judul" align="center">
                    <h2><b>Pembebanan Transmisi</b></h2>
                </div>
            	<div id="textnya">
                	<div id="pembebanan-trafo-top" style="height: 70px;">
						
					
                       
						<div class="trafo-panel-top" >
                            <div class="trafo-panel-input-top">
							
								<table>
										<tr>
											<td style = 'font-size:12px;'>GI
													<?php
					                                	echo CHtml::dropDownList('GarduInduk' , '' , CHtml::listData(MasterGarduInduk::model()->findAll() , 'id' , 'gardu_induk') , 
					                                		array(
					                                			//'class' => 'form-control',
					                                			'id' => 'gardu_induk_id',
															//	'class' => 'form-control',
																'style' => 'width:130px;margin-right:10px;',
					                                			'ajax' => array(
					                                				'update' => '#transmisi_id',
					                                				'type' => 'POST',
					                                				'url' => Yii::app()->createUrl("/ntransmisi/getTransmisi"),
					                                				'data' => array(
					                                					'gardu_induk_id' => 'js:this.value'
					                                				)

					                                			),
																
					                                		)
					                                	);
					                                ?>
											</td>
											<td style = 'font-size:12px;'>Transmisi
												 <?php
									
														$idgi = $db->createCommand("SELECT gardu_induk_id FROM master_transmisi WHERE gardu_induk_id IN(SELECT MIN(id) FROM master_gardu_induk) ORDER BY id ASC LIMIT 1")->queryScalar();
					                                	$listTransmisi =  CHtml::listData(MasterTransmisi::model()->findAll('gardu_induk_id=:gardu' ,array(':gardu' => $idgi)) , 'id' ,'transmisi');
														/*$ajax = array(
																	'update' => '#updateDiv',
					                                				'type' => 'POST',
					                                				'url' => Yii::app()->createUrl("/ntransmisi/chart"),
					                                				'data' => array(
					                                					'gardu_induk_id' => 'js:gardu_induk_id.value',
					                                					'transmisi_id' => 'js:this.value',
					                                					'tanggalJam' => 'js:tanggalJam.value',
					                                				),
																	'beforeSend' => 'function(){
																	$("#loading1").show();
																	}',
																	'complete' => 'function(){
																		$("#loading1").hide();
																	}',
														);*/
														echo CHtml::dropDownList('transmisi' , '' , $listTransmisi, array('id' =>'transmisi_id'));
					                                ?>
											</td>
											<td style = 'font-size:12px;' valign="bottom">
												
												     <?php
														echo CHtml::link('<button id = "btn_utama">Tampil</button>' ,'#');
														?>
							
											</td>
										</tr>
								</table>
                             
                            </div>
                        </div>
                       
						
                       
                    </div>
						<div id = 'updateDiv'>
							<div id="textnya">
							<div id="portlet">
								<p class="portlet-title"><u>Beban Transmisi Perjam</u></p>
							</div>
							<div id="beban-puncak">
								<?php
							echo CHtml::beginForm();
							?>
							
							<?php
                	echo CHtml::beginForm();
                	?>
                        <label style="padding-right:0px;">Tanggal</label>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(

                                'id' => 'tanggalJams',
                                'name'=>'tanggalJams',
                                // additional javascript options for the date picker plugin
                                'options' => array(
                                'showAnim' => 'fold',
                                'dateFormat'=>'yy-mm-dd',
                                 'changeMonth'=>true,
                                'changeYear'=>true,
                                'yearRange'=>'1900:2099',
                                ),
                                'htmlOptions' => array(
                                'style' => 'height:26px;',
                                 'class' => 'form-control'
                                ),
                            ));

                         ?>

                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn_jam" style = "height:27px;margin-top:1px;">Tampil</button>' , 
                        		 array('ntransmisi/ajaxJam'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perjam',
                        		 	'data' => array('tanggalJam' => 'js:tanggalJams.value' ,'gardu_induk_id' => 'js:gardu_induk_id.value' , 'transmisi_id' => 'js:transmisi_id.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading1").show();
									}',
									'complete' => 'function(){
										$("#loading1").hide();
									}',
                        		 )
								 

                        	);
                         ?>
								<div id = 'loading1' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
								<div class="export2" id = 'perjam'>
									<!-- Line Chart -->
									<?php
											
											$this->Widget('ext.highcharts.HighchartsWidget', array(
											   'options'=>array(
												  'title' => array('text' => ar::formatWaktu($date , "long" ,""),'style' => 'font-size:16px;'),
												  'credits' => false , 
												  'xAxis' => array(
													 'categories' => $listJam,
													 'type' => 'category',
													 'labels' => array(
														'rotation' => 270,
														'y' => 40,
														//'style' => array('font-size' => '5px;')
													 )
												  ),
												  'yAxis' => array(
													 'title' => array('text' => 'Beban(MW)')
												  ),
												  'plotOptions' => array(
                                                    'series' => array(
                                                            'marker' => array(
                                                                'enabled' => false,

                                                            )
                                                    )
                                                   ),
												   'colors'=>array('red' , 'blue','pink'),
												  'series' => array(
													 array('name' => 'Beban(MW)', 'data' => $nilaiPerjam ,'lineWidth' => 3),
													 
													
												  )
											   )
											));

									?>
									<!-- End Line Chart -->
								</div>
								<?php

								echo CHtml::endForm();
								?>
							</div>
							
							
							<div id="textnya">
							<div id="portlet">
								<p class="portlet-title"><u>Beban Puncak Transmisi Perhari</u></p>
							</div>
								<div id="beban-puncak">
								<?php
							echo CHtml::beginForm();
							?>
								 <label style="padding-right:0px;">Bulan & Tahun</label>
						  <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'bulanHarian'>
                            <option value="">Select</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                         <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'tahunHarian'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
						 

                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn_hari" style = "height:27px;margin-top:1px;">Tampil</button>' , 
                        		 array('ntransmisi/ajaxHarian'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perharian',
                        		 	'data' => array('bulan' => 'js:bulanHarian.value' , 'tahun' => 'js:tahunHarian.value' , 'gardu_induk_id' => 'js:gardu_induk_id.value' , 'transmisi_id' => 'js:transmisi_id.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading2").show();
									}',
									'complete' => 'function(){
										$("#loading2").hide();
									}',
                        		 )
								 

                        	);
                         ?>
								<div id = 'loading2' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
								<div class="export2" id = 'perharian'>
									<!-- Line Chart -->
									<?php
									$labelBulanTahun = explode(" ",ar::formatWaktu($date , "long" ,""));
									$labelBulanTahunFix = $labelBulanTahun[1]." - ".$labelBulanTahun[2];
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelBulanTahunFix,'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listHari,
											  'type' => 'category',
												 'labels' => array(
													'rotation' => 270,
													'y' => 40,
													//'style' => array('font-size' => '5px;')
												 )
										  ),
										  'yAxis' => array(
											 'title' => array('text' => 'Beban (MW)')
										  ),
										   'colors'=>array('green'),
										   'tooltip' => array(
												'headerFormat' => 'Tanggal {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/>',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
											  ),
										   
										  'series' => array(
											 array('name' => 'Beban(MW)', 'data' => $valueHari , 'type' => 'column'),
											
											
										  )
									   )
									));
									?>
									<!-- End Line Chart -->
								</div>
								<?php

								echo CHtml::endForm();
								?>
							</div>
							</div>
							
							
							<div id="textnya">
							<div id="portlet">
								<p class="portlet-title"><u>Beban Puncak Transmisi Perbulan</u></p>
							</div>
								<div id="beban-puncak">
								<?php
							echo CHtml::beginForm();
							?>
								
								 <label style="padding-right:0px;">Tahun</label>
						 
									 <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'tahunBulanan'>
										<option value="">Select</option>
										<?php
										$year = date('Y');
										for($a=2010;$a<=$year;$a++)
										{
											echo "<option value = '$a'>$a</option>";
										}
										?>

									</select>
									 

									<?php
										echo CHtml::ajaxLink('<button id = "btn_bulan" style = "height:27px;margin-top:1px;">Tampil</button>' , 
											 array('ntransmisi/ajaxBulanan'),
											 array(
												'type' => 'POST',
												'update' => '#perbulanan',
												'data' => array('tahun' => 'js:tahunBulanan.value' , 'gardu_induk_id' => 'js:gardu_induk_id.value','transmisi_id' => 'js:transmisi_id.value'),
												'beforeSend' => 'function(){
													$("#loading3").show();
												}',
												'complete' => 'function(){
													$("#loading3").hide();
												}',
											 )
											 

										);
									 ?>
									<div id = 'loading3' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
								   
									<div  class = 'perbulanan' class="export2" id = 'perbulanan'>	<!-- Line Chart -->
									
									<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $year,'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listBulan,
											 'type' => 'category',
												 'labels' => array(
													'rotation' => 270,
													'y' => 40,
													//'style' => array('font-size' => '5px;')
												 )
										  ),
										  'yAxis' => array(
											 'title' => array('text' => 'Beban (MW)')
										  ),
										   'colors'=>array('red'),
										   'tooltip' => array(
												'headerFormat' => 'Bulan {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/> Tanggal: {point.tanggal}',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
											  ),
										   
										  'series' => array(
											 array('name' => 'Beban(MW)', 'data' => $valueBulan , 'type' => 'column'),
											
											
										  )
									   )
									));
									?>
									<!-- End Line Chart -->
								</div>
								<?php

								echo CHtml::endForm();
								?>
							</div>
							</div>
							
							
							<div id="textnya">
							<div id="portlet">
								<p class="portlet-title"><u>Beban Puncak Transmisi Pertahun</u></p>
							</div>
								<div id="beban-puncak">
								<?php
							echo CHtml::beginForm();
							?>
								
								<label style="padding-right:0px;">Tahun</label>
									<select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'awal'>
										<option value="">Select</option>
										<?php
											$year = date('Y');
											for($a=2010;$a<=$year;$a++)
											{
												echo "<option value = '$a'>$a</option>";
											}
										?>
									</select>
									<label style="padding-right:0px;">s.d</label>
									<select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'akhir'>
										<option value="">Select</option>
										<?php
										$year = date('Y');
										for($a=2010;$a<=$year;$a++)
										{
											echo "<option value = '$a'>$a</option>";
										}
										?>
									</select>
									 

									<?php
										echo CHtml::ajaxLink('<button id = "btn_tahun" style = "height:27px;margin-top:1px;">Tampil</button>' , 
											 array('ntransmisi/ajaxTahunan'),
											 array(
												'type' => 'POST',
												'update' => '#pertahunan',
												'data' => array('awal' => 'js:awal.value' , 'akhir' => 'js:akhir.value' , 'gardu_induk_id' => 'js:gardu_induk_id.value','transmisi_id' => 'js:transmisi_id.value'),
												'beforeSend' => 'function(){
													$("#loading4").show();
												}',
												'complete' => 'function(){
													$("#loading4").hide();
												}',
											 )
											 

										);
									 ?>
									<div id = 'loading4' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
								   
									<div  class = 'pertahunan' class="export2" id = 'pertahunan'>
										<!-- Line Chart -->
									<!-- Line Chart -->
									<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelTahunan,'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listTahun,
											 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 )
										  ),
										  'yAxis' => array(
											 'title' => array('text' => 'Beban (MW)')
										  ),
										   'colors'=>array('blue'),
										   'tooltip' => array(
												'headerFormat' => 'Tahun {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/>Tanggal:{point.tanggal}',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
											  ),
										   
										  'series' => array(
											 array('name' => 'Beban', 'data' => $valueTahun , 'type' => 'column'),
											
											
										  )
									   )
									));

							?>
									<!-- End Line Chart -->
								</div>
								<?php

								echo CHtml::endForm();
								?>
							</div>
							</div>
							
						</div>

                </div>
                    
                </div>
            </div>
            <!-- End Pertama -->
        </div>