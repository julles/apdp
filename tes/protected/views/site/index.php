<style type="text/css">
.form-control;
{
	 height:26px;width:96px;
}
title: {
    text: 'Example with bold text',
    floating: true,
    align: 'right',
    x: -30,
    y: 30
}
</style>

<?php Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;?>
<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<?php
	$base = Yii::app()->baseUrl."/web/";
   // print_r($beban_perjam);	
?>
<div id="rightContent">
        	<div id="hearts">
      			<a id="putar"href="#" onclick="rotate()" title="Minimize and Maximize"></a>
    		</div>
        	<!-- Grafik Pertama -->
        	<div class="judul">
                .:  GRAFIK BEBAN SISTEM  :.
            </div>						 
           <div id="textnya">
                	<div id="portlet">
                    	<p class="portlet-title"><u>Beban Sistem Perjam</u> <?php echo CHtml::link("Link Detail" , array('/beban') , array('style' => 'float:right;font-size:14px;text-decoration:none;')); ?></p>

                    </div>
                	<div id="beban-puncak">
                	<?php
                	echo CHtml::beginForm();
                	?>
                        <label style="padding-right:0px;">Tanggal</label>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(

                                'id' => 'tanggalJam',
                                'name'=>'tanggalJam',
                                // additional javascript options for the date picker plugin
                                'options' => array(
                                'showAnim' => 'fold',
                                'dateFormat'=>'yy-mm-dd',
                                 'changeMonth'=>true,
                                'changeYear'=>true,
                                'yearRange'=>'1900:2099',
                                ),
                                'htmlOptions' => array(
                                'style' => 'height:26px;width:120px;',
                                 'class' => 'form-control'
                                ),
                            ));

                         ?>

                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn_jam" style = "height:27px;margin-top:1px;" >Tampil</button>' , 
                        		 array('beban/ajaxJam'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perjam',
                        		 	'data' => array('tanggalJam' => 'js:tanggalJam.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading1").show();
									}',
									'complete' => 'function(){
										$("#loading1").hide();
									}',
                        		 )
								);
                         ?>
                        <div id = 'loading1' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                       
                        <div  class = 'perjam' class="export2" id = 'perjam'>
                            <!-- Line Chart -->
							<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => ar::formatWaktu($date , "long" ,"") , 'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listJam,
											 'type' => 'category',
											 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 )
										  ),
										  'yAxis' => array(
											 'title' => array('text' => 'Beban (MW)'),
											 'min' => 0 ,
										  ),
                                           'plotOptions' => array(
                                                    'series' => array(
                                                            'marker' => array(
                                                                'enabled' => false,

                                                            )
                                                    )
                                                   ),
										   'colors'=>array('blue' , 'pink','red'),
										  'series' => array(
											
											 array('name' => 'Prakiraan Beban', 'data' => $prakiraan_beban, 'lineWidth' => 3),
											 array('name' => 'Prakiraan Daya Mampu', 'data' => $prakiraan_daya_mampu, 'lineWidth' => 3),
                                              array('name' => 'Realisasi Beban', 'data' => $beban_perjam , 'lineWidth' => 3),
											
										  )
									   )
									));

							?>
                            <!-- End Line Chart -->
                    	
						</div>
                    	<?php

                    	echo CHtml::endForm();
                    	?>
                	</div>
                    
                </div>
            <!-- End Grafik Pertama -->
			
			<!-- Grafik trafo -->
        	<div class="judul">
                .:  GRAFIK BEBAN TRAFO  :.
            </div>						 
           <div id="textnya">
                	<div id="portlet">
                    	<p class="portlet-title"><u>Beban Trafo Perjam</u><?php echo CHtml::link("Link Detail" , array('/ntrafo') , array('style' => 'float:right;font-size:14px;text-decoration:none;')); ?></p>
                    </div>
                	<div id="beban-puncak">
                	<?php
                	echo CHtml::beginForm();
                	?>
                        <label style="padding-right:0px;">Tanggal</label>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(

                                'id' => 'tanggalTrafo',
                                'name'=>'tanggalTrafo',
                                // additional javascript options for the date picker plugin
                                'options' => array(
                                'showAnim' => 'fold',
                                'dateFormat'=>'yy-mm-dd',
                                 'changeMonth'=>true,
                                'changeYear'=>true,
                                'yearRange'=>'1900:2099',
                                ),
                                'htmlOptions' => array(
                                'style' => 'height:26px;width:120px;',
                                 'class' => 'form-control'
                                ),
                            ));

                         ?>

                        <?php
                        	echo CHtml::ajaxLink('<button style = "height:27px;margin-top:1px;" id = "btn_jam" >Tampil</button>' , 
                        		 array('ntrafo/ajaxJamHome'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perjamTrafo',
                        		 	'data' => array('tanggalJam' => 'js:tanggalTrafo.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading2").show();
									}',
									'complete' => 'function(){
										$("#loading2").hide();
									}',
                        		 )
								 

                        	);
                         ?>
                        <div id = 'loading2' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                       
                        <div  class = 'perjamTrafo' class="export2" id = 'perjamTrafo'>
                            <!-- Line Chart -->
							<?php
											
											$this->Widget('ext.highcharts.HighchartsWidget', array(
											   'options'=>array(
												  'title' => array('text' => ar::formatWaktu($date , "long" ,""),'style' => 'font-size:16px;'),
												  'credits' => false , 
												  'xAxis' => array(
													 'categories' => $listJam,
													 'type' => 'category',
													 'labels' => array(
														'rotation' => 270,
														'y' => 40,
														//'style' => array('font-size' => '5px;')
													 )
												  ),
                                                   'plotOptions' => array(
                                                    'series' => array(
                                                            'marker' => array(
                                                                'enabled' => false,

                                                            )
                                                    )
                                                   ),
												  'yAxis' => array(
													 'title' => array('text' => 'Beban (MW)')
												  ),
												  // 'colors'=>array('red' , 'blue','pink'),
												  'series' => $chartTrafo
											   )
											));

									?>
                            <!-- End Line Chart -->
                    	
						</div>
                    	<?php

                    	echo CHtml::endForm();
                    	?>
                	</div>
                    
                </div>
            
			<!-- End Grafik trafo -->
			
			<!-- Start susut -->
            <div class="judul">
                .:  GRAFIK SUSUT TRANSMISI  :.
            </div>						 
           <div id="textnya">
                	<div id="portlet">
                    	<p class="portlet-title"><u>Susut Transmisi Perhari</u><?php echo CHtml::link("Link Detail" , array('/TRANSMISI-NERACA') , array('style' => 'float:right;font-size:14px;text-decoration:none;')); ?></p>
                    </div>
                	<div id="beban-puncak">
                        <label style="padding-right:0px;">Bulan & Tahun</label>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'bulanHarianSusut'>
                            <option value="">Select</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                        <select class="form-control" style="height:27px;font-size:12px;width:105px;" id = 'tahunHarianSusut'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn11" style = "height:27px;margin-top:1px;">Tampil</button>' , 
                        		 array('neracaEnergi/transmisiHarian'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perharianSusut',
                        		 	'data' => array('bulan' => 'js:bulanHarianSusut.value' , 'tahun' => 'js:tahunHarianSusut.value'  , 'gardu_induk_id' => '' , 'item_id' => '' ,'pilihan' => '1' ,'jenisSusut' => '2'),
                        		 	'beforeSend' => 'function(){
										$("#loading3").show();
									}',
									'complete' => 'function(){
										$("#loading3").hide();
									}',
                        		 )

                        	);
                         ?>
                        <div id = 'loading3' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        <div id = 'harian'></div>
                        <div class="export2" id = 'perharianSusut'>
                    		<!-- Bar Chart -->
								<?php
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelHarian,'style' => 'font-size:16px;'),
										  'credits' => false ,
										  'xAxis' => array(
											 'categories' => $dataHarian,
											 
											 'labels' => array(
												'rotation' => 270,
														'y' => 40,
												//'style' => array('font-size' => '5px;')
											 ),
											   //'min' => 0 ,
									          //'max' => 12
										  ),
										  'scrollbar' => array(
									      	'enabled' => false
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
										  'yAxis' => array(
										//	'min' => 0,
											 'title' => array('text' => $text)
										  ),
										  'series' => array(
											 array('name' => $name, 'data' => $hasilHarian , 'type' => 'column'),
											
											
										  )
									   )
									));
									
								?>
                            
                            <!-- End Bar Chart -->
                    	</div>
                	</div>
                    
                </div>
            <!-- End Grafik susut -->
            <!-- Start bahan bakar -->
            <div class="judul">
                .:  GRAFIK BAHAN BAKAR  :.
            </div>						 
           <div id="textnya">
                	<div id="portlet">
                    	<p class="portlet-title"><u>Bahan Bakar Perhari</u><?php echo CHtml::link("Link Detail" , array('/bahanBakar') , array('style' => 'float:right;font-size:14px;text-decoration:none;')); ?></p>
                    </div>
                	<div id="beban-puncak">
                        <label style="padding-right:0px;">Bulan & Tahun</label>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'bulanHarianBakar'>
                            <option value="">Select</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                        <select class="form-control" style="height:27px;font-size:12px;width:105px;" id = 'tahunHarianBakar'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn1" style = "height:27px;margin-top:1px;" >Tampil</button>' , 
                        		 array('bahanBakar/harian'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perharianBakar',
                        		 	'data' => array('bulan' => 'js:bulanHarianBakar.value' , 'tahun' => 'js:tahunHarianBakar.value'  , 'unit_pembangkit_id' => '' , 'jenis_pembangkit_id' => '' , 'pembangkit_id' => ''),
                        		 	'beforeSend' => 'function(){
										$("#loading4").show();
									}',
									'complete' => 'function(){
										$("#loading4").hide();
									}',
                        		 )

                        	);
                         ?>
                        <div id = 'loading4' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        <div id = 'harian'></div>
                        <div class="export2" id = 'perharianBakar'>
                    		<!-- Bar Chart -->
								<?php
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelHarian,'style' => 'font-size:16px;'),
										  'credits' => false ,
										  'xAxis' => array(
											 'categories' => $dataHarian,
											 
											 'labels' => array(
												'rotation' => 0,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 ),
											   'min' => 0 ,
									          'max' => 12
										  ),
										  'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
										  'yAxis' => array(
											'min' => 0,
											 'title' => array('text' => 'Liter')
										  ),
										  'series' => array(
											 array('name' => 'HSD', 'data' => $hsd , 'type' => 'column'),
											 array('name' => 'MFO', 'data' => $mfo , 'type' => 'column'),
											 array('name' => 'OLEIN', 'data' => $olein , 'type' => 'column'),
											 array('name' => 'BATU BARA', 'data' => $batu_bara , 'type' => 'column'),
											
										  )
									   )
									));
								
								?>
                            
                            <!-- End Bar Chart -->
                    	</div>
                	</div>
                    
                </div>
            <!-- End Grafik trafo -->
           
        </div>