<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
										   'options'=>array(
											  'title' => array('text' => $year,'style' => 'font-size:16px;'),
											  'xAxis' => array(
												 'categories' => $listBulan
											  ),
											  'yAxis' => array(
												 'title' => array('text' => $satuan)
											  ),
											  'colors'=>array('blue' , 'red'),
											  'series' => array(
												array('name' => 'Target', 'data' => $dataTarget , 'type' => 'column' ),
												array('name' => 'Real', 'data' => $dataReal , 'type' => 'column'),
												
											  )
										   )
										));

							?>