<div id = 'render1'></div>
<?php
									
			$this->Widget('ext.highcharts.HighchartsWidget', array(
				   'options'=>array(
					  'title' => array('text' => $year,'style' => 'font-size:16px;'),
					  'xAxis' => array(
						 'categories' => $listBulan
					  ),
					  'chart' => array(
							'renderTo' => 'render1',
						   
						),
					  'yAxis' => array(
						 'title' => array('text' => $satuan)
					  ),
					  'colors'=>array('blue' , 'red'),
					  'series' => array(
						array('name' => 'Target', 'data' => $dataTargetKumulatif , 'type' => 'column' ),
						array('name' => 'Real', 'data' => $dataRealKumulatif , 'type' => 'column'),
						
					  )
				   )
				));

	?>