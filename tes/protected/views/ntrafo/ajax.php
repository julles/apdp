<div id="textnya">
							<div id="portlet">
								<p class="portlet-title"><u>Beban Trafo Perjam</u></p>
							</div>
							<div id="beban-puncak">
								<?php
							echo CHtml::beginForm();
							?>
								
								<div id = 'loading1' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
								<div class="export2" id = 'perjam'>
									<!-- Line Chart -->
									<?php
											
											$this->Widget('ext.highcharts.HighchartsWidget', array(
											   'options'=>array(
												  'title' => array('text' => ar::formatWaktu($date , "long" ,"")),
												  'credits' => false , 
												  'xAxis' => array(
													 'categories' => $listJam,
													 'type' => 'category',
													 'labels' => array(
														'rotation' => '-15',
														'style' => array('font-size' => '5px;')
													 )
												  ),
												  'yAxis' => array(
													 'title' => array('text' => 'Daya (Mw)')
												  ),
												   'colors'=>array('red' , 'blue','pink'),
												  'series' => array(
													 array('name' => 'Daya(Mw)', 'data' => $nilaiPerjam),
													 
													
												  )
											   )
											));

									?>
									<!-- End Line Chart -->
								</div>
								<?php

								echo CHtml::endForm();
								?>
							</div>
							
<div id="textnya">
							<div id="portlet">
								<p class="portlet-title"><u>Beban Puncak Trafo Harian</u></p>
							</div>
							<div id="beban-puncak">
								<?php
							echo CHtml::beginForm();
							?>
								
								<div id = 'loading1' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
								<div class="export2" id = 'perjam'>
									<!-- Line Chart -->
									<?php
									$labelBulanTahun = explode(" ",ar::formatWaktu($date , "long" ,""));
									$labelBulanTahunFix = $labelBulanTahun[1]." - ".$labelBulanTahun[2];
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelBulanTahunFix),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listHari,
											  'type' => 'category',
												 'labels' => array(
													'rotation' => '-15',
													'style' => array('font-size' => '5px;')
												 )
										  ),
										  'yAxis' => array(
											 'title' => array('text' => 'Beban (Mw)')
										  ),
										   'colors'=>array('green'),
										   'tooltip' => array(
												'headerFormat' => 'Tanggal {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/>',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
											  ),
										   
										  'series' => array(
											 array('name' => 'Beban Puncak', 'data' => $valueHari , 'type' => 'column'),
											
											
										  )
									   )
									));
									?>
									<!-- End Line Chart -->
								</div>
								<?php

								echo CHtml::endForm();
								?>
							</div>
<div id="textnya">
							<div id="portlet">
								<p class="portlet-title"><u>Beban Puncak Trafo Bulanan</u></p>
							</div>
								<div id="beban-puncak">
								<?php
							echo CHtml::beginForm();
							?>
								
								<div id = 'loading1' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
								<div class="export2" id = 'perjam'>
									<!-- Line Chart -->
									<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $year),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listBulan,
											 'type' => 'category',
												 'labels' => array(
													'rotation' => '-15',
													'style' => array('font-size' => '5px;')
												 )
										  ),
										  'yAxis' => array(
											 'title' => array('text' => 'Beban (Mw)')
										  ),
										   'colors'=>array('red'),
										   'tooltip' => array(
												'headerFormat' => 'Bulan {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/> Tanggal: {point.tanggal}',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
											  ),
										   
										  'series' => array(
											 array('name' => 'Daya (Mw)', 'data' => $valueBulan , 'type' => 'column'),
											
											
										  )
									   )
									));
									?>
									<!-- End Line Chart -->
								</div>
								<?php

								echo CHtml::endForm();
								?>
							</div>
							</div>
							
							<div id="textnya">
							<div id="portlet">
								<p class="portlet-title"><u>Beban Puncak Trafo Tahunan</u></p>
							</div>
								<div id="beban-puncak">
								<?php
							echo CHtml::beginForm();
							?>
								
								<div id = 'loading1' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
								<div class="export2" id = 'perjam'>
									<!-- Line Chart -->
									<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelTahunan),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listTahun
										  ),
										  'yAxis' => array(
											 'title' => array('text' => 'Beban (Mw)')
										  ),
										   'colors'=>array('blue'),
										   'tooltip' => array(
												'headerFormat' => 'Tahun {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/>Tanggal:{point.tanggal}',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
											  ),
										   
										  'series' => array(
											 array('name' => 'Beban Puncak', 'data' => $valueTahun , 'type' => 'column'),
											
											
										  )
									   )
									));

							?>
									<!-- End Line Chart -->
								</div>
								<?php

								echo CHtml::endForm();
								?>
							</div>
							</div>