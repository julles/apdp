<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => ar::formatWaktu($date , "long" ,""),'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listJam,
											  'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 )
										  ),
										  'yAxis' => array(
												'min' => 0 ,
												'title' => array('text' => 'Beban (MW)')
										  ),
										  'plotOptions' => array(
                                            'series' => array(
                                                    'marker' => array(
                                                        'enabled' => false,

                                                    )
                                            )
                                           ),
										   'colors'=>array('blue' , 'pink','red'),
										  'series' => array(
											
											 array('name' => 'Prakiraan Beban', 'data' => $prakiraan_beban, 'lineWidth' => 3),
											 array('name' => 'Prakiraan Daya Mampu', 'data' => $prakiraan_daya_mampu, 'lineWidth' => 3),
                                              array('name' => 'Realisasi Beban', 'data' => $beban_perjam , 'lineWidth' => 3),
											
										  )
									   )
									));

							?>