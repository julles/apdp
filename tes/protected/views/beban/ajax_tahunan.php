<div id = "render2"></div>
<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelTahunan ,'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listTahun,
											 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 )
										  ),
										  'chart' => array(
												'renderTo' => 'render2',
											   
											),
										  'yAxis' => array(
												'min' => 0 ,
											 'title' => array('text' => 'Beban (MW)')
										  ),
										   'colors'=>array('blue'),
										   'tooltip' => array(
												'headerFormat' => 'Tahun {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/>Tanggal:{point.tanggal}',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
											  ),
											  'plotOptions' => array(
												'column' => array(
													 'dataLabels' => array('enabled' => true , 
															
															'crop' => false,
															'overflow' => 'none',
															
															
													),
													
												)
											 ),
										   
										  'series' => array(
											 array('name' => 'Beban Puncak', 'data' => $valueTahun , 'type' => 'column'),
											
											
										  )
									   )
									));

							?>