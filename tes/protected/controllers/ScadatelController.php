<?php
	class ScadatelController extends Controller
	{
		public $layout = '//layouts/web/utama';


		public function actionIndex()
		{
			echo '';
		}

		public function actionGangguanRtu()
		{
			$query = Yii::app()->db->createCommand()->select('g.id , tanggal_gangguan , jam_gangguan , gangguan , evaluasi , tanggal_evaluasi , jam_evaluasi, g.date_post ,  u.nama')
			->from('gangguan_rtu g')
			->join('user u' , 'u.id = g.posted_by')
			->order('tanggal_gangguan DESC')
			->query();
			$this->render('scadatel_gangguan_rtu_index' , array(
				'model' => $query
			));
		}

		public function actionGangguanTc()
		{
			$query = Yii::app()->db->createCommand()->select('g.id , tanggal_gangguan , jam_gangguan , gangguan , evaluasi , tanggal_evaluasi , jam_evaluasi, g.date_post ,  u.nama')->from('gangguan_tc g')
			->join('user u' , 'u.id = g.posted_by')
			->order('tanggal_gangguan DESC')
			->query();
			$this->render('scadatel_gangguan_tc_index' , array(
				'model' => $query
			));
		}
		

		public function actionGangguanTelekomunikasi()
		{
			$query = Yii::app()->db->createCommand()->select('g.id , tanggal_gangguan , jam_gangguan , gangguan , evaluasi , tanggal_evaluasi , jam_evaluasi, g.date_post ,  u.nama')->from('gangguan_telekomunikasi g')
			->join('user u' , 'u.id = g.posted_by')
			->order('tanggal_gangguan DESC')
			->query();
			$this->render('scadatel_gangguan_telekomunikasi_index' , array(
				'model' => $query
			));
		}
				
	}