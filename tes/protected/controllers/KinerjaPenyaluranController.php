<?php
	class KinerjaPenyaluranController extends Controller
	{
		public $layout = '//layouts/web/utama';
	
		public function actionIndex()	
		{

			$year = date("Y");
			
			$db = Yii::app()->db;
			$r = $db->createCommand("SELECT MIN(id) AS id , satuan FROM master_kinerja_penyaluran ORDER BY id ASC")->queryRow();
			$kinerja = $r['id'];
			$satuan = $r['satuan'];
			$this->render('index' , 
					array(
						'year' => $year,
						'listBulan' => ar::getDataBulananTanpaTahun($year),
						'dataReal' => TransKinerjaPenyaluranDetail::model()->getDataBulananValue($year , $kinerja , "real"),
						'dataTarget' => TransKinerjaPenyaluranDetail::model()->getDataBulananValue($year , $kinerja , "target"),
						'satuan' => $satuan,
						'dataRealKumulatif' => TransKinerjaPenyaluranDetail::model()->getDataBulananValue($year , $kinerja , "real_kumulatif"),
						'dataTargetKumulatif' => TransKinerjaPenyaluranDetail::model()->getDataBulananValue($year , $kinerja , "target_kumulatif"),
						
					)
			);
		}

		public function actionAjaxBulanan()
		{

			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			$db = Yii::app()->db;
			(!empty($_POST['year'])) ? $year = $_POST['year'] : $year = date("Y");
			$kinerja = $_POST['kinerja'];
			$satuan = $db->createCommand("SELECT satuan FROM master_kinerja_penyaluran WHERE id = '$kinerja'")->queryScalar();
			$this->renderpartial('ajax_bulanan' , 
					array(
						'year' => $year,
						'listBulan' => ar::getDataBulananTanpaTahun($year),
						'dataReal' => TransKinerjaPenyaluranDetail::model()->getDataBulananValue($year , $kinerja , "real"),
						'dataTarget' => TransKinerjaPenyaluranDetail::model()->getDataBulananValue($year , $kinerja , "target"),
						'satuan' => $satuan
					) , false , true
			);

		}

		public function actionAjaxBulananKumulatif()
		{

			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			$db = Yii::app()->db;
			(!empty($_POST['year'])) ? $year = $_POST['year'] : $year = date("Y");
			$kinerja = $_POST['kinerja'];
			$satuan = $db->createCommand("SELECT satuan FROM master_kinerja_penyaluran WHERE id = '$kinerja'")->queryScalar();
			$this->renderpartial('ajax_bulanan_kumulatif' , 
					array(
						'year' => $year,
						'listBulan' => ar::getDataBulananTanpaTahun($year),
						'dataRealKumulatif' => TransKinerjaPenyaluranDetail::model()->getDataBulananValue($year , $kinerja , "real_kumulatif"),
						'dataTargetKumulatif' => TransKinerjaPenyaluranDetail::model()->getDataBulananValue($year , $kinerja , "target_kumulatif"),
						'satuan' => $satuan
					) , false , true
			);

		}
	}