<?php
	class KegiatanController extends Controller
	{
		public $layout = '//layouts/web/utama';

		public function actionBerita()
		{

			$criteria = new CDbCriteria();
			$criteria->order = 'id DESC';
			$count = Berita::model()->count($criteria);

			$pages = new CPagination($count);
			$pages->pageSize = 10;
			$pages->applyLimit($criteria);	

			$model = Berita::model()->findAll($criteria);

			$this->render('berita' , array('model' => $model , 'pages' => $pages));
		}


		public function actionBeritaDetail($read , $aksi = "")
		{
			$model = Berita::model()->find('seo=:seo' , array(':seo' => $read));
			if($model === null)
			{
				throw new CHttpException(404,"");
			}else{

				$alamat = Yii::app()->baseUrl."/images/";

				(!empty($model->gambar)) ? $image = $alamat."berita/thumb/".$model->gambar : $image = $alamat."no.jpg";
				
				$gambar = '<img style = "width:169px;height:168px;float:left;margin-right:10px;" src="'.$image.'"  class="image-satuan"/>';

				
				if(empty($aksi))
				{
					$pages = 'berita_detail';
					$this->layout = '//layouts/web/utama';
				}elseif($aksi == 'print'){
					$pages = 'berita_print';
					$this->layout = '//layouts/web/print';
				}elseif($aksi == 'pdf'){
					$this->layout = '//layouts/web/print';
					$pages = 'berita_pdf';
				}else{
					throw new CHttpException(404,"");
				}

				$this->render($pages , array('model' => $model , 'gambar' => $gambar));
				
				
			}
		}


		public function actionPengumuman()
		{

			$criteria = new CDbCriteria();
			$criteria->order = 'id DESC';
			$count = Pengumuman::model()->count($criteria);

			$pages = new CPagination($count);
			$pages->pageSize = 10;
			$pages->applyLimit($criteria);	

			$model = Pengumuman::model()->findAll($criteria);

			$this->render('pengumuman' , array('model' => $model , 'pages' => $pages));
		}


		public function actionPengumumanDetail($read , $aksi = "")
		{
			$model = Pengumuman::model()->find('seo=:seo' , array(':seo' => $read));
			if($model === null)
			{
				throw new CHttpException(404,"");
			}else{

				$alamat = Yii::app()->baseUrl."/images/";

				(!empty($model->gambar)) ? $image = $alamat."pengumuman/thumb/".$model->gambar : $image = $alamat."no.jpg";
				
				$gambar = '<img style = "width:169px;height:168px;float:left;margin-right:10px;" src="'.$image.'"  class="image-satuan"/>';

				
				if(empty($aksi))
				{
					$pages = 'pengumuman_detail';
					$this->layout = '//layouts/web/utama';
				}elseif($aksi == 'print'){
					$pages = 'pengumuman_print';
					$this->layout = '//layouts/web/print';
				}elseif($aksi == 'pdf'){
					$this->layout = '//layouts/web/print';
					$pages = 'pengumuman_pdf';
				}else{
					throw new CHttpException(404,"");
				}

				$this->render($pages , array('model' => $model , 'gambar' => $gambar));
				
				
			}
		}


		public function actionGallery()
		{

			$criteria = new CDbCriteria();
			$criteria->order = 'id DESC';
			$count = GalleryFoto::model()->count($criteria);

			$pages = new CPagination($count);
			$pages->pageSize = 10;
			$pages->applyLimit($criteria);	

			$model = GalleryFoto::model()->findAll($criteria);

			$this->render('gallery' , array('model' => $model , 'pages' => $pages));
		}

		public function actionGalleryDetail($read)
		{
			$model = GalleryFoto::model()->find('seo=:seo' , array(':seo' => $read));
			if($model === null)
			{
				throw new CHttpException(404,"");
			}else{

				$listFoto = GalleryFotoDetail::model()->findAll('gallery_foto_id=:ids' , array(':ids' => $model->id));	
				$this->render('gallery_detail' , array('model' => $model , 'listFoto' => $listFoto));
				
			}
		}


		public function actionVideo()
		{

			$criteria = new CDbCriteria();
			$criteria->order = 'id DESC';
			$count = GalleryVideo::model()->count($criteria);

			$pages = new CPagination($count);
			$pages->pageSize = 10;
			$pages->applyLimit($criteria);	

			$model = GalleryVideo::model()->findAll($criteria);

			$this->render('video' , array('model' => $model , 'pages' => $pages));
		}


		public function actionVideoDetail($read)
		{
			$model = GalleryVideo::model()->find('seo=:seo' , array(':seo' => $read));
			if($model === null)
			{
				throw new CHttpException(404,"");
			}else{

				$listVideo = GalleryVideoDetail::model()->findAll('gallery_video_id=:ids' , array(':ids' => $model->id));	
				$this->render('video_detail' , array('model' => $model , 'listVideo' => $listVideo));
				
			}
		}

	}