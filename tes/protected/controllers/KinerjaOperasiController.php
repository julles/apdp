<?php
	class KinerjaOperasiController extends Controller
	{
		public $layout = '//layouts/web/utama';
	
		public function actionIndex()	
		{

			$year = date("Y");
			
			$db = Yii::app()->db;
			$r = $db->createCommand("SELECT MIN(id) AS id , satuan FROM master_kinerja_operasi ORDER BY id ASC")->queryRow();
			$kinerja = $r['id'];
			$satuan = $r['satuan'];
			$this->render('index' , 
					array(
						'year' => $year,
						'satuan' => $satuan , 
						'listBulan' => ar::getDataBulananTanpaTahun($year),
						'dataReal' => TransKinerjaOperasiDetail::model()->getDataBulananValue($year , $kinerja , "real"),
					)
			);
		}

		public function actionAjaxBulanan()
		{

			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			$db = Yii::app()->db;
			(!empty($_POST['year'])) ? $year = $_POST['year'] : $year = date("Y");
			$kinerja = $_POST['kinerja'];
			$satuan = $db->createCommand("SELECT satuan FROM master_kinerja_operasi WHERE id = '$kinerja'")->queryScalar();
			$this->renderpartial('ajax_bulanan' , 
					array(
						'year' => $year,
						'listBulan' => ar::getDataBulananTanpaTahun($year),
						'dataReal' => TransKinerjaOperasiDetail::model()->getDataBulananValue($year , $kinerja , "real"),
						'satuan' => $satuan
					) , false , true
			);

		}

		
	}