<?php
	class KinerjaScadatelController extends Controller
	{
		public $layout = '//layouts/web/utama';
	
		public function actionIndex()	
		{

			$year = date("Y");

			$db = Yii::app()->db;
			$getKinerja = $db->createCommand("SELECT MIN(id) AS id , satuan FROM master_kinerja_scadatel ORDER BY id ASC")->queryRow();
			$kinerja = $getKinerja['id'];
			$satuan = $getKinerja['satuan'];
			$this->render('index' , 
					array(
						'year' => $year,
						'listBulan' => ar::getDataBulananTanpaTahun($year),
						'dataReal' => TransKinerjaScadatelDetail::model()->getDataBulananValue($year , $kinerja , "real"),
						'dataTarget' => TransKinerjaScadatelDetail::model()->getDataBulananValue($year , $kinerja , "target"),
						'dataRealKumulatif' => TransKinerjaScadatelDetail::model()->getDataBulananValue($year , $kinerja , "real_kumulatif"),
						'dataTargetKumulatif' => TransKinerjaScadatelDetail::model()->getDataBulananValue($year , $kinerja , "target_kumulatif"),
						'satuan' => $satuan,
					)
			);
		}

		public function actionAjaxBulanan()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			$db = Yii::app()->db;
			(!empty($_POST['year'])) ? $year = $_POST['year'] : $year = date("Y");
			$kinerja = $_POST['kinerja'];
			$satuan = $db->createCommand("SELECT satuan FROM master_kinerja_scadatel WHERE id = '$kinerja'")->queryScalar();
			$this->renderpartial('ajax_bulanan' , 
					array(
						'year' => $year,
						'listBulan' => ar::getDataBulananTanpaTahun($year),
						'dataReal' => TransKinerjaScadatelDetail::model()->getDataBulananValue($year , $kinerja , "real"),
						'dataTarget' => TransKinerjaScadatelDetail::model()->getDataBulananValue($year , $kinerja , "target"),
						'satuan' => $satuan
					) , false , true
			);

		}

		public function actionAjaxKumulatif()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			$db = Yii::app()->db;
			(!empty($_POST['year'])) ? $year = $_POST['year'] : $year = date("Y");
			$kinerja = $_POST['kinerja'];
			$satuan = $db->createCommand("SELECT satuan FROM master_kinerja_scadatel WHERE id = '$kinerja'")->queryScalar();
			$this->renderpartial('ajax_kumulatif' , 
					array(
						'year' => $year,
						'listBulan' => ar::getDataBulananTanpaTahun($year),
						'dataRealKumulatif' => TransKinerjaScadatelDetail::model()->getDataBulananValue($year , $kinerja , "real_kumulatif"),
						'dataTargetKumulatif' => TransKinerjaScadatelDetail::model()->getDataBulananValue($year , $kinerja , "target_kumulatif"),
						'satuan' => $satuan
					) , false , true
			);
		}
	}