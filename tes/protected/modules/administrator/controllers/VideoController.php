<?php
	class VideoController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
					$model = GalleryVideo::model()->findAll();
					$hasil = '';
					foreach($model as $row)
					{
						$hasil .= "
							<tr>
								<td>$row->nama_album</td>
								<td>".ar::formatWaktu($row->date_post , 'medium' , '')."</td>
								<td>".$row->user->nama."</td>
								<td>".CHtml::link(ar::btnEdit() , array('update' , 'id' => $row->id)).
								       " ".
									  CHtml::link(ar::btnHapus() , array('hapus' , 'id' => $row->id , 'token' => ar::encrypt($row->id)) , array('onclick' => 'return confirm("Anda Yakin Menghapus Data ini ?")') )
							 ."</td>
							</tr>
						";
					}


					$this->render('index' , array('hasil' => $hasil));
		}


		public function actionCreate()
		{
			$model = new GalleryVideo;
			$detil = new GalleryVideoDetail;
			if(isset($_POST['GalleryVideo']) && isset($_POST['GalleryVideoDetail']))
			{
				$model->attributes = $_POST['GalleryVideo'];
				
				 $url = $_POST['GalleryVideoDetail']['url'];
				 @$hit = count($url);
					if($model->save())
					{

						$koneksi = Yii::app()->db;
						$transaction = $koneksi->beginTransaction();
						try
						{

							for($a=0;$a<$hit;$a++)
							{
								$simpan = new GalleryVideoDetail;
								$simpan->attributes = $_POST['GalleryVideoDetail'];
								if(!empty($simpan->url[$a]))
								{
									
									$simpan->gallery_video_id = $model->id;
									$simpan->url = $simpan->url[$a];
									$simpan->save();
								}
							}

							$transaction->commit();

							

						}catch(Exception $e){

							$transaction->rollback();
							ar::flash('danger' , 'Data gagal disimpan  , time out!!');
							$this->redirect(array('index'));
						}

						ar::flash('info' , 'Data telah disimpan');
						$this->redirect(array('index'));
					}
			}

			$this->render('_form' , array('model' => $model , 'detil' => $detil));
		}

		public function actionUpdate($id)
		{
			$model = $this->loadModel($id);
			$detil = new GalleryVideoDetail;
			if(isset($_POST['GalleryVideo']) && isset($_POST['GalleryVideoDetail']))
			{
				$model->attributes = $_POST['GalleryVideo'];
				
				 $url = $_POST['GalleryVideoDetail']['url'];
				 @$hit = count($url);
					if($model->save())
					{

						$koneksi = Yii::app()->db;
						$transaction = $koneksi->beginTransaction();
						try
						{

							for($a=0;$a<$hit;$a++)
							{
								$simpan = new GalleryVideoDetail;
								$simpan->attributes = $_POST['GalleryVideoDetail'];
								if(!empty($simpan->url[$a]))
								{
									
									$simpan->gallery_video_id = $model->id;
									$simpan->url = $simpan->url[$a];
									$simpan->save();
								}
							}

							$transaction->commit();

							

						}catch(Exception $e){

							$transaction->rollback();
							ar::flash('danger' , 'Data gagal diupdate  , time out!!');
							$this->redirect(array('update' , 'id' => $model->id));
						}

						ar::flash('info' , 'Data telah diupdate');
						$this->redirect(array('update' , 'id' => $model->id));
					}
			}

			$all = GalleryVideoDetail::model()->findAll('gallery_video_id=:id_detil' , array(':id_detil' => $model->id));
			$this->render('form_update' , array('model' => $model , 'detil' => $detil , 'all' => $all));
		}

		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			if($cek == $id)
			{
				try
				{
					$model = $this->loadModel($id);
					$deleteDetil = ar::query("DELETE FROM gallery_video_detail WHERE gallery_video_id = '$model->id'")->execute();
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}

		public function actionHapusVideo($id , $ids)
		{
			$model = $this->loadModel($id);
			$detil = GalleryVideoDetail::model()->findByPk($ids);
			if($detil->delete())
			{
							ar::flash('info' , 'Video telah dihapus ');
							$this->redirect(array('update' , 'id' => $model->id));
			
			}else{
							ar::flash('danger' , 'Data gagal dihapus  , time out!!');
							$this->redirect(array('update' , 'id' => $model->id));
			}
		}

		public function loadModel($param)
		{
			$model = GalleryVideo::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}

		protected function performAjaxValidation($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='role-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
	}