<?php
	class HakController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
					$model = Role::model()->findAll();
					$hasil = '';
					foreach($model as $row)
					{
						$hasil .= "
							<tr>
								<td>$row->role</td>
								
								<td>".CHtml::link(ar::btnEdit() , array('update' , 'id' => $row->id))."</td>
							</tr>
						";
					}

					$this->render('index' , array('hasil' => $hasil));

		}


		public function actionUpdate($id)
		{
			$model = $this->loadModel($id);
			if(isset($_POST['Role']))
			{
				$koneksi = Yii::app()->db;
				@$hit = count($_POST['cek']);
			
				$transaksi = $koneksi->beginTransaction();
				if($hit > 0)
				{
					try
					{
						$koneksi->createCommand("DELETE FROM hak_akses WHERE role_id = '$model->id'")->execute();
						for($a=0;$a<$hit;$a++)
						{
							$simpan = new HakAkses;
							$simpan->menu_id = $_POST['cek'][$a];
							$simpan->role_id = $model->id;
							$simpan->save();
							
						}
				

						$transaksi->commit();
							ar::flash('info' , 'Data telah disimpan');
							$this->redirect(array('index'));
					}catch(Exception $e){
						$transaksi->rollback();
						ar::flash('info' , 'Data gagal disimpan , ada masalah dengan database!!');
						$this->redirect(array('index'));
					}
				}	
			}

			$this->render('_form' , array('model' => $model));
		}

		public function loadModel($param)
		{
			$model = Role::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}

		protected function performAjaxValidation($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='role-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}

	}