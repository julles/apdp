<?php
	class BahanBakarController extends Controller
	{
		public $layout = '//layouts/admin/utama';
		public function actionIndex()
		{
					$criteria = new CDbCriteria;
					$criteria->order = 'tanggal DESC';
					$model = BahanBakar::model()->findAll($criteria);
					$hasil = '';
					foreach($model as $row)
					{
						$hasil .= "
							<tr>
								<td>".$row->jenisPembangkit->jenis_pembangkit."</td>
								<td>".$row->pembangkit->pembangkit."</td>
								<td>".ar::formatWaktu($row->tanggal , "medium","")."</td>
								<td>".$row->user->nama."</td>
								<td>".ar::formatWaktu($row->date_post , "medium","")."</td>
								
								<td>".CHtml::link(ar::btnEdit() , array('update' , 'id' => $row->id)).
								       " ".
									  CHtml::link(ar::btnHapus() , array('hapus' , 'id' => $row->id , 'token' => ar::encrypt($row->id)) , array('onclick' => 'return confirm("Anda Yakin Menghapus Data ini ?")') )
							 ."</td>
							</tr>
						";
					}


					$this->render('index' , array('hasil' => $hasil));
		}


		public function actionCreate()
		{
			$model = new BahanBakar;
			$command = Yii::app()->db;
			if(isset($_POST['BahanBakar']))
			{
					$model->attributes = $_POST['BahanBakar'];
					$tanggal = ar::getTanggalSqlNoWaktu($model->tanggal ,"-");
					$pembangkit = $command->createCommand("SELECT pembangkit AS nilai FROM master_pembangkit WHERE '$model->pembangkit_id'")->queryScalar();
					$cek = $command->createCommand("SELECT COUNT(id) AS nilai FROM bahan_bakar WHERE tanggal = '$tanggal' AND pembangkit_id = '$model->pembangkit_id'")->queryScalar();
					
					if($cek > 0)
					{
						$model->addError('tanggal' ,"data pembangkit $pembangkit sudah di input pada tanggal $model->tanggal");
					}else{
						if($model->save())
						{
							ar::flash('info' , 'Data telah disimpan');
							$this->redirect(array('index'));
						}
					}
			}

			$this->render('_form' , array('model' => $model));
		}

		public function actionUpdate($id)
		{
			$model = BahanBakar::model()->findByPk($id);
			$model->tanggal = ar::getTanggalViewNoWaktu($model->tanggal ,"-");
			$command = Yii::app()->db;
			if(isset($_POST['BahanBakar']))
			{
					$model->attributes = $_POST['BahanBakar'];
					
					$tanggal = ar::getTanggalSqlNoWaktu($model->tanggal , "-");
					$pembangkit = $command->createCommand("SELECT pembangkit AS nilai FROM master_pembangkit WHERE '$model->pembangkit_id'")->queryScalar();
					$cek = $command->createCommand("SELECT COUNT(id) AS nilai FROM bahan_bakar WHERE tanggal = '$tanggal' AND pembangkit_id = '$model->pembangkit_id' AND id != '$model->id'")->queryScalar();
					
					if($cek > 0)
					{
						$model->addError('tanggal' ,"data pembangkit $pembangkit sudah di input pada tanggal $model->tanggal");
					}else{
						if($model->save())
						{
							ar::flash('info' , 'Data telah disimpan');
							$this->redirect(array('index'));
						}
					}
			}

			$this->render('_form' , array('model' => $model));
		}
		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			if($cek == $id)
			{
				try
				{
					$model = $this->loadModel($id);
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}
	
		public function actionGetPembangkit()
		{
			if(!empty($_POST['jenis_pembangkit_id']))
			{
				$criteria = new CDbCriteria;
				$criteria->condition = 'jenis_pembangkit_id=:id';
				$criteria->params = array(':id' => $_POST['jenis_pembangkit_id']);
			
				$data = MasterPembangkit::model()->findAll($criteria);
				
				$hasil = CHtml::listData($data , 'id' , 'pembangkit');
				echo "<option></optio>";
				foreach($hasil as $key => $value)
				{
					echo CHtml::tag('option' , array('value' => $key) , CHtml::encode($value));
				}
			
			}
		
		}


		public function actionGetUnitPembangkit()
		{
			if(!empty($_POST['pembangkit_id']))
			{
				$criteria = new CDbCriteria;
				$criteria->condition = 'pembangkit_id=:id';
				$criteria->params = array(':id' => $_POST['pembangkit_id']);
			
				$data = MasterUnitPembangkit::model()->findAll($criteria);
				
				$hasil = CHtml::listData($data , 'id' , 'unit_pembangkit');
				
				foreach($hasil as $key => $value)
				{
					echo CHtml::tag('option' , array('value' => $key) , CHtml::encode($value));
				}
			
			}

		
		}
	
	
		public function loadModel($param)
		{
			$model = BahanBakar::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}

		protected function performAjaxValidation($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='role-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
	}