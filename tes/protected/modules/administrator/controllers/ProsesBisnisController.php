<?php

	class ProsesBisnisController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
			$count = ar::query("SELECT COUNT(id) FROM proses_bisnis")->queryScalar();
			if($count > 0)
			{
				$get = Yii::app()->db->createCommand()->select('id')
				->from('proses_bisnis')
				->order('id DESC')
				->limit(1)
				->queryScalar();


				$model = ProsesBisnis::model()->findByPk($get);
				

			}else{

				$model = new ProsesBisnis;
				
			}


			if(isset($_POST['ProsesBisnis']))
			{
				$model->attributes = $_POST['ProsesBisnis'];
				$model->posted_by = Yii::app()->session['tokenId'];
				$model->date_post = date("Y-m-d H:i:s");
				if($model->save())
				{
					ar::flash('info' , 'Data telah diUpdate!');
					$this->refresh();

				}
			}

			$this->render('_form' ,  array('model' => $model));
		


		}

	}