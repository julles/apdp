<?php
	class frekuensiController extends Controller{
		public $layout = '//layouts/admin/utama';
		public function actionIndex()
		{
					$model = Frekuensi::model()->findAll();
					$hasil = '';
					foreach($model as $row)
					{
						$hasil .= "
							<tr>
								<td>".ar::formatWaktu($row->tanggal , "long" ,"")."</td>
								<td>$row->jam</td>
								<td>$row->daya</td>
								<td>".CHtml::link(ar::btnEdit() , array('update' , 'id' => $row->id)).
								       " ".
									  CHtml::link(ar::btnHapus() , array('hapus' , 'id' => $row->id , 'token' => ar::encrypt($row->id)) , array('onclick' => 'return confirm("Anda Yakin Menghapus Data ini ?")') )
							 ."</td>
							</tr>
						";
					}


					$this->render('index' , array('hasil' => $hasil));
		}


		public function actionCreate()
		{
			$model = new Frekuensi;
			if(isset($_POST['Frekuensi']))
			{
				$model->attributes = $_POST['Frekuensi'];
				if($model->save())
				{
					ar::flash('info' , 'Data telah disimpan');
					$this->redirect(array('index'));
				}
			}

			$this->render('_form' , array('model' => $model));
		}


		public function actionUpdate($id)
		{
			$model = $this->loadModel($id);
			//$model->tanggal = ar::getTanggalViewNoWaktu($model->tanggal , '-');
			$model->tanggal = date("d/m/Y" , strtotime($model->tanggal));
			if(isset($_POST['Frekuensi']))
			{
				$model->attributes = $_POST['Frekuensi'];
				if($model->save())
				{
					ar::flash('info' , 'Data telah disimpan');
					$this->redirect(array('index'));
				}
			}

			$this->render('_form' , array('model' => $model));
		}

		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			if($cek == $id)
			{
				try
				{
					$model = $this->loadModel($id);
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}
			
		public function loadModel($param)
		{
			$model = Frekuensi::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}
	}
?>