<?php
	class UnitPembangkitController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
			$db = Yii::app()->db;
			$model = $db->createCommand()->select("u.id , unit_pembangkit , p.pembangkit , us.nama")
			->from('master_unit_pembangkit u')
			->join('master_pembangkit p' , 'u.pembangkit_id=p.id')
			->join('user us' ,'us.id = u.posted_by')
			->query();
			$this->render('index' , array('model' => $model));
		}


		public function actionCreate()
		{
			$model = new MasterUnitPembangkit;
			if(isset($_POST['MasterUnitPembangkit']))
			{
				$model->attributes = $_POST['MasterUnitPembangkit'];
				if($model->save())
				{
					ar::flash('info' , 'Data telah disimpan');
					$this->redirect(array('index'));
				}
			}
			$dataPembangkit = CHtml::listData(MasterPembangkit::model()->findAll() ,'id' , 'pembangkit');
			$this->render('_form' , array('model' => $model , 'dataPembangkit' => $dataPembangkit));
		}

		public function actionUpdate($id)
		{
			$model = $this->loadModel($id);
			
			if(isset($_POST['MasterUnitPembangkit']))
			{
				$model->attributes = $_POST['MasterUnitPembangkit'];
				if($model->save())
				{
					ar::flash('info' , 'Data telah disimpan');
					$this->redirect(array('index'));
				}
			}
			$dataPembangkit = CHtml::listData(MasterPembangkit::model()->findAll() ,'id' , 'pembangkit');
			$this->render('_form' , array('model' => $model , 'dataPembangkit' => $dataPembangkit));

		}

		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			if($cek == $id)
			{
				try
				{
					$model = $this->loadModel($id);
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}

		public function loadModel($param)
		{
			$model = MasterUnitPembangkit::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}

		protected function performAjaxValidation($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='role-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
	}