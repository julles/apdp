<?php

	class TgiKhatulistiwaController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
			$db = Yii::app()->db;
			$id = $db->createCommand("SELECT MAX(id)  FROM tgi")->queryScalar();

			if(!empty($id))
			{
				$model = Tgi::model()->findByPk($id);
			}else{
				$model = new Tgi;
			}

			if(isset($_POST['Tgi']))
			{
				$model->attributes = $_POST['Tgi'];
				if($model->save())
				{
					$db->createCommand("DELETE FROM tgi WHERE id != '$model->id'")->execute();
					ar::flash('info' , 'Data telah diupdate!');
					$this->redirect(array('index'));
				}
			}


			$this->render('_form' , array('model' => $model));

		}

	}