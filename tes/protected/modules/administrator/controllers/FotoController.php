<?php
	class FotoController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
					$model = GalleryFoto::model()->findAll();
					$hasil = '';
					foreach($model as $row)
					{
						$hasil .= "
							<tr>
								<td>$row->nama_album</td>
								<td>".ar::formatWaktu($row->date_post , 'medium' , '')."</td>
								<td>".$row->user->nama."</td>
								<td>".CHtml::link(ar::btnEdit() , array('update' , 'id' => $row->id)).
								       " ".
									  CHtml::link(ar::btnHapus() , array('hapus' , 'id' => $row->id , 'token' => ar::encrypt($row->id)) , array('onclick' => 'return confirm("Anda Yakin Menghapus Data ini ?")') )
							 ."</td>
							</tr>
						";
					}


					$this->render('index' , array('hasil' => $hasil));
		}


		public function actionCreate()
		{
			$model = new GalleryFoto;
			$detail = new GalleryFotoDetail;
			if(isset($_POST['GalleryFotoDetail']) && isset($_POST['GalleryFoto']))
			{
				$model->attributes = $_POST['GalleryFoto'];
				$koneksi = Yii::app()->db;
				

				
					@$hit = count($_POST['GalleryFotoDetail']['gambar']);
					if($model->save())
					{
						$transaksi = $koneksi->beginTransaction();
						try
						{
							for($a=0;$a<$hit;$a++)
							{
								$simpan = new GalleryFotoDetail;
								$simpan->gallery_foto_id = $model->id;
								$file = CUploadedFile::getInstance($simpan , "gambar[$a]");
								if(!empty($file))
								{
									$simpan->save();
									$ext = pathinfo($file , PATHINFO_EXTENSION);
									$gambar = $simpan->id.".$ext";


									list($width, $height) = getimagesize($file->getTempName());
								
									$dir_th = Yii::app()->basePath.'/../images/gallery/thumb/';
									$img = Yii::app()->simpleImage->load($file->getTempName());

									if ($width > $height) { 
									     $img->resizeToWidth(200);
									 }else{
								     	$img->resizeToHeight(200); 
									 }

									$img->save($dir_th.$gambar);


									$file->saveAs(Yii::app()->basePath."/../images/gallery/$gambar");
								
									$update = GalleryFotoDetail::model()->findByPk($simpan->id);
									$update->gambar = $gambar;
									$update->save();

								}
							}
							$transaksi->commit();
						}catch(Exception $e){
									$transaksi->rollback();
									ar::flash('danger' , 'Data gagal disimpan  , time out!!');
									$this->redirect(array('index'));
						}
						ar::flash('info' , 'Data telah disimpan');
						$this->redirect(array('index'));
					}
					
					//flush();
						
				
				
			}

			$this->render('_form' , array('model' => $model , 'detail' => $detail));
		}

		public function actionUpdate($id)
		{
			$model = $this->loadModel($id);
			$detail = new GalleryFotoDetail;
			if(isset($_POST['GalleryFotoDetail']) && isset($_POST['GalleryFoto']))
			{
				$model->attributes = $_POST['GalleryFoto'];
				$koneksi = Yii::app()->db;
				

				
					@$hit = count($_POST['GalleryFotoDetail']['gambar']);
					if($model->save())
					{
						$transaksi = $koneksi->beginTransaction();
						try
						{
							for($a=0;$a<$hit;$a++)
							{
								$simpan = new GalleryFotoDetail;
								$simpan->gallery_foto_id = $model->id;
								$file = CUploadedFile::getInstance($simpan , "gambar[$a]");
								if(!empty($file))
								{
									$simpan->save();
									$ext = pathinfo($file , PATHINFO_EXTENSION);
									$gambar = $simpan->id.".$ext";


									list($width, $height) = getimagesize($file->getTempName());
								
									$dir_th = Yii::app()->basePath.'/../images/gallery/thumb/';
									$img = Yii::app()->simpleImage->load($file->getTempName());

									if ($width > $height) { 
									     $img->resizeToWidth(200);
									 }else{
								     	$img->resizeToHeight(200); 
									 }

									$img->save($dir_th.$gambar);


									$file->saveAs(Yii::app()->basePath."/../images/gallery/$gambar");
								
									$update = GalleryFotoDetail::model()->findByPk($simpan->id);
									$update->gambar = $gambar;
									$update->save();

								}
							}
							$transaksi->commit();
						}catch(Exception $e){
									$transaksi->rollback();
									ar::flash('danger' , 'Data gagal diupdate  , time out!!');
									$this->redirect(array('update' , 'id' => $model->id));
						}
						ar::flash('info' , 'Data telah diupdate');
						$this->redirect(array('update' , 'id' => $model->id));
					}
					
					//flush();
						
				
				
			}


			$modelFoto = GalleryFotoDetail::model()->findAll('gallery_foto_id=:id_detil' , array('id_detil' => $model->id));
			$this->render('form_update' , array('model' => $model , 'detail' => $detail , 'modelFoto' => $modelFoto)); 
		}

		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			if($cek == $id)
			{
				$koneksi =Yii::app()->db;
				$transaksi = $koneksi->beginTransaction();

				try
				{
					$model = $this->loadModel($id);
					$detail = GalleryFotoDetail::model()->findAll('gallery_foto_id=:id_detil' , array(':id_detil' => $model->id));
					$alamat = Yii::app()->basePath."/../images/gallery/";
					


					foreach($detail as $row)
					{
						
						$big = $alamat.$row->gambar;
						$thumb = $alamat."thumb/".$row->gambar;

						if(file_exists($big))
						{
							@unlink($big);
						}

						if(file_exists($thumb))
						{
							@unlink($thumb);
						}

					}


					$delete = ar::query("DELETE FROM gallery_foto_detail WHERE gallery_foto_id = '$model->id'")->execute();


					$model->delete();
					$transaksi->commit();
				
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));

				}catch(Exception $e){
						$transaksi->rollback();
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}

		public function actionHapusGambar($id , $ids)
		{
			try
			{
				$model = $this->loadModel($id);
				$detil = GalleryFotoDetail::model()->findByPk($ids);
				$alamat = Yii::app()->basePath."/../images/gallery/";
				$big = $alamat.$detil->gambar;
				$thumb = $alamat."thumb/".$detil->gambar;
				if(file_exists($big))
				{
					@unlink($big);
				}

				if(file_exists($thumb))
				{
					@unlink($thumb);
				}

				$detil->delete();
				ar::flash('info' , 'gambar telah dihapus');
				$this->redirect(array('update' , 'id' => $model->id));
			}catch(Exception $e){
				ar::flash('danger' , 'Data gagal dihapus  , time out!!');
				$this->redirect(array('update' , 'id' => $model->id));
				
			}

		}

		public function loadModel($param)
		{
			$model = GalleryFoto::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}

		protected function performAjaxValidation($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='role-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
	}