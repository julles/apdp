<?php
	class PembebananTrafoNewController extends Controller
	{
		public $layout = '//layouts/admin/utama';
		public function actionIndex()
		{
			$db = Yii::app()->db;
			$model = $db->createCommand("SELECT * FROM trans_pembebanan_trafo order by tanggal DESC")->query();
			$this->render('index' , array('model' => $model));
		}

		public function actionCreate()
		{
			$model = new TransPembebananTrafo;
				$db = Yii::app()->db;
			$transaksi = $db->beginTransaction();
		
			if(isset($_POST['TransPembebananTrafo']))
			{
				$model->attributes = $_POST['TransPembebananTrafo'];
				$dbCek =Yii::app()->db->createCommand()->select('id')->from('trans_pembebanan_trafo');
				$cek = $dbCek->where('gardu_induk_id=:gardu AND trafo_id=:trafo AND tanggal =:tanggal ' , array(':gardu' => $model->gardu_induk_id , ':trafo' => $model->trafo_id , 'tanggal' => date("Y-m-d" ,  strtotime($model->tanggal))))->queryScalar();
				if(empty($cek))
				{			
							try
							{
								
								if($model->save())
								{
									for($a=0;$a<=27;$a++)
									{
										$nilai = $_POST['nilai'][$a];
										(empty($nilai)) ? $nilai = 0 : $nilai = $nilai;
										$jam = $_POST['jam'][$a];
										
										$sql = "INSERT INTO trans_pembebanan_trafo_detail VALUES('','$model->id','$nilai', '$jam')";
										$db->createCommand($sql)->execute();
									}
									$max_min = $db->createCommand("SELECT MIN(nilai) FROM trans_pembebanan_trafo_detail WHERE nilai < 0 AND pembebanan_trafo_id = '$model->id'")->queryScalar();
									$max_max = $db->createCommand("SELECT MAX(nilai) FROM trans_pembebanan_trafo_detail WHERE nilai > 0 AND pembebanan_trafo_id = '$model->id'")->queryScalar();
									
									//bandingkan
										$minus = abs($max_min);
										$plus = $max_max;

										if($minus > $plus)
										{
											$hasilNilai = $max_min;
										}else{
										    $hasilNilai = $max_max;
										}
										
										$dapetJam = $db->createCommand("SELECT jam FROM trans_pembebanan_trafo_detail WHERE nilai = '$hasilNilai' AND pembebanan_trafo_id = '$model->id'")->queryScalar();
									//

									$sqlUpdate = "UPDATE trans_pembebanan_trafo SET jam_max = '$dapetJam' ,  jumlah_max = '$hasilNilai' WHERE id = '$model->id'";
									$db->createCommand($sqlUpdate)->execute();
									$transaksi->commit();
									Yii::app()->user->setFlash('success' ,'Data telah disimpan!');
									$this->redirect(array('index'));
								}
							}catch(Exception $e){

									$transaksi->rollback();
									Yii::app()->user->setFlash('danger' ,'Data gagal disimpan!');
									$this->redirect(array('index'));
							}
				}else{
					$model->addError("trafo_id" , $model->trafo->trafo." sudah di input pada tanggal ".$model->tanggal);
					//return false;
				}
			}
			$this->render("_form" , array('model' => $model));
		}


		public function actionUpdate($id)
		{
			$model = TransPembebananTrafo::model()->findByPk($id);
			$db = Yii::app()->db;
			$transaksi = $db->beginTransaction();
			$model->tanggal = ar::getTanggalViewNoWaktu($model->tanggal , "-");
			if(isset($_POST['TransPembebananTrafo']))
			{
				$model->attributes = $_POST['TransPembebananTrafo'];
				$dbCek =Yii::app()->db->createCommand()->select('id')->from('trans_pembebanan_trafo');
				$cek = $dbCek->where('gardu_induk_id=:gardu AND trafo_id=:trafo AND tanggal =:tanggal AND id!=:id' , array(':id'=>$model->id,':gardu' => $model->gardu_induk_id , ':trafo' => $model->trafo_id , 'tanggal' => date("Y-m-d" ,  strtotime($model->tanggal))))->queryScalar();
				if(empty($cek))
				{
							try
							{
								
								$delete = $db->createCommand("DELETE FROM trans_pembebanan_trafo_detail WHERE pembebanan_trafo_id = '$model->id'")->execute();
								if($model->save())
								{
									for($a=0;$a<=27;$a++)
									{
										$nilai = $_POST['nilai'][$a];
										(empty($nilai)) ? $nilai = 0 : $nilai = $nilai;
										$jam = $_POST['jam'][$a];
										
										$sql = "INSERT INTO trans_pembebanan_trafo_detail VALUES('','$model->id','$nilai', '$jam')";
										$db->createCommand($sql)->execute();
									}
									$max_min = $db->createCommand("SELECT MIN(nilai) FROM trans_pembebanan_trafo_detail WHERE nilai < 0 AND pembebanan_trafo_id = '$model->id'")->queryScalar();
									$max_max = $db->createCommand("SELECT MAX(nilai) FROM trans_pembebanan_trafo_detail WHERE nilai > 0 AND pembebanan_trafo_id = '$model->id'")->queryScalar();
									
									//bandingkan
										$minus = abs($max_min);
										$plus = $max_max;

										if($minus > $plus)
										{
											$hasilNilai = $max_min;
										}else{
										    $hasilNilai = $max_max;
										}
										
										$dapetJam = $db->createCommand("SELECT jam FROM trans_pembebanan_trafo_detail WHERE nilai = '$hasilNilai' AND pembebanan_trafo_id = '$model->id'")->queryScalar();
									//

									$sqlUpdate = "UPDATE trans_pembebanan_trafo SET jam_max = '$dapetJam' ,  jumlah_max = '$hasilNilai' WHERE id = '$model->id'";
									$db->createCommand($sqlUpdate)->execute();
									$transaksi->commit();
									Yii::app()->user->setFlash('success' ,'Data telah diupdate!');
									$this->redirect(array('index'));
								}
							}catch(Exception $e){

									$transaksi->rollback();
									Yii::app()->user->setFlash('danger' ,'Data gagal diupdate!');
									$this->redirect(array('index'));
							}
				}else{
					$model->addError("trafo_id" , $model->trafo->trafo." sudah di input pada tanggal ".$model->tanggal);
				}
			}
			$this->render("_form_update" , array('model' => $model , 'db' => $db));
		}

		public function actionGetTrafo()
		{
			$all = MasterTrafo::model()->findAll("gardu_induk_id=:id" , array("id" => $_POST['gardu_id']));
			$list = CHtml::listData($all , 'id' , 'trafo');
			foreach($list as $key => $value)
			{
				echo CHtml::tag('option' , array('value' => $key) , CHtml::encode($value));
			}
		} 


		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			$db = Yii::app()->db;
			$model = $this->loadModel($id);
			if($cek == $id)
			{
				try
				{
					
					$db->createCommand("DELETE FROM trans_pembebanan_trafo_detail WHERE pembebanan_trafo_id = '$model->id'")->execute();
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}

		public function loadModel($param)
		{
			$model = TransPembebananTrafo::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}
	}