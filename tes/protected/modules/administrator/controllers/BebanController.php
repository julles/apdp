<?php
	class BebanController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
			$this->render('index');
		}

		public function actionCreate()
		{
			$model = new Beban;
			$db = Yii::app()->db;
			$transaksi = $db->beginTransaction();
			if(isset($_POST['Beban']))
			{
				try
				{
							$model->posted_by = 3;
							$model->attributes = $_POST['Beban'];
							if($model->save())
							{
								for($a=0;$a<=27;$a++)
								{
									$beban_perjam = $_POST['beban_perjam'][$a];
									$prakiraan_beban = $_POST['prakiraan_beban'][$a];
									$prakiraan_daya_mampu = $_POST['prakiraan_daya_mampu'][$a];
									(empty($beban_perjam)) ? $beban_perjam = 0 : $beban_perjam = $beban_perjam;
									(empty($prakiraan_beban)) ? $prakiraan_beban = 0 : $prakiraan_beban = $prakiraan_beban;
									(empty($prakiraan_daya_mampu)) ? $prakiraan_daya_mampu = 0 : $prakiraan_daya_mampu = $prakiraan_daya_mampu;
									
									$jam = $_POST['jam'][$a];
									$sql = "INSERT INTO beban_detail VALUES('','$model->id','$beban_perjam','$prakiraan_beban','$prakiraan_daya_mampu' , '$jam')";
									$db->createCommand($sql)->execute();
								}

								$max = $db->createCommand("SELECT beban_perjam AS max_beban ,jam FROM beban_detail WHERE beban_id = '$model->id' order by beban_perjam DESC LIMIT 1")->queryRow();
								$jam_max = $max['jam'];
								$jumlah_max = $max['max_beban'];
								$sqlUpdate = "UPDATE beban SET jam_max  = '$jam_max' , jumlah_max = '$jumlah_max' WHERE id = '$model->id'";
								$db->createCommand($sqlUpdate)->execute();
								$transaksi->commit();
								Yii::app()->user->setFlash('success' ,'Data telah disimpan!');
								$this->redirect(array('index'));
							}

				}catch(Exception $e){	
								$transaksi->rollback();
								Yii::app()->user->setFlash('success' ,'Data gagal disimpan!');
								$this->redirect(array('index'));
				}
						


			}

			$this->render('_form' , array('model' => $model));
		}

		public function actionUpdate($id)
		{
			$model = Beban::model()->findByPk($id);
			$db = Yii::app()->db;
			$transaksi = $db->beginTransaction();
			$model->tanggal = ar::getTanggalViewNoWaktu($model->tanggal , "-");
			if(isset($_POST['Beban']))
			{
				try
				{		$db->createCommand("DELETE FROM beban_detail WHERE beban_id = '$model->id'")->execute();
						$model->posted_by = 3;
						$model->attributes = $_POST['Beban'];
							if($model->save())
							{
								for($a=0;$a<=27;$a++)
								{
									$beban_perjam = $_POST['beban_perjam'][$a];
									$prakiraan_beban = $_POST['prakiraan_beban'][$a];
									$prakiraan_daya_mampu = $_POST['prakiraan_daya_mampu'][$a];
									(empty($beban_perjam)) ? $beban_perjam = 0 : $beban_perjam = $beban_perjam;
									(empty($prakiraan_beban)) ? $prakiraan_beban = 0 : $prakiraan_beban = $prakiraan_beban;
									(empty($prakiraan_daya_mampu)) ? $prakiraan_daya_mampu = 0 : $prakiraan_daya_mampu = $prakiraan_daya_mampu;
									
									$jam = $_POST['jam'][$a];
									$sql = "INSERT INTO beban_detail VALUES('','$model->id','$beban_perjam','$prakiraan_beban','$prakiraan_daya_mampu' , '$jam')";
									$db->createCommand($sql)->execute();
								}

								$max = $db->createCommand("SELECT beban_perjam AS max_beban ,jam FROM beban_detail WHERE beban_id = '$model->id' order by beban_perjam DESC LIMIT 1")->queryRow();
								$jam_max = $max['jam'];
								$jumlah_max = $max['max_beban'];
								$sqlUpdate = "UPDATE beban SET jam_max  = '$jam_max' , jumlah_max = '$jumlah_max' WHERE id = '$model->id'";
								$db->createCommand($sqlUpdate)->execute();
								$transaksi->commit();
								Yii::app()->user->setFlash('success' ,'Data telah diupdate!');
								$this->redirect(array('index'));
							}

				}catch(Exception $e){	
								$transaksi->rollback();
								Yii::app()->user->setFlash('success' ,'Data gagal diupdate!');
								$this->redirect(array('index'));
				}


			}

			$this->render('_form_update' , array('model' => $model , 'db' => $db));
		}


		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			$db = Yii::app()->db;
			$model = $this->loadModel($id);
			if($cek == $id)
			{
				try
				{
					
					$db->createCommand("DELETE FROM beban_detail WHERE beban_id = '$model->id'")->execute();
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}

		public function loadModel($param)
		{
			$model = Beban::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}
	}