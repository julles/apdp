<?php

/**
 * This is the model class for table "tegangan".
 *
 * The followings are the available columns in table 'tegangan':
 * @property integer $id
 * @property string $tanggal_full
 * @property integer $gardu_hubung_id
 * @property integer $busbar_id
 * @property string $j01_00
 * @property string $j02_00
 * @property string $j03_00
 * @property string $j04_00
 * @property string $j05_00
 * @property string $j06_00
 * @property string $j07_00
 * @property string $j08_00
 * @property string $j09_00
 * @property string $j10_00
 * @property string $j11_00
 * @property string $j12_00
 * @property string $j13_00
 * @property string $j14_00
 * @property string $j15_00
 * @property string $j16_00
 * @property string $j17_00
 * @property string $j18_00
 * @property string $j18_30
 * @property string $j19_00
 * @property string $j19_30
 * @property string $j20_00
 * @property string $j20_30
 * @property string $j21_00
 * @property string $j21_30
 * @property string $j22_00
 * @property string $j23_00
 * @property string $j24_00
 * @property string $date_post
 * @property integer $posted_by
 * @property string $hasil_rata
 */
class Tegangan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tegangan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('j24_00', 'required'),
			array('gardu_hubung_id, busbar_id, posted_by', 'numerical', 'integerOnly'=>true),
			array('j01_00, j02_00, j03_00, j04_00, j05_00, j06_00, j07_00, j08_00, j09_00, j10_00, j11_00, j12_00, j13_00, j14_00, j15_00, j16_00, j17_00, j18_00, j18_30, j19_00, j19_30, j20_00, j20_30, j21_00, j21_30, j22_00, j23_00, j24_00, hasil_rata', 'length', 'max'=>10),
			array('j01_00, j02_00, j03_00, j04_00, j05_00, j06_00, j07_00, j08_00, j09_00, j10_00, j11_00, j12_00, j13_00, j14_00, j15_00, j16_00, j17_00, j18_00, j18_30, j19_00, j19_30, j20_00, j20_30, j21_00, j21_30, j22_00, j23_00, j24_00 , tanggal_full,busbar_id,gardu_hubung_id', 'required'),
			
			array('tanggal_full, date_post', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tanggal_full, gardu_hubung_id, busbar_id, j01_00, j02_00, j03_00, j04_00, j05_00, j06_00, j07_00, j08_00, j09_00, j10_00, j11_00, j12_00, j13_00, j14_00, j15_00, j16_00, j17_00, j18_00, j18_30, j19_00, j19_30, j20_00, j20_30, j21_00, j21_30, j22_00, j23_00, j24_00, date_post, posted_by, hasil_rata', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO , 'User' , 'posted_by'),
			'busbar' => array(self::BELONGS_TO , 'MasterBusbar' , 'busbar_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal_full' => 'Tanggal Full',
			'gardu_hubung_id' => 'Gardu Hubung',
			'busbar_id' => 'Busbar',
			'j01_00' => 'J01 00',
			'j02_00' => 'J02 00',
			'j03_00' => 'J03 00',
			'j04_00' => 'J04 00',
			'j05_00' => 'J05 00',
			'j06_00' => 'J06 00',
			'j07_00' => 'J07 00',
			'j08_00' => 'J08 00',
			'j09_00' => 'J09 00',
			'j10_00' => 'J10 00',
			'j11_00' => 'J11 00',
			'j12_00' => 'J12 00',
			'j13_00' => 'J13 00',
			'j14_00' => 'J14 00',
			'j15_00' => 'J15 00',
			'j16_00' => 'J16 00',
			'j17_00' => 'J17 00',
			'j18_00' => 'J18 00',
			'j18_30' => 'J18 30',
			'j19_00' => 'J19 00',
			'j19_30' => 'J19 30',
			'j20_00' => 'J20 00',
			'j20_30' => 'J20 30',
			'j21_00' => 'J21 00',
			'j21_30' => 'J21 30',
			'j22_00' => 'J22 00',
			'j23_00' => 'J23 00',
			'j24_00' => 'J24 00',
			'date_post' => 'Date Post',
			'posted_by' => 'Posted By',
			'hasil_rata' => 'Hasil Rata',
		);
	}

	public function getRata($id)
	{
		$sql = "select (j01_00 +  j02_00 +  j03_00 +  j04_00 +  j05_00 +  j06_00 +  j07_00 +  j08_00 +  j09_00 +  j10_00 +  j11_00 +  j12_00 +  j13_00 +  j14_00 +  j15_00 +  j16_00 +  j17_00 +  j18_00 +  j18_30 +  j19_00 +  j19_30 +  j20_00 +  j20_30 +  j21_00 +  j21_30 +  j22_00 +  j23_00 +  j24_00) / 28 AS rata_rata
		  from tegangan where id = '$id'";
		 $result = Yii::app()->db->createCommand($sql)->queryScalar();
		 return $result;

	}


	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			
			$this->date_post = date("Y-m-d H:i:s");
			$this->posted_by = Yii::app()->session['tokenId'];
			return true;
		}
	}


	public function getDataJam($date , $gi , $busbar)
	{

		if(empty($gi) && empty($busbar))
		{
			$bool = "";
		}elseif(!empty($gi) && empty($busbar)){
			$bool = "AND gardu_hubung_id ='$gi'";
		}elseif(!empty($gi) && !empty($busbar)){
			$bool = "AND gardu_hubung_id ='$gi' AND busbar_id = '$busbar'";
		}

			 $q = "SELECT SUM(j01_00) AS j01_00,  
						   SUM(j02_00) AS j02_00,
						   SUM(j03_00) AS j03_00,
						   SUM(j04_00) AS j04_00,
						   SUM(j05_00) AS j05_00,
						   SUM(j06_00) AS j06_00,
						   SUM(j07_00) AS j07_00,
						   SUM(j08_00) AS j08_00,
						   SUM(j09_00) AS j09_00,
						   SUM(j10_00) AS j10_00,
						   SUM(j11_00) AS j11_00,
						   SUM(j12_00) AS j12_00,
						   SUM(j13_00) AS j13_00,
						   SUM(j14_00) AS j14_00,
						   SUM(j15_00) AS j15_00,
						   SUM(j16_00) AS j16_00,
						   SUM(j17_00) AS j17_00,
						   SUM(j18_00) AS j18_00,
						   SUM(j18_30) AS j18_30,
						   SUM(j19_00) AS j19_00,
						   SUM(j19_30) AS j19_30,
						   SUM(j20_00) AS j20_00,
						   SUM(j20_30) AS j20_30,
						   SUM(j21_00) AS j21_00,
						   SUM(j21_30) AS j21_30,
						   SUM(j22_00) AS j22_00,
						   SUM(j23_00) AS j23_00,
						   SUM(j24_00) AS j24_00
						 

				FROM tegangan
				WHERE tanggal_full = '$date' $bool
			";
			$sql = Yii::app()->db->createCommand($q)->queryRow();

			$hasil =  array(
									floatval($sql['j01_00']),
									floatval($sql['j02_00']),
									floatval($sql['j03_00']),
									floatval($sql['j04_00']),
									floatval($sql['j05_00']),
									floatval($sql['j06_00']),
									floatval($sql['j07_00']),
									floatval($sql['j08_00']),
									floatval($sql['j09_00']),
									floatval($sql['j10_00']),
									floatval($sql['j11_00']),
									floatval($sql['j12_00']),
									floatval($sql['j13_00']),
									floatval($sql['j14_00']),
									floatval($sql['j15_00']),
									floatval($sql['j16_00']),
									floatval($sql['j17_00']),
									floatval($sql['j18_00']),
									floatval($sql['j18_30']),
									floatval($sql['j19_00']),
									floatval($sql['j19_30']),
									floatval($sql['j20_00']),
									floatval($sql['j20_30']),
									floatval($sql['j21_00']),
									floatval($sql['j21_30']),
									floatval($sql['j22_00']),
									floatval($sql['j23_00']),
									floatval($sql['j24_00'])
					);
			
		return $hasil;
	}

	public function getDataHarian($month , $year ,  $gardu_hubung_id , $busbar_id)
	{
					$db = Yii::app()->db;
					$last = cal_days_in_month(CAL_GREGORIAN, date($month), date($year)); 
					$plus = 0;
					
					$where = "WHERE YEAR(tanggal_full) = $year AND MONTH(tanggal_full) = $month";

					if(empty($gardu_hubung_id) && empty($busbar_id))
					{
						$bool = $where;
					
					}elseif(!empty($gardu_hubung_id) && empty($busbar_id)){
					
						$bool = $where." AND gardu_hubung_id ='$gardu_hubung_id'";
					
					}elseif(!empty($gardu_hubung_id) && !empty($busbar_id)){
					
						$bool = $where." AND gardu_hubung_id ='$gardu_hubung_id' AND busbar_id = '$busbar_id'";
					}

					$labelGardu = $db->createCommand("SELECT gardu_hubung FROM master_gardu_hubung WHERE id = '$gardu_hubung_id'")->queryScalar();
					(empty($labelGardu)) ? $labelGardu = 'All' : $labelGardu = $labelGardu; 

					$labelBusbar = $db->createCommand("SELECT busbar FROM master_busbar WHERE id = '$busbar_id'")->queryScalar();
					(empty($labelBusbar)) ? $labelBusbar = 'All' : $labelBusbar = $labelBusbar; 


					for($a=1;$a<=$last;$a++)
					{
						$count = $db->createCommand("SELECT COUNT(id) AS nilai FROM tegangan $bool AND DAY(tanggal_full) = $a")->queryScalar();
						$row = $db->createCommand("SELECT id ,  SUM(hasil_rata) AS hasil_rata FROM tegangan $bool AND DAY(tanggal_full) = $a")->queryRow();
						(empty($row['hasil_rata'])) ? $hasilnya = 0 : $hasilnya =  $row['hasil_rata'] / $count;
						
						$out[]= array('y' => floatval($hasilnya) , 'gardu' => $labelGardu , 'busbar' => $labelBusbar);



					}
					return($out);

	}




	public function getDataBulanan($year , $gardu_hubung_id , $busbar_id)
	{
		$db = Yii::app()->db;
		$where = "WHERE YEAR(tanggal_full) = $year ";

		if(empty($gardu_hubung_id) && empty($busbar_id))
		{
			$bool = $where;
		
		}elseif(!empty($gardu_hubung_id) && empty($busbar_id)){
		
			$bool = $where." AND gardu_hubung_id ='$gardu_hubung_id'";
		
		}elseif(!empty($gardu_hubung_id) && !empty($busbar_id)){
		
			$bool = $where." AND gardu_hubung_id ='$gardu_hubung_id' AND busbar_id = '$busbar_id'";
		}

		$labelGardu = $db->createCommand("SELECT gardu_hubung FROM master_gardu_hubung WHERE id = '$gardu_hubung_id'")->queryScalar();
		(empty($labelGardu)) ? $labelGardu = 'All' : $labelGardu = $labelGardu; 

		$labelBusbar = $db->createCommand("SELECT busbar FROM master_busbar WHERE id = '$busbar_id'")->queryScalar();
		(empty($labelBusbar)) ? $labelBusbar = 'All' : $labelBusbar = $labelBusbar; 


		for($a=1;$a<=12;$a++)
		{
			$count = $db->createCommand("SELECT COUNT(id) AS nilai FROM tegangan $bool AND MONTH(tanggal_full) = $a")->queryScalar();
			$row = $db->createCommand("SELECT id ,  SUM(hasil_rata) AS hasil_rata FROM tegangan $bool AND MONTH(tanggal_full) = $a")->queryRow();
			(empty($row['hasil_rata'])) ? $hasilnya = 0 : $hasilnya =  $row['hasil_rata'] / $count;
			
			$out[]= array('y' => floatval($hasilnya) , 'gardu' => $labelGardu , 'busbar' => $labelBusbar);


		}

		return $out;
	}


	public function getDataTahunan($awal , $akhir , $gardu_hubung_id , $busbar_id)
	{
		$db = Yii::app()->db;
		if(empty($gardu_hubung_id) && empty($busbar_id))
		{
			$bool = '';
		
		}elseif(!empty($gardu_hubung_id) && empty($busbar_id)){
		
			$bool = " AND gardu_hubung_id ='$gardu_hubung_id'";
		
		}elseif(!empty($gardu_hubung_id) && !empty($busbar_id)){
		
			$bool = " AND gardu_hubung_id ='$gardu_hubung_id' AND busbar_id = '$busbar_id'";
		}

		$labelGardu = $db->createCommand("SELECT gardu_hubung FROM master_gardu_hubung WHERE id = '$gardu_hubung_id'")->queryScalar();
		(empty($labelGardu)) ? $labelGardu = 'All' : $labelGardu = $labelGardu; 

		$labelBusbar = $db->createCommand("SELECT busbar FROM master_busbar WHERE id = '$busbar_id'")->queryScalar();
		(empty($labelBusbar)) ? $labelBusbar = 'All' : $labelBusbar = $labelBusbar; 

		for($a=$awal;$a<=$akhir;$a++)
		{
			$count = $db->createCommand("SELECT COUNT(id) AS nilai FROM tegangan  WHERE YEAR(tanggal_full) = $a $bool")->queryScalar();
			$row = $db->createCommand("SELECT id ,  SUM(hasil_rata) AS hasil_rata FROM tegangan  WHERE YEAR(tanggal_full) = $a $bool")->queryRow();
			(empty($row['hasil_rata'])) ? $hasilnya = 0 : $hasilnya =  $row['hasil_rata'] / $count;
			
			$out[]= array('y' => floatval($hasilnya) , 'gardu' => $labelGardu , 'busbar' => $labelBusbar);
		}

		return $out;

	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tanggal_full',$this->tanggal_full,true);
		$criteria->compare('gardu_hubung_id',$this->gardu_hubung_id);
		$criteria->compare('busbar_id',$this->busbar_id);
		$criteria->compare('j01_00',$this->j01_00,true);
		$criteria->compare('j02_00',$this->j02_00,true);
		$criteria->compare('j03_00',$this->j03_00,true);
		$criteria->compare('j04_00',$this->j04_00,true);
		$criteria->compare('j05_00',$this->j05_00,true);
		$criteria->compare('j06_00',$this->j06_00,true);
		$criteria->compare('j07_00',$this->j07_00,true);
		$criteria->compare('j08_00',$this->j08_00,true);
		$criteria->compare('j09_00',$this->j09_00,true);
		$criteria->compare('j10_00',$this->j10_00,true);
		$criteria->compare('j11_00',$this->j11_00,true);
		$criteria->compare('j12_00',$this->j12_00,true);
		$criteria->compare('j13_00',$this->j13_00,true);
		$criteria->compare('j14_00',$this->j14_00,true);
		$criteria->compare('j15_00',$this->j15_00,true);
		$criteria->compare('j16_00',$this->j16_00,true);
		$criteria->compare('j17_00',$this->j17_00,true);
		$criteria->compare('j18_00',$this->j18_00,true);
		$criteria->compare('j18_30',$this->j18_30,true);
		$criteria->compare('j19_00',$this->j19_00,true);
		$criteria->compare('j19_30',$this->j19_30,true);
		$criteria->compare('j20_00',$this->j20_00,true);
		$criteria->compare('j20_30',$this->j20_30,true);
		$criteria->compare('j21_00',$this->j21_00,true);
		$criteria->compare('j21_30',$this->j21_30,true);
		$criteria->compare('j22_00',$this->j22_00,true);
		$criteria->compare('j23_00',$this->j23_00,true);
		$criteria->compare('j24_00',$this->j24_00,true);
		$criteria->compare('date_post',$this->date_post,true);
		$criteria->compare('posted_by',$this->posted_by);
		$criteria->compare('hasil_rata',$this->hasil_rata,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tegangan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
