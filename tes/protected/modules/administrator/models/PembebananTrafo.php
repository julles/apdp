<?php

/**
 * This is the model class for table "pembebanan_trafo".
 *
 * The followings are the available columns in table 'pembebanan_trafo':
 * @property integer $id
 * @property string $tanggal_full
 * @property integer $trafo_id
 * @property string $j01_00
 * @property string $j02_00
 * @property string $j03_00
 * @property string $j04_00
 * @property string $j05_00
 * @property string $j06_00
 * @property string $j07_00
 * @property string $j08_00
 * @property string $j09_00
 * @property string $j10_00
 * @property string $j11_00
 * @property string $j12_00
 * @property string $j13_00
 * @property string $j14_00
 * @property string $j15_00
 * @property string $j16_00
 * @property string $j17_00
 * @property string $j18_00
 * @property string $j18_30
 * @property string $j19_00
 * @property string $j19_30
 * @property string $j20_00
 * @property string $j20_30
 * @property string $j21_00
 * @property string $j21_30
 * @property string $j22_00
 * @property string $j23_00
 * @property string $j24_00
 * @property string $date_post
 * @property integer $posted_by
 * @property string $jam_max
 * @property string $jumlah_max
 */
class PembebananTrafo extends CActiveRecord
{
	//public $gardu_induk_id;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pembebanan_trafo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('trafo_id,gardu_induk_id, posted_by', 'numerical', 'integerOnly'=>true),
			array('j01_00, j02_00, j03_00, j04_00, j05_00, j06_00, j07_00, j08_00, j09_00, j10_00, j11_00, j12_00, j13_00, j14_00, j15_00, j16_00, j17_00, j18_00, j18_30, j19_00, j19_30, j20_00, j20_30, j21_00, j21_30, j22_00, j23_00, j24_00, jam_max, jumlah_max', 'length', 'max'=>10),
			array('j01_00, j02_00, j03_00, j04_00, j05_00, j06_00, j07_00, j08_00, j09_00, j10_00, j11_00, j12_00, j13_00, j14_00, j15_00, j16_00, j17_00, j18_00, j18_30, j19_00, j19_30, j20_00, j20_30, j21_00, j21_30, j22_00, j23_00, j24_00 , tanggal_full,trafo_id,gardu_induk_id', 'required'),
			//array('tanggal_full' ,  'unique'),
			array('pembanding' , 'length' , 'max' => 10),
			array('tanggal_full, date_post', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tanggal_full, trafo_id, j01_00, j02_00, j03_00, j04_00, j05_00, j06_00, j07_00, j08_00, j09_00, j10_00, j11_00, j12_00, j13_00, j14_00, j15_00, j16_00, j17_00, j18_00, j18_30, j19_00, j19_30, j20_00, j20_30, j21_00, j21_30, j22_00, j23_00, j24_00, date_post, posted_by, jam_max, jumlah_max', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User','posted_by'),
			'trafo' => array(self::BELONGS_TO,'MasterTrafo','trafo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal_full' => 'Tanggal Full',
			'trafo_id' => 'Trafo',
			'j01_00' => '01.00',
			'j02_00' => '02.00',
			'j03_00' => '03.00',
			'j04_00' => '04.00',
			'j05_00' => '05.00',
			'j06_00' => '06.00',
			'j07_00' => '07.00',
			'j08_00' => '08.00',
			'j09_00' => '09.00',
			'j10_00' => '10.00',
			'j11_00' => '11.00',
			'j12_00' => '12.00',
			'j13_00' => '13.00',
			'j14_00' => '14.00',
			'j15_00' => '15.00',
			'j16_00' => '16.00',
			'j17_00' => '17.00',
			'j18_00' => '18.00',
			'j18_30' => '18.30',
			'j19_00' => '19.00',
			'j19_30' => '19.30',
			'j20_00' => '20.00',
			'j20_30' => '20.30',
			'j21_00' => '21.00',
			'j21_30' => '21.30',
			'j22_00' => '22.00',
			'j23_00' => '23.00',
			'j24_00' => '24.00',
			'date_post' => 'Date Post',
			'posted_by' => 'Posted By',
			'jam_max' => 'Jam Max',
			'jumlah_max' => 'Jumlah Max',
			'gardu_induk_id' => 'Gardu Induk'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */


	public function getDataJam($date , $gi , $trafo)
	{

		if(empty($gi) && empty($trafo))
		{
			$bool = "";
		}elseif(!empty($gi) && empty($trafo)){
			$bool = "AND gardu_induk_id ='$gi'";
		}elseif(!empty($gi) && !empty($trafo)){
			$bool = "AND gardu_induk_id ='$gi' AND trafo_id = '$trafo'";
		}

			$q = "SELECT SUM(j01_00) AS j01_00,  
						   SUM(j02_00) AS j02_00,
						   SUM(j03_00) AS j03_00,
						   SUM(j04_00) AS j04_00,
						   SUM(j05_00) AS j05_00,
						   SUM(j06_00) AS j06_00,
						   SUM(j07_00) AS j07_00,
						   SUM(j08_00) AS j08_00,
						   SUM(j09_00) AS j09_00,
						   SUM(j10_00) AS j10_00,
						   SUM(j11_00) AS j11_00,
						   SUM(j12_00) AS j12_00,
						   SUM(j13_00) AS j13_00,
						   SUM(j14_00) AS j14_00,
						   SUM(j15_00) AS j15_00,
						   SUM(j16_00) AS j16_00,
						   SUM(j17_00) AS j17_00,
						   SUM(j18_00) AS j18_00,
						   SUM(j18_30) AS j18_30,
						   SUM(j19_00) AS j19_00,
						   SUM(j19_30) AS j19_30,
						   SUM(j20_00) AS j20_00,
						   SUM(j20_30) AS j20_30,
						   SUM(j21_00) AS j21_00,
						   SUM(j21_30) AS j21_30,
						   SUM(j22_00) AS j22_00,
						   SUM(j23_00) AS j23_00,
						   SUM(j24_00) AS j24_00
						 

				FROM pembebanan_trafo
				WHERE tanggal_full = '$date' $bool
			";
			$sql = Yii::app()->db->createCommand($q)->queryRow();

			$hasil =  array(
									floatval($sql['j01_00']),
									floatval($sql['j02_00']),
									floatval($sql['j03_00']),
									floatval($sql['j04_00']),
									floatval($sql['j05_00']),
									floatval($sql['j06_00']),
									floatval($sql['j07_00']),
									floatval($sql['j08_00']),
									floatval($sql['j09_00']),
									floatval($sql['j10_00']),
									floatval($sql['j11_00']),
									floatval($sql['j12_00']),
									floatval($sql['j13_00']),
									floatval($sql['j14_00']),
									floatval($sql['j15_00']),
									floatval($sql['j16_00']),
									floatval($sql['j17_00']),
									floatval($sql['j18_00']),
									floatval($sql['j18_30']),
									floatval($sql['j19_00']),
									floatval($sql['j19_30']),
									floatval($sql['j20_00']),
									floatval($sql['j20_30']),
									floatval($sql['j21_00']),
									floatval($sql['j21_30']),
									floatval($sql['j22_00']),
									floatval($sql['j23_00']),
									floatval($sql['j24_00'])
					);
			
		return $hasil;
	}


	/*public function getDataJam($date)
	{
		$sql = Yii::app()->db
		->createCommand()
		->select('*')
		->from('pembebanan_trafo')
		->where('tanggal_full=:tgl' , array(':tgl' => $date))
		->queryRow();
	
	
		return array(
			floatval($sql['j01_00']),
			floatval($sql['j02_00']),
			floatval($sql['j03_00']),
			floatval($sql['j04_00']),
			floatval($sql['j05_00']),
			floatval($sql['j06_00']),
			floatval($sql['j07_00']),
			floatval($sql['j08_00']),
			floatval($sql['j09_00']),
			floatval($sql['j10_00']),
			floatval($sql['j11_00']),
			floatval($sql['j12_00']),
			floatval($sql['j13_00']),
			floatval($sql['j14_00']),
			floatval($sql['j15_00']),
			floatval($sql['j16_00']),
			floatval($sql['j17_00']),
			floatval($sql['j18_00']),
			floatval($sql['j18_30']),
			floatval($sql['j19_00']),
			floatval($sql['j19_30']),
			floatval($sql['j20_00']),
			floatval($sql['j20_30']),
			floatval($sql['j21_00']),
			floatval($sql['j21_30']),
			floatval($sql['j22_00']),
			floatval($sql['j23_00']),
			floatval($sql['j24_00'])
		);
	}*/


	


	public function getDataHarian($month , $year ,  $gardu_induk_id , $trafo_id)
	{
					$db = Yii::app()->db;
					$last = cal_days_in_month(CAL_GREGORIAN, date($month), date($year)); 
					$plus = 0;
					
					$where = "WHERE YEAR(tanggal_full) = $year AND MONTH(tanggal_full) = $month";

					if(empty($gardu_induk_id) && empty($trafo_id))
					{
						$bool = $where;
					
					}elseif(!empty($gardu_induk_id) && empty($trafo_id)){
					
						$bool = $where." AND gardu_induk_id ='$gardu_induk_id'";
					
					}elseif(!empty($gardu_induk_id) && !empty($trafo_id)){
					
						$bool = $where." AND gardu_induk_id ='$gardu_induk_id' AND trafo_id = '$trafo_id'";
					}

					$labelGardu = $db->createCommand("SELECT gardu_induk FROM master_gardu_induk WHERE id = '$gardu_induk_id'")->queryScalar();
					(empty($labelGardu)) ? $labelGardu = 'All' : $labelGardu = $labelGardu; 

					$labelTrafo = $db->createCommand("SELECT trafo FROM master_trafo WHERE id = '$trafo_id'")->queryScalar();
					(empty($labelTrafo)) ? $labelTrafo = 'All' : $labelTrafo = $labelTrafo; 


					for($a=1;$a<=$last;$a++)
					{
						$pembanding = $db->createCommand("SELECT id ,  pembanding FROM pembebanan_trafo $bool AND DAY(tanggal_full) = $a")->query();
						$hasil = 0;
						while(($row = $pembanding->read()) !== false)
						{
							$max = $db->createCommand("SELECT $row[pembanding] FROM pembebanan_trafo WHERE id = '$row[id]'")->queryScalar();
							
							$hasil = $hasil + $max;
						}
						$out[]= array('y' => $hasil , 'gardu' => $labelGardu , 'trafo' => $labelTrafo);



					}
					return $out;

	}

	public function getDataBulanan($year , $gardu_induk_id , $trafo_id)
	{
		$db = Yii::app()->db;
		$where = "WHERE YEAR(tanggal_full) = $year ";

		if(empty($gardu_induk_id) && empty($trafo_id))
		{
			$bool = $where;
		
		}elseif(!empty($gardu_induk_id) && empty($trafo_id)){
		
			$bool = $where." AND gardu_induk_id ='$gardu_induk_id'";
		
		}elseif(!empty($gardu_induk_id) && !empty($trafo_id)){
		
			$bool = $where." AND gardu_induk_id ='$gardu_induk_id' AND trafo_id = '$trafo_id'";
		}

		$labelGardu = $db->createCommand("SELECT gardu_induk FROM master_gardu_induk WHERE id = '$gardu_induk_id'")->queryScalar();
		(empty($labelGardu)) ? $labelGardu = 'All' : $labelGardu = $labelGardu; 

		$labelTrafo = $db->createCommand("SELECT trafo FROM master_trafo WHERE id = '$trafo_id'")->queryScalar();
		(empty($labelTrafo)) ? $labelTrafo = 'All' : $labelTrafo = $labelTrafo; 

		for($a=1;$a<=12;$a++)
		{
				$sql = "SELECT id , pembanding FROM pembebanan_trafo $bool AND MONTH(tanggal_full) = $a";
				$pembading = $db->createCommand($sql)->query();
				$hasil = 0;
				while(($row = $pembading->read()) !== false)
				{
					$max = $db->createCommand("SELECT $row[pembanding] FROM pembebanan_trafo WHERE id = '$row[id]'")->queryScalar();
					$hasil = $hasil + $max;
				}
				$out[]= array('y' => $hasil , 'gardu' => $labelGardu , 'trafo' => $labelTrafo);

		}

		return $out;
	}


	public function getDataTahunan($awal , $akhir , $gardu_induk_id , $trafo_id)
	{
		$db = Yii::app()->db;
		if(empty($gardu_induk_id) && empty($trafo_id))
		{
			$bool = '';
		
		}elseif(!empty($gardu_induk_id) && empty($trafo_id)){
		
			$bool = " AND gardu_induk_id ='$gardu_induk_id'";
		
		}elseif(!empty($gardu_induk_id) && !empty($trafo_id)){
		
			$bool = " AND gardu_induk_id ='$gardu_induk_id' AND trafo_id = '$trafo_id'";
		}

		$labelGardu = $db->createCommand("SELECT gardu_induk FROM master_gardu_induk WHERE id = '$gardu_induk_id'")->queryScalar();
		(empty($labelGardu)) ? $labelGardu = 'All' : $labelGardu = $labelGardu; 

		$labelTrafo = $db->createCommand("SELECT trafo FROM master_trafo WHERE id = '$trafo_id'")->queryScalar();
		(empty($labelTrafo)) ? $labelTrafo = 'All' : $labelTrafo = $labelTrafo; 

		for($a=$awal;$a<=$akhir;$a++)
		{
			$sql = "SELECT id , pembanding FROM pembebanan_trafo  WHERE YEAR(tanggal_full) = $a $bool";
				$pembading = $db->createCommand($sql)->query();
				$hasil = 0;
				while(($row = $pembading->read()) !== false)
				{
					$max = $db->createCommand("SELECT $row[pembanding] FROM pembebanan_trafo WHERE id = '$row[id]'")->queryScalar();
					$hasil = $hasil + $max;
				}
				$out[]= array('y' => $hasil , 'gardu' => $labelGardu , 'trafo' => $labelTrafo);
		}

		return $out;

	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tanggal_full',$this->tanggal_full,true);
		$criteria->compare('trafo_id',$this->trafo_id);
		$criteria->compare('j01_00',$this->j01_00,true);
		$criteria->compare('j02_00',$this->j02_00,true);
		$criteria->compare('j03_00',$this->j03_00,true);
		$criteria->compare('j04_00',$this->j04_00,true);
		$criteria->compare('j05_00',$this->j05_00,true);
		$criteria->compare('j06_00',$this->j06_00,true);
		$criteria->compare('j07_00',$this->j07_00,true);
		$criteria->compare('j08_00',$this->j08_00,true);
		$criteria->compare('j09_00',$this->j09_00,true);
		$criteria->compare('j10_00',$this->j10_00,true);
		$criteria->compare('j11_00',$this->j11_00,true);
		$criteria->compare('j12_00',$this->j12_00,true);
		$criteria->compare('j13_00',$this->j13_00,true);
		$criteria->compare('j14_00',$this->j14_00,true);
		$criteria->compare('j15_00',$this->j15_00,true);
		$criteria->compare('j16_00',$this->j16_00,true);
		$criteria->compare('j17_00',$this->j17_00,true);
		$criteria->compare('j18_00',$this->j18_00,true);
		$criteria->compare('j18_30',$this->j18_30,true);
		$criteria->compare('j19_00',$this->j19_00,true);
		$criteria->compare('j19_30',$this->j19_30,true);
		$criteria->compare('j20_00',$this->j20_00,true);
		$criteria->compare('j20_30',$this->j20_30,true);
		$criteria->compare('j21_00',$this->j21_00,true);
		$criteria->compare('j21_30',$this->j21_30,true);
		$criteria->compare('j22_00',$this->j22_00,true);
		$criteria->compare('j23_00',$this->j23_00,true);
		$criteria->compare('j24_00',$this->j24_00,true);
		$criteria->compare('date_post',$this->date_post,true);
		$criteria->compare('posted_by',$this->posted_by);
		$criteria->compare('jam_max',$this->jam_max,true);
		$criteria->compare('jumlah_max',$this->jumlah_max,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/*public function rep_min($params, $bol = '')
	{
		$min = substr($params , 0 , 1);
		if($min == '-')
		{
			$return = str_replace('-', '', $params);
		}else{
			$return = $params;
		}

		return $return;
	}*/

	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			
			$this->date_post = date("Y-m-d H:i:s");
			$this->tanggal_full = date("Y-m-d" , strtotime($this->tanggal_full));
			$this->posted_by = Yii::app()->session['tokenId'];
			return true;
		}
	}
	
	public function greatest($id , $param)
	{
			$sql = "SELECT `column`, column_value
			FROM (
					SELECT id, 'j01_00' as `column`, abs(j01_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j02_00' as `column`,abs(j02_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j03_00' as `column`,abs(j03_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j04_00' as `column`,abs(j04_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j05_00' as `column`,abs(j05_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j06_00' as `column`,abs(j06_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j07_00' as `column`,abs(j07_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j08_00' as `column`,abs(j08_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j09_00' as `column`,abs(j09_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j10_00' as `column`,abs(j10_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j11_00' as `column`,abs(j11_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j12_00' as `column`,abs(j12_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j13_00' as `column`,abs(j13_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j14_00' as `column`,abs(j14_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j15_00' as `column`,abs(j15_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j16_00' as `column`,abs(j16_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j17_00' as `column`,abs(j17_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j18_00' as `column`,abs(j18_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j18_30' as `column`,abs(j18_30) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j19_00' as `column`,abs(j19_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j19_30' as `column`,abs(j19_30) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j20_00' as `column`,abs(j20_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j20_30' as `column`,abs(j20_30) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j21_00' as `column`,abs(j21_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j21_30' as `column`,abs(j21_30) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j22_00' as `column`,abs(j22_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j23_00' as `column`,abs(j23_00) as column_value FROM pembebanan_trafo
					UNION
					SELECT id, 'j24_00' as `column`,abs(j24_00) as column_value FROM pembebanan_trafo
					
			) pivot

			where column_value in (SELECT GREATEST(abs(j01_00), abs(j02_00), abs(j03_00), abs(j04_00), abs(j05_00), abs(j06_00), abs(j07_00), abs(j08_00), abs(j09_00), abs(j10_00), abs(j11_00), abs(j12_00), abs(j13_00), abs(j14_00), abs(j15_00), abs(j16_00), abs(j17_00), abs(j18_00), abs(j18_30), abs(j19_00), abs(j19_30), abs(j20_00), abs(j20_30), abs(j21_00), abs(j21_30), abs(j22_00), abs(j23_00), abs(j24_00)) from pembebanan_trafo)
			AND id = $id
			";
			
			$row = Yii::app()->db
			->createCommand($sql)
			->queryRow();
			
			if($param == 'jam')
			{
				
				$replace1 = str_replace('j' ,'' , $row['column']);
				$replace2 = str_replace('_',':' ,  $replace1);
				return $replace2;
			
			}elseif($param == 'jumlah'){
				return $row['column_value'];
				//return $sql;

			}elseif($param == 'field'){
				return $row['column'];
			}
	}
	
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PembebananTrafo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
