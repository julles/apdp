<?php

/**
 * This is the model class for table "trans_pembebanan_trafo_detail".
 *
 * The followings are the available columns in table 'trans_pembebanan_trafo_detail':
 * @property integer $id
 * @property integer $pembebanan_trafo_id
 * @property string $nilai
 * @property string $jam
 */
class TransPembebananTrafoDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trans_pembebanan_trafo_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pembebanan_trafo_id', 'numerical', 'integerOnly'=>true),
			array('nilai, jam', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pembebanan_trafo_id, nilai, jam', 'safe', 'on'=>'search'),
		);
	}


	public function listJam($tanggal)
	{
		$db = Yii::app()->db;
		$sql = "SELECT jam FROM trans_pembebanan_trafo_detail INNER JOIN trans_pembebanan_trafo ON trans_pembebanan_trafo.id = trans_pembebanan_trafo_detail.pembebanan_trafo_id WHERE tanggal = '$tanggal'";
		$cek = $db->createCommand($sql)->queryScalar();
		if(!empty($cek))
		{
			$query = $db->createCommand($sql)->query();
			while(($row = $query->read()) !== false)
			{
				$hasil[] = $row['jam'];
			}

		}else{
			$hasil = array();
		}
		return $hasil;
	}

	public function valueJam($tanggal , $gi , $trafo)
	{
		$db = Yii::app()->db;
		$sql = "SELECT jam FROM trans_pembebanan_trafo_detail INNER JOIN trans_pembebanan_trafo ON trans_pembebanan_trafo.id = trans_pembebanan_trafo_detail.pembebanan_trafo_id WHERE tanggal = '$tanggal' AND gardu_induk_id = '$gi' AND trafo_id = '$trafo'";
		$cek = $db->createCommand($sql)->queryScalar();
		if(!empty($cek))
		{	
			$query = $db->createCommand($sql)->query();
			while(($row = $query->read()) !== false)
			{
				 $sql = "SELECT nilai FROM trans_pembebanan_trafo_detail INNER JOIN trans_pembebanan_trafo ON trans_pembebanan_trafo.id = trans_pembebanan_trafo_detail.pembebanan_trafo_id WHERE tanggal = '$tanggal' AND jam = '$row[jam]' AND gardu_induk_id = '$gi' AND trafo_id = '$trafo'";
				$rows = $db->createCommand($sql)->queryRow();
				if($rows['nilai'] == 0)
				{
					$hasil[] = null;
				}else{
					$hasil[] = floatval($rows['nilai']);
				}	
					
			}
		}else{
			$hasil = array();
		}
		return $hasil;
		
	}

	public function valueJamKurva($tanggal)
	{
		$db = Yii::app()->db;
		$sqlMaster = $db->createCommand("select id , trafo FROM master_trafo")->queryAll();
		$no=0;
		$hasil = array();
		foreach($sqlMaster as $rMaster)
		{
			$no++;

					$sql = "SELECT jam , nilai FROM trans_pembebanan_trafo_detail INNER JOIN 
					trans_pembebanan_trafo ON trans_pembebanan_trafo.id = trans_pembebanan_trafo_detail.pembebanan_trafo_id 
					WHERE tanggal = '$tanggal' AND trafo_id = '$rMaster[id]'";
				
					$query = $db->createCommand($sql)->query();
					$arr_nilai = array();
					while(($row = $query->read()) !== false)
					{
						if($row['nilai'] == 0)
						{
							$arr_nilai[] = null;
						}else{
							$arr_nilai[] = floatval($row['nilai']);
						}	
							

					}
				
					$hasil[] = array('name' => $rMaster['trafo'] , 'data' => $arr_nilai , 'lineWidth' => 3);
				
				
		}

		 return $hasil;
	}

	public function getDataHarian($tanggalAkhir)
	{
		for($a=1;$a<=$tanggalAkhir;$a++)
		{
			$tgl[] = $a;
		}
		return $tgl;
	}


	public function getDataHarianValue($month , $year , $last , $gi , $trafo)
	{
		$db = Yii::app()->db;
		for($a=1;$a<=$last;$a++)
		{
			$qw = "SELECT jumlah_max , tanggal , jam_max FROM trans_pembebanan_trafo WHERE tanggal = '$year-$month-$a' AND gardu_induk_id = '$gi' AND trafo_id = '$trafo' ORDER BY abs(jumlah_max) DESC LIMIT 1";
			$exec = $db->createCommand($qw)->queryRow();
			//$tanggal = ar::formatWaktu($exec['tanggal'] ,"long" ,"");
			$hasil[] = array('y' => floatval($exec['jumlah_max']) , 'jam' => $exec['jam_max']); 
		}
		return $hasil;
	}


	public function getDataBulananValue($year , $gi , $trafo)
	{
		$db = Yii::app()->db;
		for($a=1;$a<=12;$a++)
		{
		    $qw = "SELECT jumlah_max ,  jam_max , tanggal FROM trans_pembebanan_trafo WHERE YEAR(tanggal) = '$year' AND MONTH(tanggal) = '$a' AND gardu_induk_id = '$gi' AND trafo_id = '$trafo' ORDER BY abs(jumlah_max) DESC LIMIT 1 ";
			$exec = $db->createCommand($qw)->queryRow();
			$hasil[] = array('y' => floatval($exec['jumlah_max']) , 'jam' => $exec['jam_max'] ,'tanggal' => ar::formatWaktu($exec['tanggal'] , "long" ,"") ); 
		}

		return $hasil;
	}

	public function getDataTahunanValue($awal,$akhir , $gi , $trafo)
	{
		$db = Yii::app()->db;
		for($a=$awal;$a<=$akhir;$a++)
		{
			$qw = "SELECT jumlah_max ,  jam_max , tanggal FROM trans_pembebanan_trafo WHERE YEAR(tanggal) = '$a' AND gardu_induk_id = '$gi' AND trafo_id = '$trafo' ORDER BY abs(jumlah_max) DESC LIMIT 1 ";
			$exec = $db->createCommand($qw)->queryRow();
			$hasil[] = array('y' => floatval($exec['jumlah_max']) , 'jam' => $exec['jam_max']  ,'tanggal' => ar::formatWaktu($exec['tanggal'] , "long" ,"") ); 
		}
		
		
		return $hasil;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pembebanan_trafo_id' => 'Pembebanan Trafo',
			'nilai' => 'Nilai',
			'jam' => 'Jam',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pembebanan_trafo_id',$this->pembebanan_trafo_id);
		$criteria->compare('nilai',$this->nilai,true);
		$criteria->compare('jam',$this->jam,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TransPembebananTrafoDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
