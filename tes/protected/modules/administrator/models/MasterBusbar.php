<?php

/**
 * This is the model class for table "master_busbar".
 *
 * The followings are the available columns in table 'master_busbar':
 * @property integer $id
 * @property integer $gardu_hubung_id
 * @property string $busbar
 * @property string $date_post
 * @property integer $posted_by
 *
 * The followings are the available model relations:
 * @property MasterGarduHubung $garduHubung
 */
class MasterBusbar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'master_busbar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gardu_hubung_id, posted_by', 'numerical', 'integerOnly'=>true),
			array('busbar', 'length', 'max'=>100),
			array('date_post', 'safe'),
			array('gardu_hubung_id , busbar' , 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, gardu_hubung_id, busbar, date_post, posted_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'garduHubung' => array(self::BELONGS_TO, 'MasterGarduHubung', 'gardu_hubung_id'),
			'user' => array(self::BELONGS_TO , 'User' , 'posted_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'gardu_hubung_id' => 'Gardu Hubung',
			'busbar' => 'Busbar',
			'date_post' => 'Date Post',
			'posted_by' => 'Posted By',
		);
	}

	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->date_post = date("Y-m-d");
			$this->posted_by = Yii::app()->session['tokenId'];
			return true;
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('gardu_hubung_id',$this->gardu_hubung_id);
		$criteria->compare('busbar',$this->busbar,true);
		$criteria->compare('date_post',$this->date_post,true);
		$criteria->compare('posted_by',$this->posted_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterBusbar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
