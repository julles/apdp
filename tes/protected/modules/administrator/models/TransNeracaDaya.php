<?php

/**
 * This is the model class for table "trans_neraca_daya".
 *
 * The followings are the available columns in table 'trans_neraca_daya':
 * @property integer $id
 * @property string $tanggal
 * @property string $terpasang
 * @property string $po
 * @property string $fo
 * @property string $der_pln
 * @property string $der_rental
 * @property string $derating
 * @property string $beban_puncak
 * @property string $daya_mampu
 * @property string $dm_pln
 * @property string $dm_rental
 * @property string $beban_terlayani
 * @property string $cad_ops
 * @property string $padam
 * @property string $kondisi
 * @property integer $posted_by
 * @property string $date_post
 */
class TransNeracaDaya extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trans_neraca_daya';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('posted_by', 'numerical', 'integerOnly'=>true),
			array('terpasang, po, fo, der_pln, fdr, der_rental, derating, beban_puncak, daya_mampu, dm_pln, dm_rental, beban_terlayani, cad_ops, padam ,  mo', 'length', 'max'=>10),
			array('kondisi', 'length', 'max'=>1),
			array('tanggal, date_post', 'safe'),
			array('tanggal' , 'unique'),
			array('tanggal' , 'required'),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tanggal, terpasang, po, fo, der_pln, der_rental, derating, beban_puncak, daya_mampu, dm_pln, dm_rental, beban_terlayani, cad_ops, padam, kondisi, posted_by, date_post', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	public function beforeSave()
	{
		
		if(parent::beforeSave())
		{
			$hasilTanggal = date("Y-m-d" ,  strtotime($this->tanggal));
			if($this->isNewRecord)
			{
				
				$search = Yii::app()->db->createCommand("SELECT tanggal FROM trans_neraca_daya WHERE tanggal = '$hasilTanggal'")->queryScalar();
				if(empty($search))
				{	
					$this->date_post = date("Y-m-d H:i:s");
					$this->posted_by = Yii::app()->session['tokenId'];
					$this->tanggal = $hasilTanggal;
					return true;
				}else{
					$this->addError("tanggal" ,"Tanggal Sudah Di input sebelumnya");
				}

			}else{
					$this->date_post = date("Y-m-d H:i:s");
					$this->posted_by = Yii::app()->session['tokenId'];
					$this->tanggal = $hasilTanggal;
					return true;
			}	
		}
	}

	public function getDataHarian($tanggalAkhir)
	{
		for($a=1;$a<=$tanggalAkhir;$a++)
		{
			$tgl[] = $a;
		}
		return $tgl;
	}

	public function getDataHarianValue($month , $year , $last , $field)
	{
		$db = Yii::app()->db;
		for($a=1;$a<=$last;$a++)
		{
			$qw = "SELECT $field FROM trans_neraca_daya WHERE tanggal = '$year-$month-$a'";
			$exec = floatval($db->createCommand($qw)->queryScalar());
			//$tanggal = ar::formatWaktu($exec['tanggal'] ,"long" ,"");
			$hasil[] = $exec; 
		}
		return $hasil;
	}

	public function getDataBulananValue($year , $field)
	{
		for($a=1;$a<=12;$a++)
		{
			$sql="SELECT $field FROM trans_neraca_daya  WHERE beban_puncak IN(SELECT MAX(beban_puncak) FROM beban_puncak WHERE MONTH(tanggal) = $a AND YEAR(tanggal) = $year) ORDER BY beban_puncak DESC limit 1";
			$db = Yii::app()->db->createCommand($sql)->queryScalar();
			$exec[] = floatval($db);
		}
		return $exec;
	}

	public function getDataTahunanValue($awal , $akhir , $field)
	{
		for($a=$awal;$a<=$akhir;$a++)
		{
			 $sql="SELECT $field FROM trans_neraca_daya  WHERE beban_puncak IN(SELECT MAX(beban_puncak) FROM beban_puncak WHERE YEAR(tanggal) = $a) ORDER BY beban_puncak DESC limit 1";
			$db = Yii::app()->db->createCommand($sql)->queryScalar();
			$exec[] = floatval($db);
		}
		return $exec;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal' => 'Tanggal',
			'terpasang' => 'Terpasang',
			'po' => 'Po',
			'fo' => 'Fo',
			'der_pln' => 'Der Pln',
			'der_rental' => 'Der Rental',
			'derating' => 'Derating',
			'beban_puncak' => 'Beban Puncak',
			'daya_mampu' => 'Daya Mampu',
			'dm_pln' => 'Dm Pln',
			'dm_rental' => 'Dm Rental',
			'beban_terlayani' => 'Beban Terlayani',
			'cad_ops' => 'Cad Ops',
			'padam' => 'Padam',
			'kondisi' => 'Kondisi',
			'posted_by' => 'Posted By',
			'date_post' => 'Date Post',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('terpasang',$this->terpasang,true);
		$criteria->compare('po',$this->po,true);
		$criteria->compare('fo',$this->fo,true);
		$criteria->compare('der_pln',$this->der_pln,true);
		$criteria->compare('der_rental',$this->der_rental,true);
		$criteria->compare('derating',$this->derating,true);
		$criteria->compare('beban_puncak',$this->beban_puncak,true);
		$criteria->compare('daya_mampu',$this->daya_mampu,true);
		$criteria->compare('dm_pln',$this->dm_pln,true);
		$criteria->compare('dm_rental',$this->dm_rental,true);
		$criteria->compare('beban_terlayani',$this->beban_terlayani,true);
		$criteria->compare('cad_ops',$this->cad_ops,true);
		$criteria->compare('padam',$this->padam,true);
		$criteria->compare('kondisi',$this->kondisi,true);
		$criteria->compare('posted_by',$this->posted_by);
		$criteria->compare('date_post',$this->date_post,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TransNeracaDaya the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
