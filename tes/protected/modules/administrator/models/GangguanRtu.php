<?php

/**
 * This is the model class for table "gangguan_rtu".
 *
 * The followings are the available columns in table 'gangguan_rtu':
 * @property integer $id
 * @property string $tanggal_gangguan
 * @property string $jam_gangguan
 * @property string $gangguan
 * @property string $evaluasi
 * @property string $tanggal_evaluasi
 * @property string $jam_evaluasi
 * @property string $date_post
 * @property integer $posted_by
 */
class GangguanRtu extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gangguan_rtu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('posted_by', 'numerical', 'integerOnly'=>true),
			array('tanggal_gangguan, jam_gangguan, gangguan, evaluasi, tanggal_evaluasi, jam_evaluasi, date_post', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tanggal_gangguan, jam_gangguan, gangguan, evaluasi, tanggal_evaluasi, jam_evaluasi, date_post, posted_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			
			$this->date_post = date("Y-m-d H:i:s");
			$this->posted_by = Yii::app()->session['tokenId'];
			$this->tanggal_gangguan = date("Y-m-d" ,  strtotime($this->tanggal_gangguan));
			$this->tanggal_evaluasi = date("Y-m-d" ,  strtotime($this->tanggal_evaluasi));
			return true;
		}
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal_gangguan' => 'Tanggal Gangguan',
			'jam_gangguan' => 'Jam Gangguan',
			'gangguan' => 'Gangguan',
			'evaluasi' => 'Evaluasi',
			'tanggal_evaluasi' => 'Tanggal Evaluasi',
			'jam_evaluasi' => 'Jam Evaluasi',
			'date_post' => 'Date Post',
			'posted_by' => 'Posted By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tanggal_gangguan',$this->tanggal_gangguan,true);
		$criteria->compare('jam_gangguan',$this->jam_gangguan,true);
		$criteria->compare('gangguan',$this->gangguan,true);
		$criteria->compare('evaluasi',$this->evaluasi,true);
		$criteria->compare('tanggal_evaluasi',$this->tanggal_evaluasi,true);
		$criteria->compare('jam_evaluasi',$this->jam_evaluasi,true);
		$criteria->compare('date_post',$this->date_post,true);
		$criteria->compare('posted_by',$this->posted_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GangguanRtu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
