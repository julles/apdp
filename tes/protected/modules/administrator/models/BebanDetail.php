<?php

/**
 * This is the model class for table "beban_detail".
 *
 * The followings are the available columns in table 'beban_detail':
 * @property integer $id
 * @property integer $beban_id
 * @property string $beban_perjam
 * @property string $prakiraan_beban
 * @property string $prakiraan_daya_mampu
 * @property string $jam
 */
class BebanDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'beban_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('beban_id', 'numerical', 'integerOnly'=>true),
			array('beban_perjam, prakiraan_beban, prakiraan_daya_mampu', 'length', 'max'=>10),
			array('jam', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, beban_id, beban_perjam, prakiraan_beban, prakiraan_daya_mampu, jam', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'beban_id' => 'Beban',
			'beban_perjam' => 'Beban Perjam',
			'prakiraan_beban' => 'Prakiraan Beban',
			'prakiraan_daya_mampu' => 'Prakiraan Daya Mampu',
			'jam' => 'Jam',
		);
	}


	public function listJam($tanggal)
	{
		$db = Yii::app()->db;
		$sql = "SELECT jam FROM beban_detail INNER JOIN beban ON beban.id = beban_detail.beban_id WHERE tanggal = '$tanggal'";
		$cek = $db->createCommand($sql)->queryScalar();
		if(!empty($cek))
		{
			$query = $db->createCommand($sql)->query();
			while(($row = $query->read()) !== false)
			{
				$hasil[] = $row['jam'];
			}

		}else{
			$hasil = array();
		}
		return $hasil;
	}

	/*public function valueJam($tanggal , $field)
	{
		$db = Yii::app()->db;
		echo $sql = "SELECT $field FROM beban_detail INNER JOIN beban ON beban.id = beban_detail.beban_id WHERE tanggal = '$tanggal'";
		$query = $db->createCommand($sql)->query();
		while(($row = $query->read()) !== false)
		{
			$hasil[] = floatval($row[$field]);
		}
		return $hasil;
	}*/

	public function valueJam($tanggal , $field)
	{
		$db = Yii::app()->db;
		$sql = "SELECT jam FROM beban_detail INNER JOIN beban ON beban.id = beban_detail.beban_id WHERE tanggal = '$tanggal'";
		$cek = $db->createCommand($sql)->queryScalar();
		if(!empty($cek))
		{	
			$query = $db->createCommand($sql)->query();
			while(($row = $query->read()) !== false)
			{
				 $sql = "SELECT $field FROM beban_detail INNER JOIN beban ON beban.id = beban_detail.beban_id WHERE tanggal = '$tanggal' AND jam = '$row[jam]'";
				$rows = $db->createCommand($sql)->queryRow();
				if($rows[$field] == 0)
				{
					$rows[$field] = null;	
				}else{
					$rows[$field] = floatval($rows[$field]);
				}  
				$hasil[] = $rows[$field];
			}
		}else{
			$hasil = array();
		}
		return $hasil;
		
	}


	public function getDataHarian($tanggalAkhir)
	{
		for($a=1;$a<=$tanggalAkhir;$a++)
		{
			$tgl[] = $a;
		}
		return $tgl;
	}


	public function getDataHarianValue($month , $year , $last)
	{
		$db = Yii::app()->db;
		for($a=1;$a<=$last;$a++)
		{
			$qw = "SELECT jumlah_max , tanggal , jam_max FROM beban WHERE tanggal = '$year-$month-$a'";
			$exec = $db->createCommand($qw)->queryRow();
			//$tanggal = ar::formatWaktu($exec['tanggal'] ,"long" ,"");
			$hasil[] = array('y' => floatval($exec['jumlah_max']) , 'jam' => $exec['jam_max']); 
		}
		return $hasil;
	}

	public function getDataBulananValue($year)
	{
		$db = Yii::app()->db;
		for($a=1;$a<=12;$a++)
		{
			$qw = "SELECT jumlah_max ,  jam_max , tanggal FROM beban WHERE YEAR(tanggal) = '$year' AND MONTH(tanggal) = '$a' ORDER BY jumlah_max DESC LIMIT 1 ";
			$exec = $db->createCommand($qw)->queryRow();
			$hasil[] = array('y' => floatval($exec['jumlah_max']) , 'jam' => $exec['jam_max'] ,'tanggal' => ar::formatWaktu($exec['tanggal'] , "long" ,"") ); 
		}

		return $hasil;
	}

	public function getDataTahunanValue($awal,$akhir)
	{
		$db = Yii::app()->db;
		for($a=$awal;$a<=$akhir;$a++)
		{
			$qw = "SELECT jumlah_max ,  jam_max , tanggal FROM beban WHERE YEAR(tanggal) = '$a' ORDER BY jumlah_max DESC LIMIT 1 ";
			$exec = $db->createCommand($qw)->queryRow();
			$hasil[] = array('y' => floatval($exec['jumlah_max']) , 'jam' => $exec['jam_max']  ,'tanggal' => ar::formatWaktu($exec['tanggal'] , "long" ,"") ); 
		}
		
		
		return $hasil;
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('beban_id',$this->beban_id);
		$criteria->compare('beban_perjam',$this->beban_perjam,true);
		$criteria->compare('prakiraan_beban',$this->prakiraan_beban,true);
		$criteria->compare('prakiraan_daya_mampu',$this->prakiraan_daya_mampu,true);
		$criteria->compare('jam',$this->jam,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BebanDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
