<?php

/**
 * This is the model class for table "neraca_energi_detail".
 *
 * The followings are the available columns in table 'neraca_energi_detail':
 * @property integer $id
 * @property integer $neraca_energi_id
 * @property integer $item_id
 * @property string $expor
 * @property string $impor
 * @property integer $faktor_expor
 * @property integer $faktor_impor
 */
class NeracaEnergiDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'neraca_energi_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('neraca_energi_id, item_id', 'numerical', 'integerOnly'=>true),
			//array('expor, impor,faktor_expor,faktor_impor , kwh_expor , kwh_impor', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, neraca_energi_id, item_id, expor, impor, faktor_expor, faktor_impor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'toItem' => array(self::BELONGS_TO , 'MasterItemNe' , 'item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'neraca_energi_id' => 'Neraca Energi',
			'item_id' => 'Item',
			'expor' => 'Expor',
			'impor' => 'Impor',
			'faktor_expor' => 'Faktor Expor',
			'faktor_impor' => 'Faktor Impor',
		);
	}

	public function sql($field,$where)
	{
		$sql = "
								SELECT SUM($field) FROM neraca_energi_detail 
								INNER JOIN  neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
								INNER JOIN master_gardu_induk ON neraca_energi.gardu_induk_id = master_gardu_induk.id
								INNER JOIN master_item_ne ON neraca_energi_detail.item_id = master_item_ne.id
								WHERE 
								$where
							
							";
		return $sql;
	}

	public function sqlPersen($field,$where)
	{
		$sql = "
								SELECT $field FROM neraca_energi_detail 
								INNER JOIN  neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
								INNER JOIN master_gardu_induk ON neraca_energi.gardu_induk_id = master_gardu_induk.id
								INNER JOIN master_item_ne ON neraca_energi_detail.item_id = master_item_ne.id
								WHERE 
								$where
							
							";
		return $sql;
	}

	public function minusHari($tanggal)
	{
		$db = Yii::app()->db;
		$sql = "SELECT DATE_ADD('$tanggal',INTERVAL -1 DAY);";
		return $db->createCommand($sql)->queryScalar();
	}

	public function getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field)
	{
			$db = Yii::app()->db;
			for($a=1;$a<=$lastDay;$a++)
			{
				$tanggal = "$year-$month-$a";
				$kemarin = $this->minusHari($tanggal);
				$sqlCek = $this->sqlPersen('item_id' , "jenis_id = '4' AND tanggal = '$tanggal'");
				$cek = $db->createCommand($sqlCek)->queryScalar();
				if($cek >0)
				{
					$sql = $this->sqlPersen('item_id' , "jenis_id = '4' AND tanggal = '$tanggal'");
					$qry = $db->createCommand($sql)->queryAll();	
					$hasil = 0;
					foreach($qry as $row)
					{
						$sqlFormulaKemarin = $this->sqlPersen("if(COUNT($field) = 0 , 0 , SUM($field))" , "jenis_id = '4' AND item_id = '$row[item_id]' AND tanggal = '$kemarin'");
						//$sqlFormulaFaktor = $db->createCommand("SELECT faktor_impor FROM master_faktor WHERE item_id = '$row[item_id]'")->queryScalar();
						$sqlFormulaFaktor = "SELECT faktor_impor FROM master_faktor WHERE item_id = '$row[item_id]'";
						$sqlFormula = $this->sqlPersen("SUM(($field - ($sqlFormulaKemarin)) ) * ($sqlFormulaFaktor)" , "jenis_id = '4' AND item_id = '$row[item_id]' AND tanggal = '$tanggal'");
						$execFormula = $db->createCommand($sqlFormula)->queryScalar();
						$hasil = $hasil + $execFormula;
					}
						
				}else{
					$hasil = 0;
				}	
						$Xvalue[] = floatval($hasil);
						//return $Xvalue;
			}
			return $Xvalue;
	}


	public function getDataTahunanBaru($year   , $gardu_induk , $item_id , $jenis_id , $field)
	{		
				$db = Yii::app()->db;
				for($month=1;$month<=12;$month++)
				{
					
					$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					
					$hasilhari = 0;
					for($a=1;$a<=$lastDay;$a++)
					{
						$tanggal = "$year-$month-$a";
						$kemarin = $this->minusHari($tanggal);
						$sqlCek = $this->sqlPersen('item_id' , "jenis_id = '4' AND tanggal = '$tanggal'");
						$cek = $db->createCommand($sqlCek)->queryScalar();
						if($cek >0)
						{
							$sql = $this->sqlPersen('item_id' , "jenis_id = '4' AND tanggal = '$tanggal'");
							$qry = $db->createCommand($sql)->queryAll();	
							$hasil = 0;
							foreach($qry as $row)
							{
								$sqlFormulaKemarin = $this->sqlPersen("if(COUNT($field) = 0 , 0 , $field)" , "jenis_id = '4' AND item_id = '$row[item_id]' AND tanggal = '$kemarin'");
								//$sqlFormulaFaktor = $db->createCommand("SELECT faktor_impor FROM master_faktor WHERE item_id = '$row[item_id]'")->queryScalar();
								$sqlFormulaFaktor = "SELECT faktor_impor FROM master_faktor WHERE item_id = '$row[item_id]'";

									$sqlFormula = $this->sqlPersen("($field - ($sqlFormulaKemarin))  * ($sqlFormulaFaktor) " , "jenis_id = '4' AND item_id = '$row[item_id]' AND tanggal = '$tanggal'");
								
								$execFormula = $db->createCommand($sqlFormula)->queryScalar();
								$hasil = $hasil + $execFormula;
							}
							
						}else{
							$hasil = 0;
						}	
						$hasilhari = $hasilhari + $hasil;
					}
					$Xvalue[] = floatval($hasilhari);
					
				}
				return $Xvalue;	
	}

	public function getDataRangeBaru($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field)
	{
		$db = Yii::app()->db;
		for($year=$awal;$year<=$akhir;$year++)
		{
			$hasilBulan = 0;
			for($month=1;$month<=12;$month++)
				{
					
					$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					
					$hasilhari = 0;
					for($a=1;$a<=$lastDay;$a++)
					{
						$tanggal = "$year-$month-$a";
						$kemarin = $this->minusHari($tanggal);
						$sqlCek = $this->sqlPersen('item_id' , "jenis_id = '4' AND tanggal = '$tanggal'");
						$cek = $db->createCommand($sqlCek)->queryScalar();
						if($cek >0)
						{
							$sql = $this->sqlPersen('item_id' , "jenis_id = '4' AND tanggal = '$tanggal'");
							$qry = $db->createCommand($sql)->queryAll();	
							$hasil = 0;
							foreach($qry as $row)
							{
								$sqlFormulaKemarin = $this->sqlPersen("if(COUNT($field) = 0 , 0 , $field)" , "jenis_id = '4' AND item_id = '$row[item_id]' AND tanggal = '$kemarin'");
								//$sqlFormulaFaktor = $db->createCommand("SELECT faktor_impor FROM master_faktor WHERE item_id = '$row[item_id]'")->queryScalar();
								$sqlFormulaFaktor = "SELECT faktor_impor FROM master_faktor WHERE item_id = '$row[item_id]'";

									$sqlFormula = $this->sqlPersen("($field - ($sqlFormulaKemarin))  * ($sqlFormulaFaktor) " , "jenis_id = '4' AND item_id = '$row[item_id]' AND tanggal = '$tanggal'");
								
								$execFormula = $db->createCommand($sqlFormula)->queryScalar();
								$hasil = $hasil + $execFormula;
							}
							
						}else{
							$hasil = 0;
						}	
						$hasilhari = $hasilhari + $hasil;
					}
					 $hasilBulan = $hasilBulan + $hasilhari;
					
					
				}
				$Xvalue[] = floatval($hasilBulan);
		}

		return $Xvalue;
	}

	public function getDataHarian($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field)
	{		
					for($a=1;$a<=$lastDay;$a++)
					{
						$tanggal = "$year-$month-$a";
						$where = "tanggal ='$tanggal' AND jenis_id = '$jenis_id' ";
						if(empty($gardu_induk))
						{
							$where .= "";
						}elseif(!empty($gardu_induk) && empty($item_id)){
							$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
						}elseif(!empty($gardu_induk) && !empty($item_id)){
							$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
						}	
						 $sql = $this->sql($field ,$where);
						$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
						
						$hasil[] = floatval($hasilSql);
					}
					return $hasil;
	}

	public function getDataTahunan($year   , $gardu_induk , $item_id , $jenis_id , $field)
	{		
				for($a=1;$a<=12;$a++)
				{
					$tanggal = " MONTH(tanggal) = '$a' AND YEAR(tanggal) = '$year' ";
					$where = "$tanggal AND jenis_id = '$jenis_id' ";
					if(empty($gardu_induk))
					{
						$where .= "";
					}elseif(!empty($gardu_induk) && empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
					}elseif(!empty($gardu_induk) && !empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
					}	
					 $sql = $this->sql($field ,$where);
					$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
					
					$hasil[] = floatval($hasilSql);
				}
				return $hasil;	
	}

	public function getDataRange($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field)
	{		
				for($a=$awal;$a<=$akhir;$a++)
				{
					$tanggal = " YEAR(tanggal) = '$a' ";
					$where = "$tanggal AND jenis_id = '$jenis_id' ";
					if(empty($gardu_induk))
					{
						$where .= "";
					}elseif(!empty($gardu_induk) && empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
					}elseif(!empty($gardu_induk) && !empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
					}	
					 $sql = $this->sql($field ,$where);
					$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
					
					$hasil[] = floatval($hasilSql);
				}
				return $hasil;	
	}

	public function sqlCalculate($field ,  $field2 ,$where)
	{
		$sql = "
								SELECT SUM($field2 - $field) FROM neraca_energi_detail 
								INNER JOIN  neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
								INNER JOIN master_gardu_induk ON neraca_energi.gardu_induk_id = master_gardu_induk.id
								INNER JOIN master_item_ne ON neraca_energi_detail.item_id = master_item_ne.id
								WHERE 
								$where
							
							";
		return $sql;
	}

	

	public function getDataHarianCalculate($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field , $field2)
	{		
					for($a=1;$a<=$lastDay;$a++)
					{
						$tanggal = "$year-$month-$a";
						$where = "tanggal ='$tanggal' AND jenis_id = '$jenis_id' ";
						
						
						if(empty($gardu_induk) && empty($item_id))
						{
							$where .= "";
						}elseif(!empty($gardu_induk) && empty($item_id)){
							$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
						}elseif(!empty($gardu_induk) && !empty($item_id)){
							$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
						}else{
							$where .= " AND item_id = '$item_id'";
						}	
							
						$sql = $this->sqlCalculate($field,$field2 ,$where);
						$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
						
						$hasil[] = floatval($hasilSql);
					}
					return $hasil;
	}


	public function getDataTahunanCalculate($year   , $gardu_induk , $item_id , $jenis_id , $field , $field2)
	{		
				for($a=1;$a<=12;$a++)
				{
					$tanggal = " MONTH(tanggal) = '$a' AND YEAR(tanggal) = '$year' ";
					$where = "$tanggal AND jenis_id = '$jenis_id' ";
					if(empty($gardu_induk) && empty($item_id))
						{
							$where .= "";
						}elseif(!empty($gardu_induk) && empty($item_id)){
							$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
						}elseif(!empty($gardu_induk) && !empty($item_id)){
							$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
						}else{
							$where .= " AND item_id = '$item_id'";
						}	
					$sql = $this->sqlCalculate($field,$field2 ,$where);
					$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
					
					$hasil[] = floatval($hasilSql);
				}
				return $hasil;	
	}

	public function getDataRangeCalculate($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field , $field2)
	{		
				for($a=$awal;$a<=$akhir;$a++)
				{
					$tanggal = " YEAR(tanggal) = '$a' ";
					$where = "$tanggal AND jenis_id = '$jenis_id' ";
					if(empty($gardu_induk) && empty($item_id))
					{
						$where .= "";
					}elseif(!empty($gardu_induk) && empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
					}elseif(!empty($gardu_induk) && !empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
					}else{
							$where .= " AND item_id = '$item_id'";
					}	
					 	$sql = $this->sqlCalculate($field,$field2 ,$where);
					$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
					
					$hasil[] = floatval($hasilSql);
				}
				return $hasil;	
	}
	

	public function getDataHarianTransmisi($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field)
	{		
					for($a=1;$a<=$lastDay;$a++)
					{
						$tanggal = "$year-$month-$a";
						$cek = Yii::app()->db->createCommand($this->sqlPersen("COUNT(neraca_energi_detail.id)" , " jenis_id = '5' AND tanggal = '$tanggal'"))->queryScalar();
						if($cek >0)
						{		
								$where = "tanggal ='$tanggal' AND jenis_id = '$jenis_id' ";
								if(empty($gardu_induk))
								{
									$where .= "";
								}elseif(!empty($gardu_induk) && empty($item_id)){
									$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
								}elseif(!empty($gardu_induk) && !empty($item_id)){
									$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
								}	
								 $sqlPsgi = Yii::app()->db->createCommand($this->sql("kwh_expor ","  jenis_id = '5' AND tanggal = '$tanggal'" ))->queryScalar();
								 $sqlImporExpor = Yii::app()->db->createCommand($this->sqlPersen("SUM(kwh_impor - kwh_expor)","jenis_id = '4' AND tanggal = '$tanggal'"))->queryScalar();
								
								 
									$hasilSql = $sqlImporExpor - $sqlPsgi;	
									//echo "<br/> $sqlImporExpor - $sqlPsgi";
						}else{
							$hasilSql = 0;
						}
						$hasil[] = floatval($hasilSql);
					}
					return $hasil;
	}

	public function getDataTahunanTransmisi($year   , $gardu_induk , $item_id , $jenis_id , $field)
	{		
				for($a=1;$a<=12;$a++)
				{
					$tanggal = " MONTH(tanggal) = '$a' AND YEAR(tanggal) = '$year' ";
					$where = "$tanggal AND jenis_id = '$jenis_id' ";
					if(empty($gardu_induk))
					{
						$where .= "";
					}elseif(!empty($gardu_induk) && empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
					}elseif(!empty($gardu_induk) && !empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
					}	

					
					 $sqlPsgi = $this->sql("kwh_expor ","  jenis_id = '5' AND $tanggal" );
					
					  $sql = $this->sql($field.") - (".$sqlPsgi ,$where);
					 
					 $hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
					
					$hasil[] = floatval($hasilSql);
				}
				return $hasil;	
	}

	public function getDataRangeTransmisi($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field)
	{		
				for($a=$awal;$a<=$akhir;$a++)
				{
					$tanggal = " YEAR(tanggal) = '$a' ";
					$where = "$tanggal AND jenis_id = '$jenis_id' ";
					if(empty($gardu_induk))
					{
						$where .= "";
					}elseif(!empty($gardu_induk) && empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
					}elseif(!empty($gardu_induk) && !empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
					}	
					  $sqlPsgi = $this->sql("kwh_expor ","  jenis_id = '5' AND $tanggal" );
					
					$sql = $this->sql($field.") - (".$sqlPsgi ,$where);
					$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
					
					$hasil[] = floatval($hasilSql);
				}
				return $hasil;	
	}


	


	public function getDataHarianTransmisiPersen($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field)
	{		
					$energiMasuk = $this->getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , 4 , 'impor');
					$energiKeluar = $this->getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , 4 , 'expor');
					$psgi = $this->getDataHarian($year , $month , $lastDay , $gardu_induk , $item_id , 5 , 'expor');
					
					$b = -1;
					for($a=1;$a<=$lastDay;$a++)
					{
							$b++;
							$susutTransmisi = $energiMasuk[$b] - $energiKeluar[$b] - $psgi[$b];
							if($energiMasuk[$b] == 0)
							{
								$persen = 0;
							}else{
								$persen = $susutTransmisi / $energiMasuk[$b] * 100;
							}	
								
							
							$hasil[] = $persen;

					}	
					
					return $hasil;				
	}


	public function getDataTahunanTransmisiPersen($year   , $gardu_induk , $item_id , $jenis_id , $field)
	{		
				for($month=1;$month<=12;$month++)
				{
						$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
						$energiMasuk = $this->getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , 4 , 'impor');
						$energiKeluar = $this->getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , 4 , 'expor');
						$psgi = $this->getDataHarian($year , $month , $lastDay , $gardu_induk , $item_id , 5 , 'expor');
						
						$b = -1;
						$hasilHari = 0;
						for($a=1;$a<=$lastDay;$a++)
						{
								$b++;
								$susutTransmisi = $energiMasuk[$b] - $energiKeluar[$b] - $psgi[$b];
								if($energiMasuk[$b] == 0)
								{
									$persenHari = 0;
								}else{
									$persenHari = $susutTransmisi / $energiMasuk[$b] * 100;
								}	
									
							$hasilHari = $hasilHari + $persenHari;
						}
						$persen[] = $hasilHari;

				}	
					
					return $persen;		
	}


	public function getDataRangeTransmisiPersen($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field)
	{		
				for($year=$awal;$year<=$akhir;$year++)
				{
					$hasilBulan =0;
					for($month=1;$month<=12;$month++)
					{
							$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
							$energiMasuk = $this->getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , 4 , 'impor');
							$energiKeluar = $this->getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , 4 , 'expor');
							$psgi = $this->getDataHarian($year , $month , $lastDay , $gardu_induk , $item_id , 5 , 'expor');
							
							$b = -1;
							$hasilHari = 0;
							for($a=1;$a<=$lastDay;$a++)
							{
									$b++;
									$susutTransmisi = $energiMasuk[$b] - $energiKeluar[$b] - $psgi[$b];
									if($energiMasuk[$b] == 0)
									{
										$persenHari = 0;
									}else{
										$persenHari = $susutTransmisi / $energiMasuk[$b] * 100;
									}	
										
								$hasilHari = $hasilHari + $persenHari;
							}
							$hasilBulan = $hasilBulan + $hasilHari;
					}
					$hasil[] = $hasilBulan;	
				}
				return $hasil;	
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('neraca_energi_id',$this->neraca_energi_id);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('expor',$this->expor,true);
		$criteria->compare('impor',$this->impor,true);
		$criteria->compare('faktor_expor',$this->faktor_expor);
		$criteria->compare('faktor_impor',$this->faktor_impor);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NeracaEnergiDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
