 <?php
 $base = Yii::app()->baseUrl."/time/";
 ?>   

    <link rel="stylesheet" href="<?php echo $base; ?>include/ui-1.10.0/ui-lightness/jquery-ui-1.10.0.custom.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base; ?>jquery.ui.timepicker.css?v=0.3.3" type="text/css" />

    <script type="text/javascript" src="<?php echo $base; ?>include/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="<?php echo $base; ?>include/ui-1.10.0/jquery.ui.core.min.js"></script>
    <script type="text/javascript" src="<?php echo $base; ?>include/ui-1.10.0/jquery.ui.widget.min.js"></script>
    <script type="text/javascript" src="<?php echo $base; ?>include/ui-1.10.0/jquery.ui.tabs.min.js"></script>
    <script type="text/javascript" src="<?php echo $base; ?>include/ui-1.10.0/jquery.ui.position.min.js"></script>

    <script type="text/javascript" src="<?php echo $base; ?>jquery.ui.timepicker.js?v=0.3.3"></script>

    <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-24327002-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

        function plusone_clicked() {
            $('#thankyou').fadeIn(300);
        }

        $(document).ready(function() {
            $('#floating_timepicker').timepicker({
                onSelect: function(time, inst) {
                    $('#floating_selected_time').html('You selected ' + time);
                }
            });

            $('#tabs').tabs();

        });


    </script>
    <div>
        <label for="timepicker_noPeriodLabels">Time picker without the AM/PM labels on the left (showPeriodLabels option set to false:</label>
        <input type="text" style="width: 70px;" id="timepicker_noPeriodLabels" value="13:30" />

        <script type="text/javascript">
            $(document).ready(function() {
                $('#timepicker_noPeriodLabels').timepicker({
                    showPeriodLabels: false
                });
              });

        </script>
        <a onclick="$('#script_noPeriodLabels').toggle(200)">[Show code]</a>
<pre id="script_noPeriodLabels" style="display: none" class="code">$('#timepicker').timepicker({
    showPeriodLabels: false,
});
</pre>

    </div>