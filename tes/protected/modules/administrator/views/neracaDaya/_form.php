<script>
	function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode != 46 && charCode > 31 && charCode != 45
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
</script><div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'role-form',
                                        // Please note: When you enable ajax validation, make sure the corresponding
                                        // controller action is handling ajax validation correctly.
                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                        // See class documentation of CActiveForm for details on this.
                                        'htmlOptions' => array('style' => 'height:580px;'),
                                        'enableClientValidation' => true , 
                                        'enableAjaxValidation'=>false,
                                    )); ?>
                                        
                                    <div class="col6">
                                        <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'tanggal'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                     <?php 
                                                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(

                                                            'model'=>$model,
                                                            'attribute'=>'tanggal',
                                                            // additional javascript options for the date picker plugin
                                                            'options' => array(
                                                            'showAnim' => 'fold',
                                                            'dateFormat'=>'dd-mm-yy',
                                                             'changeMonth'=>true,
                                                            'changeYear'=>true,
                                                            'yearRange'=>'1900:2099',
                                                            ),
                                                            'htmlOptions' => array(
                                                            'style' => 'height:20px;width:75px;',
                                                             'class' => 'form-control'
                                                            ),
                                                        ));

                                                     ?>
                                                    <?php echo $form->error($model,'tanggal' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'terpasang'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'terpasang',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'terpasang' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'po'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'po',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'po' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'fo'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'fo',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'fo' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										<?php
										/*
                                         <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'der_pln'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'der_pln',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'der_pln' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'der_rental'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'der_rental',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'der_rental' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										*/?>
                                         <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'derating'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'derating',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'derating' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'beban_puncak'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'beban_puncak',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'beban_puncak' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "col6" style="margin-bottom:120px !imporatant;">
                                        
                                        <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'daya_mampu'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'daya_mampu',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'daya_mampu' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										<?php
										/*
                                        <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'dm_pln'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'dm_pln',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'dm_pln' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'dm_rental'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'dm_rental',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'dm_rental' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										*/
										?>
                                         <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'beban_terlayani'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'beban_terlayani',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'beban_terlayani' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>

                                          <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'cad_ops'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'cad_ops',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'cad_ops' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                         </div>

                                         <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'padam'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'padam',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'padam' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                         </div>
										 <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'fdr'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'fdr',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'fdr' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										<div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'mo'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'mo',array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'mo' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										<?php 
										/*
                                         <div class="form-group">
                                            <div class="col-sm-1">
                                                <?php echo $form->labelEx($model,'kondisi'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->dropDownList($model,'kondisi',array('0' => 'Defisit' , '1' => 'Siaga') , array('class'=>'form-control' , "onKeyPress" => "return isNumberKey(event)" )); ?>
                                                    <?php echo $form->error($model,'kondisi' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                         </div>
										*/
										?>

                                    </div>    
                                        <div class="form-group">
                                            <div class="col-sm-1">&nbsp;</div>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                              
                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                            </div>
                                        </div>

                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Input -->
            </div>


        