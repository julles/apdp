<script>
$(document).ready(function() {
    //$('#gridYii').dataTable();
    $('#gridYii').dataTable({bFilter: false, bInfo: false ,  "bLengthChange": false});
} );
</script>
				 <table id="gridYii" class="display" cellspacing="0" width="100%">
								        <thead>
								            <tr>
								                <th width = '20%'>Jenis Gardu</th>
								                <th width = '20%'>Gardu Induk</th>
								                <th width = '20%'>Waktu Gangguan</th>
								                <th width = '20%'>Posted By</th>
								                <th width = '20%'>Date Post</th>
								                <th width = '40%'>Opsi</th>
								            </tr>
								        </thead>
								 		<tbody style = 'text-align:center;'>	
								           <?php 
								           
											foreach($model as $row)
											{
												echo"
													<tr>
														<td>".$row->jenisGardu->jenis_gardu."</td>
														<td>".$row->garduInduk->gardu_induk."</td>
														<td>".ar::formatWaktu($row->waktu_gangguan,"medium" , "")."</td>
														<td>".$row->user->nama."</td>
														<td>".ar::formatWaktu($row->date_post , "medium","")."</td>
														
														<td>".CHtml::link(ar::btnEdit() , array('update' , 'id' => $row->id)).
														       " ".
															  CHtml::link(ar::btnHapus() , array('hapus' , 'id' => $row->id , 'token' => ar::encrypt($row->id)) , array('onclick' => 'return confirm("Anda Yakin Menghapus Data ini ?")') )
													 ."</td>
													</tr>
												";
											}
								           ?>
								        </tbody>
								    </table>