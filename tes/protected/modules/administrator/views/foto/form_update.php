 <?php
$this->widget('ext.SliderPopImage.SliderPopImage', array(
'selectorImgPop' => '.thumbsgen',
'popupwithpaginate' => true,
'maxpopuwidth' => '$(window).width()*0.8',

'postfixThumb' => '_thumb',
    'relPathThumbs' => 'thumbs' 
    //'relPathThumbs' => array('thumbsTiny','thumbsMedium') //only version 1.1
));
?>  
<script type="text/javascript">
	function hilang(tes)
	{
		document.getElementById(tes).value = '';
		return true;
	}   

    function hilang()
    {
     document.getElementById('pesan').style.display = 'none';
    }
</script>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $value)
    {
            echo '

                <div onclick = "hilang()" id = "pesan" style="margin: 15px;" class="alert alert-'.$key.'">
                × &nbsp;
                   '.$value.'
                </div>

            ';
    }    
?>  
<div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'role-form',
                                        // Please note: When you enable ajax validation, make sure the corresponding
                                        // controller action is handling ajax validation correctly.
                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                        // See class documentation of CActiveForm for details on this.
                                        'htmlOptions' => array('style' => 'height:450px;' , 'enctype' => 'multipart/form-data'),
                                        'enableClientValidation' => true , 
                                        'enableAjaxValidation'=>false,
                                    )); ?>
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'nama_album'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'nama_album',array('class'=>'form-control')); ?>
                                                    <?php echo $form->error($model,'nama_album' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										<?php
										for($a=0;$a<5;$a++)
										{
										?>
										
											<div class="form-group">
												<div class="col-sm-2">
													<?php
														if($a  == 0)
														{
															echo $form->labelEx($detail,'gambar');
													
														}
													?>
												</div>
												<div class="col-sm-10">
														<?php echo $form->fileField($detail,'gambar[]',array('class'=>'form-control' ,'id' => "fileUpload$a")); ?>
														<?php echo $form->error($detail,'gambar' , array('style' => 'color:red;font-size:12px;' )); ?>
														<?php echo CHtml::link('batal' , '' , array('id' => 'batal' ,  'onclick' => "hilang('fileUpload$a')" , 'style' => 'font-size:12px;text-decoration:none;display:inherit;cursor:pointer;text-align:left;')); ?>
												</div>
											</div>
										
										
										<?php
										}
										?>

                                        <div class="form-group">
                                            <div class="col-sm-2">&nbsp;</div>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                              
                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                            </div>
                                        </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <p>&nbsp;</p>

                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i>List Gambar</div>
                                </div>
                                <div class="panel-body">
                                   
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                               <table>

                                                   <tr>
                                                  
                                                            <?php
                                                            $alamat = Yii::app()->baseUrl."/images/gallery/thumb/";
                                                            $no = 0;
                                                            foreach($modelFoto as $rows)
                                                            {
                                                                $no++;
                                                                $alamatGambar = $alamat.$rows->gambar;
                                                            ?>
                                                            
                                                                    <td>
                                                                        <img src="<?php echo $alamatGambar; ?>" style = 'width:100px;height:100px;cursor:pointer;' class = 'thumbsgen' />
                                                                        <br/>
                                                                        <?php echo CHtml::link('Hapus' , array('hapusGambar' , 'id' => $model->id , 'ids' => $rows->id) , array('style' => 'text-decoration:none;color:black;font-size:12px;margin-left:15px;' , 'onclick' => 'return confirm("Anda yakin menghapus Gambar ini ?")')); ?>
                                                                    </td>
                                                                    

                                                            <?php
                                                                if($no % 7 == 0)
                                                                {
                                                                    echo "</tr><tr>";
                                                                }
                                                            }
                                                            ?>
                                                   
                                                   
                                                   </tr>
                                               </table>
                                            </div>

                                            
                                        </div>
                                        
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <!-- End Input -->
            </div>


        