
<script>
	function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode != 46 && charCode > 31 && charCode != 45
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
</script>
 <?php
 $base = Yii::app()->baseUrl."/time/";
 ?>   


<?php
	if($model->isNewRecord)
	{
		$busbar = array();
	}else{
		$busbar = CHtml::listData(MasterBusbar::model()->findAll('gardu_hubung_id=:gardu' , array(':gardu' => $model->gardu_hubung_id)) , 'id' , 'busbar');
	}
?>
   
<div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                     <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'role-form',
                                        // Please note: When you enable ajax validation, make sure the corresponding
                                        // controller action is handling ajax validation correctly.
                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                        // See class documentation of CActiveForm for details on this.
                                        'htmlOptions' => array('style' => 'min-height:780px;'),
                                        'enableClientValidation' => true , 
                                        'enableAjaxValidation'=>false,
										'clientOptions'=> array(
	                                        'validateOnSubmit'=>true,
	                                        'validateOnChange'=>true,
	                                        'validateOnType'=>true,
	                                 	 )
                                    )); ?> 
                                        <div class="form-group">
                                                <div class="col-sm-2">
                                                        <label><?php echo $form->labelEx($model,'tanggal_full'); ?></label>
                                            </div>
                                            <div class="col-sm-10">
                                                <?php 
                                                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(

                                                            'model'=>$model,
                                                            'attribute'=>'tanggal_full',
                                                            // additional javascript options for the date picker plugin
                                                            'options' => array(
                                                            'showAnim' => 'fold',
                                                            'dateFormat'=>'yy-mm-dd',
                                                             'changeMonth'=>true,
                                                            'changeYear'=>true,
                                                            'yearRange'=>'1900:2099',
                                                            ),
                                                            'htmlOptions' => array(
                                                            'style' => 'height:20px;',
                                                             'class' => 'form-control'
                                                            ),
                                                        ));

                                                     ?>
													 <?php echo $form->error($model,'tanggal_full' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                                <div class="col-sm-2">
                                                        <label><?php echo $form->labelEx($model,'gardu_hubung_id'); ?></label>
                                            </div>
                                            <div class="col-sm-10">
                                               		 <?php echo $form->dropDownList($model,'gardu_hubung_id',CHtml::listData(MasterGarduHubung::model()->findAll() , 'id' , 'gardu_hubung'),
                                               		 			 array(
                                               		 			 		'class'=>'form-control',
                                               		 			 		'empty' => '',
                                               		 			 		'ajax' => array(
                                               		 			 			'type' => 'POST',
                                               		 			 			'url' => Yii::app()->createUrl('/administrator/tegangan/getBusbar'),
                                               		 			 			'update' => '#busbar_id',
                                               		 			 			'data' => array('gardu_hubung_id' => 'js:this.value')
                                               		 			 		 )
                                               		 			 )
                                               		 	); ?>
													 <?php echo $form->error($model,'gardu_hubung_id' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                            </div>
                                        </div>
                                       
                                         <div class="form-group">
                                                <div class="col-sm-2">
                                                        <label><?php echo $form->labelEx($model,'busbar_id'); ?></label>
                                            </div>
                                            <div class="col-sm-10">
                                               		 <?php echo $form->dropDownList($model,'busbar_id',$busbar,
                                               		 			 array(
                                               		 			 		'class'=>'form-control',
                                               		 			 		'empty' => '',
                                               		 			 		'id' => 'busbar_id'
                                               		 			 )
                                               		 	); ?>
													 <?php echo $form->error($model,'busbar_id' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                                <div class="col-sm-3">
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j01_00'); ?>
                                                     <?php echo $form->textField($model,'j01_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j01_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div> 
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j02_00'); ?>
                                                     <?php echo $form->textField($model,'j02_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j02_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j03_00'); ?>
                                                     <?php echo $form->textField($model,'j03_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j03_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j04_00'); ?>
                                                     <?php echo $form->textField($model,'j04_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j04_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j05_00'); ?>
                                                     <?php echo $form->textField($model,'j05_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j05_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j06_00'); ?>
                                                     <?php echo $form->textField($model,'j06_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j06_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
												
												 <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j07_00'); ?>
                                                     <?php echo $form->textField($model,'j07_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j07_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j08_00'); ?>
                                                     <?php echo $form->textField($model,'j08_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j08_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j09_00'); ?>
                                                     <?php echo $form->textField($model,'j09_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j09_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
												<div class="isinya">
                                                    <?php echo $form->labelEx($model,'j10_00'); ?>
                                                     <?php echo $form->textField($model,'j10_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j10_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
											
                                            </div>
                                            <div class="col-sm-3">
												<div class="isinya">
                                                    <?php echo $form->labelEx($model,'j11_00'); ?>
                                                     <?php echo $form->textField($model,'j11_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j11_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j12_00'); ?>
                                                     <?php echo $form->textField($model,'j12_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j12_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j13_00'); ?>
                                                     <?php echo $form->textField($model,'j13_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j13_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j14_00'); ?>
                                                     <?php echo $form->textField($model,'j14_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j14_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j15_00'); ?>
                                                     <?php echo $form->textField($model,'j15_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j15_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j16_00'); ?>
                                                     <?php echo $form->textField($model,'j16_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j16_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
												
												 <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j17_00'); ?>
                                                     <?php echo $form->textField($model,'j17_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j17_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j18_00'); ?>
                                                     <?php echo $form->textField($model,'j18_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j18_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j18_30'); ?>
                                                     <?php echo $form->textField($model,'j18_30',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j18_30' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
												<div class="isinya">
                                                    <?php echo $form->labelEx($model,'j19_00'); ?>
                                                     <?php echo $form->textField($model,'j19_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j19_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j19_30'); ?>
                                                     <?php echo $form->textField($model,'j19_30',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j19_30' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j20_00'); ?>
                                                     <?php echo $form->textField($model,'j20_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j20_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j20_30'); ?>
                                                     <?php echo $form->textField($model,'j20_30',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j20_30' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j21_00'); ?>
                                                     <?php echo $form->textField($model,'j21_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j21_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j21_30'); ?>
                                                     <?php echo $form->textField($model,'j21_30',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j21_30' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j22_00'); ?>
                                                     <?php echo $form->textField($model,'j22_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j22_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
												
												 <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j23_00'); ?>
                                                     <?php echo $form->textField($model,'j23_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j23_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                <div class="isinya">
                                                    <?php echo $form->labelEx($model,'j24_00'); ?>
                                                     <?php echo $form->textField($model,'j24_00',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 5)); ?>
                                                     <?php echo $form->error($model,'j24_00' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                                </div>
                                                
                                            </div>
                                        </div>
										 <div class="form-group">
                                            <div class="col-sm-2">&nbsp;</div>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                              
                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                            </div>
                                        </div>
                                     <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Input -->
            </div>


        