<?php  $base = Yii::app()->baseUrl."/time/"; ?>
<script type="text/javascript" src="<?php echo $base; ?>jquery.ui.timepicker.js?v=0.3.3"></script>
<script>
$(document).ready(
	function()
	{
		$('#timepicker').timepicker();
	}
);
</script>
<script>
	function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
</script>
<div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'role-form',
                                        // Please note: When you enable ajax validation, make sure the corresponding
                                        // controller action is handling ajax validation correctly.
                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                        // See class documentation of CActiveForm for details on this.
                                        'htmlOptions' => array('style' => 'height:350px;'),
                                        'enableClientValidation' => true , 
                                        'enableAjaxValidation'=>false,
                                    )); ?>
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'tanggal'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php 
                                                        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                                            'model'=>$model,
                                                            'attribute'=>'tanggal',
                                                            'htmlOptions' => array(
                                                                'id' => 'datepicker_for_due_date',
                                                                'size' => '10',
                                                                'class' => 'form-control',
                                                                //'style' => 'height:25px;width:200px;float:right;' 
                                                                //'style' => 'width:40%; border: 1px solid #cccccc;height:32px;'
                                                            ),
                                                            
                                                            'options'=>array(
                                                                'hourGrid' => 4,
                                                                'hourMin' => 9,
                                                                'hourMax' => 17,
                                                                'timeFormat' => 'hh:mm',
                                                                'changeMonth' => true,
                                                                'changeYear' => false,
                                                                'showOn' => 'focus', 
                                                                'dateFormat' => 'dd/mm/yy',
                                                                'showOtherMonths' => true,
                                                                'selectOtherMonths' => true,
                                                                'changeMonth' => true,
                                                                'changeYear' => true,
                                                                'showButtonPanel' => true,
                                                                ),
                                                                
                                                            ));  
                                                        ?>
                                                    <?php echo $form->error($model,'tanggal' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										<div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'jam'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'jam',array('class'=>'form-control' , 'id' => 'timepicker')); ?>
                                                    <?php echo $form->error($model,'jam' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										<div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'daya'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'daya',array('class'=>'form-control','onKeyPress' => 'return isNumberKey(event)')); ?>
                                                    <?php echo $form->error($model,'daya' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2">&nbsp;</div>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                              
                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                            </div>
                                        </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Input -->
            </div>


        