<script>
	function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode != 46 && charCode > 31 && charCode != 45
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
</script>
<div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'role-form',
                                        // Please note: When you enable ajax validation, make sure the corresponding
                                        // controller action is handling ajax validation correctly.
                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                        // See class documentation of CActiveForm for details on this.
                                        'htmlOptions' => array('style' => 'height:900px;'),
                                        'enableClientValidation' => true , 
										
                                        'enableAjaxValidation'=>false,
                                    )); ?>
										
										<?php
											echo ar::generateJam();
										?>
									
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'tanggal'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                     <?php 
                                                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(

                                                            'model'=>$model,
                                                            'attribute'=>'tanggal',
                                                            // additional javascript options for the date picker plugin
                                                            'options' => array(
                                                            'showAnim' => 'fold',
                                                            'dateFormat'=>'dd-mm-yy',
                                                             'changeMonth'=>true,
                                                            'changeYear'=>true,
                                                            'yearRange'=>'1900:2099',
                                                            ),
                                                            'htmlOptions' => array(
                                                            'style' => 'height:20px;width:75px;',
                                                             'class' => 'form-control'
                                                            ),
                                                        ));

                                                     ?>
                                                    <?php echo $form->error($model,'tanggal' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'gardu_induk_id'); ?>
                                            </div>
                                            <div class="col-sm-10">
													<?php
														$garduAjax = array(
															'type' => 'POST',
															'url' => Yii::app()->createUrl("administrator/pembebananTrafoNew/getTrafo"),
															'data' => array('gardu_id' => 'js:this.value'),
															'update' => '#trafo_id'
														);
														$listGardu = CHtml::listData(MasterGarduInduk::model()->findAll() , 'id' , 'gardu_induk');
														$optionsGardu = array("empty" => "" , "class" => "form-control" , "ajax" => $garduAjax);
													?>
                                                    <?php echo $form->dropDownList($model , 'gardu_induk_id' , $listGardu , $optionsGardu); ?>
                                                    <?php echo $form->error($model,'gardu_induk_id' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'trafo_id'); ?>
                                            </div>
                                            <div class="col-sm-10">
													<?php
														
														(isset($_POST['TransPembebananTrafo'])) ? $listTrafo = CHtml::listData(MasterTrafo::model()->findAll('gardu_induk_id=:gardu' , array(':gardu' => $model->gardu_induk_id)) , 'id' , 'trafo') : $listTrafo = array();
														$optionsTrafo = array("empty" => "" , "class" => "form-control" , "id" => 'trafo_id');
													?>
                                                    <?php echo $form->dropDownList($model , 'trafo_id' , $listTrafo , $optionsTrafo); ?>
                                                    <?php echo $form->error($model,'trafo_id' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <div class="col-sm-10">
                                                  <div class="col-sm-5"style = 'float:left;'>
                                                    <table width = ''>
												<?php
													$b = -1;
													for($a=1;$a<=28;$a++)
													{
														$b++;
														if($a <= 18)
														{	
																	($a<=9) ? $a = "0$a" : $a = $a;
																	$label = "$a.00";
																	
																	
														}else{
															if($a == 19)
															{
																$label = "18:30 ";
															}elseif($a == 20){
																$label = "19:00 ";
															}elseif($a == 21){
																$label = "19:30 ";
															}elseif($a == 22){
																$label = "20:00 ";
															}elseif($a == 23){
																$label = "20:30 ";
															}elseif($a == 24){
																$label = "21:00 ";
															}elseif($a == 25){
																$label = "21:30 ";
															}elseif($a == 26){
																$label = "22:00 ";
															}elseif($a == 27){
																$label = "23:00 ";
															}elseif($a == 28){
																$label = "24:00 ";
															}			
														}
														(isset($_POST['TransPembebananTrafo'])) ? $val = $_POST['nilai'][$b] : $val = '';
														echo "
														<tr>
															<td>$label</td>
															<td>
																<table width = '100%'>
																	<tr>
																		<td>".CHtml::textField("nilai[]", $val , array('class' =>'form-control',"style" => "width:100px;font-size:12px;" , "placeholder" => "" , "onKeyPress" => "return isNumberKey(event)" , 'maxlength' => 6))."</td>
																		
																	</tr>
																	
																</table>
															</td>
														</tr>
														";
														echo "<input type = 'hidden' name = 'jam[]' value = '$label' />";
														  if($a == 14)
	                                                        {
	                                                            echo "  </table> </div>  <div class=\"col-sm-5\" style = 'float:right;'><table>";
	                                                        }
													}
													
												?>
													</tr>
													</table>
                                            </div>
												
                                        </div>
                                        
										<div class="form-group">
                                            <div class="col-sm-2">&nbsp;</div>
                                            <div class="col-sm-10">
                                               &nbsp; <button type="submit" class="btn btn-primary">Submit</button>
                                              
                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                            </div>
                                        </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Input -->
            </div>


        