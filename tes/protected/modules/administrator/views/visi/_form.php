<script type="text/javascript">
    function hilang()
    {
     document.getElementById('pesan').style.display = 'none';
    }
</script>
<?php
	foreach(Yii::app()->user->getFlashes() as $key => $value)
	{
			echo '

				<div onclick = "hilang()" id = "pesan" style="margin: 15px;" class="alert alert-'.$key.'">
				× &nbsp;
				   '.$value.'
				</div>

			';
	}    
?>
<div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'role-form',
                                        // Please note: When you enable ajax validation, make sure the corresponding
                                        // controller action is handling ajax validation correctly.
                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                        // See class documentation of CActiveForm for details on this.
                                        'htmlOptions' => array('style' => 'min-height:800px;'),
                                        'enableClientValidation' => true , 
                                        'enableAjaxValidation'=>false,
                                    )); ?>
									
										
									
                                       <div class="form-group" style="margin-left:68px;">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'visi'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                   
                                                     <?php 
                                                                    
                                                                     $this->widget('application.extensions.cleditor.ECLEditor', array(
                                                                    'model'=>$model,
                                                                    'attribute'=>'visi', //Model attribute name. Nome do atributo do modelo.
                                                                    'options'=>array(
                                                                        'width'=>'600',
                                                                        'height'=>250,
                                                                        'useCSS'=>true,
                                                                    ),
                                                                    'value'=>$model->visi, //If you want pass a value for the widget. I think you will. Se você precisar passar um valor para o gadget. Eu acho irá.
                                                                ));
                                                               ?>

                                                    <?php echo $form->error($model,'visi' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										
										<div class="form-group" style="margin-left:68px;">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'misi'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                   
                                                     <?php 
                                                                    
                                                                     $this->widget('application.extensions.cleditor.ECLEditor', array(
                                                                    'model'=>$model,
                                                                    'attribute'=>'misi', //Model attribute name. Nome do atributo do modelo.
                                                                    'options'=>array(
                                                                        'width'=>'600',
																		'id' => 'misi',
                                                                        'height'=>350,
                                                                        'useCSS'=>true,
                                                                    ),
                                                                    'value'=>$model->misi, //If you want pass a value for the widget. I think you will. Se você precisar passar um valor para o gadget. Eu acho irá.
                                                                ));
                                                               ?>

                                                    <?php echo $form->error($model,'misi' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
                                        <div class="form-group" style="margin-left:68px;">
                                            <div class="col-sm-2">&nbsp;</div>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                              
                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                            </div>
                                        </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Input -->
            </div>