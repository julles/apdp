
<script type="text/javascript">
    function hilang()
    {
     document.getElementById('pesan').style.display = 'none';
    }
</script>
<?php
  $base = Yii::app()->baseUrl."/admin/";
?>

<?php
	echo CHtml::beginForm();
?>
<div class="lock-holder">      

    	<div class="form-group pull-left input-username">
			<div class="input-group">
				<input type="text" class="form-control" name = 'username' placeholder="Username">
				<span class="input-group-addon"><i class="fa fa-user"></i></span>    
			</div>
		</div>
		<i class="fa fa-ellipsis-h dot-left"></i>
		<i class="fa fa-ellipsis-h dot-right"></i>
		<div class="form-group pull-right input-password">
			<div class="input-group">
                <input type="password" class="form-control " placeholder="Password" name = 'password'>
                <span class="input-group-addon"><i class="fa fa-key"></i></span>
			</div>
		</div>    
	</div>
    <!-- End Lock Holder -->
    
    <!-- Avatar -->
    <div class="avatar">
    	<img src="<?php echo Yii::app()->baseUrl; ?>/images/pln.jpg" alt="" height = '98'>
    </div> 
    <!-- End Avatar -->
    
    <!-- Submit -->
	<div class="submit">
		<button type="submit" name = 'cmd_login' class="btn btn-success btn-submit"><i class="fa fa-unlock"></i> Submit</button>   
	</div>

	     <?php
                    foreach(Yii::app()->user->getFlashes() as $key => $value)
                    {
                          /*  echo '

                                <div onclick = "hilang()" id = "pesan" style="margin: 15px;" class="alert alert-'.$key.'">
                                × &nbsp;
                                   '.$value.'
                                </div>

                            ';*/

                            echo "<script>alert('$value');</script>";
                    }    
                ?>
	<?php
						echo CHtml::endForm();
					?>