<?php 
class dbimexport
{
    private $db_config = Array();
    private $link = NULL;
    public $download = false;
    public $download_path = "";
    public $file_name = NULL;
    public $file_ext = "xml";
    public $import_path = "";
    public function __construct( $db_config = NULL){
       if( isset($db_config)){
            $this->db_config = $db_config;
            $this->setconnection();
        }else{
            die("Database configuration missing in dbimexport" );
        }
    }
    private function setconnection()
    {
        $this->link = mysql_connect($this->db_config['host'], $this->db_config['user'], $this->db_config['password']);

        if (!$this->link) 
        {
            die('Not connected : ' . mysql_error());
        }

        $db_selected = mysql_select_db($this->db_config['database'], $this->link);

        if (!$db_selected) 
        {
            die ("Can't use {$db_config['database']} : " . mysql_error());
        }
    }
    
    private function execute( $sql )
    {
      return mysql_query( $sql );
    }
 
    public function export()
    {
        $dom = new DOMDocument ( '1.0' );
        $database_name = $this->db_config['database'];
         
        $database = $dom->createElement ( 'database' );
        $database = $dom->appendChild ( $database );
        $database->setAttribute ( 'name', $database_name);
 
        $schema = $dom->createElement ( 'schema' );
        $schema = $dom->appendChild ( $schema ); 
        $tableQuery = $this->execute ( "SHOW TABLES FROM {$this->db_config['database']}" );        

        while ( $tableRow = mysql_fetch_row ( $tableQuery ) )
        { 
            $table = $dom->createElement ( 'table' );
            $table = $dom->appendChild ( $table );
            $table->setAttribute ( 'name', $tableRow [ 0 ] );
             
            $fieldQuery = $this->execute ( "DESCRIBE $tableRow[0]" );
            
            while ( $fieldRow = mysql_fetch_assoc ( $fieldQuery ) )
            { 
                $field = $dom->createElement ( 'field' );
                $field = $dom->appendChild ( $field );
                $field->setAttribute ( 'name', $fieldRow [ 'Field' ] );
                $field->setAttribute ( 'name', $fieldRow [ 'Field' ] );
                $field->setAttribute ( 'type', $fieldRow [ 'Type' ]);
                $field->setAttribute ( 'null', strtolower ( $fieldRow [ 'Null' ] ) );
                 
                if ( $fieldRow [ 'Default' ] != '' )
                {
                    $field->setAttribute ( 'default', strtolower ( $fieldRow [ 'Default' ] ) );
                }
 
                if ( $fieldRow [ 'Key' ] != '' )
                {
                    $field->setAttribute ( 'key', strtolower ( $fieldRow [ 'Key' ] ) );
                }
 
                if ( $fieldRow [ 'Extra' ] != '' )
                {
                    $field->setAttribute ( 'extra', strtolower ( $fieldRow [ 'Extra' ] ) );
                }
                 
                $table->appendChild ( $field );
            }
             
            $schema->appendChild ( $table );
        }
         
        $database->appendChild ( $schema );
    
         
        $tableQuery = $this->execute ( "SHOW TABLES FROM {$this->db_config['database']}" );
         
        $data = $dom->createElement ( 'data' );
        $data = $dom->appendChild ( $data );
        $dom->appendChild ( $data );

        while ( $tableRow = mysql_fetch_row ( $tableQuery ) )
        { 
            $descQuery = $this->execute ( "DESCRIBE {$tableRow[0]}" );
            $schema = Array();
            while ( $row = mysql_fetch_assoc ( $descQuery ) )
            {
               $schema[$row['Field']] = array
                                        (
                                            "Type" =>$row['Type'],
                                            "Null" =>$row['Null'],
                                            "Key" =>$row['Key'],
                                            "Default" =>$row['Default'],
                                            "Extra" =>$row['Extra']
                                        );
            }

            $rows = $this->execute ( "SELECT * FROM {$tableRow[0]}" );            
            $table = $dom->createElement ($tableRow[0]);
            $table = $dom->appendChild ( $table );

            $data->appendChild ( $table );

            while ( $row = mysql_fetch_assoc ( $rows ) )
            { 
                $data_row = $dom->createElement ( 'row' );
                $data_row = $dom->appendChild ( $data_row );
                $table ->appendChild (  $data_row );
                 
                foreach( $row as $key => $vale )
                {
					$val = utf8_decode($vale);
                    if( strstr($schema[$key]['Type'], 'int') || strstr($schema[$key]['Type'], 'float') || strstr($schema[$key]['Type'], 'date') || strstr($schema[$key]['Type'], 'time') )
                    {
                        $field = $dom->createElement ($key,$val);
                        $field = $dom->appendChild ( $field );
                        $data_row->appendChild ( $field );

                    }
                    else
                    {
                        $field = $dom->createElement ($key);
                        $field = $dom->appendChild ( $field );
                        $data_row->appendChild ( $field );
                        $cdataNode = $dom->createCDATASection($this->text2normalize($val));
                        $cdataNode = $dom->appendChild ( $cdataNode );
                        $field->appendChild ( $cdataNode );
                    }                  
                }
            }
        }
         
        $database->appendChild ( $data );

         $database_name = ( isset($this->file_name) ) ? $this->file_name : $database_name;
 
        $dom->formatOutput = true;
        $dom->saveXML ();
         
        if( $this->download )
        {
            $filename =  "{$this->download_path}/" . time() . $this->file_ext ;
            $xml = $dom->save ( $filename );

            header('Content-type: text/appdb');
            header('Content-Disposition: attachment; filename="' . $database_name . '.' . $this->file_ext . '"');
            readfile($filename);
            @unlink($filename);
            exit;
        }
        else
        {
            $filename =  "{$this->download_path}/{$database_name}." . $this->file_ext;
            $xml = $dom->save ( $filename );
        }
    }
 
    function import()
    {
      
        if( $this->import_path == "" || !file_exists($this->import_path))
        {
            die("Database file not exists");
        }    
        
        $dom = new DOMDocument();
			
		//$xml = simplexml_load_string( utf8_encode($rss) );
        $dom->loadXML(file_get_contents($this->import_path));
 
        $schema = $dom->getElementsByTagName('schema');
        $tables = $schema->item(0)->getElementsByTagName( "table" );
        

        foreach( $tables as $table)
        { 
            $name = $table->getAttribute('name');
            $fields = $table->getElementsByTagName( "field" );
 
            $dable_data = $dom->getElementsByTagName($name);
            $rows = $dable_data->item(0)->getElementsByTagName( "row" );

            $sqlbody = "";
            foreach( $rows as $row )
            {
                $tmp_body = "";
                $tmp_head = "";
                foreach( $fields as $field )
                {
                    $field_name = $field->getAttribute('name');
                    $field_type = $field->getAttribute('type');
                    $entry = $row->getElementsByTagName($field_name);
                    $field_value = $entry->item(0)->nodeValue;
                    $field_value = $this->quote_smart($field_value);

                    $tmp_body .= ($tmp_body == "" ) ? $field_value : ",{$field_value}";

                    if( $tmp_body != "" ) $tmp_head .= ($tmp_head == "" ) ? "`{$field_name}`" : ",`{$field_name}`";
                }
               
                 $sqlbody .=  ($sqlbody == "") ?  "($tmp_body)\n" :  ",($tmp_body)\n";
            }
			if($name<>'manage_database'){
				$this->execute("TRUNCATE TABLE `{$name}` ");
				$query = "INSERT INTO `{$name}` ({$tmp_head}) VALUES {$sqlbody}";
				$this->execute($query);
			}
        }
    }

    function quote_smart($value)
	{ 
		if (get_magic_quotes_gpc()) {
			$value = stripslashes($value);
		} 
		if (!is_numeric($value)) {
			$value = "'" . mysql_real_escape_string($value) . "'";
		}
		return $value;
	}
    
    function text2normalize($str = "")
    {
        $arr_busca = array('�','�','�','�','�','�','�','�','�', '�','�','�','�','�','�','�','�','�','�','�','�','�','�','�', '�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�');
        $arr_susti = array('a','a','a','a','a','A','A','A','A','e','e','e','E','E','E','i','i','i','I','I','I','o','o','o','o','o','O','O','O','O','u','u','u','U','U','U','c','C','N','n');
        return trim(str_replace($arr_busca, $arr_susti, $str));
    }
}
?>