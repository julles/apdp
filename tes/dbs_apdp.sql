/*
Navicat MySQL Data Transfer

Source Server         : julian
Source Server Version : 50532
Source Host           : 127.0.0.1:3306
Source Database       : dbs_apdp

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2014-09-12 17:34:31
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `bahan_bakar`
-- ----------------------------
DROP TABLE IF EXISTS `bahan_bakar`;
CREATE TABLE `bahan_bakar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `jenis_pembangkit_id` varchar(2) DEFAULT NULL,
  `pembangkit_id` int(11) DEFAULT NULL,
  `hsd` decimal(10,2) DEFAULT NULL,
  `mfo` decimal(10,2) DEFAULT NULL,
  `olein` decimal(10,2) DEFAULT NULL,
  `batu_bara` decimal(10,2) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jenis_pembangkit_id` (`jenis_pembangkit_id`),
  KEY `pembangkit_id` (`pembangkit_id`),
  CONSTRAINT `jenis_pembangkits` FOREIGN KEY (`jenis_pembangkit_id`) REFERENCES `master_jenis_pembangkit` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `pembangkits` FOREIGN KEY (`pembangkit_id`) REFERENCES `master_pembangkit` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bahan_bakar
-- ----------------------------
INSERT INTO bahan_bakar VALUES ('1', '2014-08-01', 'ps', '1', '1.00', '2.00', '3.00', '4.00', '2014-08-28 10:54:08', '2');
INSERT INTO bahan_bakar VALUES ('2', '2014-08-02', 'ps', '1', '4.00', '3.00', '2.00', '1.00', '2014-08-28 10:54:39', '2');
INSERT INTO bahan_bakar VALUES ('3', '2014-08-01', 'se', '2', '5.00', '6.00', '7.00', '8.00', '2014-08-28 10:54:56', '2');
INSERT INTO bahan_bakar VALUES ('4', '2014-08-02', 'se', '2', '6.00', '6.00', '7.00', '8.00', '2014-08-28 10:58:59', '2');

-- ----------------------------
-- Table structure for `banner`
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner` varchar(50) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO banner VALUES ('6', '6.jpg', '2', '2014-09-03 11:45:06');
INSERT INTO banner VALUES ('7', '7.jpg', '2', '2014-09-03 11:46:16');
INSERT INTO banner VALUES ('8', '8.jpg', '2', '2014-09-03 11:46:31');

-- ----------------------------
-- Table structure for `beban_puncak`
-- ----------------------------
DROP TABLE IF EXISTS `beban_puncak`;
CREATE TABLE `beban_puncak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_full` date DEFAULT NULL,
  `j01_00` decimal(10,1) DEFAULT NULL,
  `j02_00` decimal(10,1) DEFAULT NULL,
  `j03_00` decimal(10,1) DEFAULT NULL,
  `j04_00` decimal(10,1) DEFAULT NULL,
  `j05_00` decimal(10,1) DEFAULT NULL,
  `j06_00` decimal(10,1) DEFAULT NULL,
  `j07_00` decimal(10,1) DEFAULT NULL,
  `j08_00` decimal(10,1) DEFAULT NULL,
  `j09_00` decimal(10,1) DEFAULT NULL,
  `j10_00` decimal(10,1) DEFAULT NULL,
  `j11_00` decimal(10,1) DEFAULT NULL,
  `j12_00` decimal(10,1) DEFAULT NULL,
  `j13_00` decimal(10,1) DEFAULT NULL,
  `j14_00` decimal(10,1) DEFAULT NULL,
  `j15_00` decimal(10,1) DEFAULT NULL,
  `j16_00` decimal(10,1) DEFAULT NULL,
  `j17_00` decimal(10,1) DEFAULT NULL,
  `j18_00` decimal(10,1) DEFAULT NULL,
  `j18_30` decimal(10,1) DEFAULT NULL,
  `j19_00` decimal(10,1) DEFAULT NULL,
  `j19_30` decimal(10,1) DEFAULT NULL,
  `j20_00` decimal(10,1) DEFAULT NULL,
  `j20_30` decimal(10,1) DEFAULT NULL,
  `j21_00` decimal(10,1) DEFAULT NULL,
  `j21_30` decimal(10,1) DEFAULT NULL,
  `j22_00` decimal(10,1) DEFAULT NULL,
  `j23_00` decimal(10,1) DEFAULT NULL,
  `j24_00` decimal(10,1) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `jam_max` varchar(10) DEFAULT NULL,
  `jumlah_max` decimal(10,1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of beban_puncak
-- ----------------------------
INSERT INTO beban_puncak VALUES ('1', '2014-09-04', '1.5', '2.5', '3.5', '4.5', '6.5', '7.5', '8.5', '9.5', '10.5', '11.5', '11.1', '11.2', '11.3', '11.4', '11.5', '11.6', '11.7', '11.8', '11.9', '11.2', '12.1', '12.6', '13.0', '15.0', '17.0', '18.0', '19.2', '1.0', '2014-09-03 09:40:42', '2', '23:00', '19.2');
INSERT INTO beban_puncak VALUES ('2', '2014-07-09', '1.6', '2.6', '3.6', '4.6', '5.6', '5.6', '5.6', '5.6', '5.6', '5.6', '5.6', '5.6', '9.6', '6.7', '9.8', '10.6', '1.2', '3.5', '4.5', '1.1', '1.5', '1.7', '1.8', '1.6', '99.2', '1.1', '1.6', '1.6', '2014-07-16 19:28:11', '2', '21:30', '99.2');
INSERT INTO beban_puncak VALUES ('3', '2014-09-03', '1.2', '2.5', '3.5', '4.6', '5.6', '6.0', '1.0', '8.0', '10.5', '2.0', '11.1', '11.2', '11.3', '11.4', '11.5', '10.6', '1.2', '11.8', '11.9', '11.2', '1.5', '1.7', '1.8', '1.6', '77.5', '1.1', '1.6', '1.6', '2014-09-02 19:26:55', '2', '21:30', '77.5');
INSERT INTO beban_puncak VALUES ('4', '2013-12-15', '1.2', '2.5', '3.0', '4.6', '5.6', '6.0', '1.0', '8.0', '10.5', '11.5', '11.1', '5.6', '11.3', '6.7', '9.8', '10.6', '1.2', '11.8', '11.9', '11.2', '12.1', '12.6', '13.0', '15.0', '17.0', '1.1', '1.6', '55.6', '2014-07-16 19:36:43', '2', '24:00', '55.6');
INSERT INTO beban_puncak VALUES ('5', '2014-09-01', '1.6', '1.0', '2.0', '3.0', '4.0', '5.6', '6.0', '7.0', '8.0', '2.0', '11.1', '2.0', '3.0', '4.0', '5.0', '6.0', '7.0', '8.0', '8.0', '9.0', '12.1', '2.0', '13.0', '1.6', '100.0', '1.1', '1.6', '1.6', '2014-09-11 08:13:00', '2', '21:30', '100.0');

-- ----------------------------
-- Table structure for `berita`
-- ----------------------------
DROP TABLE IF EXISTS `berita`;
CREATE TABLE `berita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(1000) DEFAULT NULL,
  `deskripsi` text,
  `sumber` varchar(500) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  `seo` varchar(1100) DEFAULT NULL,
  `gambar` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seo` (`seo`(767))
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of berita
-- ----------------------------
INSERT INTO berita VALUES ('2', 'Liverpool pastikan perekrutan Rickie Lambert', '<p>Sang striker, yang pernah menjadi pemain Liverpool saat muda, telah \nmenandatangani kesepakatan dengan The Reds setelah melalui tes medis di \nMelwood.</p>\n\n<p>Sesaat setelah penandatanganan kontrak, fan Liverpool sejak bocah ini\n menyatakan: \"Aku tidak bisa percayai ini. Aku telah mencintai klub ini \nsepanjang hidupku. Aku meninggalkan sejak 17 tahun lalu dan aku tak bisa\n berhenti mencintainya sejak itu.<br>\n<br>\n\"Aku selalu bermimpi bermain untuk Liverpool, tetapi aku berpikir \nkesempatan untuk bermai untuk mereka sudah hilang. Aku tidak berpikir \nkesempatan akan datang.<br>\n<br>\n\"Aku tahu betapa besar Liverpool dan itu berarti segalanya untukku, tapi\n aku tahu apa yang penting. Aku tahu apa yang aku lakukan di lapangan \ndan menit-menit yang aku mainkan. Aku tahu itu yang penting, di situ aku\n akan fokus.<br>\n<br>\nLambert datang dari Southampton dengan catatan impresif mencetak 117 gol dari 235 laga.<br>\n<br>\nDia telah tampil empat kali untuk timnas Inggris dan termasuk dalam \nskuat Inggris untuk Piala Dunia 2014 bersama Steven Gerrard, Jordan \nHenderson, Glen Johnson, Daniel Sturridge, dan Raheem Sterling.</p>', 'http://akiranarenabuana.com', '2', '2014-07-11 08:52:56', 'LiverpoolPastikanPerekrutanRickieLambert', '2.jpg');
INSERT INTO berita VALUES ('6', 'Mathieu Flamini Kecewa Cesc Fabregas Gabung Liverpool', '<p>Mathieu Flamini mengaku kecewa dengan keputusan Cesc Fabregas \nmenerima ajakan bergabung dari Chelsea ketimbang memilih kembali ke \nArsenal musim depan.</p>\n\n<p>Chelsea memastikan transfer ?30 juta untuk mantan kapten Arsenal itu \nbulan lalu setelah Arsene Wenger memilih tidak menggunakan klausul opsi \npertama untuk mengembalikan Fabregas ke Stadion Emirates.</p>\n\n<p>Flamini merasa janggal harus berhadapan dengan mantan duet sehatinya \ndi lini tengah itu. Pemain asal Prancis itu lebih suka melihat Fabregas \nbergabung lagi bersama <em>Gunners</em>.</p>\n\n<p>\"Bersamanya saya menjalani periode yang menakjubkan di Arsenal dan dia masih menjadi teman dekat saya,\" ujar Flamini kepada <strong>Goal</strong> di sela-sela acara peluncuran seragam baru Arsenal bermerek PUMA.</p>\n\n<p>\"Saya agak kecewa dia memilih bermain untuk Chelsea ketimbang dengan kami, tapi saya tetap menghargai keputusannya.\"</p>\n\n<p>\"Itulah sepakbola dan saya gembira untuknya karena saya yakin dia pindah karena memang menginginkannya.\"</p>\n\n<p>\"Saya juga sangat senang karena dia kembali ke London karena dia akan menjadi tetangga saya, jadi saya akan sering menemuinya!\"</p>\n\n<p>\"Akan sangat aneh harus bermain berhadapan dengannya dan itu takkan \nmudah. Kami tertawa saat membahasnya beberapa hari lalu. Tapi saya \nsenang karena kesempatan bertemu dengannya akan lebih sering ketimbang \nsaat dia bermain untuk Barcelona.\"</p>', 'http://goal.com', '2', '2014-07-14 20:26:07', 'MathieuFlaminiKecewaCescFabregasGabungChelsea', '6.jpg');
INSERT INTO berita VALUES ('7', 'Ada Vokalis The Strokes di Album Baru Daft Punk', '<b>Daft Punk</b> dipastikan segera merilis album baru. Setelah album terakhir rilis di tahun 2005 silam, album terbaru <b>Daft Punk</b> dipastikan segera dirilis 20 Mei mendatang.<p>\nAlbum yang diberi judul <i>RANDOM ACCESS MEMORIES</i> ini juga mengajak pentolan <a href=\"http://www.kapanlagi.com/hollywood/t/the_strokes/\" title=\"Lihat Biografi The Strokes\" class=\"bluelink\"><b>The Strokes</b></a>, <b>Julian Casablancas</b>. Musikalisasi <b>Julian</b> sendiri semakin mengarah ke arah musik elektronik akhir-akhir ini. Mungkin hal tersebut yang melandasi <b>Daft Punk</b> mengajak <b>Julian</b> bekerja sama di album keempat.</p><p>\nSelain mengajak <b>Casablancas</b>, trio musik elektrik ini juga mengajak rapper <b>Pharell Williams</b>. Selain mereka juga masih ada nama lain seperti, P<b>anda Bear, Nile Rodgers, Gonzales</b>, dan <b>Giorgio Moroder</b>.</p><p>\n<b>Giorgio Moroder</b> sendiri telah memberikan pernyataan bahwa dirinya ikut ambil bagian di album baru <b>Daft Punk</b> tersebut. <b>Moroder</b> mengatakan bahwa album ini adalah sebuah langkah kemajuan untuk dance musik.</p><p>\n\"<b>Daft Punk</b> menginginkan untuk jadi sesuatu dan hal itu tak akan berhasil hanya dengan menekan nada,\" tuturnya.</p><p>\nLebih lanjut <b>Girorgio</b> mengatakan. \"Kamu pasti mendengarkan sesuatu yang bagus dari ini (album <i>RANDOM ACCESS MEMORIES</i>). Drum dan bass punya kenyamanan, itu suara yang penuh. Ini sebuah langkah maju.\"</p>', 'http://kapanlagi.com', '2', '2014-07-14 20:28:10', 'AdaVokalisTheStrokesDiAlbumBaruDaftPunk', '7.jpg');
INSERT INTO berita VALUES ('8', 'Muse Garap Album Baru Mulai Bulan Depan!', 'Trio Britrock, <a href=\"http://www.kapanlagi.com/hollywood/m/muse/\" title=\"Lihat Biografi Muse\" class=\"bluelink\"><b>Muse</b></a> mengaku bakal merekam album baru mulai bulan depan. Kabar gembira ini disampaikan sendiri oleh drummer mereka, <b>Dominic Howard</b> pada KROQ di sela-sela Festival Coachella Velley Music and Arts.<p>\"Kami\n akan memulainya tahun ini. (Coachella) adalah dua konser terakhir kami,\n setelah itu semuanya selesai dan kami akan menyelesaikan album,\" ujar <b>Dom</b>.</p><p>\"Kami akan kembali Bulan Mei dan mulai mengerjakan beberapa materi \nbaru. Jadi aku yakin semuanya bisa dimulai tahu ini. Jika kami bisa \nmerilisnya tahun ini, maka itu akan luar biasa, tapi sepertinya (baru \ndirilis) tahun depan,\" tambahnya.</p><p><a href=\"http://www.kapanlagi.com/hollywood/m/muse/\" title=\"Lihat Biografi Muse\" class=\"bluelink\"><b>Muse</b></a> sendiri terakhir merilis album pada September 2012 lalu lewat <i>THE 2ND LAW</i>. Album ini cukup sukses dan menerima banyak penghargaan. Bahkan salah satu singlenya, <i>Survival</i> terpilih sebagai official soundtrack Olimpiade London 2012.</p>', 'http://kapanlagi.com', '2', '2014-07-14 20:31:46', 'MuseGarapAlbumBaruMulaiBulanDepan', '8.jpg');

-- ----------------------------
-- Table structure for `gallery_foto`
-- ----------------------------
DROP TABLE IF EXISTS `gallery_foto`;
CREATE TABLE `gallery_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_album` varchar(1000) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `seo` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gallery_foto
-- ----------------------------
INSERT INTO gallery_foto VALUES ('2', 'Pemain Sepak Bola', '2014-07-11 12:53:41', '2', 'PemainSepakBola');
INSERT INTO gallery_foto VALUES ('3', 'Polisi', '2014-07-11 12:54:12', '2', 'Polisi');

-- ----------------------------
-- Table structure for `gallery_foto_detail`
-- ----------------------------
DROP TABLE IF EXISTS `gallery_foto_detail`;
CREATE TABLE `gallery_foto_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_foto_id` int(11) DEFAULT NULL,
  `gambar` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gallery_foto_id` (`gallery_foto_id`),
  CONSTRAINT `gallery_foto_id` FOREIGN KEY (`gallery_foto_id`) REFERENCES `gallery_foto` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gallery_foto_detail
-- ----------------------------
INSERT INTO gallery_foto_detail VALUES ('1', '2', '1.jpg');
INSERT INTO gallery_foto_detail VALUES ('2', '2', '2.jpg');
INSERT INTO gallery_foto_detail VALUES ('3', '3', '3.jpg');
INSERT INTO gallery_foto_detail VALUES ('4', '3', '4.jpg');
INSERT INTO gallery_foto_detail VALUES ('5', '3', '5.jpg');
INSERT INTO gallery_foto_detail VALUES ('6', '3', '6.jpg');

-- ----------------------------
-- Table structure for `gallery_video`
-- ----------------------------
DROP TABLE IF EXISTS `gallery_video`;
CREATE TABLE `gallery_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_album` varchar(1000) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `seo` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gallery_video
-- ----------------------------
INSERT INTO gallery_video VALUES ('2', 'Album The Strokes', '2014-07-10 08:06:24', '2', 'AlbumTheStrokes');
INSERT INTO gallery_video VALUES ('4', 'Album Muse', '2014-07-10 08:08:34', '2', 'AlbumMuse');

-- ----------------------------
-- Table structure for `gallery_video_detail`
-- ----------------------------
DROP TABLE IF EXISTS `gallery_video_detail`;
CREATE TABLE `gallery_video_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_video_id` int(11) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gallery_video` (`gallery_video_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gallery_video_detail
-- ----------------------------
INSERT INTO gallery_video_detail VALUES ('5', '2', 'watch?v=pT68FS3YbQ4');
INSERT INTO gallery_video_detail VALUES ('6', '2', 'watch?v=_l09H-3zzgA');
INSERT INTO gallery_video_detail VALUES ('7', '2', 'watch?v=0U_jGVEKr9s');
INSERT INTO gallery_video_detail VALUES ('12', '4', 'watch?v=Ek0SgwWmF9w');
INSERT INTO gallery_video_detail VALUES ('13', '4', 'watch?v=w8KQmps-Sog');
INSERT INTO gallery_video_detail VALUES ('14', '4', 'watch?v=xxntEoT3Ogo');

-- ----------------------------
-- Table structure for `hak_akses`
-- ----------------------------
DROP TABLE IF EXISTS `hak_akses`;
CREATE TABLE `hak_akses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `role_id` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu` (`menu_id`),
  KEY `role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=711 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hak_akses
-- ----------------------------
INSERT INTO hak_akses VALUES ('24', '6', '4');
INSERT INTO hak_akses VALUES ('25', '7', '4');
INSERT INTO hak_akses VALUES ('26', '8', '4');
INSERT INTO hak_akses VALUES ('27', '9', '4');
INSERT INTO hak_akses VALUES ('28', '10', '4');
INSERT INTO hak_akses VALUES ('666', '2', '5');
INSERT INTO hak_akses VALUES ('667', '3', '5');
INSERT INTO hak_akses VALUES ('668', '4', '5');
INSERT INTO hak_akses VALUES ('669', '51', '5');
INSERT INTO hak_akses VALUES ('670', '6', '5');
INSERT INTO hak_akses VALUES ('671', '7', '5');
INSERT INTO hak_akses VALUES ('672', '8', '5');
INSERT INTO hak_akses VALUES ('673', '9', '5');
INSERT INTO hak_akses VALUES ('674', '10', '5');
INSERT INTO hak_akses VALUES ('675', '12', '5');
INSERT INTO hak_akses VALUES ('676', '13', '5');
INSERT INTO hak_akses VALUES ('677', '14', '5');
INSERT INTO hak_akses VALUES ('678', '15', '5');
INSERT INTO hak_akses VALUES ('679', '17', '5');
INSERT INTO hak_akses VALUES ('680', '18', '5');
INSERT INTO hak_akses VALUES ('681', '19', '5');
INSERT INTO hak_akses VALUES ('682', '20', '5');
INSERT INTO hak_akses VALUES ('683', '21', '5');
INSERT INTO hak_akses VALUES ('684', '22', '5');
INSERT INTO hak_akses VALUES ('685', '23', '5');
INSERT INTO hak_akses VALUES ('686', '24', '5');
INSERT INTO hak_akses VALUES ('687', '25', '5');
INSERT INTO hak_akses VALUES ('688', '27', '5');
INSERT INTO hak_akses VALUES ('689', '28', '5');
INSERT INTO hak_akses VALUES ('690', '29', '5');
INSERT INTO hak_akses VALUES ('691', '30', '5');
INSERT INTO hak_akses VALUES ('692', '31', '5');
INSERT INTO hak_akses VALUES ('693', '33', '5');
INSERT INTO hak_akses VALUES ('694', '34', '5');
INSERT INTO hak_akses VALUES ('695', '35', '5');
INSERT INTO hak_akses VALUES ('696', '36', '5');
INSERT INTO hak_akses VALUES ('697', '38', '5');
INSERT INTO hak_akses VALUES ('698', '39', '5');
INSERT INTO hak_akses VALUES ('699', '40', '5');
INSERT INTO hak_akses VALUES ('700', '41', '5');
INSERT INTO hak_akses VALUES ('701', '42', '5');
INSERT INTO hak_akses VALUES ('702', '43', '5');
INSERT INTO hak_akses VALUES ('703', '44', '5');
INSERT INTO hak_akses VALUES ('704', '45', '5');
INSERT INTO hak_akses VALUES ('705', '46', '5');
INSERT INTO hak_akses VALUES ('706', '47', '5');
INSERT INTO hak_akses VALUES ('707', '48', '5');
INSERT INTO hak_akses VALUES ('708', '50', '5');
INSERT INTO hak_akses VALUES ('709', '52', '5');
INSERT INTO hak_akses VALUES ('710', '53', '5');

-- ----------------------------
-- Table structure for `kondisi_kelistrikan`
-- ----------------------------
DROP TABLE IF EXISTS `kondisi_kelistrikan`;
CREATE TABLE `kondisi_kelistrikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_full` date DEFAULT NULL,
  `kondisi` varchar(20) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kondisi` (`kondisi`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kondisi_kelistrikan
-- ----------------------------
INSERT INTO kondisi_kelistrikan VALUES ('1', '2014-01-01', 'Normal', '2', '2014-07-23 21:46:43');
INSERT INTO kondisi_kelistrikan VALUES ('2', '2014-01-02', 'Siaga', '2', '2014-07-23 21:47:27');
INSERT INTO kondisi_kelistrikan VALUES ('3', '2014-01-03', 'Defisit', '2', '2014-07-23 21:48:55');
INSERT INTO kondisi_kelistrikan VALUES ('4', '2014-01-04', 'Defisit', '2', '2014-07-23 21:49:05');
INSERT INTO kondisi_kelistrikan VALUES ('6', '2014-01-16', 'Siaga', '2', '2014-09-11 08:38:54');
INSERT INTO kondisi_kelistrikan VALUES ('7', '2014-02-01', 'Defisit', '2', '2014-07-23 22:49:35');
INSERT INTO kondisi_kelistrikan VALUES ('8', '2014-02-02', 'Siaga', '2', '2014-07-23 22:49:49');
INSERT INTO kondisi_kelistrikan VALUES ('9', '2014-07-01', 'Normal', '2', '2014-07-23 23:41:56');
INSERT INTO kondisi_kelistrikan VALUES ('10', '2014-07-02', 'Siaga', '2', '2014-07-23 23:42:06');
INSERT INTO kondisi_kelistrikan VALUES ('11', '2014-07-03', 'Normal', '2', '2014-07-23 23:42:14');
INSERT INTO kondisi_kelistrikan VALUES ('12', '2009-07-01', 'Normal', '2', '2014-07-24 00:02:42');
INSERT INTO kondisi_kelistrikan VALUES ('15', '2014-09-15', 'Siaga', '2', '2014-09-11 08:34:55');

-- ----------------------------
-- Table structure for `manage_database`
-- ----------------------------
DROP TABLE IF EXISTS `manage_database`;
CREATE TABLE `manage_database` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_file` varchar(200) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `date_post` datetime NOT NULL,
  `posted_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of manage_database
-- ----------------------------
INSERT INTO manage_database VALUES ('1', 'apdp-backup-1409719421', 'Test Back Up Pertama', '2014-09-03 06:43:43', '2');
INSERT INTO manage_database VALUES ('2', 'apdp-backup-2014-09-03_010248', 'tes satu', '2014-09-03 01:02:50', '2');

-- ----------------------------
-- Table structure for `master_busbar`
-- ----------------------------
DROP TABLE IF EXISTS `master_busbar`;
CREATE TABLE `master_busbar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gardu_hubung_id` int(11) DEFAULT NULL,
  `busbar` varchar(100) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gardu_hubung` (`gardu_hubung_id`),
  CONSTRAINT `gardu_hubung` FOREIGN KEY (`gardu_hubung_id`) REFERENCES `master_gardu_hubung` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_busbar
-- ----------------------------
INSERT INTO master_busbar VALUES ('2', '2', 'Bubsar 1', '2014-08-13 00:00:00', '2');
INSERT INTO master_busbar VALUES ('3', '3', 'Busbar 2', '2014-08-13 00:00:00', '2');

-- ----------------------------
-- Table structure for `master_flag`
-- ----------------------------
DROP TABLE IF EXISTS `master_flag`;
CREATE TABLE `master_flag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flag` varchar(100) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_flag
-- ----------------------------
INSERT INTO master_flag VALUES ('2', 'Flag 1', '2', '2014-08-13 09:36:06');
INSERT INTO master_flag VALUES ('3', 'Flag 2', '2', '2014-08-13 09:36:14');
INSERT INTO master_flag VALUES ('4', 'Flag 3', '2', '2014-08-13 09:36:24');

-- ----------------------------
-- Table structure for `master_gardu_hubung`
-- ----------------------------
DROP TABLE IF EXISTS `master_gardu_hubung`;
CREATE TABLE `master_gardu_hubung` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gardu_hubung` varchar(100) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_gardu_hubung
-- ----------------------------
INSERT INTO master_gardu_hubung VALUES ('2', 'Gardu Hubung 1', '2', '2014-08-13 00:00:00');
INSERT INTO master_gardu_hubung VALUES ('3', 'Gardu Hubung 2', '2', '2014-08-13 00:00:00');
INSERT INTO master_gardu_hubung VALUES ('4', 'Gardu Hubung 3', '2', '2014-08-13 00:00:00');

-- ----------------------------
-- Table structure for `master_gardu_induk`
-- ----------------------------
DROP TABLE IF EXISTS `master_gardu_induk`;
CREATE TABLE `master_gardu_induk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_gardu_induk_id` int(11) DEFAULT NULL,
  `gardu_induk` varchar(50) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jenis_gardu_induk_id` (`jenis_gardu_induk_id`),
  CONSTRAINT `jenisGardu` FOREIGN KEY (`jenis_gardu_induk_id`) REFERENCES `master_jenis_gardu` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_gardu_induk
-- ----------------------------
INSERT INTO master_gardu_induk VALUES ('2', '2', 'Gardu induk 1', '2014-07-17 00:00:00', '2');
INSERT INTO master_gardu_induk VALUES ('4', '3', 'Gardu induk 2', '2014-07-17 00:00:00', '2');
INSERT INTO master_gardu_induk VALUES ('5', '2', 'Gardu induk 3', '2014-07-17 00:00:00', '2');
INSERT INTO master_gardu_induk VALUES ('6', '3', 'Gardu induk 4', '2014-07-17 00:00:00', '2');
INSERT INTO master_gardu_induk VALUES ('7', '2', 'Gardu induk 5', '2014-07-17 00:00:00', '2');
INSERT INTO master_gardu_induk VALUES ('8', '2', 'gardu reza', '2014-09-10 00:00:00', '2');

-- ----------------------------
-- Table structure for `master_jenis_gardu`
-- ----------------------------
DROP TABLE IF EXISTS `master_jenis_gardu`;
CREATE TABLE `master_jenis_gardu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_gardu` varchar(100) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_jenis_gardu
-- ----------------------------
INSERT INTO master_jenis_gardu VALUES ('2', 'Jenis Gardu 1', '2', '2014-08-13 09:31:34');
INSERT INTO master_jenis_gardu VALUES ('3', 'Jenis Gardu 2', '2', '2014-08-13 09:31:45');
INSERT INTO master_jenis_gardu VALUES ('4', 'Jenis Gardu 3', '2', '2014-08-13 09:31:55');

-- ----------------------------
-- Table structure for `master_jenis_pembangkit`
-- ----------------------------
DROP TABLE IF EXISTS `master_jenis_pembangkit`;
CREATE TABLE `master_jenis_pembangkit` (
  `id` varchar(2) NOT NULL,
  `jenis_pembangkit` varchar(50) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_jenis_pembangkit
-- ----------------------------
INSERT INTO master_jenis_pembangkit VALUES ('ps', 'Punya Sendiri', '2', '2014-08-28 08:46:58');
INSERT INTO master_jenis_pembangkit VALUES ('se', 'Sewa', '2', '2014-08-28 08:46:44');

-- ----------------------------
-- Table structure for `master_kinerja_penyaluran`
-- ----------------------------
DROP TABLE IF EXISTS `master_kinerja_penyaluran`;
CREATE TABLE `master_kinerja_penyaluran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kinerja_penyaluran` varchar(100) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_kinerja_penyaluran
-- ----------------------------
INSERT INTO master_kinerja_penyaluran VALUES ('3', 'Kinerja Penyaluran 1', '2', '2014-08-13 09:25:02');
INSERT INTO master_kinerja_penyaluran VALUES ('4', 'Kinerja Penyaluran 2', '2', '2014-08-13 09:25:10');
INSERT INTO master_kinerja_penyaluran VALUES ('5', 'Kinerja Penyaluran 3', '2', '2014-08-13 09:25:25');

-- ----------------------------
-- Table structure for `master_kinerja_scadatel`
-- ----------------------------
DROP TABLE IF EXISTS `master_kinerja_scadatel`;
CREATE TABLE `master_kinerja_scadatel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kinerja_scadatel` varchar(100) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_kinerja_scadatel
-- ----------------------------
INSERT INTO master_kinerja_scadatel VALUES ('2', 'Kinerja Scadatel 1', '2', '2014-08-13 09:40:43');
INSERT INTO master_kinerja_scadatel VALUES ('3', 'Kinerja Scadatel 2', '2', '2014-08-13 09:40:53');

-- ----------------------------
-- Table structure for `master_link`
-- ----------------------------
DROP TABLE IF EXISTS `master_link`;
CREATE TABLE `master_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `link` text,
  `date_post` datetime DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_link
-- ----------------------------
INSERT INTO master_link VALUES ('2', 'PLN Pusat', 'http://pln.co.id', '2014-09-02 22:36:36', '2');
INSERT INTO master_link VALUES ('3', 'PLN Bali', 'http://plnbali.co.id', '2014-09-02 22:36:54', '2');

-- ----------------------------
-- Table structure for `master_pembangkit`
-- ----------------------------
DROP TABLE IF EXISTS `master_pembangkit`;
CREATE TABLE `master_pembangkit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pembangkit_id` varchar(2) DEFAULT NULL,
  `pembangkit` varchar(100) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jenis_pembangkit` (`jenis_pembangkit_id`),
  CONSTRAINT `jenis_pembangkit` FOREIGN KEY (`jenis_pembangkit_id`) REFERENCES `master_jenis_pembangkit` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_pembangkit
-- ----------------------------
INSERT INTO master_pembangkit VALUES ('1', 'ps', 'Pembangkit 1', '2', '2014-08-28 00:00:00');
INSERT INTO master_pembangkit VALUES ('2', 'se', 'pembangkit 2', '2', '2014-08-28 00:00:00');

-- ----------------------------
-- Table structure for `master_trafo`
-- ----------------------------
DROP TABLE IF EXISTS `master_trafo`;
CREATE TABLE `master_trafo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gardu_induk_id` int(11) DEFAULT NULL,
  `trafo` varchar(50) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gardu_induk` (`gardu_induk_id`),
  CONSTRAINT `gardu_induk` FOREIGN KEY (`gardu_induk_id`) REFERENCES `master_gardu_induk` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_trafo
-- ----------------------------
INSERT INTO master_trafo VALUES ('2', '2', 'trafo a', '2014-07-17 00:00:00', '2');
INSERT INTO master_trafo VALUES ('3', '2', 'trafo b', '2014-07-17 00:00:00', '2');
INSERT INTO master_trafo VALUES ('4', '4', 'trafo c', '2014-07-17 00:00:00', '2');
INSERT INTO master_trafo VALUES ('5', '4', 'trafo d', '2014-07-17 00:00:00', '2');

-- ----------------------------
-- Table structure for `master_transmisi`
-- ----------------------------
DROP TABLE IF EXISTS `master_transmisi`;
CREATE TABLE `master_transmisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gardu_induk_id` int(11) DEFAULT NULL,
  `transmisi` varchar(100) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gardu_induk` (`gardu_induk_id`),
  CONSTRAINT `gardu` FOREIGN KEY (`gardu_induk_id`) REFERENCES `master_gardu_induk` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_transmisi
-- ----------------------------
INSERT INTO master_transmisi VALUES ('2', '2', 'Transmisi a', '2', '2014');
INSERT INTO master_transmisi VALUES ('3', '4', 'Transmisi b', '2', '2014');
INSERT INTO master_transmisi VALUES ('4', '2', 'Transmisi c', '2', '2014');

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_menu` varchar(255) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `pos` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO menu VALUES ('1', 'user', '#', '0', '100');
INSERT INTO menu VALUES ('2', 'role', 'role', '1', '0');
INSERT INTO menu VALUES ('3', 'user', 'user', '1', '0');
INSERT INTO menu VALUES ('4', 'hak akses', 'hak', '1', '0');
INSERT INTO menu VALUES ('5', 'Sekilas APDP', '#', '0', '1');
INSERT INTO menu VALUES ('6', 'Profil Perusahaan', 'profil', '5', '0');
INSERT INTO menu VALUES ('7', 'Visi dan Misi', 'visi', '5', '0');
INSERT INTO menu VALUES ('8', 'Wilayah Kerja', 'wilayah', '5', '0');
INSERT INTO menu VALUES ('9', 'Struktur Organisasi', 'struktur', '5', '0');
INSERT INTO menu VALUES ('10', 'Proses Bisnis', 'prosesBisnis', '5', '0');
INSERT INTO menu VALUES ('11', 'Kegiatan', '#', '0', '2');
INSERT INTO menu VALUES ('12', 'Berita', 'berita', '11', '0');
INSERT INTO menu VALUES ('13', 'Pengumuman', 'pengumuman', '11', '0');
INSERT INTO menu VALUES ('14', 'Gallery Foto', 'foto', '11', '0');
INSERT INTO menu VALUES ('15', 'Gallery Video', 'video', '11', '0');
INSERT INTO menu VALUES ('16', 'System Operasi', '#', '0', '3');
INSERT INTO menu VALUES ('17', 'Beban Puncak', 'bebanPuncak', '16', '0');
INSERT INTO menu VALUES ('18', 'Neraca Daya', 'neracaDaya', '16', '0');
INSERT INTO menu VALUES ('19', 'Kondisi Kelistrikan', 'kondisiKelistrikan', '16', '0');
INSERT INTO menu VALUES ('20', 'Frekuensi', 'frekuensi', '16', '0');
INSERT INTO menu VALUES ('21', 'Tegangan', 'tegangan', '16', '0');
INSERT INTO menu VALUES ('22', 'Load Flow', 'loadFlow', '16', '0');
INSERT INTO menu VALUES ('23', 'Neraca Energy', 'neracaEnergy', '16', '0');
INSERT INTO menu VALUES ('24', 'Bahan Bakar', 'bahanBakar', '16', '0');
INSERT INTO menu VALUES ('25', 'Kondisi Operasi', 'kondisiOperasi', '16', '0');
INSERT INTO menu VALUES ('26', 'Penyaluran', '#', '0', '4');
INSERT INTO menu VALUES ('27', 'TGI Khatulistiwa', 'tgiKhatulistiwa', '26', '0');
INSERT INTO menu VALUES ('28', 'Pembebanan Trafo', 'pembebananTrafo', '26', '0');
INSERT INTO menu VALUES ('29', 'Pembebanan Transmisi', 'pembebananTransmisi', '26', '0');
INSERT INTO menu VALUES ('30', 'Pemeliharaan', 'pemeliharaan', '26', '0');
INSERT INTO menu VALUES ('31', 'Kinerja Penyaluran', 'penyaluranKinerjaPenyaluran', '26', '0');
INSERT INTO menu VALUES ('32', 'Scadatel', '#', '0', '5');
INSERT INTO menu VALUES ('33', 'Gangguan RTU', 'gangguanRtu', '32', '0');
INSERT INTO menu VALUES ('34', 'Gangguan TC', 'gangguanTc', '32', '0');
INSERT INTO menu VALUES ('35', 'Gangguan Telekomunikasi', 'gangguanTelekomunikasi', '32', '0');
INSERT INTO menu VALUES ('36', 'Kinerja Scadatel', 'scadatelKinerjaScadatel', '32', '0');
INSERT INTO menu VALUES ('37', 'Data Master', '#', '0', '6');
INSERT INTO menu VALUES ('38', 'Gardu Induk', 'garduInduk', '37', '0');
INSERT INTO menu VALUES ('39', 'Trafo', 'trafo', '37', '0');
INSERT INTO menu VALUES ('40', 'Transmisi', 'transmisi', '37', '0');
INSERT INTO menu VALUES ('41', 'Gardu Hubung', 'garduHubung', '37', '0');
INSERT INTO menu VALUES ('42', 'Busbar', 'busbar', '37', '0');
INSERT INTO menu VALUES ('43', 'Kinerja Penyaluran', 'kinerjaPenyaluran', '37', '0');
INSERT INTO menu VALUES ('44', 'Jenis Gardu', 'jenisGardu', '37', '0');
INSERT INTO menu VALUES ('45', 'Flag', 'flag', '37', '0');
INSERT INTO menu VALUES ('46', 'Kinerja Scadatel', 'kinerjaScadatel', '37', '0');
INSERT INTO menu VALUES ('47', 'Jenis Pembangkit', 'jenisPembangkit', '37', '0');
INSERT INTO menu VALUES ('48', 'Pembangkit', 'pembangkit', '37', '0');
INSERT INTO menu VALUES ('49', 'Website', '#', '0', '0');
INSERT INTO menu VALUES ('50', 'Link Website', 'link', '49', '0');
INSERT INTO menu VALUES ('51', 'Back Up Database', 'database', '1', '0');
INSERT INTO menu VALUES ('52', 'Banner', 'banner', '49', '0');
INSERT INTO menu VALUES ('53', 'Website', 'website', '49', '0');

-- ----------------------------
-- Table structure for `neraca_daya`
-- ----------------------------
DROP TABLE IF EXISTS `neraca_daya`;
CREATE TABLE `neraca_daya` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `daya_terpasang` decimal(10,2) DEFAULT NULL,
  `plan_outage` decimal(10,2) DEFAULT NULL,
  `force_outage` decimal(10,2) DEFAULT NULL,
  `derating` decimal(10,2) DEFAULT NULL,
  `beban_puncak` decimal(10,2) DEFAULT NULL,
  `beban_terlayani` decimal(10,2) DEFAULT NULL,
  `cadangan_operasi` decimal(10,2) DEFAULT NULL,
  `padam` decimal(10,2) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of neraca_daya
-- ----------------------------
INSERT INTO neraca_daya VALUES ('12', '2014-09-03 09:00:00', '1.50', '2.50', '3.50', '4.50', '77.50', '6.70', '8.90', '10.10', '2014-09-03 09:53:22', '2');
INSERT INTO neraca_daya VALUES ('13', '2014-09-04 09:00:00', '1.50', '5.10', '3.20', '4.50', '19.20', '5.60', '1.20', '1.40', '2014-09-03 09:54:03', '2');
INSERT INTO neraca_daya VALUES ('14', '2013-12-15 09:00:00', '12.10', '31.20', '14.50', '66.10', '55.60', '7.80', '9.10', '5.60', '2014-09-03 09:54:54', '2');

-- ----------------------------
-- Table structure for `pembebanan_trafo`
-- ----------------------------
DROP TABLE IF EXISTS `pembebanan_trafo`;
CREATE TABLE `pembebanan_trafo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_full` date DEFAULT NULL,
  `gardu_induk_id` int(11) DEFAULT NULL,
  `trafo_id` int(11) DEFAULT NULL,
  `j01_00` decimal(10,1) DEFAULT NULL,
  `j02_00` decimal(10,1) DEFAULT NULL,
  `j03_00` decimal(10,1) DEFAULT NULL,
  `j04_00` decimal(10,1) DEFAULT NULL,
  `j05_00` decimal(10,1) DEFAULT NULL,
  `j06_00` decimal(10,1) DEFAULT NULL,
  `j07_00` decimal(10,1) DEFAULT NULL,
  `j08_00` decimal(10,1) DEFAULT NULL,
  `j09_00` decimal(10,1) DEFAULT NULL,
  `j10_00` decimal(10,1) DEFAULT NULL,
  `j11_00` decimal(10,1) DEFAULT NULL,
  `j12_00` decimal(10,1) DEFAULT NULL,
  `j13_00` decimal(10,1) DEFAULT NULL,
  `j14_00` decimal(10,1) DEFAULT NULL,
  `j15_00` decimal(10,1) DEFAULT NULL,
  `j16_00` decimal(10,1) DEFAULT NULL,
  `j17_00` decimal(10,1) DEFAULT NULL,
  `j18_00` decimal(10,1) DEFAULT NULL,
  `j18_30` decimal(10,1) DEFAULT NULL,
  `j19_00` decimal(10,1) DEFAULT NULL,
  `j19_30` decimal(10,1) DEFAULT NULL,
  `j20_00` decimal(10,1) DEFAULT NULL,
  `j20_30` decimal(10,1) DEFAULT NULL,
  `j21_00` decimal(10,1) DEFAULT NULL,
  `j21_30` decimal(10,1) DEFAULT NULL,
  `j22_00` decimal(10,1) DEFAULT NULL,
  `j23_00` decimal(10,1) DEFAULT NULL,
  `j24_00` decimal(10,1) NOT NULL,
  `date_post` datetime DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `jam_max` varchar(10) DEFAULT NULL,
  `jumlah_max` decimal(10,1) DEFAULT NULL,
  `pembanding` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembebanan_trafo
-- ----------------------------
INSERT INTO pembebanan_trafo VALUES ('5', '2014-08-21', '2', '2', '1.0', '2.0', '3.0', '4.0', '5.0', '6.0', '7.0', '8.0', '9.0', '9.0', '8.0', '7.0', '6.0', '5.0', '4.0', '3.0', '2.0', '1.0', '22.0', '33.0', '44.0', '12.0', '21.0', '23.5', '5.5', '4.5', '-50.6', '11.0', '2014-09-12 11:07:35', '2', '23:00', '50.6', 'j23_00');
INSERT INTO pembebanan_trafo VALUES ('6', '2014-08-21', '2', '3', '4.0', '5.0', '6.0', '7.0', '8.0', '9.0', '2.0', '1.8', '2.0', '2.0', '2.0', '3.0', '4.0', '5.0', '6.0', '7.0', '8.0', '9.0', '10.0', '11.0', '12.0', '12.0', '14.0', '15.0', '-25.1', '12.0', '14.0', '16.0', '2014-08-21 04:14:12', '2', '21:30', '25.1', 'j21_30');
INSERT INTO pembebanan_trafo VALUES ('7', '2014-08-19', '2', '2', '1.0', '2.0', '3.0', '4.0', '5.0', '5.0', '4.0', '3.0', '2.0', '1.0', '1.0', '2.0', '3.0', '4.0', '5.0', '1.0', '2.0', '3.0', '10.0', '1.0', '12.0', '23.2', '22.0', '11.0', '1.0', '1.0', '2.0', '3.0', '2014-08-21 04:10:32', '2', '20:00', '23.2', 'j20_00');
INSERT INTO pembebanan_trafo VALUES ('8', '2013-02-11', '2', '2', '1.0', '23.0', '43.0', '23.0', '32.0', '12.0', '12.0', '32.0', '12.0', '32.0', '11.0', '1.0', '1.0', '12.0', '1.0', '42.0', '12.0', '32.0', '33.0', '12.0', '12.0', '2.0', '2.0', '2.0', '21.0', '1.0', '1.0', '65.0', '2014-08-21 12:02:41', '2', '24:00', '65.0', 'j24_00');

-- ----------------------------
-- Table structure for `pembebanan_transmisi`
-- ----------------------------
DROP TABLE IF EXISTS `pembebanan_transmisi`;
CREATE TABLE `pembebanan_transmisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_full` date DEFAULT NULL,
  `gardu_induk_id` int(11) DEFAULT NULL,
  `transmisi_id` int(11) DEFAULT NULL,
  `j01_00` decimal(10,1) DEFAULT NULL,
  `j02_00` decimal(10,1) DEFAULT NULL,
  `j03_00` decimal(10,1) DEFAULT NULL,
  `j04_00` decimal(10,1) DEFAULT NULL,
  `j05_00` decimal(10,1) DEFAULT NULL,
  `j06_00` decimal(10,1) DEFAULT NULL,
  `j07_00` decimal(10,1) DEFAULT NULL,
  `j08_00` decimal(10,1) DEFAULT NULL,
  `j09_00` decimal(10,1) DEFAULT NULL,
  `j10_00` decimal(10,1) DEFAULT NULL,
  `j11_00` decimal(10,1) DEFAULT NULL,
  `j12_00` decimal(10,1) DEFAULT NULL,
  `j13_00` decimal(10,1) DEFAULT NULL,
  `j14_00` decimal(10,1) DEFAULT NULL,
  `j15_00` decimal(10,1) DEFAULT NULL,
  `j16_00` decimal(10,1) DEFAULT NULL,
  `j17_00` decimal(10,1) DEFAULT NULL,
  `j18_00` decimal(10,1) DEFAULT NULL,
  `j18_30` decimal(10,1) DEFAULT NULL,
  `j19_00` decimal(10,1) DEFAULT NULL,
  `j19_30` decimal(10,1) DEFAULT NULL,
  `j20_00` decimal(10,1) DEFAULT NULL,
  `j20_30` decimal(10,1) DEFAULT NULL,
  `j21_00` decimal(10,1) DEFAULT NULL,
  `j21_30` decimal(10,1) DEFAULT NULL,
  `j22_00` decimal(10,1) DEFAULT NULL,
  `j23_00` decimal(10,1) DEFAULT NULL,
  `j24_00` decimal(10,1) NOT NULL,
  `date_post` datetime DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `jam_max` varchar(10) DEFAULT NULL,
  `jumlah_max` decimal(10,1) DEFAULT NULL,
  `pembanding` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembebanan_transmisi
-- ----------------------------
INSERT INTO pembebanan_transmisi VALUES ('1', '2014-08-22', '2', '2', '1.0', '2.0', '3.0', '4.0', '5.6', '8.0', '9.0', '10.0', '1.0', '2.0', '3.0', '4.0', '5.0', '6.0', '7.0', '7.0', '7.0', '1.0', '2.0', '12.0', '-13.0', '14.0', '27.0', '21.9', '21.0', '21.0', '28.5', '1.0', '2014-08-22 09:50:34', '2', '23:00', '28.5', 'j23_00');
INSERT INTO pembebanan_transmisi VALUES ('3', '2014-08-22', '4', '3', '4.0', '5.0', '6.0', '6.0', '1.0', '2.0', '3.0', '4.0', '5.0', '6.0', '1.0', '2.0', '3.0', '4.0', '5.0', '6.0', '7.0', '2.0', '1.0', '1.0', '1.2', '-50.8', '1.0', '2.0', '3.0', '4.0', '5.0', '6.0', '2014-09-12 12:30:03', '2', '20:00', '50.8', 'j20_00');
INSERT INTO pembebanan_transmisi VALUES ('4', '2014-07-01', '2', '4', '2.0', '5.0', '6.0', '7.0', '8.0', '9.0', '10.0', '11.0', '12.0', '13.0', '-13.0', '-14.0', '-15.0', '-20.0', '5.0', '33.0', '1.0', '1.0', '1.0', '3.0', '4.0', '2.0', '4.0', '5.0', '-46.0', '2.0', '1.0', '1.0', '2014-08-22 09:56:15', '2', '21:30', '46.0', 'j21_30');

-- ----------------------------
-- Table structure for `pengumuman`
-- ----------------------------
DROP TABLE IF EXISTS `pengumuman`;
CREATE TABLE `pengumuman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(1000) DEFAULT NULL,
  `deskripsi` text,
  `sumber` varchar(500) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  `seo` varchar(1100) DEFAULT NULL,
  `gambar` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seo` (`seo`(767))
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pengumuman
-- ----------------------------
INSERT INTO pengumuman VALUES ('4', 'Kejari TTU Periksa Puluhan Kepala Sekolah Terkait Dugaan Korupsi DAK', 'Kejaksaan Negeri Kefamenanu, Kabupaten Timor Tengah Utara, Nusa Tenggara\n Timur, telah memeriksa 40 orang kepala sekolah sekolah dasar (SD) di \ndaerah tersebut terkait kasus dugaan korupsi dalam program dengan biaya \ndari dana alokasi khusus (DAK) pada Dinas Pendidikan, Pemuda, dan \nOlahraga (PPO) Kabupaten TTU senilai Rp 45,7 miliar.<br>\n<br>\nPara kepala sekolah ini diperiksa lantaran mereka tercatat sebagai \npenerima bantuan DAK pada 2008, 2010, dan 2011. Kepala Seksi Intelijen \nKejari Kefamenanu, Alma Wiranta, mengatakan, para kepala sekolah itu \ndiperiksa serentak sebagai saksi.<br>\n<br>\n\"Kami melakukan pemeriksaan kepada kepala sekolah yang menjabat saat \nbantuan itu diberikan dan pemeriksaannya seputar kegiatan peningkatan \nsarana, pengadaan alat peraga, dan buku-buku sesuai dengan petunjuk \nteknis DAK tahun 2008, 2010, dan 2011,\" kata Alma, Senin (2/6/2014).<br>\n<br>\nSementara itu, para kepala sekolah yang menjabat setelah 2011, juga akan\n menjalani pemeriksaan dalam kapasitas sebagai penerima paket pekerjaan \nyang dibiayai DAK pada 2008 dan 2010 yang dilaksanakan pada 2011.<br>\n<br>\nSelain kepala sekolah SD, kata Alma, kejaksaan juga akan memeriksa \nsejumlah kepala sekolah tingkat SMP yang juga mendapat bantuan DAK pada \n2008, 2010, dan 2011, dengan dugaan kasus yang sama.&nbsp;<br>\n<br>\nKasus dugaan korupsi DAK dinas PPO telah menyeret Kepala Dinas PPO \nKabupaten TTU Finsensius Saba sebagai tersangka. Selain itu, sejumlah \npejabat lainnya telah diperiksa kejaksaan, yakni Wakil Ketua DPRD TTU \nHendrikus Frengky Saunoah dan Kepala Bagian Keuangan Setda TTU Johanes \nSalu.<br>\n<br>\nDAK senilai Rp 47,5 miliar di Dinas PPO TTU untuk tahun anggaran 2008, \n2009, dan 2011 itu semuanya baru dikucurkan pada 2011. Dana tersebut \ndialokasikan untuk beragam kegiatan, antara lain pengadaan buku \npengayaan, buku referensi, buku panduan pendidik, dan alat peraga.<br>\n<br>\nPada 2008, 45 SD menjadi sasaran program yang dibiayai DAK itu. Lalu, \npada 2010, 34 SD mendapatkan program yang sama. Pada 2010, dana itu juga\n dipakai untuk pengadaan alat pendidikan di 11 SMP dan pembangunan ruang\n perpustakaan di 85 SD.<br>\n<br>\nPemeriksaan kasus ini merupakan tindak lanjut atas temuan Badan \nPemeriksa Keuangan Perwakilan NTT dengan uji petik di 30 sekolah \npenerima bantuan. Dari 220 paket pekerjaan yang dibiayai DAK itu, \nkerugian negara diperkirakan mencapai Rp 174 juta, dari kekurangan \nvolume pemenuhan pekerjaan.', 'tcs.co.id', '2', '2014-07-08 02:10:03', 'KejariTTUPeriksaPuluhanKepalaSekolahTerkaitDugaanKorupsiDAK', '4.jpg');
INSERT INTO pengumuman VALUES ('5', 'Jokowi Akan Menaikkan Harga BBM jika Terpilih Jadi Presiden', 'Calon Presiden Joko Widodo mengisyaratkan akan menaikkan harga bahan \nbakar minyak (BBM) apabila nanti terpilih menjadi presiden. Jokowi \nmerasa memiliki keberanian untuk mengambil kebijakan yang selalu \nmenimbulkan gejolak sosial, ekonomi, dan politik tersebut.<br>\n<br>\n\"Ini masalah efisiensi karena ada banyak kepentingan. Kenapa saya punya \nkeberanian (menaikkan BBM)? Itu karena saya tidak tersandera \nkepentingan-kepentingan. Saya ini orang baru, orang baru,\" kata Jokowi \ndalam konferensi pers setelah acara pemaparan platform ekonomi Jokowi-JK\n di Jakarta, Rabu (4/6/2014).<br>\n<br>\nJokowi menganggap subsidi BBM saat ini sangat memberatkan APBN. Mantan \nWali Kota Solo itu mengusulkan agar subsidi BBM dialihkan ke \nprogram-program yang padat karya seperti subsidi pupuk untuk petani.<br>\n<br>\nJokowi juga menjelaskan, kenyataannya, subsidi BBM saat ini justru tidak\n dinikmati oleh masyarakat yang tidak mampu. Menurut dia, subsidi malah \nmengalir ke tangki-tangki mobil mewah orang-orang berduit.<br>\n<br>\n\"Subsidi kita saat ini dinikmati bukan oleh masyarakat tidak mampu. \nLebih baik alihkan ke pangan, subsidi pupuk untuk petani,\" katanya.<br>\n<br>\nDalam pemaparan visi-misi ekonominya, Jokowi bahkan menyinggung masalah \nketergantungan masyarakat terhadap BBM. Menurut dia, menaikkan harga BBM\n bukanlah hal yang sulit. Dia meyakinkan peserta yang hadir untuk \nberpikir tidak rumit mengenai masalah tersebut.<br>\n<br>\n\"Kita saat ini masih sangat ketergantungan BBM. Jangan anggap ini susah.\n Anggap ini gampang, jangan berpikir ini susah. Ini gampang,\" tandasnya,\n disambut tepuk tangan peserta konferensi pers.', 'akiranarenabuana.com', '2', '2014-07-08 02:10:59', 'JokowiAkanMenaikkanHargaBBMJikaTerpilihJadiPresiden', '5.JPG');
INSERT INTO pengumuman VALUES ('6', 'Tawuran Antarwarga Seusai Bagikan Makanan Sahur', 'Tawuran antarwarga masih kerap terjadi. Korban pun berjatuhan akibat \nbentrokan yang terjadi di sejumlah daerah di Ibu Kota tersebut.\n<p>Terakhir, keributan terjadi antarkelompok setelah melakukan kegiatan \nmembagikan makanan sahur di jalan umum atau populer dengan sebutan&nbsp;sahur\n on the road. Tawuran itu terjadi di kawasan Jalan Moh Yamin, Menteng, \nJakarta Pusat, sekitar pukul 06.05 WIB, Minggu (13/7).</p>\n<p>Keributan itu mengakibatkan seorang warga, Salman (18), warga \nKebayoran Baru, Jakarta Selatan, terkena sabetan senjata tajam di bagian\n pinggang. Dan, sepeda motornya, Scoopy bernomor polisi B 3357 SMR, \ndibakar pelaku.</p>\n<p>?Sampai saat ini pelaku penusuk korban belum jelas. Kami masih \nmelakukan penyelidikan. Saksi kami masih minim,? kata Kepala Kepolisian \nSektor Menteng Ajun Komisaris Besar Gunawan, Minggu petang.</p>\n<p>Secara terpisah Kepala Bidang Humas Polda Metro Jaya Komisaris Besar \nRikwanto menjelaskan, keributan itu tepatnya terjadi di bawah jembatan \nlayang di Jalan Cikini Raya, dekat Stasiun Cikini, di seberang sebuah \nrestoran cepat saji.</p>\n<p>Awalnya korban bersama 10 temannya dengan menggunakan sejumlah \nkendaraan melintas di Jalan Cikini Raya. Di depan sebuah stasiun \npengisian bahan bakar untuk umum (SPBU), rombongan korban melihat \nrombongan&nbsp;sahur on the road&nbsp;dari kelompok lain di samping trotoar depan \nSPBU itu. Korban terus melaju, tetapi kemudian berhenti di bawah \njembatan layang tersebut.</p>\n<p>Selang lima menit kemudian, melintas rombongan kelompok lain yang \ndilihat rombongan korban di trotoar SPBU. Rombongan itu diawali tiga \nmobil dan diikuti puluhan sepeda motor. Lalu rombongan itu berhenti di \ndepan restoran cepat saji, sementara rombongan korban masih berhenti di \nbawah jembatan layang.</p>\n<p>?Tiba-tiba terjadilah tawuran kedua kelompok itu. Masih belum jelas \nsiapa lebih dahulu memulai. Tawurannya cukup cepat. Di lokasi bekas \ntawuran, selain sepeda motor korban yang terbakar, juga ditemukan sebuah\n gir diikatkan ke sebuah kain kuning, yang diduga milik kelompok pelaku \nyang menusuk korban,? tutur Rikwanto.</p>\n<p>Lebih lanjut, Rikwanto mengimbau agar masyarakat tidak perlu \nmelakukan kegiatan&nbsp;sahur on the road. Itu karena kegiatan tersebut \nbanyak&nbsp;mudharat-nya. Hal itu terjadi karena tidak sedikit terjadi \ngesekan di antara kelompok-kelompok pelaksana kegiatan tersebut, yang \nberpotensi terjadi perkara pidana atau sedikitnya mengganggu kenyamanan \ndan keselamatan pengguna jalan lain.</p>\n<p>Penerima makanan dan minuman itu juga tidak bisa mendapat kepastian \nmakanannya aman untuk disantap karena pemberinya cepat berlalu.</p>\n<p>Menurut Rikwanto, jika ingin bersedekah dengan memberi \nmakanan-minuman untuk sahur atau buka puasa, sebaiknya dibagikan dahulu \natau diprioritaskan untuk masyarakat di lingkungan permukiman sendiri. \n?Lihat sekeliling kita, apa benar para tetangga kita sudah tidak \nkesulitan makan. Kalau sudah bisa dipastikan tetangga kita bebas dari \nkesulitan makan, barulah bergerak ke luar lingkungan yang lebih luas,? \nkatanya.</p>\nAtau, lanjutnya, drop makanan itu di masjid di dekat pusat kegiatan \nmasyarakat, yang pasti akan didatangi umat yang hendak makan sahur atau \nberbuka puasa.<strong></strong>', 'kompas.com', '2', '2014-07-14 20:40:33', 'TawuranAntarwargaSeusaiBagikanMakananSahur', '6.jpg');
INSERT INTO pengumuman VALUES ('7', 'Ahok Terpukau Kemegahan Jakarta Islamic Center', 'Pelaksana Tugas (Plt) Gubernur DKI Jakarta Basuki Tjahaja Purnama \nterpukau dengan kemegahan bangunan Jakarta Islamic Center (JIC), Tugu \nUtara, Jakarta Utara. Hal itu disampaikannya saat ia melaksanakan Safari\n Ramadhan di sana. Pusat kajian Islam yang dahulunya merupakan \nlokalisasi Kramat Tunggak itu telah diubah menjadi masjid, sarana \npendidikan, hingga wisma.\n<p>\"Ini pertama kalinya saya masuk Islamic Center. Ternyata bagus banget\n ya dan megah,\" kata Basuki berdecak kagum melihat sekeliling JIC, Senin\n (14/7/2014) malam.</p>\n<p>Menurut Basuki, hal ini merupakan langkah terbaik yang dilakukan oleh\n mantan Gubernur DKI Jakarta Sutiyoso. Ketika itu, kompleks lokalisasi \nKramat Tunggak tidak hanya dibongkar, tapi direnovasi dan diubah. Ia \nmelanjutkan, jika Pemkot maupun Pemprov hanya membongkar habis lahan \nlokalisasi, maka pelaku prostitusi akan kembali ke sana.</p>\n<p>Sementara, jika dialihfungsikan, pemerintah juga dapat memberdayakan \npekerja seks komersial (PSK). \"Proses ini yang kita butuhkan. Keputusan \nBang Yos tepat sekali membangun JIC. Saya harap JIC jadi standar warga \nIndonesia sebagai pusat kajian Islam. Karena di sini juga ada wisma dan \nhotel juga untuk para wisatawan,\" ujar Basuki.</p>\n<p>Pada Safari Ramadhan ini, Basuki atas nama Pemprov DKI turut \nmenyumbang sejumlah makanan, peralatan sekolah, dan uang kepada ribuan \nanak yatim piatu.</p>', '', '2', '2014-07-14 20:41:31', 'AhokTerpukauKemegahanJakartaIslamicCenter', '7.JPG');

-- ----------------------------
-- Table structure for `penyaluran_kinerja_penyaluran`
-- ----------------------------
DROP TABLE IF EXISTS `penyaluran_kinerja_penyaluran`;
CREATE TABLE `penyaluran_kinerja_penyaluran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kinerja_item_id` int(11) DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `target` int(11) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kinerja_item` (`kinerja_item_id`),
  CONSTRAINT `kinerja_itemm` FOREIGN KEY (`kinerja_item_id`) REFERENCES `master_kinerja_penyaluran` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of penyaluran_kinerja_penyaluran
-- ----------------------------

-- ----------------------------
-- Table structure for `profil`
-- ----------------------------
DROP TABLE IF EXISTS `profil`;
CREATE TABLE `profil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi` text,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of profil
-- ----------------------------
INSERT INTO profil VALUES ('2', '<div id=\"textnya\" style=\"height:auto;\">\r\n                    <p>Berawal di akhir abad ke 19, perkembangan \r\nketenagalistrikan di Indonesia mulai ditingkatkan saat beberapa \r\nperusahaan asal Belanda yang bergerak di bidang pabrik gula dan pabrik \r\nteh mendirikan pembangkit listrik untuk keperluan sendiri.</p>\r\n                    <p>Antara tahun 1942-1945 terjadi peralihan \r\npengelolaan perusahaan- perusahaan Belanda tersebut oleh Jepang, setelah\r\n Belanda menyerah kepada pasukan tentara Jepang di awal Perang Dunia II.</p>\r\n                    <p>Proses peralihan kekuasaan kembali terjadi di \r\nakhir Perang Dunia II pada Agustus 1945, saat Jepang menyerah kepada \r\nSekutu. Kesempatan ini dimanfaatkan oleh para pemuda dan buruh listrik \r\nmelalui delegasi Buruh/Pegawai Listrik dan Gas yang bersama-sama dengan \r\nPimpinan KNI Pusat berinisiatif menghadap Presiden Soekarno untuk \r\nmenyerahkan perusahaan-perusahaan tersebut kepada Pemerintah Republik \r\nIndonesia. Pada 27 Oktober 1945, Presiden Soekarno membentuk Jawatan \r\nListrik dan Gas di bawah Departemen Pekerjaan Umum dan Tenaga dengan \r\nkapasitas pembangkit tenaga listrik sebesar 157,5 MW.</p>\r\n                    <p>Pada tanggal 1 Januari 1961, Jawatan Listrik dan \r\nGas diubah menjadi BPU-PLN (Badan Pimpinan Umum Perusahaan Listrik \r\nNegara) yang bergerak di bidang listrik, gas dan kokas yang dibubarkan \r\npada tanggal 1 Januari 1965. Pada saat yang sama, 2 (dua) perusahaan \r\nnegara yaitu Perusahaan Listrik Negara (PLN) sebagai pengelola tenaga \r\nlistrik milik negara dan Perusahaan Gas Negara (PGN) sebagai pengelola \r\ngas diresmikan.</p>\r\n                    <p>Pada tahun 1972, sesuai dengan Peraturan \r\nPemerintah No.17, status Perusahaan Listrik Negara (PLN) ditetapkan \r\nsebagai Perusahaan Umum Listrik Negara dan sebagai Pemegang Kuasa Usaha \r\nKetenagalistrikan (PKUK) dengan tugas menyediakan tenaga listrik bagi \r\nkepentingan umum.</p>\r\n                    <p>Seiring dengan kebijakan Pemerintah yang \r\nmemberikan kesempatan kepada sektor swasta untuk bergerak dalam bisnis \r\npenyediaan listrik, maka sejak tahun 1994 status PLN beralih dari \r\nPerusahaan Umum menjadi Perusahaan Perseroan (Persero) dan juga sebagai \r\nPKUK dalam menyediakan listrik bagi kepentingan umum hingga sekarang.</p>\r\n                </div>', '2', '2014-09-09 12:29:05');

-- ----------------------------
-- Table structure for `proses_bisnis`
-- ----------------------------
DROP TABLE IF EXISTS `proses_bisnis`;
CREATE TABLE `proses_bisnis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi` text,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of proses_bisnis
-- ----------------------------
INSERT INTO proses_bisnis VALUES ('1', '<div id=\"textnya\">\n                    <p>PLN sebagai Badan Usaha Milik Negara yang berbentuk Perusahaan Perseroan (Persero) berkewajiban untuk menyediakan tenaga listrik bagi kepentingan umum dengan tetap memperhatikan tujuan perusahaan yaitu menghasilkan keuntungan sesuai dengan Undang-Undang No. 19/2000. Kegiatan usaha perusahaan meliputi : Menjalankan usaha penyediaan tenaga listrik yang meliputi kegiatan pembangkitan, penyaluran, distribusi tenaga listrik, perencanaan dan pembangunan sarana penyediaan tenaga listrik. Menjalankan usaha penunjang dalam penyediaan tenaga listrik yang meliputi kegiatan konsultasi, pembangunan, pemasangan, pemeliharaan peralatan ketenagalistrikan, Pengembangan teknologi peralatan yang menunjang penyediaan tenaga listrik. Menjalankan kegiatan pengelolaan dan pemanfaatan sumber daya alam dan sumber energi lainnya untuk kepentingan penyediaan tenaga listrik, Melakukan pemberian jasa operasi dan pengaturan (dispatcher) pada pembangkitan, penyaluran, distribusi dan retail tenaga listrik, Menjalankan kegiatan perindustrian perangkat keras dan perangkat lunak bidang ketenagalistrikan dan peralatan lain yang terkait dengan tenaga listrik, Melakukan kerja sama dengan badan lain atau pihak lain atau badan penyelenggara bidang ketenagalistrikan baik dari dalam negeri maupun luar negeri di bidang pembangunan, operasional, telekomunikasi dan informasi yang berkaitan dengan ketenagalistrikan.</p>\n                </div>', '2', '2014-07-11 08:09:09');

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO role VALUES ('4', 'Admin');
INSERT INTO role VALUES ('5', 'Super Admin');

-- ----------------------------
-- Table structure for `scadatel_gangguan_rtu`
-- ----------------------------
DROP TABLE IF EXISTS `scadatel_gangguan_rtu`;
CREATE TABLE `scadatel_gangguan_rtu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_gardu_id` int(11) DEFAULT NULL,
  `gardu_induk_id` int(11) DEFAULT NULL,
  `waktu_gangguan` datetime DEFAULT NULL,
  `flag_id` int(11) DEFAULT NULL,
  `lama_gangguan` time DEFAULT NULL,
  `keterangan` text,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jenis_gardu` (`jenis_gardu_id`),
  KEY `gardu_induk` (`gardu_induk_id`),
  KEY `flag` (`flag_id`),
  CONSTRAINT `flag_i` FOREIGN KEY (`flag_id`) REFERENCES `master_flag` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `gardu_induk_i` FOREIGN KEY (`gardu_induk_id`) REFERENCES `master_gardu_induk` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `jenis_gardu` FOREIGN KEY (`jenis_gardu_id`) REFERENCES `master_jenis_gardu` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of scadatel_gangguan_rtu
-- ----------------------------
INSERT INTO scadatel_gangguan_rtu VALUES ('1', '2', '2', '2014-09-11 00:00:00', '3', '02:11:00', 'dede', '2', '2014-09-10 17:57:19');
INSERT INTO scadatel_gangguan_rtu VALUES ('2', '3', '4', '2014-09-02 00:00:00', '2', '10:15:00', 'testing', '2', '2014-09-11 07:58:53');

-- ----------------------------
-- Table structure for `scadatel_gangguan_tc`
-- ----------------------------
DROP TABLE IF EXISTS `scadatel_gangguan_tc`;
CREATE TABLE `scadatel_gangguan_tc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_gardu_id` int(11) DEFAULT NULL,
  `gardu_induk_id` int(11) DEFAULT NULL,
  `waktu_gangguan` datetime DEFAULT NULL,
  `flag_id` int(11) DEFAULT NULL,
  `lama_gangguan` time DEFAULT NULL,
  `keterangan` text,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jenis_gardu` (`jenis_gardu_id`),
  KEY `gardu_induk` (`gardu_induk_id`),
  KEY `flag` (`flag_id`),
  CONSTRAINT `scadatel_gangguan_tc_ibfk_1` FOREIGN KEY (`flag_id`) REFERENCES `master_flag` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `scadatel_gangguan_tc_ibfk_2` FOREIGN KEY (`gardu_induk_id`) REFERENCES `master_gardu_induk` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `scadatel_gangguan_tc_ibfk_3` FOREIGN KEY (`jenis_gardu_id`) REFERENCES `master_jenis_gardu` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of scadatel_gangguan_tc
-- ----------------------------
INSERT INTO scadatel_gangguan_tc VALUES ('1', '2', '2', '2014-08-01 10:10:00', '2', '03:09:00', 'testing', '2', '2014-08-27 12:37:44');
INSERT INTO scadatel_gangguan_tc VALUES ('3', '3', '4', '2014-08-27 09:00:00', '3', '05:17:00', 'dedex', '2', '2014-08-27 12:42:13');

-- ----------------------------
-- Table structure for `scadatel_gangguan_telekomunikasi`
-- ----------------------------
DROP TABLE IF EXISTS `scadatel_gangguan_telekomunikasi`;
CREATE TABLE `scadatel_gangguan_telekomunikasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_gardu_id` int(11) DEFAULT NULL,
  `gardu_induk_id` int(11) DEFAULT NULL,
  `waktu_gangguan` datetime DEFAULT NULL,
  `flag_id` int(11) DEFAULT NULL,
  `lama_gangguan` time DEFAULT NULL,
  `keterangan` text,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jenis_gardu` (`jenis_gardu_id`),
  KEY `gardu_induk` (`gardu_induk_id`),
  KEY `flag` (`flag_id`),
  CONSTRAINT `scadatel_gangguan_telekomunikasi_ibfk_1` FOREIGN KEY (`flag_id`) REFERENCES `master_flag` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `scadatel_gangguan_telekomunikasi_ibfk_2` FOREIGN KEY (`gardu_induk_id`) REFERENCES `master_gardu_induk` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `scadatel_gangguan_telekomunikasi_ibfk_3` FOREIGN KEY (`jenis_gardu_id`) REFERENCES `master_jenis_gardu` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of scadatel_gangguan_telekomunikasi
-- ----------------------------
INSERT INTO scadatel_gangguan_telekomunikasi VALUES ('2', '2', '2', '2014-08-27 09:00:00', '2', '05:17:00', 'testingss', '2', '2014-08-27 14:56:14');

-- ----------------------------
-- Table structure for `scadatel_kinerja_scadatel`
-- ----------------------------
DROP TABLE IF EXISTS `scadatel_kinerja_scadatel`;
CREATE TABLE `scadatel_kinerja_scadatel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kinerja_item_id` int(11) DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `target` int(11) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kinerja_item` (`kinerja_item_id`),
  CONSTRAINT `kinerja_item` FOREIGN KEY (`kinerja_item_id`) REFERENCES `master_kinerja_scadatel` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of scadatel_kinerja_scadatel
-- ----------------------------

-- ----------------------------
-- Table structure for `sessi_login`
-- ----------------------------
DROP TABLE IF EXISTS `sessi_login`;
CREATE TABLE `sessi_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sessi_login
-- ----------------------------
INSERT INTO sessi_login VALUES ('1', '2', '1');

-- ----------------------------
-- Table structure for `struktur`
-- ----------------------------
DROP TABLE IF EXISTS `struktur`;
CREATE TABLE `struktur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(300) DEFAULT NULL,
  `deskripsi` text,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of struktur
-- ----------------------------
INSERT INTO struktur VALUES ('1', '1.jpg', '<p style=\"line-height: 16px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);\">Dewan Komisais PT PLN (Persero) Yogo Pratomo</p><ul style=\"padding-left: 15px; color: rgb(0, 0, 0); font-family: Tahoma; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);\"><li style=\"line-height: 16px; padding-bottom: 5px;\">Yogo Pratomo, Komisaris Utama. Yogo Pratomo menjabat sebagai Komisaris Utama sejak Desember 2009. Beliau juga pernah menduduki posisi Ketua Koordinasi Harian untuk Tim Program Percepatan (sesuai Peraturan Presiden No. 72/2006 mengatur Proyek-proyek Kelistrikan 10.000 MW) sejak 2006. Beliau pernah menjabat sebagai Direktur Jenderal Kelistrikan dan Utilisasi (1999-2006), Staf Ahli Kementerian Energi dan Sumber Daya Mineral (1999-2003), Kepala Biro Perencanaan di Kementerian Energi dan Sumber Daya Mineral (1998-1999), Direktur Program Pengembangan Kelistrikan di Kantor Kementerian Energi dan Sumber Daya Mineral (1995-1998), Kepala Sub-Direktorat Program Kelistrikan (1993-1995), Kepala Seksi Program Formulasi Kelistrikan (1992-1993), dan Staf Direktorat Pengembangan Energi (1988-1992). Beliau meraih gelar Sarjana bidang Teknik Elektro dari Institut Teknologi Bandung di tahun 1980; kemudian gelar Pasca Sarjana, Master of Science bidang Ekonomi dan Kebijakan Energi dari University of Wisconsin, Madison, Amerika Serikat (1984) dan Ph.D bidang Ekonomi dan Kebijakan Energi dari University of Wisconsin, Madison, Amerika Serikat (1988). Adang Firman</li><li style=\"line-height: 16px; padding-bottom: 5px;\">Adang Firman, Komisaris Independen. Adang Firman mulai menjabat sebagai Komisaris Independen sejak Desember 2012. Beliau menjabat sebagai Kepala Kepolisian Daerah Metro Jaya (2006-2008), Wakil Kepala Divisi Operasi Kepolisian Republik Indonesia (2006), Staf Ahli Manajemen untuk Kantor Pusat Kepolisian Republik Indonesia (2004-2005), Widya Iswara Sespati Polri (2003-2004), Kepala Polisi Daerah Sumatera Barat (2001-2003), Asisten Operasi untuk Kepala Keplisian Republik Indonesia (2000-2001), Wakil Gubernur PTIK (2000). Sebelumnya, beliau lulus dari Akademi Kepolisian Angkatan Bersenjata Republik Indonesia (AKABRI) di tahun 1973, Perguruan Tinggi Ilmu Kepolisian di tahun 1981, SESPIMPOL di tahun 1990, dan LEMHANAS pada tahun 1997.</li></ul>', '2', '2014-09-09 12:51:38');

-- ----------------------------
-- Table structure for `tbl_migration`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_migration`;
CREATE TABLE `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_migration
-- ----------------------------
INSERT INTO tbl_migration VALUES ('m000000_000000_base', '1405551546');
INSERT INTO tbl_migration VALUES ('m140716_225947_cretea_rezas_table', '1405551742');

-- ----------------------------
-- Table structure for `tegangan`
-- ----------------------------
DROP TABLE IF EXISTS `tegangan`;
CREATE TABLE `tegangan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_full` date DEFAULT NULL,
  `gardu_hubung_id` int(11) DEFAULT NULL,
  `busbar_id` int(11) DEFAULT NULL,
  `j01_00` decimal(10,1) DEFAULT NULL,
  `j02_00` decimal(10,1) DEFAULT NULL,
  `j03_00` decimal(10,1) DEFAULT NULL,
  `j04_00` decimal(10,1) DEFAULT NULL,
  `j05_00` decimal(10,1) DEFAULT NULL,
  `j06_00` decimal(10,1) DEFAULT NULL,
  `j07_00` decimal(10,1) DEFAULT NULL,
  `j08_00` decimal(10,1) DEFAULT NULL,
  `j09_00` decimal(10,1) DEFAULT NULL,
  `j10_00` decimal(10,1) DEFAULT NULL,
  `j11_00` decimal(10,1) DEFAULT NULL,
  `j12_00` decimal(10,1) DEFAULT NULL,
  `j13_00` decimal(10,1) DEFAULT NULL,
  `j14_00` decimal(10,1) DEFAULT NULL,
  `j15_00` decimal(10,1) DEFAULT NULL,
  `j16_00` decimal(10,1) DEFAULT NULL,
  `j17_00` decimal(10,1) DEFAULT NULL,
  `j18_00` decimal(10,1) DEFAULT NULL,
  `j18_30` decimal(10,1) DEFAULT NULL,
  `j19_00` decimal(10,1) DEFAULT NULL,
  `j19_30` decimal(10,1) DEFAULT NULL,
  `j20_00` decimal(10,1) DEFAULT NULL,
  `j20_30` decimal(10,1) DEFAULT NULL,
  `j21_00` decimal(10,1) DEFAULT NULL,
  `j21_30` decimal(10,1) DEFAULT NULL,
  `j22_00` decimal(10,1) DEFAULT NULL,
  `j23_00` decimal(10,1) DEFAULT NULL,
  `j24_00` decimal(10,1) NOT NULL,
  `date_post` datetime DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `hasil_rata` decimal(10,1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tegangan
-- ----------------------------
INSERT INTO tegangan VALUES ('1', '2014-08-25', '2', '2', '1.0', '2.0', '3.0', '4.0', '5.0', '6.0', '7.0', '8.0', '9.0', '10.0', '11.0', '12.0', '13.0', '14.0', '15.0', '16.0', '17.0', '18.0', '19.0', '20.0', '21.0', '22.0', '23.0', '24.0', '25.0', '26.0', '27.0', '28.0', '2014-08-25 09:35:00', '2', '14.5');
INSERT INTO tegangan VALUES ('2', '2014-08-25', '3', '3', '1.0', '2.0', '1.0', '2.0', '1.0', '2.0', '1.0', '2.0', '1.0', '2.0', '1.0', '2.0', '1.0', '2.0', '1.0', '2.0', '1.0', '2.0', '1.0', '2.0', '1.0', '2.0', '1.0', '2.0', '1.0', '2.0', '1.0', '2.0', '2014-08-25 09:35:51', '2', '1.5');
INSERT INTO tegangan VALUES ('3', '2014-08-02', '3', '3', '2.0', '3.0', '4.0', '4.0', '3.0', '2.0', '3.0', '4.0', '7.0', '6.0', '8.0', '2.0', '4.0', '6.0', '-1.0', '2.0', '3.0', '4.0', '5.0', '6.0', '5.0', '6.0', '1.2', '4.5', '2.1', '3.2', '1.0', '2.0', '2014-08-25 09:38:08', '2', '3.6');

-- ----------------------------
-- Table structure for `tgi`
-- ----------------------------
DROP TABLE IF EXISTS `tgi`;
CREATE TABLE `tgi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tgi
-- ----------------------------
INSERT INTO tgi VALUES ('3', 'http://www.tgi-khatulistiwa.web.id/');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO user VALUES ('2', '5', 'Muhamad Reza', 'reza', 'reza.wikarma2@gmail.com', 'bb98b1d0b523d5e783f931550d7702b6', '2014-07-14 20:42:54');
INSERT INTO user VALUES ('3', '4', 'admin', 'admin', 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3', '2014-07-07 18:53:19');

-- ----------------------------
-- Table structure for `visi`
-- ----------------------------
DROP TABLE IF EXISTS `visi`;
CREATE TABLE `visi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visi` text,
  `misi` text,
  `date_post` datetime DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of visi
-- ----------------------------
INSERT INTO visi VALUES ('1', 'Diakui sebagai Perusahaan Kelas Dunia yang Bertumbuh kembang, Unggul dan Terpercaya dengan bertumpu pada Potensi Insani.', '<div id=\"textnya\">\n                	<ul>\n                    	<br><li>Menjalankan bisnis kelistrikan dan bidang lain yang terkait, berorientasi pada kepuasan pelanggan, anggota perusahaan dan pemegang saham.</li>\n                        <li>Menjadikan tenaga listrik sebagai media untuk meningkatkan kualitas kehidupan masyarakat.</li>\n                        <li>Mengupayakan agar tenaga listrik menjadi pendorong kegiatan ekonomi.</li>\n                        <li>Menjalankan kegiatan usaha yang berwawasan lingkungan.</li>\n                    </ul>\n                </div>', '2014-08-22 11:13:59', '2');

-- ----------------------------
-- Table structure for `website`
-- ----------------------------
DROP TABLE IF EXISTS `website`;
CREATE TABLE `website` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook` text,
  `twitter` text,
  `text_berjalan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of website
-- ----------------------------
INSERT INTO website VALUES ('1', 'https://www.facebook.com/PragmaInformatika?fref=ts', 'https://twitter.com/pragmainf', 'Listrik Untuk Kehidupan Lebih Baik');

-- ----------------------------
-- Table structure for `wilayah_kerja`
-- ----------------------------
DROP TABLE IF EXISTS `wilayah_kerja`;
CREATE TABLE `wilayah_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi` text,
  `posted_by` int(11) DEFAULT NULL,
  `date_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of wilayah_kerja
-- ----------------------------
INSERT INTO wilayah_kerja VALUES ('1', '<div id=\"textnya\">\n                    <p>Dalam menjalankan kegiatan bisnisnya PT PLN \n(Persero) selalu berusaha untuk memenuhi ketentuan yang disyaratkan \ndalam peraturan perundang-undangan di bidang lingkungan hidup. </p>\n                    <p>Program kegiatan yang telah dan sedang dilaksanakan PLN di bidang lingkungan hidup, antara lain:</p>\n                    <ul><li>Melaksanakan kebijakan umum perusahaan bidang lingkungan hidup.</li><li>Mengikuti program peduli lingkungan global/pelaksanaan Clean Development Mechanism (CDM).</li><li>Melaksanakan pendidikan dan pelatihan di bidang pengelolaan lingkungan hidup.</li></ul>\n                    <p>Sebanyak 34 unit PLN tersebar diseluruh Indonesia\n telah mendapat sertifikat ISO 14001 dan sebanyak 12 Unit telah mendapat\n sertifikat Sistem Manajemen Kesehatan dan Keselamatan Kerja (SMK3).</p>\n                </div>', '2', '2014-07-11 07:51:50');
