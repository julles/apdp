// JavaScript Document

$(function () {
	$('#container').highcharts({
		title: {
			text: 'Tuesday, 1 Juli 2014',
			x: -20 //center
		},
		xAxis: {
			labels: {rotation: -90, x: 4,
				style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif',
                    }
			},
			categories: ['01.00', '02.00', '03.00', '04.00', '05.00', '06.00',
				'07.00', '08.00', '09.00', '10.00', '11.00', '12.00', '13.00', 
				'14.00', '15.00', '16.00', '17.00', '18.00', '19.00', '20.00',
				'21.00', '22.00', '23.00', '24.00']
		},
		yAxis: {
			title: {
				text: 'Daya (MW)'
			},
			plotLines: [{
				value: 0,
				width: 1,
				color: '#808080'
			}]
		},
		tooltip: {
			valueSuffix: 'MW'
		},
		legend: {
			layout: 'vertical',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		series: [{
			data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9,
			 9.6, 7.6, 10.6, 11.0, 11.6, 13.6, 12.9, 10.1, 19.6, 20.6, 16.6, 19.0, 15.6]
		}]
	});
});