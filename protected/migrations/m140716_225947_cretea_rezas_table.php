<?php

class m140716_225947_cretea_rezas_table extends CDbMigration
{
	public function up()
	{
	}

	public function down()
	{
		echo "m140716_225947_cretea_rezas_table does not support migration down.\n";
		return false;
	}

	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable('rezas',array(
		 'id'=>'pk',
		 'name'=>'string NOT NULL',
		 'email'=>'string NOT NULL',
		 'address'=>'text NOT NULL',
		 ),'ENGINE=InnoDB'
		 );
	
	}

	public function safeDown()
	{
		 $this->dropTable('rezas');
	}
	
}