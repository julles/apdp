<?php
	class ar
	{
	
		public static function ip()
		{
		    $client  = @$_SERVER['HTTP_CLIENT_IP'];
		    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		    $remote  = $_SERVER['REMOTE_ADDR'];

		    if(filter_var($client, FILTER_VALIDATE_IP))
		    {
		        $ip = $client;
		    }
		    elseif(filter_var($forward, FILTER_VALIDATE_IP))
		    {
		        $ip = $forward;
		    }
		    else
		    {
		        $ip = $remote;
		    }

		    return $ip;
		}

		public static function searchMinus($param)
		{
			
		}

		public static function getDataHarian($param)
		{
					$tanggalAkhir = $param;
					for($a=1;$a<=$tanggalAkhir;$a++)
					{
						$tgl[] = $a;
					}
					return $tgl;
		}

		

		public static function generateJam($jam = "")
		{
			
		}

		public static function tampilJam()
		{
			return array(
				'01.00',
				'02.00',
				'03.00',
				'04.00',
				'05.00',
				'06.00',
				'07.00',
				'08.00',
				'09.00',
				'10.00',
				'11.00',
				'12.00',
				'13.00',
				'14.00',
				'15.00',
				'16.00',
				'17.00',
				'18.00',
				'18.30',
				'19.00',
				'19.30',
				'20.00',
				'20.30',
				'21.00',
				'21.30',
				'22.00',
				'23.00',
				'24.00',

			);
		}

		public static function waktuYii($tgl , $tanggal , $waktu)
		{
			return Yii::app()->dateFormatter->formatDateTime($tgl, $tanggal, $waktu);
		}	

		public static function bulanSaja($bulan)
		{
			$month  = date("F", mktime(0, 0, 0, $bulan, 10));
			return $month;
		}


		public static function getDataBulananTanpaTahun($year)
		{
			for($a=1;$a<=12;$a++)
			{
				$month  = substr(date("F", mktime(0, 0, 0, $a, 10)) , 0, 3);
				$init = $month;
				$hasil[] = $init;
			}
			
			return $hasil;
		}

		public static function getDataBulanan($year)
		{
			for($a=1;$a<=12;$a++)
			{
				$month  = date("F", mktime(0, 0, 0, $a, 10));
				$init = $month.$year;
				$hasil[] = $init;
			}
			
			return $hasil;
		}

		public static function getDataBulananAja($par = "")
		{
			for($a=1;$a<=12;$a++)
			{
				$month  = date("F", mktime(0, 0, 0, $a, 10));
				($par == 3) ? $month = substr($month , 0 , 3) : $month = $month;
				$init = $month;
				$hasil[] = $init;
			}
			
			return $hasil;
		}

		public static function getDataTahunan($awal,$akhir)
		{
			for($a=$awal;$a<=$akhir;$a++)
			{
				$hasil[] = $a;
			}
			
			
			return $hasil;
		}


		public static function btnEdit()
		{
			return '<button class="btn btn-xs btn-primary"><i class="fa fa-pencil">Edit</i></button>';
		}

		public static function btnHapus()
		{
			return '<button class="btn btn-xs btn-secondary"><i class="fa fa-pencil">Hapus</i></button>';
		}

		public static function formatWaktu($tgl , $tanggal , $waktu)
		{
			return Yii::app()->dateFormatter->formatDateTime($tgl, $tanggal, $waktu);
		}

		public static function formatWaktu2($tgl)
		{
			$explode = explode("-", $tgl);
			$exec = $explode[2]."-".$explode[1]."-".$explode[0];
			return $exec;
		}

		public static function getWaktuSql($param)
		{
			$ex = explode("/",$param);
			$hasil = $ex[2]."-".$ex[1]."-".$ex[0];
			return $hasil;
		}

		public static function tes($tgl)
		{
			$explode = explode("-", $tgl);
			$exec = $explode[0]."-".$explode[1]."-".$explode[2];
			return $exec;
		}

		public static function clean($string) 
		{
		   $string = trim(ucwords($string));
		   $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

		   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		}
		

		public static function baseUrl()
		{
			return Yii::app()->request->baseUrl;
		}

		
		public static function createUrl($param)
		{
			return Yii::app()->createUrl($param);
		} 

		public static function db()
		{
			return Yii::app()->db;
		}

		public static function query($param)
		{
			return self::db()->createCommand($param);
		}


		public static function queryBuilder()
		{
			return self::db()->createCommand();
		}

		public static function namaMenu()
		{
			$controller = Yii::app()->controller->id;

			$get = self::queryBuilder()
			->select('nama_menu')
			->from('menu')
			->where('controller=:cont' , array(':cont' => $controller))
			->queryScalar();

			return $get;
		}

		public static function labelAksi($self = "")
		{
			$aksi = Yii::app()->controller->action->id;
			if($aksi == 'create')
			{
				$nama = 'Form Tambah Data';
			}elseif($aksi == 'update'){
				$nama = 'Form Update Data';
			}else{
				$nama = $self;
			}

			return $nama;
		}

		public static function flash($key , $value)
		{
			return Yii::app()->user->setFlash($key , $value);
		}


		public static function encrypt($ENTEXT)
		{
			return base64_encode(base64_encode($ENTEXT));
		}
		 
		public static function decrypt($DETEXT)
		{
			return base64_decode(base64_decode($DETEXT));
		}


		public static function getTanggalSql($param)
		{
			$ex = explode(" ",$param);
			$exTanggal = explode("/",$ex[0]);
			$hasilTanggal = $exTanggal[2]."-".$exTanggal[1]."-".$exTanggal[0];
			$execute = $hasilTanggal." ".$ex[1];

			return $execute;
		}
	
		
	
		public static function getTanggalView($param)
		{
			$ex = explode(" ",$param);
			$exTanggal = explode("-",$ex[0]);
			$hasilTanggal = $exTanggal[2]."/".$exTanggal[1]."/".$exTanggal[0];
			$execute = $hasilTanggal." ".$ex[1];

			return $execute;
		}
		
		public static function getTanggalSqlNoWaktu($param , $pemisah)
		{
			$exTanggal = explode($pemisah,$param);
			$hasilTanggal = $exTanggal[2]."$pemisah".$exTanggal[1]."$pemisah".$exTanggal[0];
			return $hasilTanggal;
		}
		
		
		public static function getTanggalViewNoWaktu($param , $pemisah)
		{
			
			$exTanggal = explode($pemisah,$param);
			$hasilTanggal = $exTanggal[2]."$pemisah".$exTanggal[1]."$pemisah".$exTanggal[0];
			$execute = $hasilTanggal;

			return $execute;
		}

		public  static function jamKeMenit($hours)
		{
			if (strstr($hours, ':'))
			{
				# Split hours and minutes.
				$separatedData = split(':', $hours);

				$minutesInHours    = $separatedData[0] * 60;
				$minutesInDecimals = $separatedData[1];

				$totalMinutes = $minutesInHours + $minutesInDecimals;
			}
			else
			{
				$totalMinutes = $hours * 60;
			}

			return $totalMinutes;
		}


		function menitKeJam($minutes)
		{
			$hours          = floor($minutes / 60);
			$decimalMinutes = $minutes - floor($minutes/60) * 60;

			# Put it together.
			$hoursMinutes = sprintf("%d:%02.0f", $hours, $decimalMinutes);
			return $hoursMinutes;
		}


	}