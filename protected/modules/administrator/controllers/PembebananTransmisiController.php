<?php
	class PembebananTransmisiController extends Controller
	{
		public $layout = '//layouts/admin/utama';
	
	
		public function actionGetTransmisi()
		{
		
			$data = MasterTransmisi::model()->findAll("gardu_induk_id=:gardu_induk_id" , array(':gardu_induk_id' => $_POST['gardu_induk_id']) , array('order' => 'id DESC'));
			$data = CHtml::listdata($data , 'id' ,'transmisi');
			echo "<option value=''></option>";
			foreach($data as $key => $value)
			{
				echo CHtml::tag('option' , array('value' => $key) , CHtml::encode($value) , true);
			}
		}
	
		public function actionIndex()
		{
					$model = PembebananTransmisi::model()->findAll();
					$hasil = '';
					foreach($model as $row)
					{
							$hasil  .= "
								<tr>
									<td>".$row->transmisi->garduInduk->gardu_induk."</td>
									<td>".$row->transmisi->transmisi."</td>
									<td>".ar::formatWaktu($row->tanggal_full, 'medium','')."</td>
									<td>".ar::formatWaktu($row->date_post, 'medium','')."</td>
									<td>".$row->user->nama."</td>
									<td>".CHtml::link(ar::btnEdit() , array('update' , 'id' => $row->id)).
								       " ".
									  CHtml::link(ar::btnHapus() , array('hapus' , 'id' => $row->id , 'token' => ar::encrypt($row->id)) , array('onclick' => 'return confirm("Anda Yakin Menghapus Data ini ?")') )
							 ."</td>
								</tr>
							";
					}	

					$this->render('index' , array('hasil' => $hasil));
		}


		public function actionCreate()
		{
			$model = new PembebananTransmisi;
			if(isset($_POST['PembebananTransmisi']))
			{
				$koneksi = Yii::app()->db;
				$transaksi = $koneksi->beginTransaction();
				$model->attributes = $_POST['PembebananTransmisi'];
				if($model->validate())
				{	
					try
					{
							$model->attributes = $_POST['PembebananTransmisi'];
							if($model->save())
							{
								
								$jamTinggi = PembebananTransmisi::model()->greatest($model->id , 'jam');
								$jumlah = PembebananTransmisi::model()->greatest($model->id , 'jumlah');
								$field = PembebananTransmisi::model()->greatest($model->id , 'field');
								/*$update = PembebananTransmisi::model()->findByPk($model->id);
								$update->jam_max = $jamTinggi;
								$update->jumlah_max = $jumlah;
								$update->save();*/
								
								$update = Yii::app()->db->createCommand("update pembebanan_transmisi SET jam_max = '$jamTinggi' , jumlah_max = '$jumlah' , pembanding = '$field' WHERE id = '$model->id'")->execute();
										
							}
								$transaksi->commit();
								//echo $jumlah."<h1>TES</h1>";
								Yii::app()->user->setFlash('info' , 'Data telah disimpan!');
								$this->redirect(array('index'));
							
				
					}catch(Exception $e){
								 
								$transaksi->rollback();
								Yii::app()->user->setFlash('error' , 'Data gagal disimpan! , cek database anda!');
							$this->redirect(array('index'));
					}	
				}
			}
			$this->render('_form' , array('model' => $model));
		}

		public function actionUpdate($id)
		{
			$model = $this->loadModel($id);
			$model->tanggal_full = ar::getTanggalViewNoWaktu($model->tanggal_full , "-");
			if(isset($_POST['PembebananTransmisi']))
			{
				$koneksi = Yii::app()->db;
				$transaksi = $koneksi->beginTransaction();
				$model->attributes = $_POST['PembebananTransmisi'];
				if($model->validate())
				{	
					try
					{
							$model->attributes = $_POST['PembebananTransmisi'];
							if($model->save())
							{
								
								$jamTinggi = PembebananTransmisi::model()->greatest($model->id , 'jam');
								$jumlah = PembebananTransmisi::model()->greatest($model->id , 'jumlah');
								$field = PembebananTransmisi::model()->greatest($model->id , 'field');
								
								/*$update = PembebananTransmisi::model()->findByPk($model->id);
								$update->jam_max = $jamTinggi;
								$update->jumlah_max = $jumlah;
								$update->save();*/
								
								$update = Yii::app()->db->createCommand("update pembebanan_transmisi SET jam_max = '$jamTinggi' , jumlah_max = '$jumlah' , pembanding = '$field' WHERE id = '$model->id'")->execute();
										
							}
								$transaksi->commit();
								//echo $jumlah."<h1>TES</h1>";
								Yii::app()->user->setFlash('info' , 'Data telah disimpan!');
								$this->redirect(array('index'));
							
				
					}catch(Exception $e){
								 
								$transaksi->rollback();
								Yii::app()->user->setFlash('error' , 'Data gagal disimpan! , cek database anda!');
							$this->redirect(array('index'));
					}	
				}
			}
			$this->render('_form' , array('model' => $model));
		}

		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			if($cek == $id)
			{
				try
				{
					$model = $this->loadModel($id);
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}

		public function loadModel($param)
		{
			$model = PembebananTransmisi::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}

		protected function performAjaxValidation($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='role-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
	}