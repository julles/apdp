<?php
	class TransKinerjaPenyaluranController extends Controller
	{
		public $layout = '//layouts/admin/utama';
		
		public function actionIndex()
		{
			$db = Yii::app()->db->createCommand();
			$model = $db->select("t.date_post , tahun , t.id AS idna ,  k.kinerja_penyaluran")->from('trans_kinerja_penyaluran t')
			//->join("user u" , "t.posted_by = u.id")
			->join('master_kinerja_penyaluran k' , 't.kinerja_penyaluran_id=k.id')
			->query();

			$this->render('index' , 
				array(
					'model' => $model
				)
			);
		}

		public function actionCreate()
		{
			$model = new TransKinerjaPenyaluran;
			$db = Yii::app()->db;
			$transaksi = $db->beginTransaction();
			if(isset($_POST['TransKinerjaPenyaluran']))
			{
				$model->attributes = $_POST['TransKinerjaPenyaluran'];
				$cek = $db->createCommand()->select("id")->from("trans_kinerja_penyaluran")->where('tahun=:tahun AND kinerja_penyaluran_id=:kinerja' , array(':tahun' => $model->tahun , ':kinerja' => $model->kinerja_penyaluran_id ))->queryScalar();
				if(!empty($cek))
				{
					$model->addError("kinerja_penyaluran_id" , "kinerja sudah di input sebelumnya" );
					//return false;
				}else{	
					try 
					{
						if($model->save())
						{
							for($a=0;$a<12;$a++)
							{
							 	$sql = "INSERT INTO trans_kinerja_penyaluran_detail VALUES('','$model->id' , '".$_POST['bulan'][$a]."' , '".$_POST['target'][$a]."' , '".$_POST['real'][$a]."' , '".$_POST['target_kumulatif'][$a]."' , '".$_POST['real_kumulatif'][$a]."')";
								$exec = $db->createCommand($sql)->execute();
							}		
						}

						$transaksi->commit();
						Yii::app()->user->setFlash('success' , 'Data telah disimpan!');
						$this->redirect(array('index'));
					} catch (Exception $e) {
							$transaksi->rollback();
						Yii::app()->user->setFlash('danger' , 'Data gagal disimpan!');
						$this->redirect(array('index'));
					}
				}	
					
			}
			$this->render('_form' , array('model' => $model));
		}


		public function actionUpdate($id)
		{
			$model = $this->loadModel($id);
			$db = Yii::app()->db;
			$transaksi = $db->beginTransaction();
			if(isset($_POST['TransKinerjaPenyaluran']))
			{
				$model->attributes = $_POST['TransKinerjaPenyaluran'];
				$cek = $db->createCommand()->select("id")->from("trans_kinerja_penyaluran")->where('tahun=:tahun AND kinerja_penyaluran_id=:kinerja AND id!=:idna' , array(':tahun' => $model->tahun , ':kinerja' => $model->kinerja_penyaluran_id ,':idna' => $model->id))->queryScalar();
				if(!empty($cek))
				{
					$model->addError("kinerja_penyaluran_id" , "kinerja sudah di input sebelumnya" );
					//return false;
				}else{	
					try 
					{
						if($model->save())
						{
							$sqlDel = "DELETE FROM trans_kinerja_penyaluran_detail WHERE trans_kinerja_penyaluran_id = '$model->id'";
							$exec = $db->createCommand($sqlDel)->execute();

							for($a=0;$a<12;$a++)
							{
							 	$sql = "INSERT INTO trans_kinerja_penyaluran_detail VALUES('','$model->id' , '".$_POST['bulan'][$a]."' , '".$_POST['target'][$a]."' , '".$_POST['real'][$a]."' , '".$_POST['target_kumulatif'][$a]."' , '".$_POST['real_kumulatif'][$a]."')";
								$exec = $db->createCommand($sql)->execute();
							}		
						}

						$transaksi->commit();
						Yii::app()->user->setFlash('success' , 'Data telah diupdate!');
						$this->redirect(array('index'));
					} catch (Exception $e) {
							$transaksi->rollback();
						Yii::app()->user->setFlash('danger' , 'Data gagal diupdate!');
						$this->redirect(array('index'));
					}
				}	
					
			}
			$this->render('_form' , array('model' => $model));
		}

		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			$db = Yii::app()->db;
			$model = $this->loadModel($id);
			if($cek == $id)
			{
				try
				{
					
					$db->createCommand("DELETE FROM trans_kinerja_penyaluran_detail WHERE trans_kinerja_penyaluran_id = '$model->id'")->execute();
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}

		public function loadModel($param)
		{
			$model = TransKinerjaPenyaluran::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}
	}