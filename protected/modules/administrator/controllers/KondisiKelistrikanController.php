<?php
	class KondisiKelistrikanController extends Controller
	{
			public $layout = '//layouts/admin/utama';

			public function actionIndex()
			{
					$criteria = new CDbCriteria;
					$criteria->order = 'tanggal_full DESC';
					$model = KondisiKelistrikan::model()->findAll($criteria);
					$hasil = '';
					foreach($model as $row)
					{
							$hasil  .= "
								<tr>
									<td>".ar::formatWaktu($row->tanggal_full, 'medium','')."</td>
									<td>".$row->kondisi."</td>
									
									<td>".CHtml::link(ar::btnEdit() , array('update' , 'id' => $row->id)).
								       " ".
									  CHtml::link(ar::btnHapus() , array('hapus' , 'id' => $row->id , 'token' => ar::encrypt($row->id)) , array('onclick' => 'return confirm("Anda Yakin Menghapus Data ini ?")') )
							 ."</td>
								</tr>
							";
					}	

					$this->render('index' , array('hasil' => $hasil));
			}


			public function actionCreate()
			{
				$model = new KondisiKelistrikan;
				$kondisi = array(
					'Normal' => 'Normal',
					'Siaga' => 'Siaga',
					'Defisit' => 'Defisit'
				);

				if(isset($_POST['KondisiKelistrikan']))
				{
					$model->attributes = $_POST['KondisiKelistrikan'];
					if($model->validate())
					{
						try
						{
							if($model->save())
							{
								ar::flash('info' , 'Data telah disimpan');
								$this->redirect(array('index'));
							}
						}catch(Exception $e){
								ar::flash('error' , 'Data gagal disimpan!');
								$this->redirect(array('index'));
						}
					}
				}


				$this->render('_form' , array(
						'model' => $model,
						'kondisi' => $kondisi,
				));
			}


			public function actionUpdate($id)
			{
				$model = $this->loadModel($id);
				$model->tanggal_full = ar::getTanggalViewNoWaktu($model->tanggal_full,"-");
				$kondisi = array(
					'Normal' => 'Normal',
					'Siaga' => 'Siaga',
					'Defisit' => 'Defisit'
				);

				if(isset($_POST['KondisiKelistrikan']))
				{
					$model->attributes = $_POST['KondisiKelistrikan'];
					if($model->validate())
					{
						try
						{
							if($model->save())
							{
								ar::flash('info' , 'Data telah disimpan');
								$this->redirect(array('index'));
							}
						}catch(Exception $e){
								ar::flash('error' , 'Data gagal disimpan!');
								$this->redirect(array('index'));
						}
					}
				}


				$this->render('_form' , array(
						'model' => $model,
						'kondisi' => $kondisi,
				));
			}



			public function actionHapus($id , $token = "")
			{
				$cek = ar::decrypt($token);
				if($cek == $id)
				{
					try
					{
						$model = $this->loadModel($id);
						if($model->delete())
						{
							ar::flash('info' , 'Data telah dihapus');
							$this->redirect(array('index'));
						}
					
					}catch(Exception $e){
							ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
							$this->redirect(array('index'));
					}
					

				}else{
					throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
					
				}
			}

		public function loadModel($param)
		{
			$model = KondisiKelistrikan::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}


			protected function performAjaxValidation($model)
			{
				if(isset($_POST['ajax']) && $_POST['ajax']==='role-form')
				{
					echo CActiveForm::validate($model);
					Yii::app()->end();
				}
			}
	}