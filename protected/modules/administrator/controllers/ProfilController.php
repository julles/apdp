<?php

	class ProfilController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
			$count = ar::query("SELECT COUNT(id) FROM profil")->queryScalar();
			if($count > 0)
			{
				$get = Yii::app()->db->createCommand()->select('id')
				->from('profil')
				->order('id DESC')
				->limit(1)
				->queryScalar();


				$model = Profil::model()->findByPk($get);
				

			}else{

				$model = new Profil;
				
			}


			if(isset($_POST['Profil']))
			{
				$model->attributes = $_POST['Profil'];
				$model->posted_by = Yii::app()->session['tokenId'];
				$model->date_post = date("Y-m-d H:i:s");
				if($model->save())
				{
					ar::flash('info' , 'Data telah diUpdate!');
					$this->refresh();

				}
			}

			$this->render('_form' ,  array('model' => $model));
		


		}

	}