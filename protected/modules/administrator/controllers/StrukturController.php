<?php

	class StrukturController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
			$count = ar::query("SELECT COUNT(id) FROM struktur")->queryScalar();
			if($count > 0)
			{
				$get = Yii::app()->db->createCommand()->select('id')
				->from('struktur')
				->order('id DESC')
				->limit(1)
				->queryScalar();


				$model = Struktur::model()->findByPk($get);
				

			}else{

				$model = new Struktur;
				
			}



			

			if(isset($_POST['Struktur']))
			{
				$model->attributes = $_POST['Struktur'];
				$model->posted_by = Yii::app()->session['tokenId'];
				$model->date_post = date("Y-m-d H:i:s");
				$file = CUploadedFile::getInstance($model , 'gambar');
				if(!empty($file))
				{
						if($model->save())
						{
							$ext = pathinfo($file , PATHINFO_EXTENSION);
							$gambar = $model->id.".$ext";


							list($width, $height) = getimagesize($file->getTempName());
							
							$dir_th = Yii::app()->basePath.'/../images/struktur/thumb/';
							$img = Yii::app()->simpleImage->load($file->getTempName());

							if ($width > $height) { 
							     $img->resizeToWidth(565);
							 }else{
						     	$img->resizeToHeight(565); 
							 }

							  $img->save($dir_th.$gambar);


								$file->saveAs(Yii::app()->basePath."/../images/struktur/$gambar");

								$update = Struktur::model()->findByPk($model->id);
								$update->gambar = $gambar;
								$update->save();



								ar::flash('info' , 'Data telah diUpdate!');
								$this->refresh();

						}
			
				}else{
						if(!$model->isNewRecord)
						{
							$model->gambar = $_POST['patokanGambar'];
						}
						if($model->save())
						{
								ar::flash('info' , 'Data telah diUpdate!');
								$this->refresh();
						}

				}
			}

			$this->render('_form' ,  array('model' => $model));
		


		}

	}