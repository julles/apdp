<?php
	class KinerjaPenyaluranController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
					$model = MasterKinerjaPenyaluran::model()->findAll();
					$hasil = '';
					foreach($model as $row)
					{
						$hasil .= "
							<tr>
								<td>$row->kinerja_penyaluran</td>
								<td>".$row->user->nama."</td>
								<td>".ar::formatWaktu($row->date_post , "medium","")."</td>
								
								<td>".CHtml::link(ar::btnEdit() , array('update' , 'id' => $row->id)).
								       " ".
									  CHtml::link(ar::btnHapus() , array('hapus' , 'id' => $row->id , 'token' => ar::encrypt($row->id)) , array('onclick' => 'return confirm("Anda Yakin Menghapus Data ini ?")') )
							 ."</td>
							</tr>
						";
					}


					$this->render('index' , array('hasil' => $hasil));
		}


		public function actionCreate()
		{
			$model = new MasterKinerjaPenyaluran;
			if(isset($_POST['MasterKinerjaPenyaluran']))
			{
				$model->attributes = $_POST['MasterKinerjaPenyaluran'];
				if($model->validate())
				{
					if($model->save())
					{
						ar::flash('info' , 'Data telah disimpan');
						$this->redirect(array('index'));

					}
				}
			}

			$this->render('_form' , array('model' => $model));
		}

		public function actionUpdate($id)
		{
			$model = $this->loadModel($id);
			if(isset($_POST['MasterKinerjaPenyaluran']))
			{
				$model->attributes = $_POST['MasterKinerjaPenyaluran'];
				if($model->save())
				{
					ar::flash('info' , 'Data telah diupdate!');
					$this->redirect(array('index'));
				}
			}

			$this->render('_form' , array('model' => $model));
		}

		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			if($cek == $id)
			{
				try
				{
					$model = $this->loadModel($id);
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}

		public function loadModel($param)
		{
			$model = MasterKinerjaPenyaluran::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}

		protected function performAjaxValidation($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='role-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
	}