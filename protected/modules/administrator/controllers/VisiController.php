<?php
	class VisiController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
			$count = ar::query("SELECT COUNT(id) FROM visi")->queryScalar();
			if($count > 0)
			{
				$get = Yii::app()->db->createCommand()->select('id')
				->from('visi')
				->order('id DESC')
				->limit(1)
				->queryScalar();


				$model = Visi::model()->findByPk($get);
				

			}else{

				$model = new Visi;
				
			}


			if(isset($_POST['Visi']))
			{
				$model->attributes = $_POST['Visi'];
				$model->posted_by = Yii::app()->session['tokenId'];
				$model->date_post = date("Y-m-d H:i:s");
				if($model->save())
				{
					ar::flash('info' , 'Data telah diUpdate!');
					$this->refresh();

				}
			}

			$this->render('_form' ,  array('model' => $model));
		


		}

	}