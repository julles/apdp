<?php

class DefaultController extends Controller
{

	public $layout = '//layouts/admin/utama';
	public function actionIndex()
	{	
		if(empty(Yii::app()->session['tokenId']))
		{
				echo "
					<script>alert('anda belum login!!!');</script>
				";
				$this->redirect(array('login'));
		}else{
			$this->render('index');
		}
		
	}


	public function makeTimeSessi()
	  {

	  		$timeout = 10;
			Yii::app()->session['expires_by'] = time() + $timeout;
			 
	  }


	public function actionLogin()
	{
			$this->layout = '//layouts/admin/login';
			$db = Yii::app()->db;
			if(isset($_POST['cmd_login']))
			{

			 

					$search = $db->createCommand()->select('id , role_id')
					->from('user')
					->where('username=:username AND password=:password' , array(':username' => $_POST['username'] , ':password' => md5($_POST['password'])))
					->queryRow();

					if(!empty($search['id']))
					{
							$sqlSessi = "SELECT user_id FROM sessi_login WHERE user_id = '$search[id]'";

							$cekSessi = $db->createCommand($sqlSessi." AND status = '1'")->queryScalar();						
							if(!empty($cekSessi))
							{
								//ar::flash('danger' ,"Anda tidak bisa login dengan username $_POST[username]! , akun ini sedang login!!");
								//$this->refresh();
								Yii::app()->session['tokenId'] = $search['id'];
								Yii::app()->session['tokenRole'] = $search['role_id'];
									$this->makeTimeSessi();
								$this->redirect(array('index'));
							}else{
									$cekHitSessi = $db->createCommand($sqlSessi)->queryScalar();	
									if(empty($cekHitSessi))
									{
										$simpanSessi = new SessiLogin;
										
									}else{
										$simpanSessi = SessiLogin::model()->findByAttributes(array("user_id" => $search['id']));
									}
									$simpanSessi->user_id = $search['id'];
									$simpanSessi->status = 1;
									$simpanSessi->save();

									Yii::app()->session['tokenId'] = $search['id'];
									Yii::app()->session['tokenRole'] = $search['role_id'];
									$this->makeTimeSessi();
									$this->redirect(array('index'));
						
							}

							

					}else{
						ar::flash('danger' ,'username atau password salah!!');
						$this->refresh();
					}
			}
			$this->render('login');
	}

	public function actionSetSessi()
	{

		 //echo Yii::app()->session['tesis'] = 'kika';

		Yii::app()->session->remove('tokenRole');

		Yii::app()->session->remove('expires_by');
		Yii::app()->session->remove('tokenId');

		$this->redirect(array('/administrator/default/login'));

	}

	public function actionLogout()
	{
		Yii::app()->db->createCommand("UPDATE sessi_login SET status = '0' WHERE user_id = '".Yii::app()->session['tokenId']."'")->execute();

		Yii::app()->session->remove('tokenRole');

		Yii::app()->session->remove('expires_by');
		Yii::app()->session->remove('tokenId');

		$this->redirect(array('/site/'));
	}
}