<?php
	class PengumumanController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
					$model = Pengumuman::model()->findAll();
					$hasil = '';
					foreach($model as $row)
					{
						$hasil .= "
							<tr>
								<td>$row->judul</td>
								<td>".ar::formatWaktu($row->date_post , 'medium' , '')."</td>
								
								<td>".CHtml::link(ar::btnEdit() , array('update' , 'id' => $row->id)).
								       " ".
									  CHtml::link(ar::btnHapus() , array('hapus' , 'id' => $row->id , 'token' => ar::encrypt($row->id)) , array('onclick' => 'return confirm("Anda Yakin Menghapus Data ini ?")') )
							 ."</td>
							</tr>
						";
					}


					$this->render('index' , array('hasil' => $hasil));
		}


		public function actionCreate()
		{
			$model = new Pengumuman;
			if(isset($_POST['Pengumuman']))
			{
				$model->attributes = $_POST['Pengumuman'];
				$file = CUploadedFile::getInstance($model , 'gambar');
				if(!empty($file))
				{
						if($model->save())
						{
							$ext = pathinfo($file , PATHINFO_EXTENSION);
							$gambar = $model->id.".$ext";


							list($width, $height) = getimagesize($file->getTempName());
							
							$dir_th = Yii::app()->basePath.'/../images/pengumuman/thumb/';
							$img = Yii::app()->simpleImage->load($file->getTempName());

							if ($width > $height) { 
							     $img->resizeToWidth(565);
							 }else{
						     	$img->resizeToHeight(565); 
							 }

							  $img->save($dir_th.$gambar);


								$file->saveAs(Yii::app()->basePath."/../images/pengumuman/$gambar");

								$update = $this->loadModel($model->id);
								$update->gambar = $gambar;
								$update->save();



								ar::flash('info' , 'Data telah disimpan!');
								$this->redirect(array('index'));

						}
			
				}else{
						if(!$model->isNewRecord)
						{
							$model->gambar = $_POST['patokanGambar'];
						}


						if($model->save())
						{
								ar::flash('info' , 'Data telah disimpan!');
								$this->redirect(array('index'));
						}

				}
			}

			$this->render('_form' , array('model' => $model));
		}

		public function actionUpdate($id)
		{
			$model = $this->loadModel($id);
			if(isset($_POST['Pengumuman']))
			{
				$model->attributes = $_POST['Pengumuman'];
				$file = CUploadedFile::getInstance($model , 'gambar');
				if(!empty($file))
				{
						if($model->save())
						{
							$ext = pathinfo($file , PATHINFO_EXTENSION);
							$gambar = $model->id.".$ext";


							list($width, $height) = getimagesize($file->getTempName());
							
							$dir_th = Yii::app()->basePath.'/../images/pengumuman/thumb/';
							$img = Yii::app()->simpleImage->load($file->getTempName());

							if ($width > $height) { 
							     $img->resizeToWidth(565);
							 }else{
						     	$img->resizeToHeight(565); 
							 }

							  $img->save($dir_th.$gambar);


								$file->saveAs(Yii::app()->basePath."/../images/pengumuman/$gambar");

								$update = $this->loadModel($model->id);
								$update->gambar = $gambar;
								$update->save();



								ar::flash('info' , 'Data telah diupdate!');
								$this->redirect(array('index'));

						}
			
				}else{
						if(!$model->isNewRecord)
						{
							$model->gambar = $_POST['patokanGambar'];
						}


						if($model->save())
						{
								ar::flash('info' , 'Data telah diupdate!');
								$this->redirect(array('index'));
						}

				}
			}

			$this->render('_form' , array('model' => $model));

		}

		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			if($cek == $id)
			{
				try
				{
					$model = $this->loadModel($id);
					$alamat = Yii::app()->basePath."/../images/pengumuman/";
					
					$big = $alamat.$model->gambar;
					$thumb = $alamat."thumb/".$model->gambar;

					if(file_exists($big))
					{
						@unlink($big);
					}

					if(file_exists($thumb))
					{
						@unlink($thumb);
					}

					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}

		public function loadModel($param)
		{
			$model = Pengumuman::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}

		protected function performAjaxValidation($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='berita-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
	}