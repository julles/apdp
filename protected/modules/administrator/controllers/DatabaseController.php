<?php
	class DatabaseController extends Controller
	{
		public $layout = '//layouts/admin/utama';
		public function actionIndex(){
			$model = ManageDatabase::model()->findAll();
			$hasil = '';
			foreach($model as $row){
				$hasil .= "
					<tr>
						<td>";
						$hasil.= CHtml::link('<i class="fa fa-plus"></i>&nbsp;'.$row->nama_file.'.xml' , array('download', 'file' => $row->nama_file) , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
						$hasil.=" </td>
						<td>".$row->keterangan."</td>
						<td>".$row->user->nama."</td>
						<td>".$row->date_post."</td><td>";
						$hasil.= CHtml::link('Restore' , array('restore','file'=>$row->nama_file) , array('class' => 'btn btn-primary' , 'style' => 'text-decoration:none;'));
						$hasil.="</td><!--<td>".ar::formatWaktu($row->date_post , "medium","")."</td>-->
					</tr>
				";
			} 
			$this->render('index' , array('hasil' => $hasil));
		}
		public function actionCreate(){
			$model = new ManageDatabase;
			$name = "apdp-backup-".date("Y-m-d_his");
			if(isset($_POST['ManageDatabase'])){
				$model->nama_file = $name;
				$db_config = Array
							(
								'dbtype' => "MYSQL",
								'host' => "localhost",
								'database' => "dbs_apdp",
								'user' => "root",
								'password' => "",
							);
				$dbimexport = new dbimexport($db_config);  
				$dbimexport->download = false;
				$dbimexport->download_path = Yii::getPathOfAlias('webroot')."/BACK_UP_DB/"; 
				$dbimexport->file_name = $name;
				$dbimexport->export();
				/*
				switch( $_GET['select'] )
				{
					case "download_inline":
						$dbimexport->download_path = "";
						$dbimexport->download = true;
						$dbimexport->file_name = date("Y-m-d_H-i-s");
						$dbimexport->export();
					break;

					case "save_to_disc":
						$dbimexport->download = false;
						$dbimexport->download_path = $_SERVER['DOCUMENT_ROOT'] . "/development/apdp/BACK_UP_DB/";
						$dbimexport->file_name = $name."-backup-".time();
						$dbimexport->export();
					break;

					case "import":
						$dbimexport->import_path = $_SERVER['DOCUMENT_ROOT'] . "/development/apdp/BACK_UP_DB/db_belajar_yii-backup-1409689957.xml";
						$dbimexport->import();
					break;
				}
				*/
				$model->nama_file = $name;
				$model->attributes = $_POST['ManageDatabase'];
				if($model->save())
				{
					ar::flash('info' , 'Data telah disimpan, Back up file xml berada di BACK_UP_DB/'.$name.'.xml');
					$this->redirect(array('index'));
				}
			}
			//print_r($_POST);

			$this->render('_form' , array('model' => $model));			
		}
		public function actionDownload($file){
			$filena=$file.'.xml'; 
			$loc =  Yii::getPathOfAlias('webroot')."/BACK_UP_DB/";  
			Yii::app()->getRequest()->sendFile($filena , file_get_contents( $loc.$filena ) );
		}
		public function actionRestore($file){
			$filena=$file.'.xml'; 
			$loc =  Yii::getPathOfAlias('webroot')."/BACK_UP_DB/";  
			$db_config = Array
						(
							'dbtype' => "MYSQL",
							'host' => "localhost",
							'database' => "dbs_apdp",
							'user' => "root",
							'password' => "",
						);
			$dbimexport = new dbimexport($db_config);  
			$dbimexport->download = false; 
			$dbimexport->import_path = $loc.$filena;
			$dbimexport->import();
			$this->redirect(array('index'));
		}
	}
?>