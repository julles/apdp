<?php
	class TransKinerjaScadatelController extends Controller
	{
		public $layout = '//layouts/admin/utama';
		
		public function actionIndex()
		{
			$db = Yii::app()->db->createCommand();
			$model = $db->select("t.date_post , tahun , t.id AS idna ,  k.kinerja_scadatel")->from('trans_kinerja_scadatel t')
			->join("master_kinerja_scadatel k" , "t.kinerja_scadatel_id = k.id")
			//->join("user u" , "t.posted_by = u.id")
			->order('t.date_post DESC')
			->query();

			$this->render('index' , 
				array(
					'model' => $model
				)
			);
		}

		public function actionCreate()
		{
			$model = new TransKinerjaScadatel;
			$db = Yii::app()->db;
			$transaksi = $db->beginTransaction();
			if(isset($_POST['TransKinerjaScadatel']))
			{
				$model->attributes = $_POST['TransKinerjaScadatel'];
				$cek = $db->createCommand()->select("id")->from("trans_kinerja_scadatel")->where('tahun=:tahun AND kinerja_scadatel_id=:kinerja' , array(':tahun' => $model->tahun , ':kinerja' => $model->kinerja_scadatel_id ))->queryScalar();
				if(!empty($cek))
				{
					$model->addError("kinerja_scadatel_id" , "kinerja sudah di input sebelumnya" );
					//return false;
				}else{	
					try 
					{
						if($model->save())
						{
							for($a=0;$a<12;$a++)
							{
							 	$sql = "INSERT INTO trans_kinerja_scadatel_detail VALUES('','$model->id' , '".$_POST['bulan'][$a]."' , '".$_POST['target'][$a]."' , '".$_POST['real'][$a]."', '".$_POST['target_kumulatif'][$a]."' , '".$_POST['real_kumulatif'][$a]."')";
								$exec = $db->createCommand($sql)->execute();
							}		
						}

						$transaksi->commit();
						Yii::app()->user->setFlash('success' , 'Data telah disimpan!');
						$this->redirect(array('index'));
					} catch (Exception $e) {
							$transaksi->rollback();
						Yii::app()->user->setFlash('danger' , 'Data gagal disimpan!');
						$this->redirect(array('index'));
					}
				}	
					
			}
			$this->render('_form' , array('model' => $model));
		}


		public function actionUpdate($id)
		{
			$model = $this->loadModel($id);
			$db = Yii::app()->db;
			$transaksi = $db->beginTransaction();
			if(isset($_POST['TransKinerjaScadatel']))
			{
				$model->attributes = $_POST['TransKinerjaScadatel'];
				$cek = $db->createCommand()->select("id")->from("trans_kinerja_scadatel")->where('tahun=:tahun AND kinerja_scadatel_id=:kinerja AND id!=:idna' , array(':tahun' => $model->tahun , ':kinerja' => $model->kinerja_scadatel_id ,':idna' => $model->id))->queryScalar();
				if(!empty($cek))
				{
					$model->addError("kinerja_scadatel_id" , "kinerja sudah di input sebelumnya" );
					//return false;
				}else{	
					try 
					{
						if($model->save())
						{
							$sqlDel = "DELETE FROM trans_kinerja_scadatel_detail WHERE trans_kinerja_scadatel_id = '$model->id'";
							$exec = $db->createCommand($sqlDel)->execute();

							for($a=0;$a<12;$a++)
							{
							 	$sql = "INSERT INTO trans_kinerja_scadatel_detail VALUES('','$model->id' , '".$_POST['bulan'][$a]."' , '".$_POST['target'][$a]."' , '".$_POST['real'][$a]."', '".$_POST['target_kumulatif'][$a]."' , '".$_POST['real_kumulatif'][$a]."')";
								$exec = $db->createCommand($sql)->execute();
							}		
						}

						$transaksi->commit();
						Yii::app()->user->setFlash('success' , 'Data telah diupdate!');
						$this->redirect(array('index'));
					} catch (Exception $e) {
							$transaksi->rollback();
						Yii::app()->user->setFlash('danger' , 'Data gagal diupdate!');
						$this->redirect(array('index'));
					}
				}	
					
			}
			$this->render('_form' , array('model' => $model));
		}

		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			$db = Yii::app()->db;
			$model = $this->loadModel($id);
			if($cek == $id)
			{
				try
				{
					
					$db->createCommand("DELETE FROM trans_kinerja_scadatel_detail WHERE trans_kinerja_scadatel_id = '$model->id'")->execute();
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}

		public function loadModel($param)
		{
			$model = TransKinerjaScadatel::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}
	}