<?php

	class NeracaDayaController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
			$this->render('index');
		}

		public function actionCreate()
		{
			$model = new TransNeracaDaya;
			if(isset($_POST['TransNeracaDaya']))
			{
				$model->attributes = $_POST['TransNeracaDaya'];
				if($model->save())
				{
					ar::flash('info' , 'Data telah disimpan!');
					$this->redirect(array('index'));
				}
			}
			$this->render('_form' , array('model' => $model));
		}


		public function actionUpdate($id)
		{
			$model = TransNeracaDaya::model()->findByPk($id);
			
			$model->tanggal = ar::getTanggalViewNoWaktu($model->tanggal , "-");

			if(isset($_POST['TransNeracaDaya']))
			{
				$model->attributes = $_POST['TransNeracaDaya'];
				$search = ar::query("SELECT id FROM trans_neraca_daya WHERE id != '$model->id' AND tanggal = '". date("Y-m-d" ,  strtotime($model->tanggal))."'")->queryScalar();
				if(!empty($search))
				{
					$model->addError("tanggal" ,"Tanggal sudah diinput sebelumnya!");

				}else{
					
					if($model->save())
						{
							ar::flash('info' , 'Data telah disimpan!');
							$this->redirect(array('index'));
						}
				}		
						
			}
			$this->render('_form' , array('model' => $model));
		}

		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			if($cek == $id)
			{
				try
				{
					$model = $this->loadModel($id);
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}

		public function loadModel($param)
		{
			$model = TransNeracaDaya::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}


	}