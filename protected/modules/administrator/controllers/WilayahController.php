<?php

	class WilayahController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
			$count = ar::query("SELECT COUNT(id) FROM wilayah_kerja")->queryScalar();
			if($count > 0)
			{
				$get = Yii::app()->db->createCommand()->select('id')
				->from('wilayah_kerja')
				->order('id DESC')
				->limit(1)
				->queryScalar();


				$model = WilayahKerja::model()->findByPk($get);
				

			}else{

				$model = new WilayahKerja;
				
			}


			if(isset($_POST['WilayahKerja']))
			{
				$model->attributes = $_POST['WilayahKerja'];
				$model->posted_by = Yii::app()->session['tokenId'];
				$model->date_post = date("Y-m-d H:i:s");
				if($model->save())
				{
					ar::flash('info' , 'Data telah diUpdate!');
					$this->refresh();

				}
			}

			$this->render('_form' ,  array('model' => $model));
		


		}

	}