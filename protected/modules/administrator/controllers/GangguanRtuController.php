<?php
	class GangguanRtuController extends Controller
	{
		public $layout = '//layouts/admin/utama';

		public function actionIndex()
		{
					$query = Yii::app()->db->createCommand()->select('g.id , tanggal_gangguan , jam_gangguan , g.date_post')->from('gangguan_rtu g')
					//->join('user u' , 'u.id = g.posted_by')
					->order('tanggal_gangguan DESC')
					->query();
					$this->render('index' , array(
						'model' => $query
					));
		}


		public function actionCreate()
		{
			/*$year = date('Y');
			$month = date('m');
			$day = date('d');
			$sql =  "SELECT count(id) FROM scadatel_gangguan_rtu WHERE year(waktu_gangguan) = $year AND month(waktu_gangguan) = $month AND day(waktu_gangguan) = $day";
			*/
			
			//$hit = Yii::app()->db->createCommand($sql)->queryScalar();
			//if($hit<=0)
		//	{
				$model = new GangguanRtu;
				if(isset($_POST['GangguanRtu']))
				{
					$model->attributes = $_POST['GangguanRtu'];
					if($model->validate())
					{
						if($model->save())
						{
							ar::flash('info' , 'Data telah disimpan');
							$this->redirect(array('index'));

						}
					}
				}

				
				$this->render('_form' , 
					array(
						'model' => $model,
					)
				);
			/*}else{
				ar::flash('info' , 'Anda sudah menginput Waktu Gangguan Rtu untuk Hari ini!');
				$this->redirect(array('index'));
			}*/
		}

		public function actionUpdate($id)
		{
			$model = $this->loadModel($id);
          
            $model->tanggal_gangguan = 	ar::getTanggalViewNoWaktu($model->tanggal_gangguan , "-");
			$model->tanggal_evaluasi = 	ar::getTanggalViewNoWaktu($model->tanggal_evaluasi , "-");
			
			if(isset($_POST['GangguanRtu']))
			{
				$model->attributes = $_POST['GangguanRtu'];
				if($model->validate())
				{
					if($model->save())
					{
						ar::flash('info' , 'Data telah disimpan');
						$this->redirect(array('index'));

					}
				}
			}

			
			$this->render('_form' , 
				array(
					'model' => $model,
				)
			);
		}

		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			if($cek == $id)
			{
				try
				{
					$model = $this->loadModel($id);
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}

		public function actionPencarian()
		{



				Yii::app()->clientScript->scriptMap['jquery.dataTables.js'] = false;
				$jenis_gardu =  $_POST['jenis_gardu'];
				$gardu_induk = $_POST['gardu_induk'];
				$criteria = new CDbCriteria();
				$criteria->compare('jenis_gardu_id' ,  $jenis_gardu , 'true');
				$criteria->compare('gardu_induk_id' ,  $gardu_induk , 'true');

				$model = GangguanRtu::model()->findAll($criteria);
				$this->renderpartial('pencarian' , array('model' => $model));
		}
                
                
                public function actionGetGardu()
                {
                    $sql = MasterGarduInduk::model()->findAll('jenis_gardu_induk_id=:jenis' , array(':jenis' => $_POST['id']));
                    $data = CHtml::listData($sql , 'id' ,'gardu_induk');
                    
                    echo "<option></option>";
                    
                    foreach($data as $key => $value)
                    {
                        echo CHtml::tag('option' , array('value' => $key) ,  CHtml::encode($value) , true);
                    }
                }
                
		public function loadModel($param)
		{
			$model = GangguanRtu::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}

		protected function performAjaxValidation($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='role-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
	}