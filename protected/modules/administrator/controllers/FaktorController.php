<?php
	class FaktorController extends Controller
	{
		public $layout = '//layouts/admin/utama';
		public function actionIndex()
		{
			$db = Yii::app()->db;
			$sql = "SELECT master_gardu_induk.gardu_induk , master_gardu_induk.id FROM master_faktor 
				INNER JOIN master_gardu_induk ON master_gardu_induk.id = master_faktor.gardu_induk_id
				GROUP BY master_faktor.gardu_induk_id
			";
			$data = $db->createCommand($sql)->queryAll();
			$this->render('index' , array('data' => $data));
		}

		public function actionGetDetail()
		{
			$db = Yii::app()->db;
			$gi = $_POST['gardu_induk_id'];
			$sql = "SELECT DISTINCT(jenis_id) , master_jenis_ne.jenis FROM master_item_ne
					INNER JOIN master_jenis_ne
					ON master_item_ne.jenis_id = master_jenis_ne.id WHERE gardu_induk_id = '$gi'";
			$data = $db->createCommand($sql)->queryAll();
			$this->renderpartial('tabel' , array('data' => $data ,'db' => $db , 'gi' => $gi));
		}

		public function actionCreate()
		{
			$db = Yii::app()->db;
			$model = new MasterFaktor;
			$dataGardu = CHtml::listData(MasterGarduInduk::model()->findAll(array('condition' => 'id NOT IN (SELECT gardu_induk_id FROM master_faktor)')) , 'id' , 'gardu_induk');
			$ajax = array(
				'type' => 'POST',
				'url' => Yii::app()->createUrl('/administrator/faktor/getDetail'),
				'data' => array('gardu_induk_id' => 'js:this.value'),
				'update' => '#divUpdate',
			);
			$opsiGardu = array('class' => 'form-control' , 'empty' => '' , 'ajax' => $ajax);
			
			if(isset($_POST['MasterFaktor']))
			{
				$model->attributes = $_POST['MasterFaktor'];
				$sql = "SELECT COUNT(id) FROM master_item_ne WHERE gardu_induk_id = '$model->gardu_induk_id'";
				$count = $db->createCommand($sql)->queryScalar();
				if($count > 0)
				{
					$transaksi = $db->beginTransaction();
					try
					{
										for($a=0;$a<$count;$a++)
										{
											$simpan = new MasterFaktor;
											$simpan->gardu_induk_id = $model->gardu_induk_id;
											$simpan->item_id = $_POST['item_id'][$a];
											$simpan->faktor_ekspor = $_POST['faktor_ekspor'][$a];
											$simpan->faktor_impor  = $_POST['faktor_impor'][$a];
											$simpan->save();
										}

										$transaksi->commit();
										ar::flash('info' , 'Data telah disimpan');
										$this->redirect(array('index'));

					}catch(Exception $e){
										$transaksi->rollback();
										ar::flash('danger' , 'Data gagal disimpan');
										$this->redirect(array('index'));
					}
				}else{
										ar::flash('danger' , 'Anda belum input data master item');
										$this->redirect(array('index'));
				}
						
			}

			$this->render('_form' , array(
					'model' => $model,
					'dataGardu' => $dataGardu,
					'opsiGardu' => $opsiGardu,
				)
			);
		}

		public function actionUpdate($id)
		{
			$modelGi = MasterGarduInduk::model()->findByPk($id);
			$model = new MasterFaktor;
			$db = Yii::app()->db;
			$gi = $modelGi->id;
			$sql = "SELECT DISTINCT(jenis_id) , master_jenis_ne.jenis FROM master_item_ne
					INNER JOIN master_jenis_ne
					ON master_item_ne.jenis_id = master_jenis_ne.id WHERE gardu_induk_id = '$gi'";
			$data = $db->createCommand($sql)->queryAll();

			if(isset($_POST['MasterFaktor']))
			{
				$model->attributes = $_POST['MasterFaktor'];
				$sql = "SELECT COUNT(id) FROM master_item_ne WHERE gardu_induk_id = '$modelGi->id'";
				$count = $db->createCommand($sql)->queryScalar();
				if($count > 0)
				{
					$transaksi = $db->beginTransaction();
					try
					{
										$db->createCommand("DELETE FROM master_faktor WHERE gardu_induk_id = '$gi'")->execute();
										for($a=0;$a<$count;$a++)
										{
											$simpan = new MasterFaktor;
											$simpan->gardu_induk_id = $modelGi->id;
											$simpan->item_id = $_POST['item_id'][$a];
											$simpan->faktor_ekspor = $_POST['faktor_ekspor'][$a];
											$simpan->faktor_impor  = $_POST['faktor_impor'][$a];
											$simpan->save();
										}

										$transaksi->commit();
										ar::flash('info' , 'Data telah disimpan');
										$this->redirect(array('index'));

					}catch(Exception $e){
										$transaksi->rollback();
										ar::flash('danger' , 'Data gagal disimpan');
										$this->redirect(array('index'));
					}
				}else{
										ar::flash('danger' , 'Anda belum input data master item');
										$this->redirect(array('index'));
				}
						
			}

			$this->render('form_update' ,
				array(
					'model' => $model,
					'modelGi' => $modelGi,
					'data' => $data,
					'gi' => $gi , 
					'db' => $db
				)
			);
		}

		public function actionHapus($id , $token = "")
		{
			$db = Yii::app()->db;
			$cek = ar::decrypt($token);
			if($cek == $id)
			{
				try
				{
					$model = MasterGarduInduk::model()->findByPk($id);
					$sql = "DELETE FROM master_faktor WHERE gardu_induk_id = '$model->id'";
					
					if($db->createCommand($sql) ->execute())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}
	}