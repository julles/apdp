<?php
	class NeracaEnergyController extends Controller
	{
		public $layout = '//layouts/admin/utama';
		public function actionIndex()
		{
			$db = Yii::app()->db;
			$data = $db->createCommand()->select('DISTINCT(gardu_induk_id) , tanggal ,gardu_induk , n.id')->from('neraca_energi n')
			->join('master_gardu_induk g' , 'g.id=n.gardu_induk_id')
			->order('tanggal DESC')
			->queryAll();
			
			$this->render('index' , array('data' => $data));
		}

		public function actionGetDetail()
		{
			$db = Yii::app()->db;
			$gi = $_POST['gardu_induk_id'];
			$sql = "SELECT DISTINCT(jenis_id) , master_jenis_ne.jenis FROM master_item_ne
					INNER JOIN master_jenis_ne
					ON master_item_ne.jenis_id = master_jenis_ne.id WHERE gardu_induk_id = '$gi'";
			$data = $db->createCommand($sql)->queryAll();
			$this->renderpartial('tabel' , array('data' => $data ,'db' => $db , 'gi' => $gi));
		}


		public function actionValidasi()
		{
			$db = Yii::app()->db;
			$gi = $_POST['gi'];
			$tanggal = $_POST['tanggal'];
			$cek = $db->createCommand()->select('id')->from('neraca_energi')->where('gardu_induk_id=:gardu_induk_id AND tanggal=:tanggal' , array(':gardu_induk_id' => $gi , ':tanggal' => ar::getTanggalSqlNoWaktu($tanggal , "-")))->queryScalar();
			$model = new NeracaEnergi;
			if(empty($cek))
			{
				echo "
					<script>
						document.getElementById('form').submit();
					</script>
				";
			}else{
				/*echo "<script>
				alert('Gardu Induk sudah di input pada tanggal $tanggal');
				</script>";*/
				echo "Gardu Induk sudah di input pada tanggal $tanggal";
			}
			
		}

		public function actionValidasiUpdate()
		{
			$db = Yii::app()->db;
			$gi = $_POST['gi'];
			$tanggal = $_POST['tanggal'];
			$cek = $db->createCommand()->select('id')->from('neraca_energi')->where('gardu_induk_id=:gardu_induk_id AND tanggal=:tanggal AND id!=:id' , array(':gardu_induk_id' => $gi , ':tanggal' => ar::getTanggalSqlNoWaktu($tanggal , "-"),':id' => $_POST['id']))->queryScalar();
			$model = new NeracaEnergi;
			if(empty($cek))
			{
				echo "
					<script>
						document.getElementById('form').submit();
					</script>
				";
			}else{
				echo "<script>
				alert('Gardu Induk sudah di input pada tanggal $tanggal');
				</script>";
			}
			
		}

		public function actionCreate()
		{
			extract($_POST,EXTR_SKIP);
			$db = Yii::app()->db;
			$model = new NeracaEnergi;
			$dataGardu = CHtml::listData(MasterGarduInduk::model()->findAll() , 'id' , 'gardu_induk');
			$ajax = array(
				'type' => 'POST',
				'url' => Yii::app()->createUrl('/administrator/neracaEnergy/getDetail'),
				'data' => array('gardu_induk_id' => 'js:this.value'),
				'update' => '#divUpdate',
			);
			$opsiGardu = array('class' => 'form-control' , 'empty' => '' , 'ajax' => $ajax , 'id' => 'gardu_induk_id');
			
			if(isset($_POST['NeracaEnergi']))
			{
				$model->attributes = $_POST['NeracaEnergi'];
				$cek = $db->createCommand()->select('id')->from('neraca_energi')->where('gardu_induk_id=:gardu_induk_id AND tanggal=:tanggal' , array(':gardu_induk_id' => $model->gardu_induk_id , ':tanggal' => ar::getTanggalSqlNoWaktu($model->tanggal , "-")))->queryScalar();
				if(empty($cek))
				{	
					//$transaksi = $db->beginTransaction();
					//try
					//{
							
							$sqlCount= "SELECT COUNT(id) FROM master_item_ne WHERE gardu_induk_id  ='$model->gardu_induk_id'";
							$count = $db->createCommand($sqlCount)->queryScalar();
							
							if($model->save())
							{
								$tanggalSebelum = $model->tanggal;
								for($a=0;$a<$count;$a++)
								{
									$simpan = new NeracaEnergiDetail;
									$simpan->neraca_energi_id = $model->id;
									$simpan->item_id = $item_id[$a];
									$simpan->expor = $ekspor[$a];
									$simpan->impor = $impor[$a];
									$simpan->faktor_expor = $faktor_ekspor[$a];
									$simpan->faktor_impor = $faktor_impor[$a];
									$sebelum = $db->createCommand("SELECT if(COUNT(expor) =0 , 0 , expor) AS expor, if(COUNT(impor) =0 , 0 , impor) AS impor FROM neraca_energi_detail
										 INNER JOIN neraca_energi ON neraca_energi_detail.neraca_energi_id = neraca_energi.id WHERE tanggal IN(SELECT DATE_ADD('".$tanggalSebelum."', INTERVAL -1 DAY)) AND item_id = '$item_id[$a]' ")->queryRow();
									
									if(empty($sebelum['expor']))
									{
										$exporna = 0;
									}else{
										$exporna = ($ekspor[$a] - $sebelum['expor']) * $faktor_ekspor[$a];
									}

									if(empty($sebelum['impor']))
									{
										$imporna = 0;
									}else{
										$imporna = ($impor[$a] - $sebelum['impor']) * $faktor_impor[$a];
									}
									//echo $sebelum['expor']."<br/>";
									
									$simpan->kwh_expor = $exporna;
									$simpan->kwh_impor = $imporna;
									$simpan->save();
								}
						//				$transaksi->commit();
										ar::flash('info' , 'Data telah disimpan');
										$this->redirect(array('index'));
										//echo "<script>document.location.href='http://apdp-kalbar.com/administrator/neracaEnergy/index'</script>";
							}

					//}catch(Exception $e){
							//			$transaksi->rollback();
										//ar::flash('danger' , 'Data gagal disimpan');
									//$this->redirect(array('index'));
					//}
				}else{
					$model->addError('tanggal' , 'Gardu Induk sudah di input pada tanggal diatas!');
					return false;
				}
						
							
			}

			$this->render('_form' , 
				array(
					'model' => $model , 
					'dataGardu' => $dataGardu,
					'opsiGardu' => $opsiGardu
				)
			);
		}

		public function actionUpdate($id)
		{
			extract($_POST,EXTR_SKIP);
			$db = Yii::app()->db;
			$model =  NeracaEnergi::model()->findByPk($id);
			$model->tanggal = ar::getTanggalViewNoWaktu($model->tanggal , '-');
			$dataGardu = CHtml::listData(MasterGarduInduk::model()->findAll() , 'id' , 'gardu_induk');
			$ajax = array(
				'type' => 'POST',
				'url' => Yii::app()->createUrl('/administrator/neracaEnergy/getDetail'),
				'data' => array('gardu_induk_id' => 'js:this.value'),
				'update' => '#divUpdate',
			);
			$opsiGardu = array('class' => 'form-control' , 'empty' => '' , 'ajax' => $ajax , 'id' => 'gardu_induk_id');
			
			if(isset($_POST['NeracaEnergi']))
			{
				$model->attributes = $_POST['NeracaEnergi'];
				$cek = $db->createCommand()->select('id')->from('neraca_energi')->where('gardu_induk_id=:gardu_induk_id AND tanggal=:tanggal AND id!=:id' , array(':gardu_induk_id' => $model->gardu_induk_id , ':tanggal' => ar::getTanggalSqlNoWaktu($model->tanggal , "-") , ':id' => $model->id))->queryScalar();
				if(empty($cek))
				{	
					//$transaksi = $db->beginTransaction();
					//try
					//{
							
							$sqlCount= "SELECT COUNT(id) FROM master_item_ne WHERE gardu_induk_id  ='$model->gardu_induk_id'";
							$count = $db->createCommand($sqlCount)->queryScalar();
							
							if($model->save())
							{
								$tanggalSebelum = $model->tanggal;
								$delete = $db->createCommand()->delete('neraca_energi_detail','neraca_energi_id=:ids',array(':ids' => $model->id));
								for($a=0;$a<$count;$a++)
								{
									$simpan = new NeracaEnergiDetail;
									$simpan->neraca_energi_id = $model->id;
									$simpan->item_id = $item_id[$a];
									$simpan->expor = $ekspor[$a];
									$simpan->impor = $impor[$a];
									$simpan->faktor_expor = $faktor_ekspor[$a];
									$simpan->faktor_impor = $faktor_impor[$a];
									$sebelum = $db->createCommand("SELECT if(COUNT(expor) =0 , 0 , expor) AS expor, if(COUNT(impor) =0 , 0 , impor) AS impor FROM neraca_energi_detail
										 INNER JOIN neraca_energi ON neraca_energi_detail.neraca_energi_id = neraca_energi.id WHERE tanggal IN(SELECT DATE_ADD('".$tanggalSebelum."', INTERVAL -1 DAY)) AND item_id = '$item_id[$a]' ")->queryRow();
									
									//if(empty($sebelum['expor']))
									//{
									//	$exporna = 0;
									//}else{
										$exporna = ($ekspor[$a] - $sebelum['expor']) * $faktor_ekspor[$a];
									//}

									//if(empty($sebelum['impor']))
									//{
									//	$imporna = 0;
									//}else{
										$imporna = ($impor[$a] - $sebelum['impor']) * $faktor_impor[$a];
									//}
									//echo $sebelum['expor']."<br/>";
									
									$simpan->kwh_expor = $exporna;
									$simpan->kwh_impor = $imporna;
									$simpan->save();
								}
					//					$transaksi->commit();
										ar::flash('info' , 'Data telah disimpan');
										$this->redirect(array('index'));
										//echo "<script>document.location.href='http://apdp-kalbar.com/administrator/neracaEnergy/index'</script>";
							}

					//}catch(Exception $e){
					//     				$transaksi->rollback();
										//ar::flash('danger' , 'Data gagal disimpan');
										//$this->redirect(array('index'));
					//}
				}else{
					$model->addError('tanggal' , 'Gardu Induk sudah di input pada tanggal diatas!');
					return false;
				}
						
							
			}

			$this->render('_form_update' , 
				array(
					'model' => $model , 
					'dataGardu' => $dataGardu,
					'opsiGardu' => $opsiGardu
				)
			);
		}

		public function actionHapus($id , $token = "")
		{
			$db = Yii::app()->db;
			$cek = ar::decrypt($token);
			if($cek == $id)
			{
				try
				{
					$model = NeracaEnergi::model()->findByPk($id);
					$db->createCommand()->delete('neraca_energi_detail','neraca_energi_id=:ids' , array(':ids' => $id));
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}
	}