<?php
	class ItemNeracaController extends Controller
	{
		public $layout = '//layouts/admin/utama';
		public function actionIndex()
		{
			$db = Yii::app()->db;
			$data = $db->createCommand("SELECT master_item_ne.*, master_gardu_induk.gardu_induk , master_jenis_ne.jenis FROM  master_item_ne
					INNER JOIN master_gardu_induk ON master_gardu_induk.id = master_item_ne.gardu_induk_id
					INNER JOIN master_jenis_ne ON master_jenis_ne.id = master_item_ne.jenis_id
			")
			->queryAll();
			
			$this->render('index' , array('data' => $data));
		}

		public function actionCreate()
		{
			$model = new MasterItemNe;
			$dataJenis = CHtml::listData(MasterJenisNe::model()->findAll() , 'id' , 'jenis');
			$dataGardu = CHtml::listData(MasterGarduInduk::model()->findAll() , 'id' , 'gardu_induk');

			if(isset($_POST['MasterItemNe']))
			{
				$model->attributes = $_POST['MasterItemNe'];
				if($model->save())
				{
							ar::flash('info' , 'Data telah disimpan');
							$this->redirect(array('index'));
				}
			}

			$this->render('_form',
				array(
					'model' => $model,
					'dataJenis' => $dataJenis,
					'dataJenis' => $dataJenis,
					'dataGardu' => $dataGardu
				) 
			);
		}

		public function actionUpdate($id)
		{
			$model = $this->loadModel($id);
			$dataJenis = CHtml::listData(MasterJenisNe::model()->findAll() , 'id' , 'jenis');
			$dataGardu = CHtml::listData(MasterGarduInduk::model()->findAll() , 'id' , 'gardu_induk');

			if(isset($_POST['MasterItemNe']))
			{
				$model->attributes = $_POST['MasterItemNe'];
				if($model->save())
				{
							ar::flash('info' , 'Data telah disimpan');
							$this->redirect(array('index'));
				}
			}

			$this->render('_form',
				array(
					'model' => $model,
					'dataJenis' => $dataJenis,
					'dataJenis' => $dataJenis,
					'dataGardu' => $dataGardu
				) 
			);
		}
		
		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			if($cek == $id)
			{
				try
				{
					$model = $this->loadModel($id);
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}

		public function loadModel($param)
		{
			$model = MasterItemNe::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}
	}