<?php
	class TeganganController extends Controller
	{
		public $layout = '//layouts/admin/utama';
	
	
		public function actionGetBusbar()
		{
		
			$data = MasterBusbar::model()->findAll("gardu_hubung_id=:gardu_hubung_id" , array(':gardu_hubung_id' => $_POST['gardu_hubung_id']));
			$data = CHtml::listdata($data , 'id' ,'busbar');
			echo "<option value=''></option>";
			foreach($data as $key => $value)
			{
				echo CHtml::tag('option' , array('value' => $key) , CHtml::encode($value) , true);
			}
		}
	
		public function actionIndex()
		{
					$model = Tegangan::model()->findAll();
					$hasil = '';
					foreach($model as $row)
					{
							$hasil  .= "
								<tr>
									<td>".$row->busbar->garduHubung->gardu_hubung."</td>
									<td>".$row->busbar->busbar."</td>
									<td>".ar::formatWaktu($row->tanggal_full, 'medium','')."</td>
									<td>".ar::formatWaktu($row->date_post, 'medium','')."</td>
									<td>".$row->user->nama."</td>
									<td>".CHtml::link(ar::btnEdit() , array('update' , 'id' => $row->id)).
								       " ".
									  CHtml::link(ar::btnHapus() , array('hapus' , 'id' => $row->id , 'token' => ar::encrypt($row->id)) , array('onclick' => 'return confirm("Anda Yakin Menghapus Data ini ?")') )
							 ."</td>
								</tr>
							";
					}	

					$this->render('index' , array('hasil' => $hasil));
		}


		public function actionCreate()
		{
			$model = new Tegangan;
			if(isset($_POST['Tegangan']))
			{
				$koneksi = Yii::app()->db;
				$transaksi = $koneksi->beginTransaction();
				$model->attributes = $_POST['Tegangan'];
				if($model->validate())
				{	
					try
					{
							$model->attributes = $_POST['Tegangan'];
							if($model->save())
							{
								
								$rata = Tegangan::model()->getRata($model->id);	
								$update = $koneksi->createCommand("UPDATE tegangan SET hasil_rata = '$rata' WHERE id = '$model->id'")->execute();	
							}
								$transaksi->commit();
								//echo $jumlah."<h1>TES</h1>";
								Yii::app()->user->setFlash('info' , 'Data telah disimpan!');
								$this->redirect(array('index'));
							
				
					}catch(Exception $e){
								 
								$transaksi->rollback();
								Yii::app()->user->setFlash('error' , 'Data gagal disimpan! , cek database anda!');
							$this->redirect(array('index'));
					}	
				}
			}
			$this->render('_form' , array('model' => $model));
		}

		public function actionUpdate($id)
		{
			$model = $this->loadModel($id);
			if(isset($_POST['Tegangan']))
			{
				$koneksi = Yii::app()->db;
				$transaksi = $koneksi->beginTransaction();
				$model->attributes = $_POST['Tegangan'];
				if($model->validate())
				{	
					try
					{
							$model->attributes = $_POST['Tegangan'];
							if($model->save())
							{
								$rata = Tegangan::model()->getRata($model->id);	
								$update = $koneksi->createCommand("UPDATE tegangan SET hasil_rata = '$rata' WHERE id = '$model->id'")->execute();		
							}
								$transaksi->commit();
								//echo $jumlah."<h1>TES</h1>";
								Yii::app()->user->setFlash('info' , 'Data telah disimpan!');
								$this->redirect(array('index'));
							
				
					}catch(Exception $e){
								 
								$transaksi->rollback();
								Yii::app()->user->setFlash('error' , 'Data gagal disimpan! , cek database anda!');
							$this->redirect(array('index'));
					}	
				}
			}
			$this->render('_form' , array('model' => $model));
		}

		public function actionHapus($id , $token = "")
		{
			$cek = ar::decrypt($token);
			if($cek == $id)
			{
				try
				{
					$model = $this->loadModel($id);
					if($model->delete())
					{
						ar::flash('info' , 'Data telah dihapus');
						$this->redirect(array('index'));
					}
				
				}catch(Exception $e){
						ar::flash('danger' , 'Data gagal dihapus , data ini masih digunakan data lain');
						$this->redirect(array('index'));
				}
				

			}else{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
				
			}
		}

		public function loadModel($param)
		{
			$model = Tegangan::model()->findByPk($param);
			if($model === null || !is_numeric($param))
			{
				throw new CHttpException(404 , 'Maaf Halaman tidak ditemukan!');
			}else{
				return $model;	
			}
		}

		protected function performAjaxValidation($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='role-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
	}