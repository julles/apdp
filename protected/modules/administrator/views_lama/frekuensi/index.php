<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl."/admin/"; ?>css/jquery.dataTables.css">
<script src = "<?php echo Yii::app()->baseUrl."/admin/js/jquery.dataTables.js"; ?>"></script>




<script>
$(document).ready(function() {
    $('#gridYii').dataTable();
} );
</script>





<script type="text/javascript">
    function hilang()
    {
     document.getElementById('pesan').style.display = 'none';
    }
</script>


<div id="konten-table">
            	<!-- Title -->
            	<div>
                	<p class="portlet-title"><u><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></u></p>
                </div>
                <!-- End Title -->
                <div id = 'msg'>
                <?php
                    foreach(Yii::app()->user->getFlashes() as $key => $value)
                    {
                            echo '

                                <div onclick = "hilang()" id = "pesan" style="margin: 15px;" class="alert alert-'.$key.'">
                                × &nbsp;
                                   '.$value.'
                                </div>

                            ';
                    }    
                ?>
                </div>
            	<!-- Search -->
            	<div class="row" style = 'margin-bottom:20px;'>
                	<div class="col6">
                    	<div class="tambah">
                        	<?php
                                echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Tambah' , array('create') , array('class' => 'btn btn-primary' , 'style' => 'text-decoration:none;'));
                            ?>
                        </div>
                    </div>
                   
                </div>
                <!-- End Search -->
                


                <!-- Table -->
                <div class="row">
                	<div class="col12">
                    	<div class="panel-table">
                       
			                       <table id="gridYii" class="display" cellspacing="0" width="100%">
								        <thead>
								            <tr>
								                <th width = '30%'>Tanggal</th>
								                <th width = '20%'>Jam</th>
												<th width = '20%'>Daya</th>
								                <th width = '40%'>Opsi</th>
								            </tr>
								        </thead>
								 		<tbody style = 'text-align:center;'>	
								           <?php 
								           	echo $hasil;
								           ?>
								        </tbody>
								    </table>
                        </div>
                    </div>
                </div>
                <!-- End Table -->
                
                <!-- Pagination -->
            	
                <!-- End Pagination -->
            </div>