<?php
$db = Yii::app()->db;
$gi = $model->gardu_induk_id;
			$sql = "SELECT DISTINCT(jenis_id) , master_jenis_ne.jenis FROM master_item_ne
					INNER JOIN master_jenis_ne
					ON master_item_ne.jenis_id = master_jenis_ne.id WHERE gardu_induk_id = '$gi'";
			$data = $db->createCommand($sql)->queryAll();
?>
<script>
$(document).ready(
		function()
		{
			$("#form").submit(
				function()
				{
				   var tesData={'gi':$("#gardu_induk_id").val(),'tanggal':$("#tanggal").val() , 'id':'<?php echo $model->id; ?>'};
					$.ajax({
						url:'<?php echo Yii::app()->createUrl("/administrator/neracaEnergy/validasiUpdate"); ?>',
						type:'POST',
						data: tesData,
						success:function(hasil){
							$("#tes").html(hasil);
						}
					});
					return false;
				}
			);
		}
	  );
</script>
<div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'form',
                                        // Please note: When you enable ajax validation, make sure the corresponding
                                        // controller action is handling ajax validation correctly.
                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                        // See class documentation of CActiveForm for details on this.
                                        'htmlOptions' => array('style' => 'min-height:950px;'),
										'clientOptions' => array(
											'validateOnSubmit' => true
										),
                                        'enableClientValidation' => true , 
                                        'enableAjaxValidation'=>false,
                                    )); ?>
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'gardu_induk_id'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->dropDownList($model,'gardu_induk_id',$dataGardu ,$opsiGardu); ?>
                                                    <?php echo $form->error($model,'gardu_induk_id' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                                <div class="col-sm-2">
                                                        <label><?php echo $form->labelEx($model,'tanggal'); ?></label>
                                            </div>
                                            <div class="col-sm-10">
                                                <?php
                                                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
															
                                                            'model'=>$model,
                                                            'attribute'=>'tanggal',
                                                            // additional javascript options for the date picker plugin
                                                            'options' => array(
                                                            'showAnim' => 'fold',
                                                            'dateFormat'=>'dd-mm-yy',
                                                             'changeMonth'=>true,
                                                            'changeYear'=>true,
                                                            'yearRange'=>'1900:2099',
                                                            ),
                                                            'htmlOptions' => array(
                                                            'style' => 'height:20px;',
                                                             'class' => 'form-control',
															 'id' => 'tanggal'
                                                            ),
                                                        ));

                                                     ?>
													 <?php echo $form->error($model,'tanggal' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                            </div>
                                        </div>

										
										<div class="form-group">
                                            <div class="col-sm-12">
												<div id = 'divUpdate'>
													<?php
														$this->renderpartial('tabel_update' , array('data' => $data ,'db' => $db , 'gi' => $gi ,'model' => $model, 'aidna' => $model->id));
													?>
												</div>
                                            </div>
                                          
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2">&nbsp;</div>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                              
                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                            </div>
                                        </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				 
                <!-- End Input -->
            </div>


        