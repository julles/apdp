<div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'role-form',
                                        // Please note: When you enable ajax validation, make sure the corresponding
                                        // controller action is handling ajax validation correctly.
                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                        // See class documentation of CActiveForm for details on this.
                                        'htmlOptions' => array('style' => 'height:250px;'),
                                        'enableClientValidation' => true , 
                                        'enableAjaxValidation'=>false,
                                    )); ?>
                                        
										<div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'pembangkit_id'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->dropDownList($model,'pembangkit_id',$dataPembangkit , array('class'=>'form-control' , 'empty' => '')); ?>
                                                    <?php echo $form->error($model,'pembangkit_id' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'unit_pembangkit'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'unit_pembangkit',array('class'=>'form-control')); ?>
                                                    <?php echo $form->error($model,'unit_pembangkit' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										
                                        <div class="form-group">
                                            <div class="col-sm-2">&nbsp;</div>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                              
                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                            </div>
                                        </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Input -->
            </div>


        