
 <?php
$this->widget('ext.SliderPopImage.SliderPopImage', array(
'selectorImgPop' => '.thumbsgen',
'popupwithpaginate' => true,
'maxpopuwidth' => '$(window).width()*0.8',

'postfixThumb' => '_thumb',
    'relPathThumbs' => 'thumbs' 
    //'relPathThumbs' => array('thumbsTiny','thumbsMedium') //only version 1.1
));
?>   
<script type="text/javascript">
    function hilang()
    {
     document.getElementById('pesan').style.display = 'none';
    }

    function hilangPatok()
    {
        var tanya = confirm("Anda Yakin Menghapus Gambar ini?");
        if(tanya)
        {
             document.getElementById('patokanGambar').value = '';
             document.getElementById('tampilGambar').style.display = 'none';

        }
    }
	
	
	function dapetBatal()
	{
		document.getElementById('fileUpload').value = '';
	}

</script>
<?php
	foreach(Yii::app()->user->getFlashes() as $key => $value)
	{
			echo '

				<div onclick = "hilang()" id = "pesan" style="margin: 15px;" class="alert alert-'.$key.'">
				× &nbsp;
				   '.$value.'
				</div>

			';
	}    
?>
<div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                                'id'=>'berita-form',
                                                // Please note: When you enable ajax validation, make sure the corresponding
                                                // controller action is handling ajax validation correctly.
                                                // There is a call to performAjaxValidation() commented in generated controller code.
                                                // See class documentation of CActiveForm for details on this.
                                                'enableAjaxValidation'=>false,
                                                'enableClientValidation' => true,
                                                 /*'clientOptions'=> array(
                                                        'validateOnSubmit'=>true,
                                                        'validateOnChange'=>true,
                                                        'validateOnType'=>true,
                                                 ),*/
                                                    'htmlOptions' => array('enctype' => 'multipart/form-data' , 'style' => 'height:400px;'),
                                            )); ?>
									
										
										
											
											<div class="form-group">
												<div class="col-sm-2">
													<?php echo $form->labelEx($model,'banner'); ?>
												</div>
												<div class="col-sm-10">
														<?php echo $form->fileField($model,'banner',array('class'=>'form-control' ,  'id' => 'fileUpload')); ?>
														<?php echo $form->error($model,'banner' , array('style' => 'color:red;font-size:12px;'  )); ?>
														<?php echo CHtml::link('batal' , '' , array('id' => 'batal' ,  'onclick' => 'dapetBatal()' , 'style' => 'font-size:12px;text-decoration:none;display:inherit;cursor:pointer;')); ?>
												</div>
											</div>
											 <button type="submit" class="btn btn-primary">Submit</button>
                                              
                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                        </div>


                                        <?php
                                        if(!$model->isNewRecord)
                                        {
                                            $alamat = Yii::app()->baseUrl."/images/";
                                            if(empty($model->banner))
											{
												$gambar = $alamat."no.jpg";
												$linkHapus = '';
											}else{
												$gambar = $alamat."berita/thumb/".$model->banner;
												$linkHapus = CHtml::link('Hapus' , '#' , array('onclick' => 'hilangPatok()' , 'style' => 'font-size:12px;text-decoration:none;'));
											}

                                        ?>
											
                                            <div class="form-group" id = 'tampilGambar'>
                                                <div class="col-sm-2">
                                                    <?php echo $form->labelEx($model,'gambar_sebelumnya'); ?>
                                                </div>
                                                <div class="col-sm-10">
                                                      <img class = 'thumbsgen'  src = "<?php echo $gambar; ?>" style = 'width:100px;height:100px;cursor:pointer;' />
                                                        <br/>
                                                      <?php
                                                        echo $linkHapus;
                                                      ?>
                                                </div>
                                            </div>
                                            <input type = 'hidden' name = 'patokanGambar' value = "<?php echo $model->gambar; ?>" id = "patokanGambar" />
                                       
                                        <?php
                                        }
                                        ?>
										
										
                                        
                                        
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Input -->
            </div>