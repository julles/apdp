<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl."/admin/"; ?>css/jquery.dataTables.css">
<script src = "<?php echo Yii::app()->baseUrl."/admin/js/jquery.dataTables.js"; ?>"></script>




<script>
$(document).ready(function() {
    $('#gridYii').dataTable();
} );
</script>





<script type="text/javascript">
    function hilang()
    {
     document.getElementById('pesan').style.display = 'none';
    }
</script>


<div id="konten-table">
            	<!-- Title -->
            	<div>
                	<p class="portlet-title"><u><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></u></p>
                </div>
                <!-- End Title -->
                <div id = 'msg'>
                <?php
                    foreach(Yii::app()->user->getFlashes() as $key => $value)
                    {
                            echo '

                                <div onclick = "hilang()" id = "pesan" style="margin: 15px;" class="alert alert-'.$key.'">
                                × &nbsp;
                                   '.$value.'
                                </div>

                            ';
                    }    
                ?>
                </div>
            	<!-- Search -->
            	<div class="row" style = 'margin-bottom:20px;'>
                	<div class="col6">
                    	<div class="tambah">
                        	<?php
                                echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Tambah' , array('create') , array('class' => 'btn btn-primary' , 'style' => 'text-decoration:none;'));
                            ?>
                        </div>
                    </div>
                   
                </div>
                <!-- End Search -->
                


                <!-- Table -->
                <div class="row">
                	<div class="col12">
                    	<div class="panel-table">
                       
			                       <table id="gridYii" class="display" cellspacing="0" width="100%">
								        <thead>
								            <tr>
								                <th  width = '10%'>No</th>
								                <th>Tanggal</th>
								                <th>Date Post</th>
								                <th>Posted By</th>
								                
								                <th width = '20%'>Opsi</th>
								            </tr>
								        </thead>
								 		
										<tbody style = 'text-align:center;'>	
								        <?php
										$query = "SELECT beban.id , tanggal , beban.date_post , user.nama FROM beban INNER JOIN user ON user.id = beban.posted_by ORDER BY tanggal DESC";
										$sqlTampil = Yii::app()->db->createCommand($query)->query();
										$no = 0;
										while(($r = $sqlTampil->read())!==false)
										{
										$no++;
										?>
										   <tr>
												<td><?php echo $no; ?></td>
												<td><?php echo ar::formatWaktu($r['tanggal'] , "long" ,""); ?></td>
												<td><?php echo ar::formatWaktu($r['date_post'] , "long" ,""); ?></td>
												<td><?php  echo $r['nama'];?></td>
												<td><?php echo CHtml::link(ar::btnEdit() , array('update' , 'id' => $r['id'])) ?> | <?php echo CHtml::link(ar::btnHapus() , array('hapus' , 'id' => $r['id'] , 'token' => ar::encrypt($r['id'])) , array('onclick' => 'return confirm("Anda Yakin Menghapus Data ini ?")') );?></td>
										   </tr>
								        <?php
										}
										?>
										</tbody>
								    </table>
                        </div>
                    </div>
                </div>
                <!-- End Table -->
                
                <!-- Pagination -->
            	
                <!-- End Pagination -->
            </div>