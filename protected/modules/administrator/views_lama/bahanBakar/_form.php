<script>
	function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode != 46 && charCode > 31 && charCode != 45
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
</script>
<?php
	$dataJenis = CHtml::listData(MasterJenisPembangkit::model()->findAll() , 'id' , 'jenis_pembangkit');
	$optionsJenis = array(
		'class' => 'form-control',
		'empty' => '---',
		'ajax' => array(
			'type' => 'post',
			'url' => Yii::app()->createUrl('/administrator/bahanBakar/getPembangkit'),
			'data' => array(
				'jenis_pembangkit_id' => 'js:this.value'
			),
			'update' => '#pembangkit_id'
		),
	);


	$optionsPembangkit = array(
		'class' => 'form-control',
		'empty' => '---',
		'id' => 'pembangkit_id',
		'ajax' => array(
			'type' => 'POST',
			'url' => Yii::app()->createUrl("/administrator/bahanBakar/getUnitPembangkit"),
			'data' => array(
				'pembangkit_id' => 'js:this.value',

			),
			'update' => '#unit_pembangkit_id'
		),

	);

	$optionsUnitPembangkit = array(
		'class' => 'form-control',
		'empty' => '---',
		'id' => 'unit_pembangkit_id',
	);


	if($model->isNewRecord)
	{
		if(!isset($_POST['BahanBakar']))
		{
			$dataPembangkit = array();
			$dataUnitPembangkit = array();
		}else{
			$dataPembangkit = CHtml::listData(MasterPembangkit::model()->findAll('jenis_pembangkit_id=:jenis' , array(':jenis' => $model->jenis_pembangkit_id)) , 'id' , 'pembangkit');
			$dataUnitPembangkit = CHtml::listData(MasterUnitPembangkit::model()->findAll('pembangkit_id=:pembangkit' , array(':pembangkit' => $model->pembangkit_id)) , 'id' , 'unit_pembangkit');
		}
	}else{
		$dataPembangkit = CHtml::listData(MasterPembangkit::model()->findAll('jenis_pembangkit_id=:jenis' , array(':jenis' => $model->jenis_pembangkit_id)) , 'id' , 'pembangkit');
		$dataUnitPembangkit = CHtml::listData(MasterUnitPembangkit::model()->findAll('pembangkit_id=:pembangkit' , array(':pembangkit' => $model->pembangkit_id)) , 'id' , 'unit_pembangkit');
	}

?>
<div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">

                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'role-form',
                                        // Please note: When you enable ajax validation, make sure the corresponding
                                        // controller action is handling ajax validation correctly.
                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                        // See class documentation of CActiveForm for details on this.
                                        'htmlOptions' => array('style' => 'height:652px;'),
                                        'enableClientValidation' => true ,
                                        'enableAjaxValidation'=>false,
										'clientOptions'=> array(
	                                        'validateOnSubmit'=>true,
	                                        'validateOnChange'=>true,
	                                        'validateOnType'=>true,
	                                 	 )
                                    )); ?>
										 <div class="form-group">
                                                <div class="col-sm-2">
                                                        <label><?php echo $form->labelEx($model,'tanggal'); ?></label>
                                            </div>
                                            <div class="col-sm-10">
                                                <?php
                                                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(

                                                            'model'=>$model,
                                                            'attribute'=>'tanggal',
                                                            // additional javascript options for the date picker plugin
                                                            'options' => array(
                                                            'showAnim' => 'fold',
                                                            'dateFormat'=>'dd-mm-yy',
                                                             'changeMonth'=>true,
                                                            'changeYear'=>true,
                                                            'yearRange'=>'1900:2099',
                                                            ),
                                                            'htmlOptions' => array(
                                                            'style' => 'height:20px;',
                                                             'class' => 'form-control'
                                                            ),
                                                        ));

                                                     ?>
													 <?php echo $form->error($model,'tanggal' , array('style' => 'color:red;font-size:12px;margin-left:50px;' )); ?>
                                            </div>
                                        </div>


                                    	<div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'jenis_pembangkit_id'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->dropDownList($model,'jenis_pembangkit_id',$dataJenis,$optionsJenis); ?>
                                                    <?php echo $form->error($model,'jenis_pembangkit_id' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>

										<div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'pembangkit_id'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->dropDownList($model,'pembangkit_id',$dataPembangkit,$optionsPembangkit); ?>
                                                    <?php echo $form->error($model,'pembangkit_id' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>

										<div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'unit_pembangkit_id'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->dropDownList($model,'unit_pembangkit_id',$dataUnitPembangkit,$optionsUnitPembangkit); ?>
                                                    <?php echo $form->error($model,'unit_pembangkit_id' , array('style' => 'color:red;font-size:12px;' )); ?>

                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'hsd'); ?>
                                            </div>
                                            <div class="col-sm-10">

														<?php echo $form->textField($model,'hsd',array('class'=>'form-control' , 'onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 8, 'style' => 'width:40%;float:left;', 'placeholder' => 'Pemakaian')); ?>
														<?php echo $form->textField($model,'hsd_stok',array('class'=>'form-control' , 'onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 8, 'style' => 'width:50%;float:right;', 'placeholder' => 'Stok')); ?>
											</div>
										</div>
										 <div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'mfo'); ?>
                                            </div>


											  <div class="col-sm-10">

														<?php echo $form->textField($model,'mfo',array('class'=>'form-control' , 'onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 8, 'style' => 'width:40%;float:left;', 'placeholder' => 'Pemakaian')); ?>
														<?php echo $form->textField($model,'mfo_stok',array('class'=>'form-control' , 'onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 8, 'style' => 'width:50%;float:right;', 'placeholder' => 'Stok')); ?>

											  </div>

                                        </div>
										 <div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'olein'); ?>
                                            </div>
                                             <div class="col-sm-10">

														<?php echo $form->textField($model,'olein',array('class'=>'form-control' , 'onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 8, 'style' => 'width:40%;float:left;', 'placeholder' => 'Pemakaian')); ?>
														<?php echo $form->textField($model,'olein_stok',array('class'=>'form-control' , 'onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 8, 'style' => 'width:50%;float:right;', 'placeholder' => 'Stok')); ?>

											  </div>
                                        </div>
										<div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'batu_bara'); ?>
                                            </div>
                                               <div class="col-sm-10">

														<?php echo $form->textField($model,'batu_bara',array('class'=>'form-control' , 'onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 8, 'style' => 'width:40%;float:left;', 'placeholder' => 'Pemakaian')); ?>
														<?php echo $form->textField($model,'batu_bara_stok',array('class'=>'form-control' , 'onKeyPress' => 'return isNumberKey(event)' , 'maxlength' => 8, 'style' => 'width:50%;float:right;', 'placeholder' => 'Stok')); ?>

											  </div>
                                        </div>



                                        <div class="form-group">
                                            <div class="col-sm-2">&nbsp;</div>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary">Submit</button>

                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                            </div>
                                        </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Input -->
            </div>
