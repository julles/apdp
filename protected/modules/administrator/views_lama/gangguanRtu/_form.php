<?php 	$base = Yii::app()->baseUrl."/time/"; ?>
    <script type="text/javascript" src="<?php echo $base; ?>jquery.ui.timepicker.js?v=0.3.3"></script>
   

<script>
$(document).ready(
	function()
	{
		$('#timepicker').timepicker();
		$('#timepicker2').timepicker();
	}
	
);
</script>
<div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'role-form',
                                        // Please note: When you enable ajax validation, make sure the corresponding
                                        // controller action is handling ajax validation correctly.
                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                        // See class documentation of CActiveForm for details on this.
                                        'htmlOptions' => array('style' => 'height:650px;'),
                                        'enableClientValidation' => true , 
                                        'enableAjaxValidation'=>false,
                                    )); ?>
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'tanggal_gangguan'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php 
                                                        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                                            'model'=>$model,
                                                            'attribute'=>'tanggal_gangguan',
                                                            'htmlOptions' => array(
                                                                'id' => 'datepicker_for_due_date',
                                                                'size' => '10',
                                                                'class' => 'form-control',
                                                               // 'style' => 'height:25px;width:200px;float:right;' 
                                                                //'style' => 'width:40%; border: 1px solid #cccccc;height:32px;'
                                                            ),
                                                            
                                                            'options'=>array(
                                                                'hourGrid' => 4,
                                                                'hourMin' => 9,
                                                                'hourMax' => 17,
                                                                'timeFormat' => 'hh:mm',
                                                                'changeMonth' => true,
                                                                'changeYear' => false,
                                                                'showOn' => 'focus', 
                                                                'dateFormat' => 'dd-mm-yy',
                                                                'showOtherMonths' => true,
                                                                'selectOtherMonths' => true,
                                                                'changeMonth' => true,
                                                                'changeYear' => true,
                                                                'showButtonPanel' => true,
                                                                ),
                                                                
                                                            ));  
                                                        ?>
                                                    <?php echo $form->error($model,'tanggal_gangguan' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										 <div class="form-group" >
                                            <div class="col-sm-2">
												
                                                <?php echo $form->labelEx($model,'jam_gangguan'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'jam_gangguan',array('class'=>'form-control' , 'id' => 'timepicker' , 'readonly' => true , 'style' => 'width:100px;')); ?>
                                                    <?php echo $form->error($model,'jam_gangguan' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										<div class="form-group" >
                                            <div class="col-sm-2">
												
                                                <?php echo $form->labelEx($model,'gangguan'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textArea($model,'gangguan',array('class'=>'form-control')); ?>
                                                    <?php echo $form->error($model,'gangguan' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										<div class="form-group" >
                                            <div class="col-sm-2">
												
                                                <?php echo $form->labelEx($model,'evaluasi'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textArea($model,'evaluasi',array('class'=>'form-control')); ?>
                                                    <?php echo $form->error($model,'evaluasi' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										 <div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'tanggal_evaluasi'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php 
                                                        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                                            'model'=>$model,
                                                            'attribute'=>'tanggal_evaluasi',
                                                            'htmlOptions' => array(
                                                                'id' => 'datepicker_for_due_date2',
                                                                'size' => '10',
                                                                'class' => 'form-control',
                                                               // 'style' => 'height:25px;width:200px;float:right;' 
                                                                //'style' => 'width:40%; border: 1px solid #cccccc;height:32px;'
                                                            ),
                                                            
                                                            'options'=>array(
                                                                'hourGrid' => 4,
                                                                'hourMin' => 9,
                                                                'hourMax' => 17,
                                                                'timeFormat' => 'hh:mm',
                                                                'changeMonth' => true,
                                                                'changeYear' => false,
                                                                'showOn' => 'focus', 
                                                                'dateFormat' => 'dd-mm-yy',
                                                                'showOtherMonths' => true,
                                                                'selectOtherMonths' => true,
                                                                'changeMonth' => true,
                                                                'changeYear' => true,
                                                                'showButtonPanel' => true,
                                                                ),
                                                                
                                                            ));  
                                                        ?>
                                                    <?php echo $form->error($model,'tanggal_evaluasi' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										 <div class="form-group" >
                                            <div class="col-sm-2">
												
                                                <?php echo $form->labelEx($model,'jam_evaluasi'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'jam_evaluasi',array('class'=>'form-control' , 'id' => 'timepicker2' , 'readonly' => true , 'style' => 'width:100px;')); ?>
                                                    <?php echo $form->error($model,'jam_evaluasi' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										
                                        <div class="form-group">
                                            <div class="col-sm-2">&nbsp;</div>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                              
                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                            </div>
                                        </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Input -->
            </div>


        