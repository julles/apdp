<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl."/admin/"; ?>css/jquery.dataTables.css">
<script src = "<?php echo Yii::app()->baseUrl."/admin/js/jquery.dataTables.js"; ?>"></script>




<script>
$(document).ready(function() {
    $('#gridYii').dataTable({'ordering':false,"iDisplayLength": 100});
} );
</script>





<script type="text/javascript">
    function hilang()
    {
     document.getElementById('pesan').style.display = 'none';
    }
</script>


<div id="konten-table">
            	<!-- Title -->
            	<div>
                	<p class="portlet-title"><u><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></u></p>
                </div>
                <!-- End Title -->
                <div id = 'msg'>
                <?php
                    foreach(Yii::app()->user->getFlashes() as $key => $value)
                    {
                            echo '

                                <div onclick = "hilang()" id = "pesan" style="margin: 15px;" class="alert alert-'.$key.'">
                                � &nbsp;
                                   '.$value.'
                                </div>

                            ';
                    }    
                ?>
                </div>
            	<!-- Search -->
            	<div class="row" style = 'margin-bottom:20px;'>
                	<div class="col6">
                    	<div class="tambah">
                        	<?php
                                echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Tambah' , array('create') , array('class' => 'btn btn-primary' , 'style' => 'text-decoration:none;'));
                            ?>
                        </div>
                    </div>
                   
                </div>
                <!-- End Search -->
                


                <!-- Table -->
                <div class="row">
                	<div class="col12">
                    	<div class="panel-table">
                       
			                       <table id="gridYii" class="display" cellspacing="0" width="100%">
								        <thead>
								            <tr>
								               <th>No</th>
								               <th>Tanggal</th>
								               <th>Gardu Induk</th>
								               <th>Trafo</th>
								              
								             
								               <th>Opsi</th>
								             </tr>
								        </thead>
								 		<tbody style = 'text-align:center;'>	
								           <?php
											$sql = "SELECT trans_pembebanan_trafo.id  , tanggal , trans_pembebanan_trafo.date_post , master_gardu_induk.gardu_induk , master_trafo.trafo  FROM trans_pembebanan_trafo
													INNER JOIN master_gardu_induk
													ON master_gardu_induk.id = trans_pembebanan_trafo.gardu_induk_id
													INNER JOIN master_trafo
													ON master_trafo.id = trans_pembebanan_trafo.trafo_id
													
                                                    ORDER  BY tanggal DESC
											";
											$query = Yii::app()->db->createCommand($sql)->query();
											$no = 0;
											while(($r = $query->read())!==false)
											{
											$no++;
											echo "
												<tr>
													<td>$no</td>
													<td>".ar::formatWaktu($r['tanggal'] , 'long' , '')."</td>
													<td>$r[gardu_induk]</td>
													<td>$r[trafo]</td>
													
													
													<td>".CHtml::link(ar::btnEdit() , array('update' , 'id' => $r['id'])) ." | 
														".CHtml::link(ar::btnHapus() , array('hapus' , 'id' => $r['id'] , 'token' => ar::encrypt($r['id'])) , array('onclick' => 'return confirm("Anda Yakin Menghapus Data ini ?")') )."
													</td>
												</tr>
											
											";
											}
										   ?>
								        </tbody>
								    </table>
                        </div>
                    </div>
                </div>
                <!-- End Table -->
                
                <!-- Pagination -->
            	
                <!-- End Pagination -->
            </div>