<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl."/admin/"; ?>css/jquery.dataTables.css">
<script src = "<?php echo Yii::app()->baseUrl."/admin/js/jquery.dataTables.js"; ?>"></script>




<script>
$(document).ready(function() {
    $('#gridYii').dataTable();
} );
</script>





<script type="text/javascript">
    function hilang()
    {
     document.getElementById('pesan').style.display = 'none';
    }
</script>


<div id="konten-table">
            	<!-- Title -->
            	<div>
                	<p class="portlet-title"><u><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></u></p>
                </div>
                <!-- End Title -->
                <div id = 'msg'>
                <?php
                    foreach(Yii::app()->user->getFlashes() as $key => $value)
                    {
                            echo '

                                <div onclick = "hilang()" id = "pesan" style="margin: 15px;" class="alert alert-'.$key.'">
                                × &nbsp;
                                   '.$value.'
                                </div>

                            ';
                    }    
                ?>
                </div>
            	<!-- Search -->
            	<div class="row" style = 'margin-bottom:20px;'>
                	<div class="col6">
                    	<div class="tambah">
                        	<?php
                                echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Tambah' , array('create') , array('class' => 'btn btn-primary' , 'style' => 'text-decoration:none;'));
                            ?>
                        </div>
                    </div>
                   
                </div>
                <!-- End Search -->
                


                <!-- Table -->
                <div class="row">
                	<div class="col12">
                    	<div class="panel-table">
                       
			                       <table id="gridYii" class="display" cellspacing="0" width="100%">
								        <thead>
								            <tr>
								                <th>Gardu Induk</th>
								                
								                <th>Opsi</th>
								            </tr>
								        </thead>
								 		<tbody style = 'text-align:center;'>	
								          
										  <?php
											foreach($data as $row)
											{
												echo "
													<tr>
														<td>$row[gardu_induk]</td>
														<td>".CHtml::link(ar::btnEdit() , array('update' , 'id' => $row['id'])) ?> | <?php echo CHtml::link(ar::btnHapus() , array('hapus' , 'id' => $row['id'] , 'token' => ar::encrypt($row['id'])) , array('onclick' => 'return confirm("Anda Yakin Menghapus Data ini ?")') )."</td>
													</tr>
												";
											}
										  ?>
										  
								        </tbody>
								    </table>
                        </div>
                    </div>
                </div>
                <!-- End Table -->
                
                <!-- Pagination -->
            	
                <!-- End Pagination -->
            </div>