<script>
	function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode != 46 && charCode > 31 && charCode != 45
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
</script>
<?php
$stile = array(
	'style' => 'width:40px;font-size:12px;border:0.5px solid #666;',
	'onKeyPress' => 'return isNumberKey(event)'
);		

?>

 <table class="table" border = '1' width = '100%' style = 'font-size:12px;border-collapse:collapse;background-color:white;'>
	  <thead>
		<tr>
		 
		  <th>kWh Meter</th>
		  <th>Faktor Ekspor</th>
		  <th>Faktor Impor</th>
		</tr>
	  </thead>
	  <tbody>
		<?php
		foreach($data as $row)
		{

		?>
				<?php
					$sql = "SELECT id , item FROM master_item_ne WHERE gardu_induk_id = '$gi' AND jenis_id = '$row[jenis_id]'";
					$detail = $db->createCommand($sql)->queryAll();
					foreach($detail as $rowDetail)
					{
						if(Yii::app()->controller->action->id == 'update')
						{
							$getValue = "SELECT faktor_ekspor , faktor_impor FROM master_faktor WHERE item_id = '$rowDetail[id]' AND gardu_induk_id = '$gi'";
							$exec = $db->createCommand($getValue)->queryRow();
							$ekspor = $exec['faktor_ekspor'];
							$impor = $exec['faktor_impor'];
						}else{
							$ekspor = "";
							$impor = "";
						}
				?>
					<tr style = 'text-align:center;'>
					  <td><?php echo $rowDetail['item']; ?></td>
					  <td><?php echo CHtml::textField("faktor_ekspor[]" , $ekspor , $stile); ?></td>
					  <td><?php echo CHtml::textField("faktor_impor[]" , $impor , $stile); ?></td>
					  <input type = 'hidden' name = 'item_id[]' value = "<?php echo $rowDetail['id']; ?>" />
					 </tr> 
				<?php
					}
		}
				?>
	  </tbody>
	</table>
		
