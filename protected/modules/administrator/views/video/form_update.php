<script type="text/javascript">
    

     function hilang()
    {
     document.getElementById('pesan').style.display = 'none';
    }
</script>

<?php
    foreach(Yii::app()->user->getFlashes() as $key => $value)
    {
            echo '

                <div onclick = "hilang()" id = "pesan" style="margin: 15px;" class="alert alert-'.$key.'">
                × &nbsp;
                   '.$value.'
                </div>

            ';
    }    
?>  

<div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'role-form',
                                        // Please note: When you enable ajax validation, make sure the corresponding
                                        // controller action is handling ajax validation correctly.
                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                        // See class documentation of CActiveForm for details on this.
                                        'htmlOptions' => array('style' => 'height:650px;'),
                                        'enableClientValidation' => true , 
                                        'enableAjaxValidation'=>false,
                                    )); ?>
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'nama_album'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'nama_album',array('class'=>'form-control')); ?>
                                                    <?php echo $form->error($model,'nama_album' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>

                                        <?php
                                        for($a=0;$a<5;$a++)
                                        {
                                        ?>

                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($detil,'url'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($detil,'url[]',array('class'=>'form-control')); ?>
                                                    <?php echo $form->error($detil,'url' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>


                                        <?php
                                        }
                                        ?>

                                        <div class="form-group">
                                            <div class="col-sm-2">&nbsp;</div>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                              
                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                            </div>
                                        </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <p>&nbsp;</p>

                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i>List Video</div>
                                </div>
                                <div class="panel-body">
                                   
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                               <table>

                                                   <tr>
                                                  
                                                            <?php
                                                            $no = 0;
                                                            foreach($all as $rows)
                                                            {
                                                                $no++;
                                                            ?>
                                                            
                                                                    <td>
                                                                      
                                                                        <?php
                                                                            
                                                                            $this->widget('ext.Yiippod.Yiippod', array(
                                                                            'video'=>"http://youtube.com/$rows->url",
                                                                            'id' => 'yiippodplayer',
                                                                            'width'=>100,
                                                                            'height'=>100,
                                                                            'bgcolor'=>'#000',
                                                                            ));
                                                                        ?>
                                                                        <br/>
                                                                        <?php echo CHtml::link('Hapus' , array('hapusVideo' , 'id' => $model->id , 'ids' => $rows->id) , array('style' => 'text-decoration:none;color:black;font-size:12px;margin-left:15px;' , 'onclick' => 'return confirm("Anda yakin menghapus Video ini ?")')); ?>
                                                                    </td>
                                                                    

                                                            <?php
                                                                if($no % 2 == 0)
                                                                {
                                                                    echo "</tr><tr>";
                                                                }
                                                            }
                                                            ?>
                                                   
                                                   
                                                   </tr>
                                               </table>
                                            </div>

                                            
                                        </div>
                                        
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- End Input -->
            </div>


        