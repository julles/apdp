<style type="text/css">
    #rtus
    {
        width:58.333% !important;
    }
</style>
<div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'role-form',
                                        // Please note: When you enable ajax validation, make sure the corresponding
                                        // controller action is handling ajax validation correctly.
                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                        // See class documentation of CActiveForm for details on this.
                                        'htmlOptions' => array('style' => 'height:173px; !important'),
                                        'enableClientValidation' => true , 
                                        'enableAjaxValidation'=>false,
                                    )); ?>


                                        <div class="form-group" style = 'margin-left:68px;'>
                                            
                                            <div class="col-sm-9" id = 'rtus'>
                                             <label  for="ScadatelGangguanRtu_jenis_gardu_id"><div style="float:left;">Kondisi</div> <div style="text-align:center;margin-left:110px;">Tanggal</div></label>
                                                    <?php echo $form->dropDownList($model,'kondisi',$kondisi , array('empty' => '' ,  'style' => 'height:25px;width:200px;')); ?>
                                                   <?php 
                                                        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                                            'model'=>$model,
                                                            'attribute'=>'tanggal_full',
                                                            'htmlOptions' => array(
                                                                'id' => 'datepicker_for_due_date',
                                                                'size' => '10',
                                                                'class' => 'form-control',
                                                                'style' => 'height:20px;width:200px;float:right;' 
                                                                //'style' => 'width:40%; border: 1px solid #cccccc;height:32px;'
                                                            ),
                                                            
                                                            'options'=>array(
                                                                'hourGrid' => 4,
                                                                'hourMin' => 9,
                                                                'hourMax' => 17,
                                                                'timeFormat' => 'hh:mm',
                                                                'changeMonth' => true,
                                                                'changeYear' => false,
                                                                'showOn' => 'focus', 
                                                                'dateFormat' => 'dd-mm-yy',
                                                                'showOtherMonths' => true,
                                                                'selectOtherMonths' => true,
                                                                'changeMonth' => true,
                                                                'changeYear' => true,
                                                                'showButtonPanel' => true,
                                                                ),
                                                                
                                                            ));  
                                                        ?>
                                                    <?php echo $form->error($model,'kondisi' , array('style' => 'color:red;font-size:12px;float:left;' )); ?>
                                                 
                                                    <?php echo $form->error($model,'tanggal_full' , array('style' => 'color:red;font-size:12px;text-align:center;margin-left:178px;' )); ?>
                                                
                                            </div>
                                        </div>


                                         
                                         <div class="form-group" style = 'margin-left:68px;'>
                                            <div class="col-sm-2">&nbsp;</div>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                              
                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                            </div>
                                        </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Input -->
            </div>


        