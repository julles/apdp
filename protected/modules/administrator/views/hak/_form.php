
 <style type="text/css">
        body {
            font-family: verdana, arial;
            font-size: 0.8em;
        }

        code {
            white-space: pre;
        }
    </style>

    <!-- start tree configuration -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript"
            src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css"
          href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css"/>
<!--
    <script type="text/javascript" src="minified/jquery.tree.min.js"></script>
    <link rel="stylesheet" type="text/css" href="minified/jquery.tree.min.css"/>
    <!-- end tree configuration -->

    <!-- start development configuration -->

    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl."/tree/"; ?>js/jquery.tree.js"></script>
<!--    <script type="text/javascript" src="src/js/jquery.treeajax.js"></script>
    <script type="text/javascript" src="src/js/jquery.treecheckbox.js"></script>
    <script type="text/javascript" src="src/js/jquery.treecollapse.js"></script>
    <script type="text/javascript" src="src/js/jquery.treecontextmenu.js"></script>
    <script type="text/javascript" src="src/js/jquery.treednd.js"></script>
    <script type="text/javascript" src="src/js/jquery.treeselect.js"></script> -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl."/tree/"; ?>css/jquery.tree.css"/>
<!--    <link rel="stylesheet" type="text/css" href="src/css/jquery.treecollapse.css"/>
    <link rel="stylesheet" type="text/css" href="src/css/jquery.treecontextmenu.css"/>
    <link rel="stylesheet" type="text/css" href="src/css/jquery.treednd.css"/>-->
    <!-- start development configuration -->

    <script type="text/javascript">
        //<!--
        $(document).ready(function() {
            $( "#accordion" ).accordion({
                'collapsible': true,
                'active': null
            });
            $('.jquery').each(function() {
                eval($(this).html());
            });
            $('.button').button();
              $('#example-0 div').tree({
                });
        });
        //-->


    </script>


<div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'role-form',
                                        // Please note: When you enable ajax validation, make sure the corresponding
                                        // controller action is handling ajax validation correctly.
                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                        // See class documentation of CActiveForm for details on this.
                                        'htmlOptions' => array('style' => 'min-height:1182px;'),
                                        'enableClientValidation' => true , 
                                        'enableAjaxValidation'=>false,
                                    )); ?>


                                         <div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'role'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->textField($model,'role',array('class'=>'form-control' , 'readonly' => true)); ?>
                                                    <?php echo $form->error($model,'role' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'menu'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <div id="example-0" style="font-size:12px;">
                                                        <div>
                                                                 <li><input type="checkbox"><span>Cek All</span>
                                                                    <ul>

                                                                            <?php
                                                                                $menu1 = Menu::model()->findAll('parent_id=:parent_id' , array('parent_id' => 0));
                                                                               
                                                                                foreach($menu1 as $row1)
                                                                                {
                                                                            ?>

                                                                            <li><input type="checkbox"><span><?php echo $row1->nama_menu; ?></span>
                                                                                <ul>
                                                                                    <?php
                                                                                    $menu2 = Menu::model()->findAll('parent_id=:parent' , array(':parent' => $row1->id));
                                                                                    foreach($menu2 as $row2)
                                                                                    {

                                                                                         $cari = ar::query("SELECT id FROM hak_akses WHERE menu_id = '$row2[id]' AND role_id = '$model->id'")->queryScalar();
                                                                                           (!empty($cari)) ? $ceks = 'checked' : $ceks = '';

                                                                                    ?>
                                                                                        <li><input type="checkbox" <?php echo $ceks; ?> name = "cek[]" value = "<?php echo $row2->id; ?>"><span><?php echo $row2->nama_menu; ?></span>
                                                                                    <?php
                                                                                    }
                                                                                    ?>
                                                                                </ul>
                                                                               
                                                                           </li>
                                                                           <?php
                                                                                }
                                                                           ?>

                                                                      </ul>
                                                                </li>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2">&nbsp;</div>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                              
                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                            </div>
                                        </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Input -->
            </div>