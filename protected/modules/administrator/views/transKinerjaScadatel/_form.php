<script>
	function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
</script>
<?php
	$tahun = array();
	for($a=2000;$a<=2020;$a++)
	{
		$tahun[$a] = intval($a);
	}
?>
<div id="konten-table">
                <!-- Input -->
                <div class="row">
                    <div class="col12">
                        <div class="panel-side">
                            <div class="panel">
                              <div class="panel-heading">
                                    
                                    <div class="panel-title"><i class="fa fa-share"></i><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></div>
                                </div>
                                <div class="panel-body">
                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                        'id'=>'role-form',
                                        // Please note: When you enable ajax validation, make sure the corresponding
                                        // controller action is handling ajax validation correctly.
                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                        // See class documentation of CActiveForm for details on this.
                                        'htmlOptions' => array('style' => 'height:450px;'),
                                        'enableClientValidation' => true , 
                                        'enableAjaxValidation'=>false,
                                    )); ?>
                                        
										<div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'kinerja_scadatel_id'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->dropDownList($model,'kinerja_scadatel_id',CHtml::listData(MasterKinerjaScadatel::model()->findAll() , 'id' , 'kinerja_scadatel') ,array('class'=>'form-control' , 'empty' => '')); ?>
                                                    <?php echo $form->error($model,'kinerja_scadatel_id' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <div class="col-sm-2">
                                                <?php echo $form->labelEx($model,'tahun'); ?>
                                            </div>
                                            <div class="col-sm-10">
                                                    <?php echo $form->dropDownList($model,'tahun', $tahun , array('class'=>'form-control' , 'empty' => '')); ?>
                                                    <?php echo $form->error($model,'tahun' , array('style' => 'color:red;font-size:12px;' )); ?>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <div class="col-sm-12">
												
												<table style = "width:100%;">
												<tr>
												<?php
												$b=-1;
												for($a=1;$a<=12;$a++)
												{
													$b++;
													$bulan = substr(ar::bulanSaja($a) , 0 ,3);
													if(isset($_POST['TransKinerjaScadatel']))
													{
														$target = $_POST['target'][$b];
														$real = $_POST['real'][$b];
													}else{
														if($model->isNewRecord)
														{
															$target = "";
															$real = "";
															$target_kumulatif = "";
															$real_kumulatif = "";
														}else{
															$search = Yii::app()->db->createCommand()->select("real , target , target_kumulatif , real_kumulatif")->from('trans_kinerja_scadatel_detail c')
															->join('trans_kinerja_scadatel p' , 'p.id=c.trans_kinerja_scadatel_id' )
															->where('tahun=:thn AND trans_kinerja_scadatel_id=:knrj AND bulan=:bln' , array(':bln' => $a, ':thn' => $model->tahun , ':knrj' => $model->id))->queryRow();
															$target = $search['target'];
															$real = $search['real'];
															$real_kumulatif = $search['real_kumulatif'];
															$target_kumulatif = $search['target_kumulatif'];
														}
													}
												?>
													
														<td><?php echo $bulan; ?></td>
														<td>
															<input type = "hidden" name= "bulan[]" value = "<?php echo $a; ?>" />
															<?php echo CHtml::textField("target[]" ,$target , array("style" => "width:40px;" ,"placeholder" => 'target' , "onKeyPress" => "return isNumberKey(event)","maxlength" => 7)); ?>
															<?php echo CHtml::textField("real[]" , $real , array("style" => "width:40px;","placeholder" => 'real' , "onKeyPress" => "return isNumberKey(event)" ,"maxlength" => 7)); ?>
															<?php echo CHtml::textField("target_kumulatif[]" , $target_kumulatif , array("style" => "width:80px;font-size:12px;","placeholder" => 'Target Kumulatif' , "onKeyPress" => "return isNumberKey(event)" ,"maxlength" => 7)); ?>
															<?php echo CHtml::textField("real_kumulatif[]" , $real_kumulatif , array("style" => "width:80px;font-size:12px;","placeholder" => 'Real Kumulatif' , "onKeyPress" => "return isNumberKey(event)" ,"maxlength" => 7)); ?>
														</td>
												<?php
												echo ($a % 2 ==0) ? "</tr><tr>" : "";
												?>
												
												<?php
												}
												?>
												</tr>
												</table>
											
											</div>
                                        </div
										
										
                                        <div class="form-group">
                                            <div class="col-sm-2">&nbsp;</div>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                              
                                                    <?php
                                                        echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Cancel' , array('index') , array('class' => 'btn btn-secondary' , 'style' => 'text-decoration:none;'));
                                                    ?>
                                            </div>
                                        </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Input -->
            </div>


        