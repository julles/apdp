<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl."/admin/"; ?>css/jquery.dataTables.css">
<script src = "<?php echo Yii::app()->baseUrl."/admin/js/jquery.dataTables.js"; ?>"></script>





<script>
$(document).ready(function() {
    $('#gridYii').dataTable({'ordering':false,"iDisplayLength": 100});
} );
</script>





<script type="text/javascript">
    function hilang()
    {
     document.getElementById('pesan').style.display = 'none';
    }
</script>


<div id="konten-table">
            	<!-- Title -->
            	<div>
                	<p class="portlet-title"><u><?php echo ar::labelAksi()." ".ucwords(ar::namaMenu()); ?></u></p>
                </div>
                <!-- End Title -->
                <div id = 'msg'>
                <?php
                    foreach(Yii::app()->user->getFlashes() as $key => $value)
                    {
                            echo '

                                <div onclick = "hilang()" id = "pesan" style="margin: 15px;" class="alert alert-'.$key.'">
                                × &nbsp;
                                   '.$value.'
                                </div>

                            ';
                    }    
                ?>
                </div>
            	<!-- Search -->
            	<div class="row" style = 'margin-bottom:20px;'>
                	<div class="col6">
                    	<div class="tambah">
                        	<?php
                                echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;Tambah' , array('create') , array('class' => 'btn btn-primary' , 'style' => 'text-decoration:none;'));
                            ?>
                        </div>
                    </div>
                   
                </div>
                <!-- End Search -->
                


                <!-- Table -->
                <div class="row">
                	<div class="col12">
                    	<div class="panel-table">
                    		<?php
                    		/*	
                    			echo CHtml::beginForm();
                    		?>
	                    		<div class="form-group" style = 'float:right;'>
	                                <div class="col-sm-2">
										
	                                   &nbsp;

	                                </div>
	                                <div class="col-sm-10">
	                                		Pencarian :  
	                                        <?php
	                                        	echo CHtml::dropDownList('jenisGardu' , '10' , $dataJenisGardu , $htmlOptionsGardu);
	                                        ?>

	                                         <?php
	                                        	echo CHtml::dropDownList('garduInduk' , '10' , $dataGarduInduk , $htmlOptionsGarduInduk);
	                                        ?>

	                                        <?php
	                                        	echo CHtml::ajaxLink('Search' , array('gangguanRtu/pencarian') , 
	                                        		array(
	                                        			'type' => 'POST',
	                                        			'update' => '#divTable',
	                                        			'data' => array(
	                                        					'jenis_gardu' => 'js:jenis_gardu.value',
	                                        					'gardu_induk' => 'js:gardu_induk.value'
	                                        			),
	                                        			'beforeSend' => 'function(){$("#loading").show();}',
	                                        			'complete' => 'function(){$("#loading").hide();}',
	                                        		),
	                                        		array(
	                                        			'class' => 'btn btn-primary',
	                                        			'style' => 'text-decoration:none;font-size:12px;'
	                                        		)
	                                        	);
	                                        ?>
	                                        
	                                </div>
	                            </div>

	                           
	                        <?php

	                        echo CHtml::endForm();
							*/
	                        ?>

	                        	<div id = 'loading' style  = "display:none;"><img src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                       			<div id = 'divTable'>
			                       <table id="gridYii" class="display" cellspacing="0" width="100%">
								        <thead>
								            <tr>
											
								                <th>No</th>
								                <th>Tanggal Gangguan</th>
								                <th>Jam Gangguan</th>
								                <th>Date Post</th>
								               
								                <th>Opsi</th>
								                
								            </tr>
								        </thead>
								 		<tbody style = 'text-align:center;'>	
											<?php
											$no=0;
											while(($r = $model->read())!==false)
											{
											$no++;
											?>
											   <tr>
													<td><?php echo $no; ?></td>
													<td><?php echo ar::waktuYii($r['tanggal_gangguan'] , "medium" ,""); ?></td>
													<td><?php echo $r['jam_gangguan']; ?></td>
													<td><?php echo ar::waktuYii($r['date_post'] , "medium" ,""); ?></td>
													
													<td><?php echo CHtml::link(ar::btnEdit() , array('update' , 'id' => $r['id'])) ?> | <?php echo CHtml::link(ar::btnHapus() , array('hapus' , 'id' => $r['id'] , 'token' => ar::encrypt($r['id'])) , array('onclick' => 'return confirm("Anda Yakin Menghapus Data ini ?")') );?></td>
											   </tr>
											<?php
											}
											?>
								        </tbody>
								    </table>
								</div>
                        </div>
                    </div>
                </div>
                <!-- End Table -->
                
                <!-- Pagination -->
            	
                <!-- End Pagination -->
            </div>