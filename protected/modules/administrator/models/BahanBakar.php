<?php

/**
 * This is the model class for table "bahan_bakar".
 *
 * The followings are the available columns in table 'bahan_bakar':
 * @property integer $id
 * @property string $tanggal
 * @property string $jenis_pembangkit_id
 * @property integer $pembangkit_id
 * @property string $hsd
 * @property string $mfo
 * @property string $olein
 * @property string $batu_bara
 * @property string $date_post
 * @property integer $posted_by
 *
 * The followings are the available model relations:
 * @property MasterPembangkit $pembangkit
 * @property MasterJenisPembangkit $jenisPembangkit
 */
class BahanBakar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bahan_bakar';
	}
	
	/*public function cekTanggal($tanggal ,  $pembangkit_id)
	{
		$cek = Yii::app()->db->createCommand("SELECT COUNT(id) FROM bahan_bakar WHERE tanggal = '$tanggal' AND pembangkit_id = '$pembangkit_id'")->queryScalar();
		if($cek > 0)
		{
			$this->addError('tes' , 'testing');
			return false;
			
		}
	}*/
	

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pembangkit_id, posted_by', 'numerical', 'integerOnly'=>true),
			array('jenis_pembangkit_id', 'length', 'max'=>2),
			array('hsd, mfo, olein, batu_bara , hsd_stok, mfo_stok, olein_stok, batu_bara_stok ', 'length', 'max'=>10),
			array('pembangkit_id , jenis_pembangkit_id , unit_pembangkit_id, tanggal','required'),
			array('tanggal, date_post', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tanggal, jenis_pembangkit_id, pembangkit_id, hsd, mfo, olein, batu_bara, date_post, posted_by', 'safe', 'on'=>'search'),
		);
	}
	
	
	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			
			$this->tanggal = ar::getTanggalSqlNoWaktu($this->tanggal , "-");
			$this->date_post = date("Y-m-d H:i:s");
			$this->posted_by = Yii::app()->session['tokenId'];
			return true;
		}
	}
	


	public function getDataHarianValue($year , $month , $tanggalAkhir , $jenis_pembangkit_id  , $pembangkit_id , $unit_pembangkit_id , $field)
	{
					$db = Yii::app()->db;
						if(empty($jenis_pembangkit_id))
						{
							for($a=1;$a<=$tanggalAkhir;$a++)
							{
								$sql = "SELECT SUM($field) FROM bahan_bakar WHERE tanggal = '$year-$month-$a'";
								$hasil = $db->createCommand($sql)->queryScalar();
								$out[] = floatval($hasil);
							}
						
						}elseif(!empty($jenis_pembangkit_id) && empty($pembangkit_id) && empty($unit_pembangkit_id)){
							for($a=1;$a<=$tanggalAkhir;$a++)
							{
								$sql = "SELECT SUM($field) FROM bahan_bakar WHERE tanggal = '$year-$month-$a' AND jenis_pembangkit_id = '$jenis_pembangkit_id'";
								$hasil = $db->createCommand($sql)->queryScalar();
								$out[] = floatval($hasil);
							}

						}elseif(!empty($jenis_pembangkit_id) && !empty($pembangkit_id) && empty($unit_pembangkit_id)){
							for($a=1;$a<=$tanggalAkhir;$a++)
							{
								 $sql = "SELECT SUM($field) FROM bahan_bakar WHERE tanggal = '$year-$month-$a' AND jenis_pembangkit_id = '$jenis_pembangkit_id' AND pembangkit_id = '$pembangkit_id'";
								$hasil = $db->createCommand($sql)->queryScalar();
								$out[] = floatval($hasil);
							}
							}elseif(!empty($jenis_pembangkit_id) && !empty($pembangkit_id) && !empty($unit_pembangkit_id)){
							for($a=1;$a<=$tanggalAkhir;$a++)
							{
								$sql = "SELECT SUM($field) FROM bahan_bakar WHERE tanggal = '$year-$month-$a' AND jenis_pembangkit_id = '$jenis_pembangkit_id' AND pembangkit_id = '$pembangkit_id' AND unit_pembangkit_id = '$unit_pembangkit_id'";
								$hasil = $db->createCommand($sql)->queryScalar();
								$out[] = floatval($hasil);
							}
						}
						return $out;
						
						
	}
	
	public function getDataBulananValue($year  ,$jenis_pembangkit_id , $pembangkit_id , $unit_pembangkit_id , $field)
	{
					$db = Yii::app()->db;
						if(empty($jenis_pembangkit_id))
						{
							for($a=1;$a<=12;$a++)
							{
								$sql = "SELECT SUM($field) FROM bahan_bakar WHERE MONTH(tanggal) = '$a' AND YEAR(tanggal) = '$year'";
								$hasil = $db->createCommand($sql)->queryScalar();
								$out[] = floatval($hasil);
							}
						
						}elseif(!empty($jenis_pembangkit_id) && empty($pembangkit_id) && empty($unit_pembangkit_id)){
							for($a=1;$a<=12;$a++)
							{
								$sql = "SELECT SUM($field) FROM bahan_bakar WHERE MONTH(tanggal) = '$a' AND YEAR(tanggal) = '$year' AND jenis_pembangkit_id = '$jenis_pembangkit_id'";
								$hasil = $db->createCommand($sql)->queryScalar();
								$out[] = floatval($hasil);
							}

						}elseif(!empty($jenis_pembangkit_id) && !empty($pembangkit_id) && empty($unit_pembangkit_id)){
							for($a=1;$a<=12;$a++)
							{
								 $sql = "SELECT SUM($field) FROM bahan_bakar WHERE MONTH(tanggal) = '$a' AND YEAR(tanggal) = '$year' AND jenis_pembangkit_id = '$jenis_pembangkit_id' AND pembangkit_id = '$pembangkit_id'";
								$hasil = $db->createCommand($sql)->queryScalar();
								$out[] = floatval($hasil);
							}
							}elseif(!empty($jenis_pembangkit_id) && !empty($pembangkit_id) && !empty($unit_pembangkit_id)){
							for($a=1;$a<=12;$a++)
							{
								$sql = "SELECT SUM($field) FROM bahan_bakar WHERE MONTH(tanggal) = '$a' AND YEAR(tanggal) = '$year' AND jenis_pembangkit_id = '$jenis_pembangkit_id' AND pembangkit_id = '$pembangkit_id' AND unit_pembangkit_id = '$unit_pembangkit_id'";
								$hasil = $db->createCommand($sql)->queryScalar();
								$out[] = floatval($hasil);
							}
						}
						return $out;
						
						
	}


	public function getDataRangeValue($awal , $akhir  ,$jenis_pembangkit_id , $pembangkit_id , $unit_pembangkit_id , $field)
	{	
						$db = Yii::app()->db;
						
						if(empty($jenis_pembangkit_id))
						{
							for($a=$awal;$a<=$akhir;$a++)
							{
								$sql = "SELECT SUM($field) FROM bahan_bakar WHERE YEAR(tanggal) = '$a'";
								$hasil = $db->createCommand($sql)->queryScalar();
								$out[] = floatval($hasil);
							}
						
						}elseif(!empty($jenis_pembangkit_id) && empty($pembangkit_id) && empty($unit_pembangkit_id)){
							for($a=$awal;$a<=$akhir;$a++)
							{
								$sql = "SELECT SUM($field) FROM bahan_bakar WHERE YEAR(tanggal) = '$a' AND jenis_pembangkit_id = '$jenis_pembangkit_id'";
								$hasil = $db->createCommand($sql)->queryScalar();
								$out[] = floatval($hasil);
							}

						}elseif(!empty($jenis_pembangkit_id) && !empty($pembangkit_id) && empty($unit_pembangkit_id)){
							for($a=$awal;$a<=$akhir;$a++)
							{
								 $sql = "SELECT SUM($field) FROM bahan_bakar WHERE  YEAR(tanggal) = '$a' AND jenis_pembangkit_id = '$jenis_pembangkit_id' AND pembangkit_id = '$pembangkit_id'";
								$hasil = $db->createCommand($sql)->queryScalar();
								$out[] = floatval($hasil);
							}
						}elseif(!empty($jenis_pembangkit_id) && !empty($pembangkit_id) && !empty($unit_pembangkit_id)){
							for($a=$awal;$a<=$akhir;$a++)
							{
								$sql = "SELECT SUM($field) FROM bahan_bakar WHERE YEAR(tanggal) = '$a' AND jenis_pembangkit_id = '$jenis_pembangkit_id' AND pembangkit_id = '$pembangkit_id' AND unit_pembangkit_id = '$unit_pembangkit_id'";
								$hasil = $db->createCommand($sql)->queryScalar();
								$out[] = floatval($hasil);
							}
						}
						return $out;
	}
	
	
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pembangkit' => array(self::BELONGS_TO, 'MasterPembangkit', 'pembangkit_id'),
			'jenisPembangkit' => array(self::BELONGS_TO, 'MasterJenisPembangkit', 'jenis_pembangkit_id'),
			'unitPembangkit' => array(self::BELONGS_TO, 'MasterUnitPembangkit', 'unit_pembangkit_id'),
			//'user' => array(self::BELONGS_TO , 'User' , 'posted_by')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal' => 'Tanggal',
			'jenis_pembangkit_id' => 'Jenis Pembangkit',
			'pembangkit_id' => 'Pembangkit',

			'unit_pembangkit_id' => 'Unit Pembangkit',
			'hsd' => 'Hsd',
			'mfo' => 'Mfo',
			'olein' => 'Olein',
			'batu_bara' => 'Batu Bara',
			'date_post' => 'Date Post',
			'posted_by' => 'Posted By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('jenis_pembangkit_id',$this->jenis_pembangkit_id,true);
		$criteria->compare('pembangkit_id',$this->pembangkit_id);
		$criteria->compare('hsd',$this->hsd,true);
		$criteria->compare('mfo',$this->mfo,true);
		$criteria->compare('olein',$this->olein,true);
		$criteria->compare('batu_bara',$this->batu_bara,true);
		$criteria->compare('date_post',$this->date_post,true);
		$criteria->compare('posted_by',$this->posted_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BahanBakar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
