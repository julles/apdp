<?php

/**
 * This is the model class for table "scadatel_gangguan_telekomunikasi".
 *
 * The followings are the available columns in table 'scadatel_gangguan_telekomunikasi':
 * @property integer $id
 * @property integer $jenis_gardu_id
 * @property integer $gardu_induk_id
 * @property string $waktu_gangguan
 * @property integer $flag_id
 * @property string $lama_gangguan
 * @property string $keterangan
 * @property integer $posted_by
 * @property string $date_post
 *
 * The followings are the available model relations:
 * @property MasterFlag $flag
 * @property MasterGarduInduk $garduInduk
 * @property MasterJenisGardu $jenisGardu
 */
class ScadatelGangguanTelekomunikasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'scadatel_gangguan_telekomunikasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jenis_gardu_id, gardu_induk_id, flag_id, posted_by', 'numerical', 'integerOnly'=>true),
			array('jenis_gardu_id, gardu_induk_id, waktu_gangguan, flag_id, lama_gangguan' , 'required'),
			
			array('waktu_gangguan, lama_gangguan, keterangan, date_post', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, jenis_gardu_id, gardu_induk_id, waktu_gangguan, flag_id, lama_gangguan, keterangan, posted_by, date_post', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'flag' => array(self::BELONGS_TO, 'MasterFlag', 'flag_id'),
			'garduInduk' => array(self::BELONGS_TO, 'MasterGarduInduk', 'gardu_induk_id'),
			'jenisGardu' => array(self::BELONGS_TO, 'MasterJenisGardu', 'jenis_gardu_id'),
			'user' => array(self::BELONGS_TO , 'User' , 'posted_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'jenis_gardu_id' => 'Jenis Gardu',
			'gardu_induk_id' => 'Gardu Induk',
			'waktu_gangguan' => 'Waktu Gangguan',
			'flag_id' => 'Flag',
			'lama_gangguan' => 'Lama Gangguan',
			'keterangan' => 'Keterangan',
			'posted_by' => 'Posted By',
			'date_post' => 'Date Post',
		);
	}


	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->posted_by = Yii::app()->session['tokenId'];
			$this->date_post = date("Y-m-d H:i:s");
			$this->waktu_gangguan = ar::getTanggalSql($this->waktu_gangguan);
			return true;
		}

	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('jenis_gardu_id',$this->jenis_gardu_id);
		$criteria->compare('gardu_induk_id',$this->gardu_induk_id);
		$criteria->compare('waktu_gangguan',$this->waktu_gangguan,true);
		$criteria->compare('flag_id',$this->flag_id);
		$criteria->compare('lama_gangguan',$this->lama_gangguan,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('posted_by',$this->posted_by);
		$criteria->compare('date_post',$this->date_post,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ScadatelGangguanTelekomunikasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
