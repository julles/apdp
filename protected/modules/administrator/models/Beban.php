<?php

/**
 * This is the model class for table "beban".
 *
 * The followings are the available columns in table 'beban':
 * @property integer $id
 * @property string $tanggal
 * @property integer $posted_by
 * @property string $date_post
 * @property string $jam_max
 * @property string $jumlah_max
 */
class Beban extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'beban';
	}

	/**
	 * @return array validation rules for model attributes.
	 */

	public function unik($att)
	{
		if($this->isNewRecord)
		{
			$db = Yii::app()->db;
			$cekTanggal = $db->createCommand("SELECT tanggal FROM beban WHERE tanggal = '".date("Y-m-d" ,  strtotime($this->$att))."'")->queryScalar();
			if(!empty($cekTanggal))
			{
				$this->addError('tanggal' , 'Tanggal Sudah di input sebelumnya');
			}
		}
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('posted_by', 'numerical', 'integerOnly'=>true),
			array('jumlah_max', 'length', 'max'=>10),
			array('tanggal, date_post, jam_max', 'safe'),
			array('tanggal' , 'required'),
			array('tanggal' , 'unik'),
			//array('tanggal' , 'unik'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tanggal, posted_by, date_post, jam_max, jumlah_max', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			
			$this->date_post = date("Y-m-d H:i:s");
			//$this->posted_by = Yii::app()->session['tokenId'];
			$this->tanggal = date("Y-m-d" ,  strtotime($this->tanggal));
			return true;
		}
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal' => 'Tanggal',
			'posted_by' => 'Posted By',
			'date_post' => 'Date Post',
			'jam_max' => 'Jam Max',
			'jumlah_max' => 'Jumlah Max',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('posted_by',$this->posted_by);
		$criteria->compare('date_post',$this->date_post,true);
		$criteria->compare('jam_max',$this->jam_max,true);
		$criteria->compare('jumlah_max',$this->jumlah_max,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Beban the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
