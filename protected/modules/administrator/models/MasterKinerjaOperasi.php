<?php

/**
 * This is the model class for table "master_kinerja_operasi".
 *
 * The followings are the available columns in table 'master_kinerja_operasi':
 * @property integer $id
 * @property string $kinerja_operasi
 * @property string $satuan
 * @property integer $posted_by
 * @property string $date_post
 */
class MasterKinerjaOperasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'master_kinerja_operasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('posted_by', 'numerical', 'integerOnly'=>true),
			array('kinerja_operasi', 'length', 'max'=>100),
			array('satuan', 'length', 'max'=>50),
			array('kinerja_operasi' , 'required'),
			array('kinerja_operasi' , 'unique'),
			array('date_post', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, kinerja_operasi, satuan, posted_by, date_post', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

			'user' => array(self::BELONGS_TO , 'User' , 'posted_by'),
		);
	}

	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->posted_by = Yii::app()->session['tokenId'];
			$this->date_post = date("Y-m-d H:i:s");
			return true;
		}

	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'kinerja_operasi' => 'Kinerja Operasi',
			'satuan' => 'Satuan',
			'posted_by' => 'Posted By',
			'date_post' => 'Date Post',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('kinerja_operasi',$this->kinerja_operasi,true);
		$criteria->compare('satuan',$this->satuan,true);
		$criteria->compare('posted_by',$this->posted_by);
		$criteria->compare('date_post',$this->date_post,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterKinerjaOperasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
