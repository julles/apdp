<?php

/**
 * This is the model class for table "master_transmisi".
 *
 * The followings are the available columns in table 'master_transmisi':
 * @property integer $id
 * @property integer $gardu_induk_id
 * @property string $transmisi
 * @property integer $posted_by
 * @property integer $date_post
 *
 * The followings are the available model relations:
 * @property MasterGarduInduk $garduInduk
 */
class MasterTransmisi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'master_transmisi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gardu_induk_id, posted_by, date_post', 'numerical', 'integerOnly'=>true),
			array('transmisi', 'length', 'max'=>100),
			array('gardu_induk_id , transmisi' , 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, gardu_induk_id, transmisi, posted_by, date_post', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'garduInduk' => array(self::BELONGS_TO, 'MasterGarduInduk', 'gardu_induk_id'),
			
			'user' => array(self::BELONGS_TO , 'User' , 'posted_by')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'gardu_induk_id' => 'Gardu Induk',
			'transmisi' => 'Transmisi',
			'posted_by' => 'Posted By',
			'date_post' => 'Date Post',
		);
	}


	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->date_post = date("Y-m-d");
			$this->posted_by = Yii::app()->session['tokenId'];
			return true;
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('gardu_induk_id',$this->gardu_induk_id);
		$criteria->compare('transmisi',$this->transmisi,true);
		$criteria->compare('posted_by',$this->posted_by);
		$criteria->compare('date_post',$this->date_post);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterTransmisi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
