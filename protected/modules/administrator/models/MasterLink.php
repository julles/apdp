<?php

/**
 * This is the model class for table "master_link".
 *
 * The followings are the available columns in table 'master_link':
 * @property integer $id
 * @property string $nama
 * @property string $link
 * @property string $date_post
 * @property integer $posted_by
 */
class MasterLink extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'master_link';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('posted_by', 'numerical', 'integerOnly'=>true),
			array('nama', 'length', 'max'=>100),
			array('nama ,  link' , 'required'),
			array('link' , 'url' , 'defaultScheme' => 'http'),
			array('link, date_post', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama, link, date_post, posted_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

			'user' => array(self::BELONGS_TO , 'User' , 'posted_by'),
		);
	}


	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->posted_by = Yii::app()->session['tokenId'];
			$this->date_post = date("Y-m-d H:i:s");
			return true;
		}

	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'link' => 'Link',
			'date_post' => 'Date Post',
			'posted_by' => 'Posted By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('date_post',$this->date_post,true);
		$criteria->compare('posted_by',$this->posted_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterLink the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
