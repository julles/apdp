<?php

/**
 * This is the model class for table "neraca_energi".
 *
 * The followings are the available columns in table 'neraca_energi':
 * @property integer $id
 * @property integer $gardu_induk_id
 * @property string $tanggal
 * @property string $date_post
 * @property integer $posted_by
 */
class NeracaEnergi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'neraca_energi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gardu_induk_id, posted_by', 'numerical', 'integerOnly'=>true),
			array('gardu_induk_id, tanggal','required'),
			array('tanggal, date_post', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, gardu_induk_id, tanggal, date_post, posted_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			
			$this->tanggal = ar::getTanggalSqlNoWaktu($this->tanggal , "-");
			$this->date_post = date("Y-m-d H:i:s");
			$this->posted_by = 1;
			//$this->posted_by = Yii::app()->session['tokenId'];
			return true;
		}
	}
	
	
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'gardu_induk_id' => 'Gardu Induk',
			'tanggal' => 'Tanggal',
			'date_post' => 'Date Post',
			'posted_by' => 'Posted By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('gardu_induk_id',$this->gardu_induk_id);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('date_post',$this->date_post,true);
		$criteria->compare('posted_by',$this->posted_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NeracaEnergi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
