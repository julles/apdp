<?php

/**
 * This is the model class for table "manage_database".
 *
 * The followings are the available columns in table 'manage_database':
 * @property integer $id
 * @property string $nama_file
 * @property string $keterangan
 * @property string $date_post
 * @property integer $posted_by
 */
class ManageDatabase extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manage_database';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array( 
			array(  'keterangan',  'required'),
			array('posted_by', 'numerical', 'integerOnly'=>true),
			array('nama_file, keterangan', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama_file, keterangan, date_post, posted_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'masterTrafo' => array(self::HAS_MANY, 'MasterTrafo', 'gardu_induk_id'),
			'user' => array(self::BELONGS_TO , 'User' , 'posted_by')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_file' => 'Nama File',
			'keterangan' => 'Keterangan',
			'date_post' => 'Date Post',
			'posted_by' => 'Posted By',
		);
	}
	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->date_post = date("Y-m-d h:i:s");
			$this->posted_by = Yii::app()->session['tokenId'];
			return true;
		}
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_file',$this->nama_file,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('date_post',$this->date_post,true);
		$criteria->compare('posted_by',$this->posted_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ManageDatabase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
