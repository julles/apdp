<?php

/**
 * This is the model class for table "beban_puncak".
 *
 * The followings are the available columns in table 'beban_puncak':
 * @property integer $id
 * @property string $tanggal_full
 * @property string $01_00
 * @property string $02_00
 * @property string $03_00
 * @property string $04_00
 * @property string $05_00
 * @property string $06_00
 * @property string $07_00
 * @property string $08_00
 * @property string $09_00
 * @property string $j10_00
 * @property string $j11_00
 * @property string $j12_00
 * @property string $j13_00
 * @property string $j14_00
 * @property string $j15_00
 * @property string $j16_00
 * @property string $j17_00
 * @property string $j18_00
 * @property string $j18_30
 * @property string $j19_00
 * @property string $j19_30
 * @property string $j20_00
 * @property string $j20_30
 * @property string $j21_00
 * @property string $j21_30
 * @property string $j22_00
 * @property string $j23_00
 * @property string $j24_00
 * @property string $date_post
 * @property integer $posted_by
 */
class BebanPuncak extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'beban_puncak';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('posted_by', 'numerical', 'integerOnly'=>true),
			array('j01_00, j02_00, j03_00, j04_00, j05_00, j06_00, j07_00, j08_00, j09_00, j10_00, j11_00, j12_00, j13_00, j14_00, j15_00, j16_00, j17_00, j18_00, j18_30, j19_00, j19_30, j20_00, j20_30, j21_00, j21_30, j22_00, j23_00, j24_00,jam_max,jumlah_max', 'length', 'max'=>10),
			array('j01_00, j02_00, j03_00, j04_00, j05_00, j06_00, j07_00, j08_00, j09_00, j10_00, j11_00, j12_00, j13_00, j14_00, j15_00, j16_00, j17_00, j18_00, j18_30, j19_00, j19_30, j20_00, j20_30, j21_00, j21_30, j22_00, j23_00, j24_00 , tanggal_full', 'required'),
			array('tanggal_full, date_post', 'safe'),
			array('tanggal_full' ,  'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tanggal_full, 01_00, 02_00, 03_00, 04_00, 05_00, 06_00, 07_00, 08_00, 09_00, j10_00, j11_00, j12_00, j13_00, j14_00, j15_00, j16_00, j17_00, j18_00, j18_30, j19_00, j19_30, j20_00, j20_30, j21_00, j21_30, j22_00, j23_00, j24_00, date_post, posted_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User','posted_by')
		);
	}

	public function getDataJam($date)
	{
		$sql = Yii::app()->db
		->createCommand()
		->select('*')
		->from('beban_puncak')
		->where('tanggal_full=:tgl' , array(':tgl' => $date))
		->queryRow();
	
	
		return array(
			floatval($sql['j01_00']),
			floatval($sql['j02_00']),
			floatval($sql['j03_00']),
			floatval($sql['j04_00']),
			floatval($sql['j05_00']),
			floatval($sql['j06_00']),
			floatval($sql['j07_00']),
			floatval($sql['j08_00']),
			floatval($sql['j09_00']),
			floatval($sql['j10_00']),
			floatval($sql['j11_00']),
			floatval($sql['j12_00']),
			floatval($sql['j13_00']),
			floatval($sql['j14_00']),
			floatval($sql['j15_00']),
			floatval($sql['j16_00']),
			floatval($sql['j17_00']),
			floatval($sql['j18_00']),
			floatval($sql['j18_30']),
			floatval($sql['j19_00']),
			floatval($sql['j19_30']),
			floatval($sql['j20_00']),
			floatval($sql['j20_30']),
			floatval($sql['j21_00']),
			floatval($sql['j21_30']),
			floatval($sql['j22_00']),
			floatval($sql['j23_00']),
			floatval($sql['j24_00'])
		);
	}

	
	public function greatest($id , $param)
	{
			$sql = "SELECT `column`, column_value
			FROM (
					SELECT id, 'j01_00' as `column`, j01_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j02_00' as `column`, j02_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j03_00' as `column`, j03_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j04_00' as `column`, j04_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j05_00' as `column`, j05_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j06_00' as `column`, j06_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j07_00' as `column`, j07_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j08_00' as `column`, j08_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j09_00' as `column`, j09_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j10_00' as `column`, j10_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j11_00' as `column`, j11_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j12_00' as `column`, j12_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j13_00' as `column`, j13_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j14_00' as `column`, j14_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j15_00' as `column`, j15_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j16_00' as `column`, j16_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j17_00' as `column`, j17_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j18_00' as `column`, j18_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j18_30' as `column`, j18_30 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j19_00' as `column`, j19_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j19_30' as `column`, j19_30 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j20_00' as `column`, j20_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j20_30' as `column`, j20_30 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j21_00' as `column`, j21_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j21_30' as `column`, j21_30 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j22_00' as `column`, j22_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j23_00' as `column`, j23_00 as column_value FROM beban_puncak
					UNION
					SELECT id, 'j24_00' as `column`, j24_00 as column_value FROM beban_puncak
					
			) pivot

			where column_value in (SELECT GREATEST(j01_00, j02_00, j03_00, j04_00, j05_00, j06_00, j07_00, j08_00, j09_00, j10_00, j11_00, j12_00, j13_00, j14_00, j15_00, j16_00, j17_00, j18_00, j18_30, j19_00, j19_30, j20_00, j20_30, j21_00, j21_30, j22_00, j23_00, j24_00) from beban_puncak)
			AND id = $id
			";
			
			$row = Yii::app()->db
			->createCommand($sql)
			->queryRow();
			
			if($param == 'jam')
			{
				
				$replace1 = str_replace('j' ,'' , $row['column']);
				$replace2 = str_replace('_',':' ,  $replace1);
				return $replace2;
			
			}elseif($param == 'jumlah'){
				return $row['column_value'];
			}
	}

	public function getDataHarian($param)
	{
				$tanggalAkhir = $param;
				for($a=1;$a<=$tanggalAkhir;$a++)
				{
					$tgl[] = $a;
				}
				return $tgl;
	}

	public function getDataHarianValue($bol = "" , $month , $year , $last)
	{
		$db = Yii::app()->db->createCommand();
		$tanggalAkhir = $last;
		for($a=1;$a<=$tanggalAkhir;$a++)
		{
			$sql2 = $db->select('jumlah_max,jam_max')
			->from('beban_puncak')
			->where("month(tanggal_full)=:month AND year(tanggal_full)=:year AND day(tanggal_full)=:day" , array(':month' => $month , ':year' => $year,':day' => $a))
			->queryRow();	

			$init = floatval($sql2['jumlah_max']);
			$hasil[] = array('y' => $init,'reza' => $sql2['jam_max']); 

		}

		return $hasil;
	}
	
	public function getDataBulanan($year)
	{
		for($a=1;$a<=12;$a++)
		{
			$month  = date("F", mktime(0, 0, 0, $a, 10));
			$init = $month.$year;
			$hasil[] = $init;
		}
		
		return $hasil;
	}
	
	public function getDataBulananValue($year)
	{
		$db = Yii::app()->db->createCommand();
		for($a=1;$a<=12;$a++)
		{
			$sql = $db->select('jumlah_max as jum_max, jam_max , day(tanggal_full) AS tanggal')
			->from('beban_puncak')
			->where('jumlah_max in
						(select max(jumlah_max) from beban_puncak where  MONTH(tanggal_full)=:month AND YEAR(tanggal_full)=:year)' 
					, array(':year' => $year , ':month' => $a)
			)
			->queryRow();
			$init = floatval($sql['jum_max']);
			$hasil[] = array('y' => $init , 'tanggal' => $sql['tanggal'] , 'jam' => $sql['jam_max']);
			
		}
		
		return $hasil;
	}
	
	public function getDataTahunan($awal,$akhir)
	{
		for($a=$awal;$a<=$akhir;$a++)
		{
			$hasil[] = $a;
		}
		
		
		return $hasil;
	}

	
	public function getDataTahunanValue($awal,$akhir)
	{
		$db = Yii::app()->db->createCommand();
		for($a=$awal;$a<=$akhir;$a++)
		{
			$sql = $db->select('jumlah_max as jum_max , jam_max , day(tanggal_full) as tanggal , month(tanggal_full) as bulan')
			->from('beban_puncak')
			->where('jumlah_max in
					(select max(jumlah_max) from beban_puncak where YEAR(tanggal_full)=:tahun)' , 
					array(':tahun' => $a)
			)
			->queryRow();
			$init = floatval($sql['jum_max']);
			$hasil[] = array('y' => $init ,'bulan' => $sql['bulan'], 'tanggal' => $sql['tanggal'] , 'jam' => $sql['jam_max']);
		}
		
		
		return $hasil;
	}
	
	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			
			$this->date_post = date("Y-m-d H:i:s");
			$this->posted_by = Yii::app()->session['tokenId'];
			$this->tanggal_full = date("Y-m-d" ,  strtotime($this->tanggal_full));
			return true;
		}
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal_full' => 'Tanggal',
			'j01_00' => '01.00',
			'j02_00' => '02.00',
			'j03_00' => '03.00',
			'j04_00' => '04.00',
			'j05_00' => '05.00',
			'j06_00' => '06.00',
			'j07_00' => '07.00',
			'j08_00' => '08.00',
			'j09_00' => '09.00',
			'j10_00' => '10.00',
			'j11_00' => '11.00',
			'j12_00' => '12.00',
			'j13_00' => '13.00',
			'j14_00' => '14.00',
			'j15_00' => '15.00',
			'j16_00' => '16.00',
			'j17_00' => '17.00',
			'j18_00' => '18.00',
			'j18_30' => '18.30',
			'j19_00' => '19.00',
			'j19_30' => '19.30',
			'j20_00' => '20.00',
			'j20_30' => '20.30',
			'j21_00' => '21.00',
			'j21_30' => '21.30',
			'j22_00' => '22.00',
			'j23_00' => '23.00',
			'j24_00' => '24.00',
			'date_post' => 'Date Post',
			'posted_by' => 'Posted By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tanggal_full',$this->tanggal_full,true);
		$criteria->compare('01_00',$this->j01_00,true);
		$criteria->compare('02_00',$this->j02_00,true);
		$criteria->compare('03_00',$this->j03_00,true);
		$criteria->compare('04_00',$this->j04_00,true);
		$criteria->compare('05_00',$this->j05_00,true);
		$criteria->compare('06_00',$this->j06_00,true);
		$criteria->compare('07_00',$this->j07_00,true);
		$criteria->compare('08_00',$this->j08_00,true);
		$criteria->compare('09_00',$this->j09_00,true);
		$criteria->compare('j10_00',$this->j10_00,true);
		$criteria->compare('j11_00',$this->j11_00,true);
		$criteria->compare('j12_00',$this->j12_00,true);
		$criteria->compare('j13_00',$this->j13_00,true);
		$criteria->compare('j14_00',$this->j14_00,true);
		$criteria->compare('j15_00',$this->j15_00,true);
		$criteria->compare('j16_00',$this->j16_00,true);
		$criteria->compare('j17_00',$this->j17_00,true);
		$criteria->compare('j18_00',$this->j18_00,true);
		$criteria->compare('j18_30',$this->j18_30,true);
		$criteria->compare('j19_00',$this->j19_00,true);
		$criteria->compare('j19_30',$this->j19_30,true);
		$criteria->compare('j20_00',$this->j20_00,true);
		$criteria->compare('j20_30',$this->j20_30,true);
		$criteria->compare('j21_00',$this->j21_00,true);
		$criteria->compare('j21_30',$this->j21_30,true);
		$criteria->compare('j22_00',$this->j22_00,true);
		$criteria->compare('j23_00',$this->j23_00,true);
		$criteria->compare('j24_00',$this->j24_00,true);
		$criteria->compare('date_post',$this->date_post,true);
		$criteria->compare('posted_by',$this->posted_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BebanPuncak the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
