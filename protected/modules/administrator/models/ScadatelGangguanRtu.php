<?php

/**
 * This is the model class for table "scadatel_gangguan_rtu".
 *
 * The followings are the available columns in table 'scadatel_gangguan_rtu':
 * @property integer $id
 * @property integer $jenis_gardu_id
 * @property integer $gardu_induk_id
 * @property string $waktu_gangguan
 * @property integer $flag_id
 * @property string $lama_gangguan
 * @property string $keterangan
 * @property integer $posted_by
 * @property string $date_post
 *
 * The followings are the available model relations:
 * @property MasterFlag $flag
 * @property MasterGarduInduk $garduInduk
 * @property MasterJenisGardu $jenisGardu
 */
class ScadatelGangguanRtu extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'scadatel_gangguan_rtu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jenis_gardu_id, gardu_induk_id, flag_id, posted_by', 'numerical', 'integerOnly'=>true),
			array('jenis_gardu_id, gardu_induk_id, waktu_gangguan, flag_id, lama_gangguan' , 'required'),
			array('waktu_gangguan, lama_gangguan, keterangan, date_post', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, jenis_gardu_id, gardu_induk_id, waktu_gangguan, flag_id, lama_gangguan, keterangan, posted_by, date_post', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'flag' => array(self::BELONGS_TO, 'MasterFlag', 'flag_id'),
			'garduInduk' => array(self::BELONGS_TO, 'MasterGarduInduk', 'gardu_induk_id'),
			'jenisGardu' => array(self::BELONGS_TO, 'MasterJenisGardu', 'jenis_gardu_id'),
			'user' => array(self::BELONGS_TO , 'User' , 'posted_by'),
		);
	}


	public function chartLabelGarduAll()
	{
		$model = Yii::app()->db->createCommand("SELECT master_gardu_induk.gardu_induk AS gardu_induk , master_jenis_gardu.jenis_gardu AS jenis_gardu FROM scadatel_gangguan_rtu 
			INNER JOIN master_gardu_induk ON master_gardu_induk.id = scadatel_gangguan_rtu.gardu_induk_id 
			INNER JOIN master_jenis_gardu ON master_jenis_gardu.id = scadatel_gangguan_rtu.jenis_gardu_id
			GROUP BY scadatel_gangguan_rtu.jenis_gardu_id ,  scadatel_gangguan_rtu.gardu_induk_id"
		)
		->query();
		$no = 0;
		while(($row = $model->read()) !== false)
		{
			$hasil[] = $row['jenis_gardu']." - ".$row['gardu_induk'];
		}

		return $hasil;
	}

	public function chartValueGarduAll()
	{
		$model = Yii::app()->db->createCommand("SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(lama_gangguan))) as lama_gangguan from scadatel_gangguan_rtu GROUP BY jenis_gardu_id ,  gardu_induk_id")
		->query();
		$no = 0;
		$no = 0;
		while(($row = $model->read()) !== false)
		{
			$no++;
			$exp = explode(":", $row['lama_gangguan']);
			$hasil[] = array('y' => intval($exp[0]) , 'reza' => intval($exp[1]));
			//$hasil[] = array('y' => ar::jamKeMenit($row['lama_gangguan']) ,  'reza' => $row['lama_gangguan']);
		}

		return $hasil;
	}

	/*public function chartLabelGarduParam()
	{
		return array(1,2,3);
	}

	public function chartValueGarduParam()
	{
		return array(1,2,3);
	}*/

	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->posted_by = Yii::app()->session['tokenId'];
			$this->date_post = date("Y-m-d H:i:s");
			$this->waktu_gangguan = ar::getTanggalSql($this->waktu_gangguan." 00:00:00");
			return true;
		}

	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'jenis_gardu_id' => 'Jenis Gardu',
			'gardu_induk_id' => 'Gardu Induk',
			'waktu_gangguan' => 'Waktu Gangguan',
			'flag_id' => 'Flag',
			'lama_gangguan' => 'Lama Gangguan',
			'keterangan' => 'Keterangan',
			'posted_by' => 'Posted By',
			'date_post' => 'Date Post',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('jenis_gardu_id',$this->jenis_gardu_id);
		$criteria->compare('gardu_induk_id',$this->gardu_induk_id);
		$criteria->compare('waktu_gangguan',$this->waktu_gangguan,true);
		$criteria->compare('flag_id',$this->flag_id);
		$criteria->compare('lama_gangguan',$this->lama_gangguan,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('posted_by',$this->posted_by);
		$criteria->compare('date_post',$this->date_post,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ScadatelGangguanRtu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
