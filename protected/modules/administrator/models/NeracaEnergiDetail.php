<?php

/**
 * This is the model class for table "neraca_energi_detail".
 *
 * The followings are the available columns in table 'neraca_energi_detail':
 * @property integer $id
 * @property integer $neraca_energi_id
 * @property integer $item_id
 * @property string $expor
 * @property string $impor
 * @property integer $faktor_expor
 * @property integer $faktor_impor
 */
class NeracaEnergiDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'neraca_energi_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('neraca_energi_id, item_id', 'numerical', 'integerOnly'=>true),
			//array('expor, impor,faktor_expor,faktor_impor , kwh_expor , kwh_impor', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, neraca_energi_id, item_id, expor, impor, faktor_expor, faktor_impor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'toItem' => array(self::BELONGS_TO , 'MasterItemNe' , 'item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'neraca_energi_id' => 'Neraca Energi',
			'item_id' => 'Item',
			'expor' => 'Expor',
			'impor' => 'Impor',
			'faktor_expor' => 'Faktor Expor',
			'faktor_impor' => 'Faktor Impor',
		);
	}

	public function sql($field,$where)
	{
		$sql = "
								SELECT SUM($field) FROM neraca_energi_detail 
								INNER JOIN  neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
								INNER JOIN master_gardu_induk ON neraca_energi.gardu_induk_id = master_gardu_induk.id
								INNER JOIN master_item_ne ON neraca_energi_detail.item_id = master_item_ne.id
								WHERE 
								$where
							
							";
		return $sql;
	}

	public function sqlPersen($field,$where)
	{
		$sql = "
								SELECT $field FROM neraca_energi_detail 
								INNER JOIN  neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
								INNER JOIN master_gardu_induk ON neraca_energi.gardu_induk_id = master_gardu_induk.id
								INNER JOIN master_item_ne ON neraca_energi_detail.item_id = master_item_ne.id
								WHERE 
								$where
							
							";
		return $sql;
	}

	public function minusHari($tanggal)
	{
		$db = Yii::app()->db;
		$sql = "SELECT DATE_ADD('$tanggal',INTERVAL -1 DAY);";
		return $db->createCommand($sql)->queryScalar();
	}

	/*public function getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field)
	{
			$db = Yii::app()->db;
			for($a=1;$a<=$lastDay;$a++)
			{
				$tanggal = "$year-$month-$a";
				$kemarin = $this->minusHari($tanggal);
				$sqlCek = $this->sqlPersen('item_id' , "jenis_id = '4' AND tanggal = '$tanggal'");
				$cek = $db->createCommand($sqlCek)->queryScalar();
				if($cek >0)
				{
					$sql = $this->sqlPersen('item_id' , "jenis_id = '4' AND tanggal = '$tanggal'");
					$qry = $db->createCommand($sql)->queryAll();	
					$hasil = 0;
					foreach($qry as $row)
					{
						$sqlFormulaKemarin = $this->sqlPersen("if(COUNT($field) = 0 , 0 , SUM($field))" , "jenis_id = '4' AND item_id = '$row[item_id]' AND tanggal = '$kemarin'");
						//$sqlFormulaFaktor = $db->createCommand("SELECT faktor_impor FROM master_faktor WHERE item_id = '$row[item_id]'")->queryScalar();
						$sqlFormulaFaktor = "SELECT faktor_impor FROM master_faktor WHERE item_id = '$row[item_id]'";
						$sqlFormula = $this->sqlPersen("SUM(($field - ($sqlFormulaKemarin)) ) * ($sqlFormulaFaktor)" , "jenis_id = '4' AND item_id = '$row[item_id]' AND tanggal = '$tanggal'");
						$execFormula = $db->createCommand($sqlFormula)->queryScalar();
						$hasil = $hasil + $execFormula;
					}
						
				}else{
					$hasil = 0;
				}	
						$Xvalue[] = floatval($hasil);
						//return $Xvalue;
			}
			return $Xvalue;
	}*/

	public function getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field)
	{
		$db = Yii::app()->db;
		($gardu_induk == "") ? $where_gardu = "" : $where_gardu = "AND master_item_ne.gardu_induk_id = '$gardu_induk' ";
		for($a=1;$a<=$lastDay;$a++)
		{
			$now = "$year-$month-$a";
			$yesterday = $this->minusHari($now);
			$sql = $db->createCommand("SELECT neraca_energi_detail.item_id AS item_id , item , impor , master_faktor.faktor_impor FROM neraca_energi_detail
				INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
				INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
				INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
				INNER JOIN master_gardu_induk ON master_gardu_induk.id = master_item_ne.gardu_induk_id
				where tanggal = '$now' AND jenis_id = '4' $where_gardu group by item_id 
				"
			)->queryAll();
			
				if($a==26)
				{
				echo "SELECT neraca_energi_detail.item_id AS item_id , item , impor , master_faktor.faktor_impor FROM neraca_energi_detail
				INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
				INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
				INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
				INNER JOIN master_gardu_induk ON master_gardu_induk.id = master_item_ne.gardu_induk_id
				where tanggal = '$now' AND jenis_id = '4' $where_gardu group by item_id 
				";
				}

			$resultCounter = 0;
			foreach($sql as $r)
			{
				$stand_hari_ini = $r['impor'];
				$stand_kemarin =  $db->createCommand("SELECT impor FROM neraca_energi_detail
				INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
				INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
				INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
				where tanggal = '$yesterday' AND jenis_id = '4' AND neraca_energi_detail.item_id = '$r[item_id]' GROUP BY neraca_energi_detail.item_id
				")->queryRow();
				
				$faktor = $r['faktor_impor'];
				$result = ($stand_hari_ini - $stand_kemarin['impor']) * $faktor;
				
				$resultCounter = $resultCounter + $result;
			}



			$hasil[] = round($resultCounter);
		}

		return $hasil;
	}

	public function getDataHarianBaruKeluar($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field)
	{
		$db = Yii::app()->db;
		($gardu_induk == "") ? $where_gardu = "" : $where_gardu = "AND master_item_ne.gardu_induk_id = '$gardu_induk' ";
		for($a=1;$a<=$lastDay;$a++)
		{
			$now = "$year-$month-$a";
			$yesterday = $this->minusHari($now);
			$sql = $db->createCommand("SELECT neraca_energi_detail.item_id AS item_id , item , expor , master_faktor.faktor_ekspor FROM neraca_energi_detail
				INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
				INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
				INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
				INNER JOIN master_gardu_induk ON master_gardu_induk.id = master_item_ne.gardu_induk_id
				where tanggal = '$now' AND jenis_id = '4' $where_gardu group by item_id 
				"
			)->queryAll();
			
			$resultCounter = 0;
			foreach($sql as $r)
			{
				$stand_hari_ini = $r['expor'];
				$stand_kemarin =  $db->createCommand("SELECT expor FROM neraca_energi_detail
				INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
				INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
				INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
				where tanggal = '$yesterday' AND jenis_id = '4' AND neraca_energi_detail.item_id = '$r[item_id]' group by neraca_energi_detail.item_id 
				")->queryRow();
				
				$faktor = $r['faktor_ekspor'];
				$result = ($stand_hari_ini - $stand_kemarin['expor']) * $faktor;
				
				$resultCounter = $resultCounter + $result;
				
			}



			$hasil[] = round($resultCounter);
		}

		return $hasil;
	}


	/*public function getDataTahunanBaru($year   , $gardu_induk , $item_id , $jenis_id , $field)
	{		
				$db = Yii::app()->db;
				for($month=1;$month<=12;$month++)
				{
					
					$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					
					$hasilhari = 0;
					for($a=1;$a<=$lastDay;$a++)
					{
						$tanggal = "$year-$month-$a";
						$kemarin = $this->minusHari($tanggal);
						$sqlCek = $this->sqlPersen('item_id' , "jenis_id = '4' AND tanggal = '$tanggal'");
						$cek = $db->createCommand($sqlCek)->queryScalar();
						if($cek >0)
						{
							$sql = $this->sqlPersen('item_id' , "jenis_id = '4' AND tanggal = '$tanggal'");
							$qry = $db->createCommand($sql)->queryAll();	
							$hasil = 0;
							foreach($qry as $row)
							{
								$sqlFormulaKemarin = $this->sqlPersen("if(COUNT($field) = 0 , 0 , $field)" , "jenis_id = '4' AND item_id = '$row[item_id]' AND tanggal = '$kemarin'");
								//$sqlFormulaFaktor = $db->createCommand("SELECT faktor_impor FROM master_faktor WHERE item_id = '$row[item_id]'")->queryScalar();
								$sqlFormulaFaktor = "SELECT faktor_impor FROM master_faktor WHERE item_id = '$row[item_id]'";

									$sqlFormula = $this->sqlPersen("($field - ($sqlFormulaKemarin))  * ($sqlFormulaFaktor) " , "jenis_id = '4' AND item_id = '$row[item_id]' AND tanggal = '$tanggal'");
								
								$execFormula = $db->createCommand($sqlFormula)->queryScalar();
								$hasil = $hasil + $execFormula;
							}
							
						}else{
							$hasil = 0;
						}	
						$hasilhari = $hasilhari + $hasil;
					}
					$Xvalue[] = floatval($hasilhari);
					
				}
				return $Xvalue;	
	}*/

	public function getDataTahunanBaru($year   , $gardu_induk , $item_id , $jenis_id , $field)
	{
		$db = Yii::app()->db;
		for($month=1;$month<=12;$month++)
		{
				$db = Yii::app()->db;
				$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				($gardu_induk == "") ? $where_gardu = "" : $where_gardu = "AND master_item_ne.gardu_induk_id = '$gardu_induk' ";
				$counterTahun = 0;
				for($a=1;$a<=$lastDay;$a++)
				{
					$now = "$year-$month-$a";
					$yesterday = $this->minusHari($now);
					$sql = $db->createCommand("SELECT neraca_energi_detail.item_id AS item_id , item , impor , master_faktor.faktor_impor FROM neraca_energi_detail
						INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
						INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
						INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
						INNER JOIN master_gardu_induk ON master_gardu_induk.id = master_item_ne.gardu_induk_id
						where tanggal = '$now' AND jenis_id = '4' $where_gardu group by item_id 
						"
					)->queryAll();
					
					$resultCounter = 0;
					foreach($sql as $r)
					{
						$stand_hari_ini = $r['impor'];
						$stand_kemarin =  $db->createCommand("SELECT impor FROM neraca_energi_detail
						INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
						INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
						INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
						where tanggal = '$yesterday' AND jenis_id = '4' AND neraca_energi_detail.item_id = '$r[item_id]' group by neraca_energi_detail.item_id 
						")->queryRow();

						$faktor = $r['faktor_impor'];
						$result = ($stand_hari_ini - $stand_kemarin['impor']) * $faktor;
						
						$resultCounter = $resultCounter + $result;
					}
					$hasilHarian = round($resultCounter);
					$counterTahun = $counterTahun + $hasilHarian;
				}
					$hasil[] = floatval($counterTahun);
		}
		return $hasil;
	}

	public function getDataTahunanBaruKeluar($year   , $gardu_induk , $item_id , $jenis_id , $field)
	{
		$db = Yii::app()->db;
		for($month=1;$month<=12;$month++)
		{
				$db = Yii::app()->db;
				$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				($gardu_induk == "") ? $where_gardu = "" : $where_gardu = "AND master_item_ne.gardu_induk_id = '$gardu_induk' ";
				$counterTahun = 0;
				for($a=1;$a<=$lastDay;$a++)
				{
					$now = "$year-$month-$a";
					$yesterday = $this->minusHari($now);
					$sql = $db->createCommand("SELECT neraca_energi_detail.item_id AS item_id , item , expor , master_faktor.faktor_ekspor FROM neraca_energi_detail
						INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
						INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
						INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
						INNER JOIN master_gardu_induk ON master_gardu_induk.id = master_item_ne.gardu_induk_id
						where tanggal = '$now' AND jenis_id = '4' $where_gardu GROUP BY item_id ASC  
						"
					)->queryAll();
					
					$resultCounter = 0;
					foreach($sql as $r)
					{
						$stand_hari_ini = $r['expor'];
						$stand_kemarin =  $db->createCommand("SELECT expor FROM neraca_energi_detail
						INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
						INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
						INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
						where tanggal = '$yesterday' AND jenis_id = '4' AND neraca_energi_detail.item_id = '$r[item_id]' group by neraca_energi_detail.item_id 
						")->queryRow();

						$faktor = $r['faktor_ekspor'];
						$result = ($stand_hari_ini - $stand_kemarin['expor']) * $faktor;
						
						$resultCounter = $resultCounter + $result;
					}
					$hasilHarian = round($resultCounter);
					$counterTahun = $counterTahun + $hasilHarian;
				}
					$hasil[] = floatval($counterTahun);
		}
		return $hasil;
	}

	/*public function getDataRangeBaru($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field)
	{
		$db = Yii::app()->db;
		for($year=$awal;$year<=$akhir;$year++)
		{
			$hasilBulan = 0;
			for($month=1;$month<=12;$month++)
				{
					
					$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					
					$hasilhari = 0;
					for($a=1;$a<=$lastDay;$a++)
					{
						$tanggal = "$year-$month-$a";
						$kemarin = $this->minusHari($tanggal);
						$sqlCek = $this->sqlPersen('item_id' , "jenis_id = '4' AND tanggal = '$tanggal'");
						$cek = $db->createCommand($sqlCek)->queryScalar();
						if($cek >0)
						{
							$sql = $this->sqlPersen('item_id' , "jenis_id = '4' AND tanggal = '$tanggal'");
							$qry = $db->createCommand($sql)->queryAll();	
							$hasil = 0;
							foreach($qry as $row)
							{
								$sqlFormulaKemarin = $this->sqlPersen("if(COUNT($field) = 0 , 0 , $field)" , "jenis_id = '4' AND item_id = '$row[item_id]' AND tanggal = '$kemarin'");
								//$sqlFormulaFaktor = $db->createCommand("SELECT faktor_impor FROM master_faktor WHERE item_id = '$row[item_id]'")->queryScalar();
								$sqlFormulaFaktor = "SELECT faktor_impor FROM master_faktor WHERE item_id = '$row[item_id]'";

									$sqlFormula = $this->sqlPersen("($field - ($sqlFormulaKemarin))  * ($sqlFormulaFaktor) " , "jenis_id = '4' AND item_id = '$row[item_id]' AND tanggal = '$tanggal'");
								
								$execFormula = $db->createCommand($sqlFormula)->queryScalar();
								$hasil = $hasil + $execFormula;
							}
							
						}else{
							$hasil = 0;
						}	
						$hasilhari = $hasilhari + $hasil;
					}
					 $hasilBulan = $hasilBulan + $hasilhari;
					
					
				}
				$Xvalue[] = floatval($hasilBulan);
		}

		return $Xvalue;
	}*/

	public function getDataRangeBaru($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field)
	{
		for($year=$awal;$year<=$akhir;$year++)
		{
			$counterRange = 0;
			for($month=1;$month<=12;$month++)
			{
					$db = Yii::app()->db;
					$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					($gardu_induk == "") ? $where_gardu = "" : $where_gardu = "AND master_item_ne.gardu_induk_id = '$gardu_induk' ";
					$counterTahun = 0;
					for($a=1;$a<=$lastDay;$a++)
					{
						$now = "$year-$month-$a";
						$yesterday = $this->minusHari($now);
						$sql = $db->createCommand("SELECT neraca_energi_detail.item_id AS item_id , item , impor , master_faktor.faktor_impor FROM neraca_energi_detail
							INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
							INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
							INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
							INNER JOIN master_gardu_induk ON master_gardu_induk.id = master_item_ne.gardu_induk_id
							where tanggal = '$now' AND jenis_id = '4' $where_gardu group by item_id   
							"
						)->queryAll();
						
						$resultCounter = 0;
						foreach($sql as $r)
						{
							$stand_hari_ini = $r['impor'];
							$stand_kemarin =  $db->createCommand("SELECT impor FROM neraca_energi_detail
							INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
							INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
							INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
							where tanggal = '$yesterday' AND jenis_id = '4' AND neraca_energi_detail.item_id = '$r[item_id]' group by neraca_energi_detail.item_id
							")->queryRow();

							$faktor = $r['faktor_impor'];
							$result = ($stand_hari_ini - $stand_kemarin['impor']) * $faktor;
							
							$resultCounter = $resultCounter + $result;
						}
						$hasilHarian = round($resultCounter);
						$counterTahun = $counterTahun + $hasilHarian;
					}
				$counterRange = $counterRange + $counterTahun;
					
			}
			$hasil[] = floatval($counterRange);
		}

		return $hasil;
	}


	public function getDataRangeBaruKeluar($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field)
	{
		for($year=$awal;$year<=$akhir;$year++)
		{
			$counterRange = 0;
			for($month=1;$month<=12;$month++)
			{
					$db = Yii::app()->db;
					$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					($gardu_induk == "") ? $where_gardu = "" : $where_gardu = "AND master_item_ne.gardu_induk_id = '$gardu_induk' ";
					$counterTahun = 0;
					for($a=1;$a<=$lastDay;$a++)
					{
						$now = "$year-$month-$a";
						$yesterday = $this->minusHari($now);
						$sql = $db->createCommand("SELECT neraca_energi_detail.item_id AS item_id , item , expor , master_faktor.faktor_ekspor FROM neraca_energi_detail
							INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
							INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
							INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
							INNER JOIN master_gardu_induk ON master_gardu_induk.id = master_item_ne.gardu_induk_id
							where tanggal = '$now' AND jenis_id = '4' $where_gardu GROUP BY item_id   
							"
						)->queryAll();
						
						$resultCounter = 0;
						foreach($sql as $r)
						{
							$stand_hari_ini = $r['expor'];
							$stand_kemarin =  $db->createCommand("SELECT expor FROM neraca_energi_detail
							INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
							INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
							INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
							where tanggal = '$yesterday' AND jenis_id = '4' AND neraca_energi_detail.item_id = '$r[item_id]' group by neraca_energi_detail.item_id 
							")->queryRow();

							$faktor = $r['faktor_ekspor'];
							$result = ($stand_hari_ini - $stand_kemarin['expor']) * $faktor;
							
							$resultCounter = $resultCounter + $result;
						}
						$hasilHarian = round($resultCounter);
						$counterTahun = $counterTahun + $hasilHarian;
					}
				$counterRange = $counterRange + $counterTahun;
					
			}
			$hasil[] = floatval($counterRange);
		}

		return $hasil;
	}

	/*public function getDataHarian($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field)
	{		
					for($a=1;$a<=$lastDay;$a++)
					{
						$tanggal = "$year-$month-$a";
						$where = "tanggal ='$tanggal' AND jenis_id = '$jenis_id' ";
						if(empty($gardu_induk))
						{
							$where .= "";
						}elseif(!empty($gardu_induk) && empty($item_id)){
							$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
						}elseif(!empty($gardu_induk) && !empty($item_id)){
							$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
						}
						$sql = $this->sql($field ,$where);
												
						
						
						$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
						
						$hasil[] = floatval($hasilSql);
					}
					return $hasils;
					
	}*/

	public function getDataHarian($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field)
	{
		$db = Yii::app()->db;
		($gardu_induk == "") ? $where_gardu = "" : $where_gardu = "AND master_item_ne.gardu_induk_id = '$gardu_induk' ";
		for($a=1;$a<=$lastDay;$a++)
		{
			$now = "$year-$month-$a";
			$yesterday = $this->minusHari($now);
			$sql = $db->createCommand("SELECT neraca_energi_detail.item_id AS item_id , expor , faktor_ekspor FROM neraca_energi_detail
				INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
				INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
				INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
				INNER JOIN master_gardu_induk ON master_gardu_induk.id = master_item_ne.gardu_induk_id
				where tanggal = '$now' AND jenis_id = '5' $where_gardu GROUP BY item_id   
				"
			)->queryAll();
			
			$resultCounter = 0;
			foreach($sql as $r)
			{
				$stand_hari_ini = $r['expor'];
				$stand_kemarin =  $db->createCommand("SELECT expor FROM neraca_energi_detail
				INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
				INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
				INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
				where tanggal = '$yesterday' AND jenis_id = '5' AND neraca_energi_detail.item_id = '$r[item_id]' GROUP BY neraca_energi_detail.item_id 
				")->queryRow();

				$faktor = $r['faktor_ekspor'];
				$result = ($stand_hari_ini - $stand_kemarin['expor']) * $faktor;
				
				$resultCounter = $resultCounter + $result;
			}

			$hasil[] = round($resultCounter);
		}

		return $hasil;
	}

	public function getDataTahunan($year   , $gardu_induk , $item_id , $jenis_id , $field)
	{
		$db = Yii::app()->db;
		for($month=1;$month<=12;$month++)
		{
				$db = Yii::app()->db;
				$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				($gardu_induk == "") ? $where_gardu = "" : $where_gardu = "AND master_item_ne.gardu_induk_id = '$gardu_induk' ";
				$counterTahun = 0;
				for($a=1;$a<=$lastDay;$a++)
				{
					$now = "$year-$month-$a";
					$yesterday = $this->minusHari($now);
					$sql = $db->createCommand("SELECT neraca_energi_detail.item_id AS item_id , expor , faktor_ekspor FROM neraca_energi_detail
						INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
						INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
						INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
						INNER JOIN master_gardu_induk ON master_gardu_induk.id = master_item_ne.gardu_induk_id
						where tanggal = '$now' AND jenis_id = '5' $where_gardu GROUP BY item_id 
						"
					)->queryAll();
					
					$resultCounter = 0;
					foreach($sql as $r)
					{
						$stand_hari_ini = $r['expor'];
						$stand_kemarin =  $db->createCommand("SELECT expor FROM neraca_energi_detail
						INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
						INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
						INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
						where tanggal = '$yesterday' AND jenis_id = '5' AND neraca_energi_detail.item_id = '$r[item_id]' GROUP BY neraca_energi_detail.item_id 
						")->queryRow();

						$faktor = $r['faktor_ekspor'];
						$result = ($stand_hari_ini - $stand_kemarin['expor']) * $faktor;
						
						$resultCounter = $resultCounter + $result;
					}
					$hasilHarian = round($resultCounter);
					$counterTahun = $counterTahun + $hasilHarian;
				}
					$hasil[] = floatval($counterTahun);
		}
		return $hasil;
	}

	public function getDataRange($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field)
	{
		for($year=$awal;$year<=$akhir;$year++)
		{
			$counterRange = 0;
			for($month=1;$month<=12;$month++)
			{
					$db = Yii::app()->db;
					$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					($gardu_induk == "") ? $where_gardu = "" : $where_gardu = "AND master_item_ne.gardu_induk_id = '$gardu_induk' ";
					$counterTahun = 0;
					for($a=1;$a<=$lastDay;$a++)
					{
						$now = "$year-$month-$a";
						$yesterday = $this->minusHari($now);
						$sql = $db->createCommand("SELECT neraca_energi_detail.item_id AS item_id , expor , faktor_ekspor FROM neraca_energi_detail
						INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
						INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
						INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
						INNER JOIN master_gardu_induk ON master_gardu_induk.id = master_item_ne.gardu_induk_id
						where tanggal = '$now' AND jenis_id = '5' $where_gardu GROUP BY item_id  
						"
					)->queryAll();
						
						$resultCounter = 0;
						foreach($sql as $r)
						{
							$stand_hari_ini = $r['expor'];
						$stand_kemarin =  $db->createCommand("SELECT expor FROM neraca_energi_detail
						INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
						INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
						INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
						where tanggal = '$yesterday' AND jenis_id = '5' AND neraca_energi_detail.item_id = '$r[item_id]' GROUP BY neraca_energi_detail.item_id 	
						")->queryRow();

						$faktor = $r['faktor_ekspor'];
						$result = ($stand_hari_ini - $stand_kemarin['expor']) * $faktor;
							
							$resultCounter = $resultCounter + $result;
						}
						$hasilHarian = round($resultCounter);
						$counterTahun = $counterTahun + $hasilHarian;
					}
				$counterRange = $counterRange + $counterTahun;
					
			}
			$hasil[] = floatval($counterRange);
		}

		return $hasil;
	}

	/*public function getDataTahunan($year   , $gardu_induk , $item_id , $jenis_id , $field)
	{		
				for($a=1;$a<=12;$a++)
				{
					$tanggal = " MONTH(tanggal) = '$a' AND YEAR(tanggal) = '$year' ";
					$where = "$tanggal AND jenis_id = '$jenis_id' ";
					if(empty($gardu_induk))
					{
						$where .= "";
					}elseif(!empty($gardu_induk) && empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
					}elseif(!empty($gardu_induk) && !empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
					}	
					 $sql = $this->sql($field ,$where);
					$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
					
					$hasil[] = floatval($hasilSql);
				}
				return $hasil;	
	}*/

	/*public function getDataRange($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field)
	{		
				for($a=$awal;$a<=$akhir;$a++)
				{
					$tanggal = " YEAR(tanggal) = '$a' ";
					$where = "$tanggal AND jenis_id = '$jenis_id' ";
					if(empty($gardu_induk))
					{
						$where .= "";
					}elseif(!empty($gardu_induk) && empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
					}elseif(!empty($gardu_induk) && !empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
					}	
					 $sql = $this->sql($field ,$where);
					$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
					
					$hasil[] = floatval($hasilSql);
				}
				return $hasil;	
	}*/

	public function sqlCalculate($field ,  $field2 ,$where)
	{
		$sql = "
								SELECT ($field2 - $field) AS nilai , gardu_induk , item FROM neraca_energi_detail 
								INNER JOIN  neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
								INNER JOIN master_gardu_induk ON neraca_energi.gardu_induk_id = master_gardu_induk.id
								INNER JOIN master_item_ne ON neraca_energi_detail.item_id = master_item_ne.id
								WHERE 
								$where group by item_id order by item_id 
							
							";
		return $sql;
	}
	public function sqlCalculateSum($field ,  $field2 ,$where)
	{
		$sql = "
								SELECT SUM($field2 - $field)  FROM neraca_energi_detail 
								INNER JOIN  neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
								INNER JOIN master_gardu_induk ON neraca_energi.gardu_induk_id = master_gardu_induk.id
								INNER JOIN master_item_ne ON neraca_energi_detail.item_id = master_item_ne.id
								WHERE 
								$where 
							
							";
		return $sql;
	}

	/*public function hitungExporImporDistribusi($field , $faktor , $tanggal , $jenis_id , $gardu_induk , $item_id)
	{
		$where = "";
		if(empty($gardu_induk) && empty($item_id))
		{
			$where .= "";
		}elseif(!empty($gardu_induk) && empty($item_id)){
			$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
		}elseif(!empty($gardu_induk) && !empty($item_id)){
			$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND neraca_energi_detail.item_id = '$item_id'";
		}else{
			$where .= " AND neraca_energi_detail.item_id = '$item_id'";
		}	
		$db = Yii::app()->db;
		$sqlExpor = $db->createCommand("SELECT neraca_energi_detail.item_id AS item_id , item , $field , master_faktor.$faktor FROM neraca_energi_detail
					INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
					INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
					INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
					INNER JOIN master_gardu_induk ON master_gardu_induk.id = master_item_ne.gardu_induk_id
					where tanggal = '$tanggal' AND jenis_id = '$jenis_id' $where ORDER BY item_id ASC  
					"
				)->query();
				$yesterday = $this->minusHari($tanggal);
				$resultCounterExpor = 0;
				while(($r = $sqlExpor->read()) !== false)
				{
					$stand_hari_ini_expor = $r[$field];
					$stand_kemarin_expor =  $db->createCommand("SELECT $field FROM neraca_energi_detail
					INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
					INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
					INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
					where tanggal = '$yesterday' AND jenis_id = $jenis_id AND neraca_energi_detail.item_id = '$r[item_id]'
					")->queryRow();



					$faktor_expor = $r[$faktor];
					$result_expor = ($stand_hari_ini_expor - $stand_kemarin_expor[$field]) * $faktor_expor;
					
					$resultCounterExpor = $resultCounterExpor + $result_expor;
					
				}

				return $resultCounterExpor;
	}*/

	/*public function getDataHarianCalculate($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field , $field2)
	{
		$db = Yii::app()->db;
		($gardu_induk == "") ? $where_gardu = "" : $where_gardu = "AND master_item_ne.gardu_induk_id = '$gardu_induk' ";
		for($a=1;$a<=$lastDay;$a++)
		{
				$now = "$year-$month-$a";
				$yesterday = $this->minusHari($now);
				$impor = $this->hitungExporImporDistribusi('impor' , 'faktor_impor' , $now , 1 , $gardu_induk , $item_id);
				$expor = $this->hitungExporImporDistribusi('expor' , 'faktor_ekspor' , $now , 1  , $gardu_induk , $item_id);
				$result = $impor - $expor;
				
				$hasil[] = floatval(round($result));
		}
		return $hasil;
	}*/

	public function getDataHarianCalculate($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field , $field2)
	{			
					for($a=1;$a<=$lastDay;$a++)
					{
						$tanggal = "$year-$month-$a";
						$where = "tanggal ='$tanggal' AND jenis_id = '$jenis_id' ";
						
						
						if(empty($gardu_induk) && empty($item_id))
						{
							$where .= "";
							$sql = $this->sqlCalculate($field,$field2 ,$where);
							$r_all = Yii::app()->db->createCommand($sql)->queryAll();
							$plus = 0;
							foreach($r_all as $r)
							{
								$plus = $plus + round($r['nilai']);
								if($a == 9){echo round($r['nilai'])."   <br/>";}
							}	
							$hasilSql = $plus;


						}elseif(!empty($gardu_induk) && empty($item_id)){
							$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
							$sql = $this->sqlCalculate($field,$field2 ,$where);
							$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();
						}elseif(!empty($gardu_induk) && !empty($item_id)){
							$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
							$sql = $this->sqlCalculate($field,$field2 ,$where);
							$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();
						}else{
							$where .= " AND item_id = '$item_id'";
							$sql = $this->sqlCalculate($field,$field2 ,$where);
							$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();
						}	
							
						//if($a == 1){ echo $sql; }
							
						$hasil[] = floatval($hasilSql);
					}
					return $hasil;
	}


	public function getDataTahunanCalculate($year   , $gardu_induk , $item_id , $jenis_id , $field , $field2)
	{		
				for($month=1;$month<=12;$month++)
				{
					$lastDay = date("t", strtotime("$year-$month-1"));
					$plus = 0;
					for($a=1;$a<=$lastDay;$a++)
					{
						$tanggal = "$year-$month-$a";
						$where = "tanggal ='$tanggal' AND jenis_id = '$jenis_id' ";
						
						
						if(empty($gardu_induk) && empty($item_id))
						{
							$where .= "";
							$sql = $this->sqlCalculateSum($field,$field2 ,$where);
						}elseif(!empty($gardu_induk) && empty($item_id)){
							$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
							$sql = $this->sqlCalculate($field,$field2 ,$where);
						}elseif(!empty($gardu_induk) && !empty($item_id)){
							$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
							$sql = $this->sqlCalculate($field,$field2 ,$where);
						}else{
							$where .= " AND item_id = '$item_id'";
							$sql = $this->sqlCalculate($field,$field2 ,$where);
						}	
							
						//$sql = $this->sqlCalculate($field,$field2 ,$where);
						//if($a == 2){ echo $sql; }
						$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
						$plus = $plus + $hasilSql;
					}
					$hasil[] = floatval($plus);
				}
				return $hasil;	
	}

	public function getDataRangeCalculate($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field , $field2)
	{		
				for($year=$awal;$year<=$akhir;$year++)
				{
					$plus_bulan = 0;
					for($month=1;$month<=12;$month++)
					{
						$lastDay = date("t", strtotime("$year-$month-1"));
						$plus_hari = 0;
						for($a=1;$a<=$lastDay;$a++)
						{
							$tanggal = "$year-$month-$a";
							$where = "tanggal ='$tanggal' AND jenis_id = '$jenis_id' ";
							
							
							if(empty($gardu_induk) && empty($item_id))
							{
								$where .= "";
								$sql = $this->sqlCalculateSum($field,$field2 ,$where);
							}elseif(!empty($gardu_induk) && empty($item_id)){
								$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
								$sql = $this->sqlCalculate($field,$field2 ,$where);
							}elseif(!empty($gardu_induk) && !empty($item_id)){
								$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
								$sql = $this->sqlCalculate($field,$field2 ,$where);
							}else{
								$where .= " AND item_id = '$item_id'";
								$sql = $this->sqlCalculate($field,$field2 ,$where);
							}	
								
							//$sql = $this->sqlCalculate($field,$field2 ,$where);
							//if($a == 2){ echo $sql; }
							$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
							$plus_hari = $plus_hari + $hasilSql;
						}
						$plus_bulan = $plus_bulan + $plus_hari;
					}
					$hasil[] = floatval($plus_bulan);
				}
				return $hasil;	
	}
	

	/*public function getDataHarianTransmisi($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field)
	{		
					for($a=1;$a<=$lastDay;$a++)
					{
						$tanggal = "$year-$month-$a";
						$cek = Yii::app()->db->createCommand($this->sqlPersen("COUNT(neraca_energi_detail.id)" , " jenis_id = '5' AND tanggal = '$tanggal'"))->queryScalar();
						if($cek >0)
						{		
								$where = "tanggal ='$tanggal' AND jenis_id = '$jenis_id' ";
								if(empty($gardu_induk))
								{
									$where .= "";
								}elseif(!empty($gardu_induk) && empty($item_id)){
									$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
								}elseif(!empty($gardu_induk) && !empty($item_id)){
									$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
								}	
								 $sqlPsgi = Yii::app()->db->createCommand($this->sql("kwh_expor ","  jenis_id = '5' AND tanggal = '$tanggal'" ))->queryScalar();
								 $sqlImporExpor = Yii::app()->db->createCommand($this->sqlPersen("SUM(kwh_impor - kwh_expor)","jenis_id = '4' AND tanggal = '$tanggal'"))->queryScalar();
								
								 
									$hasilSql = $sqlImporExpor - $sqlPsgi;	
									//echo "<br/> $sqlImporExpor - $sqlPsgi";
						}else{
							$hasilSql = 0;
						}
						$hasil[] = floatval($hasilSql);
					}
					return $hasil;
	}*/

	public function hitungExporImpor($field , $faktor , $tanggal , $jenis_id )
	{
		$db = Yii::app()->db;
		$sqlExpor = $db->createCommand("SELECT neraca_energi_detail.item_id AS item_id , item , $field , master_faktor.$faktor FROM neraca_energi_detail
					INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
					INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
					INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
					INNER JOIN master_gardu_induk ON master_gardu_induk.id = master_item_ne.gardu_induk_id
					where tanggal = '$tanggal' AND jenis_id = '$jenis_id'  GROUP BY item_id   
					"
				)->query();
				$yesterday = $this->minusHari($tanggal);
				$resultCounterExpor = 0;
				while(($r = $sqlExpor->read()) !== false)
				{
					$stand_hari_ini_expor = $r[$field];
					$stand_kemarin_expor =  $db->createCommand("SELECT $field FROM neraca_energi_detail
					INNER JOIN neraca_energi ON neraca_energi.id = neraca_energi_detail.neraca_energi_id
					INNER JOIN master_item_ne ON master_item_ne.id = neraca_energi_detail.item_id
					INNER JOIN master_faktor ON master_faktor.item_id = neraca_energi_detail.item_id
					where tanggal = '$yesterday' AND jenis_id = $jenis_id AND neraca_energi_detail.item_id = '$r[item_id]' GROUP BY neraca_energi_detail.item_id 
					")->queryRow();



					$faktor_expor = $r[$faktor];
					$result_expor = ($stand_hari_ini_expor - $stand_kemarin_expor[$field]) * $faktor_expor;
					
					$resultCounterExpor = $resultCounterExpor + $result_expor;
					
				}

				return $resultCounterExpor;
	}

	public function getDataHarianTransmisi($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field)
	{
		$db = Yii::app()->db;
		($gardu_induk == "") ? $where_gardu = "" : $where_gardu = "AND master_item_ne.gardu_induk_id = '$gardu_induk' ";
		for($a=1;$a<=$lastDay;$a++)
		{
				$now = "$year-$month-$a";
				$yesterday = $this->minusHari($now);
				$impor = $this->hitungExporImpor('impor' , 'faktor_impor' , $now , 4 );
				$expor = $this->hitungExporImpor('expor' , 'faktor_ekspor' , $now , 4 );
				$psgi = $this->hitungExporImpor('expor' , 'faktor_ekspor' , $now , 5 );
				$result = $impor - $expor - $psgi;
				if($a == 26)
				{
					echo $impor."impor <br/>";
					echo $expor."expor <br/>";
					echo $psgi."psgi <br/>";
					echo $result."result  <br/>";
				}
				$hasil[] = round($result);
		}



		return $hasil;
	}
	public function getDataTahunanTransmisi($year   , $gardu_induk , $item_id , $jenis_id , $field)
	{
		$db = Yii::app()->db;
		for($a=1;$a<=12;$a++)
		{	
				$lastDay = cal_days_in_month(CAL_GREGORIAN, $a, $year);
				$konterHari = 0;
				for($b=1;$b<=$lastDay;$b++)
				{
						$now = "$year-$a-$b";
						$yesterday = $this->minusHari($now);
						$impor = $this->hitungExporImpor('impor' , 'faktor_impor' , $now , 4 );
						$expor = $this->hitungExporImpor('expor' , 'faktor_ekspor' , $now , 4 );
						$psgi = $this->hitungExporImpor('expor' , 'faktor_ekspor' , $now , 5 );
						$result = $impor - $expor - $psgi;
						$konterHari = $konterHari + round($result);
				}
				$hasil[] = floatval($konterHari);
		}
			return $hasil;
	}

	public function getDataRangeTransmisi($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field)
	{
		$db = Yii::app()->db;
		for($year=$awal;$year<=$akhir;$year++)
		{
			$konterBulan = 0;
			for($month=1;$month<=12;$month++)
			{	
				$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				$konterHari = 0;
				for($b=1;$b<=$lastDay;$b++)
				{
							$now = "$year-$month-$b";
							$yesterday = $this->minusHari($now);
							$impor = $this->hitungExporImpor('impor' , 'faktor_impor' , $now , 4 );
							$expor = $this->hitungExporImpor('expor' , 'faktor_ekspor' , $now , 4 );
							$psgi = $this->hitungExporImpor('expor' , 'faktor_ekspor' , $now , 5 );
							$result = $impor - $expor - $psgi;
							$konterHari = $konterHari + round($result);
				}
				$konterBulan = $konterBulan + $konterHari;
			}
			$hasil[] = floatval($konterBulan);
		}
		return $hasil;
	}
	/*public function getDataTahunanTransmisi($year   , $gardu_induk , $item_id , $jenis_id , $field)
	{		
				for($a=1;$a<=12;$a++)
				{
					$tanggal = " MONTH(tanggal) = '$a' AND YEAR(tanggal) = '$year' ";
					$where = "$tanggal AND jenis_id = '$jenis_id' ";
					if(empty($gardu_induk))
					{
						$where .= "";
					}elseif(!empty($gardu_induk) && empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
					}elseif(!empty($gardu_induk) && !empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
					}	

					
					 $sqlPsgi = $this->sql("kwh_expor ","  jenis_id = '5' AND $tanggal" );
					
					  $sql = $this->sql($field.") - (".$sqlPsgi ,$where);
					 
					 $hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
					
					$hasil[] = floatval($hasilSql);
				}
				return $hasil;	
	}*/

	/*public function getDataRangeTransmisi($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field)
	{		
				for($a=$awal;$a<=$akhir;$a++)
				{
					$tanggal = " YEAR(tanggal) = '$a' ";
					$where = "$tanggal AND jenis_id = '$jenis_id' ";
					if(empty($gardu_induk))
					{
						$where .= "";
					}elseif(!empty($gardu_induk) && empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id = '$gardu_induk'";
					}elseif(!empty($gardu_induk) && !empty($item_id)){
						$where .= " AND neraca_energi.gardu_induk_id  = '$gardu_induk' AND item_id = '$item_id'";
					}	
					  $sqlPsgi = $this->sql("kwh_expor ","  jenis_id = '5' AND $tanggal" );
					
					$sql = $this->sql($field.") - (".$sqlPsgi ,$where);
					$hasilSql = Yii::app()->db->createCommand($sql)->queryScalar();	
					
					$hasil[] = floatval($hasilSql);
				}
				return $hasil;	
	}*/


	
	public function getDataHarianTransmisiPersen($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field)
	{
			$db = Yii::app()->db;
			($gardu_induk == "") ? $where_gardu = "" : $where_gardu = "AND master_item_ne.gardu_induk_id = '$gardu_induk' ";
			for($a=1;$a<=$lastDay;$a++)
			{
					$now = "$year-$month-$a";
					$yesterday = $this->minusHari($now);
					$impor = $this->hitungExporImpor('impor' , 'faktor_impor' , $now , 4 ); // energi masuk
					$expor = $this->hitungExporImpor('expor' , 'faktor_ekspor' , $now , 4 ); // energi keluar
					$psgi = $this->hitungExporImpor('expor' , 'faktor_ekspor' , $now , 5 );
					$susut = $impor - $expor - $psgi;
					if($impor == 0)
					{
						$result = 0;
					}else{
						$result = $susut / $impor * 100;
					}	
						

					$hasil[] = floatval(round($result , 2));
			}

			return $hasil;
	}

	public function getDataTahunanTransmisiPersen($year   , $gardu_induk , $item_id , $jenis_id , $field)
	{
		$db = Yii::app()->db;
		for($month=1;$month<=12;$month++)
		{	
			$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$konterHari = 0;
			for($a=1;$a<=$lastDay;$a++)
			{
					$now = "$year-$month-$a";
					$yesterday = $this->minusHari($now);
					$impor = $this->hitungExporImpor('impor' , 'faktor_impor' , $now , 4 ); // energi masuk
					$expor = $this->hitungExporImpor('expor' , 'faktor_ekspor' , $now , 4 ); // energi keluar
					$psgi = $this->hitungExporImpor('expor' , 'faktor_ekspor' , $now , 5 );
					$susut = $impor - $expor - $psgi;
					if($impor == 0)
					{
						$result = 0;
					}else{
						$result = $susut / $impor * 100;
					}	
							

					$hasilHari = round($result , 2);
					$konterHari = $konterHari +  $hasilHari;

			}

				$hasil[] = floatval($konterHari);

		}
		return $hasil;
	}

	public function getDataRangeTransmisiPersen($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field)
	{
		$db = Yii::app()->db;
		for($year=$awal;$year<=$akhir;$year++)
		{
				$konterBulan=0;
				for($month=1;$month<=12;$month++)
				{	
					$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					$konterHari = 0;
					for($a=1;$a<=$lastDay;$a++)
					{
							$now = "$year-$month-$a";
							$yesterday = $this->minusHari($now);
							$impor = $this->hitungExporImpor('impor' , 'faktor_impor' , $now , 4 ); // energi masuk
							$expor = $this->hitungExporImpor('expor' , 'faktor_ekspor' , $now , 4 ); // energi keluar
							$psgi = $this->hitungExporImpor('expor' , 'faktor_ekspor' , $now , 5 );
							$susut = $impor - $expor - $psgi;
							if($impor == 0)
							{
								$result = 0;
							}else{
								$result = $susut / $impor * 100;
							}	
									

							$hasilHari = round($result , 2);
							$konterHari = $konterHari +  $hasilHari;

					}

						$konterBulan = $konterBulan + $konterHari;

				}
				$hasil[] = floatval(round($konterBulan));
		}

		return $hasil;
	}

	/*public function getDataHarianTransmisiPersen($year , $month , $lastDay , $gardu_induk , $item_id , $jenis_id , $field)
	{		
					$energiMasuk = $this->getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , 4 , 'impor');
					$energiKeluar = $this->getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , 4 , 'expor');
					$psgi = $this->getDataHarian($year , $month , $lastDay , $gardu_induk , $item_id , 5 , 'expor');
					
					$b = -1;
					for($a=1;$a<=$lastDay;$a++)
					{
							$b++;
							$susutTransmisi = $energiMasuk[$b] - $energiKeluar[$b] - $psgi[$b];
							if($energiMasuk[$b] == 0)
							{
								$persen = 0;
							}else{
								$persen = $susutTransmisi / $energiMasuk[$b] * 100;
							}	
								
							
							$hasil[] = $persen;

					}	
					
					return $hasil;				
	}*/


	/*public function getDataTahunanTransmisiPersen($year   , $gardu_induk , $item_id , $jenis_id , $field)
	{		
				for($month=1;$month<=12;$month++)
				{
						$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
						$energiMasuk = $this->getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , 4 , 'impor');
						$energiKeluar = $this->getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , 4 , 'expor');
						$psgi = $this->getDataHarian($year , $month , $lastDay , $gardu_induk , $item_id , 5 , 'expor');
						
						$b = -1;
						$hasilHari = 0;
						for($a=1;$a<=$lastDay;$a++)
						{
								$b++;
								$susutTransmisi = $energiMasuk[$b] - $energiKeluar[$b] - $psgi[$b];
								if($energiMasuk[$b] == 0)
								{
									$persenHari = 0;
								}else{
									$persenHari = $susutTransmisi / $energiMasuk[$b] * 100;
								}	
									
							$hasilHari = $hasilHari + $persenHari;
						}
						$persen[] = $hasilHari;

				}	
					
					return $persen;		
	}*/


	/*public function getDataRangeTransmisiPersen($awal , $akhir   , $gardu_induk , $item_id , $jenis_id , $field)
	{		
				for($year=$awal;$year<=$akhir;$year++)
				{
					$hasilBulan =0;
					for($month=1;$month<=12;$month++)
					{
							$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
							$energiMasuk = $this->getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , 4 , 'impor');
							$energiKeluar = $this->getDataHarianBaru($year , $month , $lastDay , $gardu_induk , $item_id , 4 , 'expor');
							$psgi = $this->getDataHarian($year , $month , $lastDay , $gardu_induk , $item_id , 5 , 'expor');
							
							$b = -1;
							$hasilHari = 0;
							for($a=1;$a<=$lastDay;$a++)
							{
									$b++;
									$susutTransmisi = $energiMasuk[$b] - $energiKeluar[$b] - $psgi[$b];
									if($energiMasuk[$b] == 0)
									{
										$persenHari = 0;
									}else{
										$persenHari = $susutTransmisi / $energiMasuk[$b] * 100;
									}	
										
								$hasilHari = $hasilHari + $persenHari;
							}
							$hasilBulan = $hasilBulan + $hasilHari;
					}
					$hasil[] = $hasilBulan;	
				}
				return $hasil;	
	}*/

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('neraca_energi_id',$this->neraca_energi_id);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('expor',$this->expor,true);
		$criteria->compare('impor',$this->impor,true);
		$criteria->compare('faktor_expor',$this->faktor_expor);
		$criteria->compare('faktor_impor',$this->faktor_impor);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NeracaEnergiDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
