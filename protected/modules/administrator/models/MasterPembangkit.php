<?php

/**
 * This is the model class for table "master_pembangkit".
 *
 * The followings are the available columns in table 'master_pembangkit':
 * @property integer $id
 * @property integer $jenis_pembangkit_id
 * @property string $pembangkit
 * @property integer $posted_by
 * @property string $date_post
 *
 * The followings are the available model relations:
 * @property MasterJenisPembangkit $jenisPembangkit
 */
class MasterPembangkit extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'master_pembangkit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(' posted_by', 'numerical', 'integerOnly'=>true),
			array('jenis_pembangkit_id', 'length', 'max'=>2),
			array('pembangkit', 'length', 'max'=>100),
			array('date_post', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, jenis_pembangkit_id, pembangkit, posted_by, date_post', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jenisPembangkit' => array(self::BELONGS_TO, 'MasterJenisPembangkit', 'jenis_pembangkit_id'),
			'user' => array(self::BELONGS_TO , 'User' , 'posted_by'),
		);
	}

	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->date_post = date("Y-m-d");
			$this->posted_by = Yii::app()->session['tokenId'];
			return true;
		}
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'jenis_pembangkit_id' => 'Jenis Pembangkit',
			'pembangkit' => 'Pembangkit',
			'posted_by' => 'Posted By',
			'date_post' => 'Date Post',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('jenis_pembangkit_id',$this->jenis_pembangkit_id);
		$criteria->compare('pembangkit',$this->pembangkit,true);
		$criteria->compare('posted_by',$this->posted_by);
		$criteria->compare('date_post',$this->date_post,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterPembangkit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
