<?php

/**
 * This is the model class for table "berita".
 *
 * The followings are the available columns in table 'berita':
 * @property integer $id
 * @property string $judul
 * @property string $deskripsi
 * @property string $sumber
 * @property integer $posted_by
 * @property string $date_post
 * @property string $seo
 * @property string $gambar
 */
class Berita extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'berita';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('posted_by', 'numerical', 'integerOnly'=>true),
			//array('judul, gambar', 'length', 'max'=>1000),
			array('sumber', 'length', 'max'=>500),
			array('seo', 'length', 'max'=>1100),
			array('deskripsi, date_post', 'safe'),
			array('judul' , 'length' , 'max' => 50),
			array('judul' , 'required'),
			array('sumber' , 'url' , 'defaultScheme' => 'http'),
			array('gambar' , 'file' , 'types' => 'jpg,png,gif' , 'allowEmpty' => true),
			array('judul' ,'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, judul, deskripsi, sumber, posted_by, date_post, seo, gambar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'user' => array(self::BELONGS_TO , 'User' , 'posted_by')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'judul' => 'Judul',
			'deskripsi' => 'Deskripsi',
			'sumber' => 'Sumber',
			'posted_by' => 'Posted By',
			'date_post' => 'Date Post',
			'seo' => 'Seo',
			'gambar' => 'Gambar',
		);
	}


	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->seo = ar::clean($this->judul);
			$this->date_post = date("Y-m-d H:i:s");
			//$this->posted_by = Yii::app()->session['tokenId'];
			return true;
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = array('user');
		/*$criteria->compare('id',$this->id);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('deskripsi',$this->deskripsi,true);
		$criteria->compare('sumber',$this->sumber,true);
		$criteria->compare('posted_by',$this->posted_by);
		$criteria->compare('date_post',$this->date_post,true);
		$criteria->compare('seo',$this->seo,true);
		$criteria->compare('gambar',$this->gambar,true);
		*/

		$criteria->addSearchCondition('judul' , $this->judul , true , 'or');
		$criteria->addSearchCondition('sumber' , $this->judul , true , 'or');
		$criteria->addSearchCondition('user.nama' , $this->judul , true , 'or');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Berita the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
