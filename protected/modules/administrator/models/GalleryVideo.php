<?php

/**
 * This is the model class for table "gallery_video".
 *
 * The followings are the available columns in table 'gallery_video':
 * @property integer $id
 * @property string $nama_album
 * @property string $date_post
 * @property integer $posted_by
 * @property string $seo
 */
class GalleryVideo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gallery_video';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('posted_by', 'numerical', 'integerOnly'=>true),
			array('nama_album, seo', 'length', 'max'=>1000),
			array('nama_album' ,'required'),
			array('nama_album' , 'unique'),
			array('date_post', 'safe'),
			array('nama_album' ,'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama_album, date_post, posted_by, seo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				//'user' => array(self::BELONGS_TO , 'User' , 'posted_by')
		);
	}

	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->seo = ar::clean($this->nama_album);
			$this->date_post = date("Y-m-d H:i:s");
			$this->posted_by = Yii::app()->session['tokenId'];
			return true;
		}
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_album' => 'Nama Album',
			'date_post' => 'Date Post',
			'posted_by' => 'Posted By',
			'seo' => 'Seo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = array('user');
		/*$criteria->compare('id',$this->id);
		$criteria->compare('nama_album',$this->nama_album,true);
		$criteria->compare('date_post',$this->date_post,true);
		$criteria->compare('posted_by',$this->posted_by);
		$criteria->compare('seo',$this->seo,true);
		*/
		$criteria->addSearchCondition('nama_album' , $this->nama_album , true , 'or');
		$criteria->addSearchCondition('user.nama' , $this->nama_album , true , 'or');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GalleryVideo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
