<?php

/**
 * This is the model class for table "trans_kinerja_operasi_detail".
 *
 * The followings are the available columns in table 'trans_kinerja_operasi_detail':
 * @property integer $id
 * @property integer $trans_kinerja_operasi_id
 * @property string $bulan
 * @property string $real
 */
class TransKinerjaOperasiDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trans_kinerja_operasi_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('trans_kinerja_operasi_id', 'numerical', 'integerOnly'=>true),
			array('bulan, real', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, trans_kinerja_operasi_id, bulan, real', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'trans_kinerja_operasi_id' => 'Trans Kinerja Penyaluran',
			'bulan' => 'Bulan',
			'real' => 'Real',
		);
	}

	public function getDataBulananValue($year , $kinerja , $field)
	{
		$db = Yii::app()->db;
		for($a=1;$a<=12;$a++)
		{
			$qw = "SELECT `$field` FROM trans_kinerja_operasi_detail INNER JOIN trans_kinerja_operasi
					ON trans_kinerja_operasi_detail.trans_kinerja_operasi_id = trans_kinerja_operasi.id
				 WHERE tahun = '$year' AND kinerja_operasi_id = '$kinerja' AND bulan ='$a'";
			$exec = $db->createCommand($qw)->queryScalar();
			//$hasil[] = array('y' => floatval($exec['jumlah_max']) , 'jam' => $exec['jam_max'] ,'tanggal' => ar::formatWaktu($exec['tanggal'] , "long" ,"") ); 
			$hasil[] = floatval($exec);
		}

		return $hasil;
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('trans_kinerja_operasi_id',$this->trans_kinerja_operasi_id);
		$criteria->compare('bulan',$this->bulan,true);
		$criteria->compare('real',$this->real,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TransKinerjaOperasiDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
