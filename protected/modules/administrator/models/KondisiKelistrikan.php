<?php

/**
 * This is the model class for table "kondisi_kelistrikan".
 *
 * The followings are the available columns in table 'kondisi_kelistrikan':
 * @property integer $id
 * @property string $tanggal_full
 * @property string $kondisi
 * @property integer $posted_by
 * @property string $date_post
 */
class KondisiKelistrikan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kondisi_kelistrikan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('posted_by', 'numerical', 'integerOnly'=>true),
			array('kondisi', 'length', 'max'=>20),
			array('tanggal_full, date_post', 'safe'),
			array('kondisi,tanggal_full','required'),
			array('tanggal_full' , 'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tanggal_full, kondisi, posted_by, date_post', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//	'user' => array(self::BELONGS_TO, 'User','posted_by')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal_full' => 'Tanggal Full',
			'kondisi' => 'Kondisi',
			'posted_by' => 'Posted By',
			'date_post' => 'Date Post',
		);
	}


	//

	public function beforeSave()
	{
		if(parent::beforeSave())
		{
			
			$this->date_post = date("Y-m-d H:i:s");
			$this->posted_by = Yii::app()->session['tokenId'];


			$replace = str_replace("/", "-", $this->tanggal_full);
			$this->tanggal_full = date("Y-m-d" , strtotime($replace));
			return true;
		}
	}


	public function getDataHarian($param)
	{
				$tanggalAkhir = $param;
				for($a=1;$a<=$tanggalAkhir;$a++)
				{
					$tgl[] = $a;
				}
				return $tgl;
	}

	public function getDataHarianValue($bol = "" , $month , $year , $last)
	{
		$db = Yii::app()->db->createCommand();
		$tanggalAkhir = $last;
		
		for($a=1;$a<=$tanggalAkhir;$a++)
		{
			$sql2 = $db->select('count(id) AS jml , kondisi')
			->from('kondisi_kelistrikan')
			->where("month(tanggal_full)=:month AND year(tanggal_full)=:year AND day(tanggal_full)=:day" ,
				 array(':month' => $month , ':year' => $year,':day' => $a)
			)
			->queryRow();	

			if(!empty($sql2['kondisi']))
			{
				if($sql2['kondisi'] == 'Normal')
				{
					$warna = 'green';
					$y = 2;
				}elseif($sql2['kondisi'] == 'Siaga'){
					$warna = 'yellow';
					$y = 1;
				}else{
					$warna = 'red';
					$y = 0;
				}

				$kondisi = $sql2['kondisi'];

			}else{
					$warna = 'black';
					$kondisi = 'Data Belum Di Input';	
					$y=null;
			}

			$init = intval($sql2['jml']);
			$hasil[] = array('y' => $y,'color' => $warna , 'kondisi' => $kondisi); 

		}

		return $hasil;
	}

	public function getDataBulanan($year)
	{
		for($a=1;$a<=12;$a++)
		{
			$month  = substr(date("F", mktime(0, 0, 0, $a, 10)) , 0 , 3);
			$init = $month;
			$hasil[] = $init;
		}
		
		return $hasil;
	}

	public function getDataBulananValue($year , $bol = "")
	{
		$db = Yii::app()->db->createCommand();

		

		for($a=1;$a<=12;$a++)
		{
			$sql = $db->select('COUNT(id) as jml , kondisi')
			->from('kondisi_kelistrikan')
			->where('MONTH(tanggal_full)=:month AND YEAR(tanggal_full)=:year AND kondisi=:kondisi' 
					, array(':year' => $year , ':month' => $a , 'kondisi' => $bol)
			)
			->queryRow();
			
					

			$init = intval($sql['jml']);
			$hasil[] = array('y' => $init);
					
		}
		
		return $hasil;
	}

	public function getDataTahunan($awal,$akhir)
	{
		for($a=$awal;$a<=$akhir;$a++)
		{
			$hasil[] = $a;
		}
		
		
		return $hasil;
	}


	public function getDataTahunanValue($awal,$akhir , $bol)
	{
		$db = Yii::app()->db->createCommand();

		

		for($a=$awal;$a<=$akhir;$a++)
		{
			$sql = $db->select('COUNT(id) as jml , kondisi')
			->from('kondisi_kelistrikan')
			->where('YEAR(tanggal_full)=:year AND kondisi=:kondisi' 
					, array(':year' => $a  , 'kondisi' => $bol)
			)
			->queryRow();
			
					

			$init = intval($sql['jml']);
			$hasil[] = array('y' => $init);
					
		}
		
		return $hasil;
		
		
	
	}

	//

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tanggal_full',$this->tanggal_full,true);
		$criteria->compare('kondisi',$this->kondisi,true);
		$criteria->compare('posted_by',$this->posted_by);
		$criteria->compare('date_post',$this->date_post,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return KondisiKelistrikan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
