<div id = "render1"></div>
<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $year ,'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listBulan,
											 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 )
										  ),
											'chart' => array(
												'renderTo' => 'render1',
											   
											),
										  'yAxis' => array(
											'min' => 0 ,
											 'title' => array('text' => 'Beban (MW)'),
											 
										  ),
										   'colors'=>array('red'),
										   'tooltip' => array(
												'headerFormat' => 'Bulan {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/> Tanggal:{point.tanggal}<br/>',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
											  ),
											'plotOptions' => array(
												'column' => array(
													'dataLabels' => array('enabled' => true , 
															'color' => 'black',
															'crop' => false,
															'overflow' => 'none'
													),
													
													
												)
											 ),
										  'series' => array(
											 array('name' => 'Beban Puncak', 'data' => $valueBulan , 'type' => 'column'),
											
											
										  )
									   )
									));

							?>