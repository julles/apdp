<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>



<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
			
            	<div id="hearts">
      				
					<?php
						//echo CHtml::link("" , '#' , array('id' => 'putars' , 'onclick' => '' , 'tittle' => 'Minimize and Maximize'));
					?>
                </div>
                <div class="judul" align="center">
                    <h2><b>Beban Puncak</b></h2>
                </div>
            	<div id="textnya">
                	<div id="portlet">
                    	<p class="portlet-title"><u>Beban Perjam</u></p>
                    </div>
                	<div id="beban-puncak">
                	<?php
                	echo CHtml::beginForm();
                	?>
                        <label style="padding-right:0px;">Tanggal</label>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(

                                'id' => 'tanggalJam',
                                'name'=>'tanggalJam',
                                // additional javascript options for the date picker plugin
                                'options' => array(
                                'showAnim' => 'fold',
                                'dateFormat'=>'yy-mm-dd',
                                 'changeMonth'=>true,
                                'changeYear'=>true,
                                'yearRange'=>'1900:2099',
                                ),
                                'htmlOptions' => array(
                                'style' => 'height:26px;width:120px;',
                                 'class' => 'form-control'
                                ),
                            ));

                         ?>

                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn_jam" style = "height:27px;margin-top:1px;" >Tampil</button>' , 
                        		 array('beban/ajaxJam'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perjam',
                        		 	'data' => array('tanggalJam' => 'js:tanggalJam.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading1").show();
									}',
									'complete' => 'function(){
										$("#loading1").hide();
									}',
                        		 )
								 

                        	);
                         ?>
                        <div id = 'loading1' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                       
                        <div  class = 'perjam' class="export2" id = 'perjam'>
                            <!-- Line Chart -->
							<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => ar::formatWaktu($date , "long" ,"") ,'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listJam,
											  'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 )
										  ),
										  'yAxis' => array(
											'min' => 0,
											 'title' => array('text' => 'Beban (MW)')
										  ),
										  'plotOptions' => array(
                                                    'series' => array(
                                                            'marker' => array(
                                                                'enabled' => false,

                                                            )
                                                    )
                                                   ),
										    'colors'=>array('blue' , 'pink','red'),
										  'series' => array(
											
											 array('name' => 'Prakiraan Beban', 'data' => $prakiraan_beban, 'lineWidth' => 3),
											 array('name' => 'Prakiraan Daya Mampu', 'data' => $prakiraan_daya_mampu, 'lineWidth' => 3),
                                              array('name' => 'Realisasi Beban', 'data' => $beban_perjam , 'lineWidth' => 3),
											
										  )
									   )
									));

							?>
                            <!-- End Line Chart -->
                    	
						</div>
                    	<?php

                    	echo CHtml::endForm();
                    	?>
                	</div>
                    
                </div>
				
				<div id="textnya">
                	<div id="portlet">
                    	<p class="portlet-title"><u>Beban Puncak Perhari</u></p>
                    </div>
                	<div id="beban-puncak">
                	<?php
                	echo CHtml::beginForm();
                	?>
                        <label style="padding-right:0px;">Bulan & Tahun</label>
						  <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'bulanHarian'>
                            <option value="">Select</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                         <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'tahunHarian'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
						 

                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn_jam" style = "height:27px;margin-top:1px;" >Tampil</button>' , 
                        		 array('beban/ajaxHarian'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perharian',
                        		 	'data' => array('bulan' => 'js:bulanHarian.value' , 'tahun' => 'js:tahunHarian.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading2").show();
									}',
									'complete' => 'function(){
										$("#loading2").hide();
									}',
                        		 )
								 

                        	);
                         ?>
                        <div id = 'loading2' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                       
                        <div  class = 'perharian' class="export2" id = 'perharian'>
                            <!-- Line Chart -->
							<?php
									$labelBulanTahun = explode(" ",ar::formatWaktu($date , "long" ,""));
									$labelBulanTahunFix = $labelBulanTahun[1]." - ".$labelBulanTahun[2];
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelBulanTahunFix ,'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listHari,
											  'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 )
										  ),
										  'yAxis' => array(
											 'min' => 0,
											 'title' => array('text' => 'Beban (MW)')
										  ),
										   'colors'=>array('green'),
										   'tooltip' => array(
												'headerFormat' => 'Tanggal {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/>',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
											  ),
										   
										  'series' => array(
											 array('name' => 'Beban Puncak', 'data' => $valueHari , 'type' => 'column'),
											
											
										  )
									   )
									));

							?>
                            <!-- End Line Chart -->
                    	
						</div>
                    	<?php

                    	echo CHtml::endForm();
                    	?>
                	</div>
                    
                </div>
				<div id="textnya">
                	<div id="portlet">
                    	<p class="portlet-title"><u>Beban Puncak Perbulan</u></p>
                    </div>
                	<div id="beban-puncak">
                	<?php
                	echo CHtml::beginForm();
                	?>
                        <label style="padding-right:0px;">Tahun</label>
						 
                         <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'tahunBulanan'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
						 

                        <?php
                        	echo CHtml::ajaxLink('<button style = "height:27px;margin-top:1px;" id = "btn_jam" >Tampil</button>' , 
                        		 array('beban/ajaxBulanan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perbulanan',
                        		 	'data' => array('tahun' => 'js:tahunBulanan.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading3").show();
									}',
									'complete' => 'function(){
										$("#loading3").hide();
									}',
                        		 )
								 

                        	);
                         ?>
                        <div id = 'loading3' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                       
                        <div  class = 'perbulanan' class="export2" id = 'perbulanan'>
                            <!-- Line Chart -->
							<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $year ,'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listBulan,
											 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 )
										  ),
										  'yAxis' => array(
											 'title' => array('text' => 'Beban (MW)')
										  ),
										   'colors'=>array('red'),
										   'tooltip' => array(
												'headerFormat' => 'Bulan {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/> Tanggal: {point.tanggal}',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
											  ),
											  'plotOptions' => array(
												'column' => array(
													 'dataLabels' => array('enabled' => true , 
															
															'crop' => false,
                                                            'color' => 'black',
															'overflow' => 'none',
															
															
													),
													
												)
											 ),
										   
										   
										  'series' => array(
											 array('name' => 'Beban Puncak', 'data' => $valueBulan , 'type' => 'column'),
											
											
										  )
									   )
									));

							?>
                            <!-- End Line Chart -->
                    	
						</div>
                    	<?php

                    	echo CHtml::endForm();
                    	?>
                	</div>
                    
                </div>
				<div id="textnya">
                	<div id="portlet">
                    	<p class="portlet-title"><u>Beban Puncak Pertahun</u></p>
                    </div>
                	<div id="beban-puncak">
                	<?php
                	echo CHtml::beginForm();
                	?>
                         <label style="padding-right:0px;">Tahun</label>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'awal'>
                            <option value="">Select</option>
                            <?php
	                        	$year = date('Y');
	                        	for($a=2010;$a<=$year;$a++)
	                        	{
	                        		echo "<option value = '$a'>$a</option>";
	                        	}
                        	?>
                        </select>
                        <label style="padding-right:0px;">s.d</label>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'akhir'>
                        	<option value="">Select</option>
                            <?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>
                        </select>
						 

                        <?php
                        	echo CHtml::ajaxLink('<button style = "height:27px;margin-top:1px;" id = "btn_jam" >Tampil</button>' , 
                        		 array('beban/ajaxTahunan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#pertahunan',
                        		 	'data' => array('awal' => 'js:awal.value' , 'akhir' => 'js:akhir.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading4").show();
									}',
									'complete' => 'function(){
										$("#loading4").hide();
									}',
                        		 )
								 

                        	);
                         ?>
                        <div id = 'loading4' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                       
                        <div  class = 'pertahunan' class="export2" id = 'pertahunan'>
                            <!-- Line Chart -->
							<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelTahunan ,'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listTahun,
											 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 )
										  ),
										  'yAxis' => array(
												'min' => 0,
											 'title' => array('text' => 'Beban (MW)','style' => 'font-size:16px;')
										  ),
										   'colors'=>array('blue'),
										   'tooltip' => array(
												'headerFormat' => 'Tahun {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/>Tanggal:{point.tanggal}',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
											  ),
											  
											 'plotOptions' => array(
												'column' => array(
													 'dataLabels' => array('enabled' => true , 
															'color' => 'black',
															'crop' => false,
															'overflow' => 'none',
															
															
													),
													
												)
											 ),
										   
										  'series' => array(
											 array('name' => 'Beban Puncak', 'data' => $valueTahun , 'type' => 'column'),
											
											
										  )
									   )
									));

							?>
                            <!-- End Line Chart -->
                    	
						</div>
                    	<?php

                    	echo CHtml::endForm();
                    	?>
                	</div>
                    
                </div>
				
				
				
            </div>
            <!-- End Pertama -->
        </div>

        <script>
	$(function() {
  		var special = ['reveal', 'top', 'boring', 'perspective', 'extra-pop'];
  		// Toggle Nav on Click
  		$('#hearts a').click(function() {
			var delay_ = 300;
			checkWidth = $('#leftContent').css( "width");
			var widthnya="594";
			var widthnyaChart="594";
			heightnya = "400";
			widthbaru = "594"
			widthnya5 = "267";
			widthnya6 = "565";
			LineCharts = "564";
			if( checkWidth < '186'){
			  $('#leftContent').animate( {"width":"186", "height":"100" + "%"}, delay_, "linear");
			  $('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			  $('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			  $('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			  $('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
			}else if(checkWidth > "0"){
			  $('#leftContent').animate({ "width":"0", "height":"0"}, delay_, "linear" );	
				widthnya = "780";
				widthnyaChart = "1000";
				heightnya = "500";
				widthnya5 = "325";
				widthnya6 = "750";
				LineCharts = "750";
			}
			$('#rightContent').animate({ "width": widthnya}, delay_, "linear" );
			$('.judul').animate({ "width": widthnya}, delay_, "linear" );
			$('.containerKanan').animate({ "width": widthnya2}, delay_, "linear" );
			$('#perjam').animate({ "width": widthnya2}, delay_, "linear" );
			$('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			$('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			$('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			$('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
  		});
	});
</script>