<div id = "bulanan"></div>
<?php
		
		$this->Widget('ext.highcharts.HighchartsWidget', array(
		   'options'=>array(
			  'title' => array('text' => $labelBulanTahunFixs,'style' => 'font-size:16px;'),
			  'credits' => false , 
			   'chart' => array(
					'renderTo' => 'bulanan',
				   
				),
			  'xAxis' => array(
				 'categories' => $listHari,
				 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 )
			  ),
			  'yAxis' => array(
				'min' => 0 ,
				 'title' => array('text' => 'Beban (MW)')
			  ),
			   'colors'=>array('green'),
			   'tooltip' => array(
					'headerFormat' => 'Tanggal {point.key}<br/>',
					'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/>',
					'footerFormat'=> '</table>',
					'shared' => true,
					'useHtml' => true
				  ),
			   
			  'series' => array(
				 array('name' => 'Beban Puncak', 'data' => $valueHari , 'type' => 'column'),
				
				
			  )
		   )
		));

?>