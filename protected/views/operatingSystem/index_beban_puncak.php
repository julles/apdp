
<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>


<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
			
            	<div id="hearts">
      				
					<?php
						//echo CHtml::link("" , '#' , array('id' => 'putars' , 'onclick' => '' , 'tittle' => 'Minimize and Maximize'));
					?>
                </div>
                <div class="judul" align="center">
                    <h2><b>Beban Puncak</b></h2>
                </div>
            	<div id="textnya">
                	<div id="portlet">
                    	<p class="portlet-title"><u>Beban Puncak Perjam</u></p>
                    </div>
                	<div id="beban-puncak">
                	<?php
                	echo CHtml::beginForm();
                	?>
                        <label style="padding-right:0px;">Tanggal</label>
                        <?php 
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(

                                'id' => 'tanggalJam',
                                'name'=>'tanggalJam',
                                // additional javascript options for the date picker plugin
                                'options' => array(
                                'showAnim' => 'fold',
                                'dateFormat'=>'yy-mm-dd',
                                 'changeMonth'=>true,
                                'changeYear'=>true,
                                'yearRange'=>'1900:2099',
                                ),
                                'htmlOptions' => array(
                                'style' => 'height:26px;',
                                 'class' => 'form-control'
                                ),
                            ));

                         ?>

                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn_jam" class = "btn btn-primary">Tampil</button>' , 
                        		 array('operatingSystem/bebanPuncakDetailJam'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perjam',
                        		 	'data' => array('tanggalJam' => 'js:tanggalJam.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading1").show();
									}',
									'complete' => 'function(){
										$("#loading1").hide();
									}',
                        		 )
								 

                        	);
                         ?>
                        <div id = 'loading1' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                       
                        <div  class = 'perjam' class="export2" id = 'perjam'>
                            <!-- Line Chart -->
							<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),
							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $hariIni),
									      'width' => '100%',
										  //'htmlOptions' => array('id' => 'wawa'),
										  'chart' => array(
												//'width' => ''
										  ),
									      'xAxis' => array(
									         'categories' => $tampil_jam,
									          
									          'min' => 0 ,
									          'max' => 10
									      ),
									      
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'yAxis' => array(
									         'title' => array('text' => 'Daya (Mw)')
									      ),


									     
									      'series' => array(
									      	
									         array('name' => 'Mw', 'data' => $hasilJam),
									         
									         
									      )
									   )
									));

							?>
                            <!-- End Line Chart -->
                    	
						</div>
                    	<?php

                    	echo CHtml::endForm();
                    	?>
                	</div>
                    <div id="portlet">
                    	<p class="portlet-title"><u>Beban Puncak Harian</u></p>
                    </div>
                	<div id="beban-puncak">
                        <label style="padding-right:0px;">Bulan & Tahun</label>
                        <select class="form-control" style="width:100px;" id = 'bulanHarian'>
                            <option value="">Select</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                        <select class="form-control" style="width:100px;" id = 'tahunHarian'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2000;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn_harian" class = "btn btn-primary">Tampil</button>' , 
                        		 array('operatingSystem/bebanPuncakDetailHarian'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perharian',
                        		 	'data' => array('bulan' => 'js:bulanHarian.value' , 'tahun' => 'js:tahunHarian.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading2").show();
									}',
									'complete' => 'function(){
										$("#loading2").hide();
									}',
                        		 )

                        	);
                         ?>
                        <div id = 'loading2' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        <div id = 'harian'></div>
                        <div class="export2" id = 'perharian'>
                    		<!-- Bar Chart -->

                            <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $hariIniHarian),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'harian',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tgl,

									          'min' => 0 ,
									          'max' => 10
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Daya (Mw)')
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Tanggal {point.key}<br/>',
											'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.reza}',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
									      'series' => array(
									      	
									         array('name' => 'Mw', 'data' => $modelHarian  ,'type' => 'column'),
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									));

							?>
                            <!-- End Bar Chart -->
                    	</div>
                	</div>
                    <div id="portlet">
                    	<p class="portlet-title"><u>Beban Puncak Bulanan</u></p>
                    </div>
                    <?php
                    	echo CHtml::beginForm();
                    ?>
                    <div id="beban-puncak">
                        <label style="padding-right:0px;">Tahun</label>
                        <select class="form-control" style="width:100px;" id = 'tahunBulanan'>
                            <option value="">Select</option>
                           <?php
                        	$year = date('Y');
                        	for($a=2000;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>
                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn_bulanan" class = "btn btn-primary">Tampil</button>' , 
                        		 array('operatingSystem/bebanPuncakDetailBulanan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perbulanan',
                        		 	'data' => array('tahun' => 'js:tahunBulanan.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading3").show();
									}',
									'complete' => 'function(){
										$("#loading3").hide();
									}',
                        		 )

                        	);
                         ?>

                        <?php
                        	echo CHtml::endForm();
                        ?>
                        <div id = 'loading3' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        
                        <div id = 'bulanan'></div>
                        <div class="export2" id = 'perbulanan'>
                    		<!-- Bar Chart -->
                           	<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $titleBulan),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'bulanan',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $bulan,

									          'min' => 0 ,
									          'max' => 3
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Daya (Mw)')
									      ),
									      
									     
									     'tooltip' => array(
											'headerFormat' => '{point.key}<br/>',
											'pointFormat' => '{series.name}:{point.y}<br/>Tanggal: {point.tanggal}<br/>Jam: {point.jam}',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
									      'series' => array(
									      	
									         array('name' => 'Mw', 'data' => $hasilBulan  ,'type' => 'column'),
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									));
									
							?>
                           
                            <!-- End Bar Chart -->
                    	</div>
                	</div>
					<div id="portlet">
                    	<p class="portlet-title"><u>Beban Puncak Tahunan</u></p>
                    </div>
                    <div id="beban-puncak">
                    <?php
                    echo CHtml::beginForm();
                    ?>
                        <label style="padding-right:0px;">Tahun</label>
                        <select class="form-control" style="width:100px;" id = 'awal'>
                            <option value="">Select</option>
                            <?php
	                        	$year = date('Y');
	                        	for($a=2000;$a<=$year;$a++)
	                        	{
	                        		echo "<option value = '$a'>$a</option>";
	                        	}
                        	?>
                        </select>
                        <label style="padding-right:0px;">s.d</label>
                        <select class="form-control" style="width:100px;" id = 'akhir'>
                        	<option value="">Select</option>
                            <?php
                        	$year = date('Y');
                        	for($a=2000;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>
                        </select>
                      <?php
                        	echo CHtml::ajaxLink('<button id = "btn_tahunan" class = "btn btn-primary">Tampil</button>' , 
                        		 array('operatingSystem/bebanPuncakDetailTahunan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#pertahunan',
                        		 	'data' => array('awal' => 'js:awal.value' , 'akhir' => 'js:akhir.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading4").show();
									}',
									'complete' => 'function(){
										$("#loading4").hide();
									}',
                        		 )

                        	);
                         ?>
                        
                        <div id = 'loading4' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        
                        <div id = 'tahunan'></div>
                        <div class="export2" id = 'pertahunan'>
                    		<!-- Bar Chart -->
                            <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => 'Pertahun'),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'tahunan',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tahun,

									          'min' => 0 ,
									          'max' => 3
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Daya (Mw)')
									      ),
									      'tooltip' => array(
											'headerFormat' => '{point.key}<br/>',
											'pointFormat' => '{series.name}:{point.y}<br/>Tanggal: {point.tanggal}<br/>Jam: {point.jam}',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
									      
									     
									     /* 'tooltip' => array(
											'headerFormat' => '<span style="font-size:10px">{point.key}</span><table>',
											'pointFormat' => '<tr><td style="color:{series.color};padding:0">{series.name}: </td></tr><tr><td style="padding:0"><b>{point.y:.1f} mw</b></td></tr><tr><td style="padding:0"><b>Jam :</b></td><td style="padding:0"><b>08.00</b></td></tr>',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),*/
									      'series' => array(
									      	
									         array('name' => 'Mw', 'data' => $hasilTahun  ,'type' => 'column'),
									       
									        
									         
									      )
									   )
									));
									
							?>
                           
                            <!-- End Bar Chart -->
                    	</div>
                    	<?php
                    		echo CHtml::endForm();
                    	?>
                	</div>
                </div>
            </div>
            <!-- End Pertama -->
        </div>

        <script>
	$(function() {
  		var special = ['reveal', 'top', 'boring', 'perspective', 'extra-pop'];
  		// Toggle Nav on Click
  		$('#hearts a').click(function() {
			var delay_ = 300;
			checkWidth = $('#leftContent').css( "width");
			var widthnya="594";
			var widthnyaChart="594";
			heightnya = "400";
			widthbaru = "594"
			widthnya5 = "267";
			widthnya6 = "565";
			LineCharts = "564";
			if( checkWidth < '186'){
			  $('#leftContent').animate( {"width":"186", "height":"100" + "%"}, delay_, "linear");
			  $('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			  $('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			  $('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			  $('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
			}else if(checkWidth > "0"){
			  $('#leftContent').animate({ "width":"0", "height":"0"}, delay_, "linear" );	
				widthnya = "780";
				widthnyaChart = "1000";
				heightnya = "500";
				widthnya5 = "325";
				widthnya6 = "750";
				LineCharts = "750";
			}
			$('#rightContent').animate({ "width": widthnya}, delay_, "linear" );
			$('.judul').animate({ "width": widthnya}, delay_, "linear" );
			$('.containerKanan').animate({ "width": widthnya2}, delay_, "linear" );
			$('#perjam').animate({ "width": widthnya2}, delay_, "linear" );
			$('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			$('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			$('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			$('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
  		});
	});
</script>