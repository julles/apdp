

<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script>
	$(document).ready(
		function()
		{
			$("#putar").click(
				function()
				{
					$("#btn1").click();
					$("#btn2").click();
					$("#btn3").click();
				//	$("#btn4").click();
					
				}
			);
		}
	);
</script>

<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
            	<div id="hearts">
      				<a id="putar"href="#" onclick="rotate()" title="Minimize and Maximize"></a>
                </div>
                <div class="judul" align="center">
                    <h2><b>Bahan Bakar</b></h2>
                </div>
            	<div id="textnya">
                	
            		<div id="pembebanan-trafo-top" style="height: 100px;">
                        <div class="trafo-panel-top" style = "width:237px;">
                            <div class="trafo-panel-input-top">
                                <label style="padding-right:20px;">Jenis Pembangkit</label>
                                <!--select class="form-control">
                                    <option value="">All</option>
                                    <option value="1">Gardu Induk 1</option>
                                    <option value="2">Gardu Induk 2</option>
                                    <option value="3">Gardu Induk 3</option>
                                    <option value="4">Gardu Induk 4</option>
                                    <option value="5">Gardu Induk 5</option>
                                </select-->
                                <?php
                                	echo CHtml::dropDownList('JenisPembangkit' , '' , CHtml::listData(MasterJenisPembangkit::model()->findAll(array('order' => 'id DESC')) , 'id' , 'jenis_pembangkit') , 
                                		array(
                                			'class' => '',
                                			
                                			'id' => 'jenis_pembangkit_id',
                                			'ajax' => array(
                                				'update' => '#pembangkit_id',
                                				'type' => 'POST',
                                				'url' => Yii::app()->createUrl("/operatingSystem/getPembangkit"),
                                				'data' => array(
                                					'jenis_pembangkit_id' => 'js:this.value'
                                				)

                                			)
                                		)
                                	);
                                ?>
                                
                            </div>
                        </div>
                        <div class="trafo-panel-top">
                            <div class="trafo-panel-input-top" style = "width:237px;">
                                <label style="padding-right:10px;">Pembangkit</label>
                                <?php
                                	echo CHtml::dropDownList('pembangkit' , '' , $comboPembangkit , array('class' => '' , 'id' =>'pembangkit_id'));
                                ?>
                            </div>
                        </div>
						
						<div class="trafo-panel-top">
                            <div class="trafo-panel-input-top" style = "width:237px;">
                                <label style="padding-right:10px;">Unit Pembangkit</label>
                                <?php
                                	echo CHtml::dropDownList('pembangkit' , '' , array() , array('class' => '' , 'id' =>'pembangkit_id'));
                                ?>
                            </div>
                        </div>
						<div class="trafo-panel-top">
                            <div class="trafo-panel-input-top" style = "width:237px;">
                                <label style="padding-right:10px;">Pilihan Nilai</label>
                              
								 <?php
                                	echo CHtml::dropDownList('pembangkit' , '' , array(1 => 'stok' , 2 => 'pemakaian') , array('class' => '' , 'id' =>'pembangkit_id'));
                                ?>
                            </div>
                        </div>
						
						
                        <!--div class="trafo-panel-top" style="text-align: left;">
                            <div class="trafo-panel-input-top">
                                <input type="submit" value="Tampil" class="btn btn-primary">
                            </div>
                        </div-->
                    </div>


                    <div id="portlet">
                    	<p class="portlet-title"><u>Bahan Bakar Harian</u></p>
                    </div>
                	<div id="beban-puncak">
                        <label style="padding-right:0px;">Bulan & Tahun</label>
                        <select class="form-control" style="width:100px;" id = 'bulanHarian'>
                            <option value="">Select</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                        <select class="form-control" style="width:100px;" id = 'tahunHarian'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2000;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn1" class = "btn btn-primary">Tampil</button>' , 
                        		 array('operatingSystem/bahanBakarDetailHarian'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perharian',
                        		 	'data' => array('bulan' => 'js:bulanHarian.value' , 'tahun' => 'js:tahunHarian.value' , 'jenis_pembangkit_id' => 'js:jenis_pembangkit_id.value' , 'pembangkit_id' => 'js:pembangkit_id.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading2").show();
									}',
									'complete' => 'function(){
										$("#loading2").hide();
									}',
                        		 )

                        	);
                         ?>
                        <div id = 'loading2' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        <div id = 'harian'></div>
                        <div class="export2" id = 'perharian'>
                    		<!-- Bar Chart -->

                            <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $labelBulanTahun),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'harian',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tampilHari,

									          'min' => 0 ,
									          'max' => 2
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Kondisi'),
											 //'labels' => array('format' => '{kondisi}'),
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Tanggal {point.key}<br/>',
											//'pointFormat' => 'Pembangkit : wase<br/>',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
										  //'colors'=>array('green' , 'yellow'),
									      'series' => array(
										     array('name' => 'HSD', 'data' => $hasilHarianHsd  ,'type' => 'column'),
										     array('name' => 'MFO', 'data' => $hasilHarianMfo  ,'type' => 'column'),
										     array('name' => 'Olein', 'data' => $hasilHarianOlein  ,'type' => 'column'),
										     array('name' => 'Batu Bara', 'data' => $hasilHarianBatuBara  ,'type' => 'column'),
											
											
									      )
									   )
									));

							?>
                            <!-- End Bar Chart -->
                    	</div>
                	</div>
                    
                	<div id="portlet">
                    	<p class="portlet-title"><u>Bahan Bakar Bulanan</u></p>
                    </div>
                	<div id="beban-puncak">
                        <label style="padding-right:0px;">Tahun</label>
                       
                        <select class="form-control" style="width:100px;" id = 'tahunBulanan'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2000;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn2" class = "btn btn-primary">Tampil</button>' , 
                        		 array('operatingSystem/bahanBakarDetailBulanan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perbulanan',
                        		 	'data' => array('tahun' => 'js:tahunBulanan.value' , 'jenis_pembangkit_id' => 'js:jenis_pembangkit_id.value' , 'pembangkit_id' => 'js:pembangkit_id.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading3").show();
									}',
									'complete' => 'function(){
										$("#loading3").hide();
									}',
                        		 )

                        	);
                         ?>
                        <div id = 'loading3' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        <div id = 'bulanan'></div>
                        <div class="export3" id = 'perbulanan'>
                    		<!-- Bar Chart -->

                            <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $labelBulan),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'perbulanan',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tampilBulan,

									          'min' => 0 ,
									          'max' => 2
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Kondisi'),
											 //'labels' => array('format' => '{kondisi}'),
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Tanggal {point.key}<br/>',
											//'pointFormat' => 'Pembangkit : wase<br/>',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
										  //'colors'=>array('green' , 'yellow'),
									      'series' => array(
										     array('name' => 'HSD', 'data' => $hasilBulananHsd  ,'type' => 'column'),
										     array('name' => 'MFO', 'data' => $hasilBulananMfo  ,'type' => 'column'),
										     array('name' => 'Olein', 'data' => $hasilBulananOlein  ,'type' => 'column'),
										     array('name' => 'Batu Bara', 'data' => $hasilBulananBatuBara  ,'type' => 'column'),
											
											
									      )
									   )
									));

							?>
                            <!-- End Bar Chart -->
                    	</div>





                	</div>



                	<div id="portlet">
                    	<p class="portlet-title"><u>Bahan Bakar Tahunan</u></p>
                    </div>
                	<div id="beban-puncak">
                        <label style="padding-right:0px;">Tahun</label>
                       
                        <select class="form-control" style="width:100px;" id = 'awal'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2000;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
                        S / D
                         <select class="form-control" style="width:100px;" id = 'akhir'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2000;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn3" class = "btn btn-primary">Tampil</button>' , 
                        		 array('operatingSystem/bahanBakarDetailTahunan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#pertahunan',
                        		 	'data' => array('awal' => 'js:awal.value' , 'akhir' => 'js:akhir.value' , 'jenis_pembangkit_id' => 'js:jenis_pembangkit_id.value' , 'pembangkit_id' => 'js:pembangkit_id.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading4").show();
									}',
									'complete' => 'function(){
										$("#loading4").hide();
									}',
                        		 )

                        	);
                         ?>
                        <div id = 'loading4' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        <div id = 'tahunan'></div>
                        <div class="export4" id = 'pertahunan'>
                    		<!-- Bar Chart -->

                            <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $labelTahun),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'pertahunan',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tampilTahun,

									          'min' => 0 ,
									          'max' => 2
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Kondisi'),
											 //'labels' => array('format' => '{kondisi}'),
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Tanggal {point.key}<br/>',
											//'pointFormat' => 'Pembangkit : wase<br/>',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
										  //'colors'=>array('green' , 'yellow'),
									      'series' => array(
										     array('name' => 'HSD', 'data' => $hasilTahunHsd  ,'type' => 'column'),
										     array('name' => 'MFO', 'data' => $hasilTahunMfo  ,'type' => 'column'),
										     array('name' => 'Olein', 'data' => $hasilTahunOlein  ,'type' => 'column'),
										     array('name' => 'Batu Bara', 'data' => $hasilTahunBatuBara  ,'type' => 'column'),
											
											
									      )
									   )
									));

							?>
                            <!-- End Bar Chart -->
                    	</div>





                	</div>


                </div>
            </div>
            <!-- End Pertama -->
        </div>

        <script>
	$(function() {
  		var special = ['reveal', 'top', 'boring', 'perspective', 'extra-pop'];
  		// Toggle Nav on Click
  		$('#hearts a').click(function() {
			var delay_ = 300;
			checkWidth = $('#leftContent').css( "width");
			var widthnya="594";
			heightnya = "400";
			widthbaru = "594"
			widthnya5 = "267";
			widthnya6 = "565";
			LineCharts = "564";
			if( checkWidth < '186'){
			  $('#leftContent').animate( {"width":"186", "height":"100" + "%"}, delay_, "linear");
			  $('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			  $('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			  $('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			  $('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
			}else if(checkWidth > "0"){
			  $('#leftContent').animate({ "width":"0", "height":"0"}, delay_, "linear" );	
				widthnya = "780";
				heightnya = "500";
				widthnya5 = "325";
				widthnya6 = "750";
				LineCharts = "750";
			}
			$('#rightContent').animate({ "width": widthnya}, delay_, "linear" );
			$('.judul').animate({ "width": widthnya}, delay_, "linear" );
			$('.containerKanan').animate({ "width": widthnya}, delay_, "linear" );
			$('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			$('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			$('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			$('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
  		});
	});
</script>