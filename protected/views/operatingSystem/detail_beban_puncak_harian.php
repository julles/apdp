
 <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $hariIniHarian),
									      'theme' => 'epcGray',
							                    'chart' => array(
							                        'renderTo' => "harian",
							                       
							                    ),
									      
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tgl,

									          'min' => 0 ,
									          'max' => 10
									      ),
									      'yAxis' => array(
									        'title' => array('text' => 'Daya (Mw)')
									      ),

									      	'tooltip' => array(
												'headerFormat' => 'Tanggal {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.reza}',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
									     	 ),
									     
									      'series' => array(
									      	
									         array('name' => 'Mw', 'data' => $modelHarian  ,'type' => 'column'),
									        
									         
									      )
									   )
									));

							?>
