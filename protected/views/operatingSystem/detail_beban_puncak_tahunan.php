  

  <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => 'Pertahun'),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'tahunan',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tahun,

									          'min' => 0 ,
									          'max' => $max
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Daya (Mw)')
									      ),
									      
									     
									     /* 'tooltip' => array(
											'headerFormat' => '<span style="font-size:10px">{point.key}</span><table>',
											'pointFormat' => '<tr><td style="color:{series.color};padding:0">{series.name}: </td></tr><tr><td style="padding:0"><b>{point.y:.1f} mw</b></td></tr><tr><td style="padding:0"><b>Jam :</b></td><td style="padding:0"><b>08.00</b></td></tr>',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),*/
									      'series' => array(
									      	
									         array('name' => 'Mw', 'data' => $hasilTahun  ,'type' => 'column'),
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									));
									
							?>