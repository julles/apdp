  <?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $labelTahun),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'pertahunan',
						                       
						                    ),
									      'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tampilTahun,

									          'min' => 0 ,
									          'max' => 2
									      ),
									      'yAxis' => array(
									         'title' => array('text' => 'Kondisi'),
											 //'labels' => array('format' => '{kondisi}'),
									      ),
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Tanggal {point.key}<br/>',
											//'pointFormat' => 'Pembangkit : wase<br/>',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
										  //'colors'=>array('green' , 'yellow'),
									      'series' => array(
										     array('name' => 'HSD', 'data' => $hasilTahunHsd  ,'type' => 'column'),
										     array('name' => 'MFO', 'data' => $hasilTahunMfo  ,'type' => 'column'),
										     array('name' => 'Olein', 'data' => $hasilTahunOlein  ,'type' => 'column'),
										     array('name' => 'Batu Bara', 'data' => $hasilTahunBatuBara  ,'type' => 'column'),
											
											
									      )
									   )
									));

							?>