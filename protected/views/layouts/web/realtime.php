<?php
 date_default_timezone_set('Asia/Jakarta');
		 $date = date("Y-m-d");
		 $bulan = date('m');
		 $jam = date('H');
		 $year = date('Y');
		 $menit = substr(date("i") , 0, 1);
		 if($jam > 18)
		 {
			 if($menit < 3)
			 {
				$waktu = "$jam.0";
			 }else{
				$waktu = "$jam.3";
			 }
		}else{
			$waktu = "$jam.0";
		}
		 $db = Yii::app()->db;
		 
									
									$prakiraan_hari_ini = $db->createCommand(
										"SELECT  prakiraan_beban  AS nilai,tanggal , jam FROM beban_detail INNER JOIN beban ON beban.id = beban_detail.beban_id WHERE tanggal = '$date' ORDER BY prakiraan_beban DESC LIMIT 1"
									)->queryRow();
									
									$bulan = $db->createCommand("SELECT beban_perjam AS nilai_bulan , tanggal , jam  FROM beban_detail 
										INNER JOIN beban ON beban_detail.beban_id = beban.id 
										WHERE MONTH(tanggal) = $bulan AND YEAR(tanggal) = $year 
										ORDER BY beban_perjam DESC limit 1")
									->queryRow();
									
									$tertinggi = $db->createCommand(
										"SELECT  beban_perjam as nilai_tinggi , jam , tanggal FROM beban_detail INNER JOIN beban ON beban.id = beban_detail.beban_id ORDER BY beban_perjam DESC LIMIT 1"
									)->queryRow();
?>

<?php
						echo "
								<table style = 'float:left;font-size:12px !important;color:white;font-family:Tahoma;text-align:left;'>
									<tr >
										<td colspan = 3 style = 'border-bottom:1px #ccc;'>Prakiraan Beban Hari ini  </td>
										
										
										
									</tr>
									<tr>
											<td> BP </td>
											<td>:</td>
											<td>$prakiraan_hari_ini[nilai] MW</td>
									</tr>

									<tr>
										<td>Tgl</td> 
										<td>:</td> 
										<td>".ar::waktuYii($prakiraan_hari_ini['tanggal'] , "medium","")."  </td>
									</tr>
									<tr>
										<td>Jam </td>
										<td>:</td> 
											<td>$prakiraan_hari_ini[jam]</td>
									</tr>
									
									
									<tr >
										
										<td colspan = 3 style = 'border-top:1px white solid;'>Beban Puncak Bulan ini  </td>
									</tr>
									<tr>
											<td> BP</td>
											<td> :</td>
											<td> $bulan[nilai_bulan] MW</td>
									</tr>
									<tr>
											<td> Tgl</td>
											<td> :</td>
											<td> ".ar::waktuYii($bulan['tanggal'] , "medium" ,"")." </td>
									</tr>
									
									<tr>
											<td> Jam</td>
											<td> :</td>
											<td> $bulan[jam] </td>
									</tr>
									
									<tr >
										
										<td colspan = 3 style = 'border-top:1px solid white;'>Beban Puncak Tertinggi  </td>
									</tr>
									
									<tr>
											<td> BP</td>
											<td> :</td>
											<td> $tertinggi[nilai_tinggi] MW</td>
									</tr>
									
									<tr>
										<td>Tgl</td> 
										<td>:</td> 
										<td>".ar::waktuYii($tertinggi['tanggal'] , "medium","")."  </td>
									</tr>
									<tr>
											<td> Jam</td>
											<td> :</td>
											<td> ".$tertinggi['jam']."</td>
									</tr>
									
									
								</table>
						";
?>
