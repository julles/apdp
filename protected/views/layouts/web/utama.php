    
<?php
    $base = Yii::app()->baseUrl."/web/";
    $website = Yii::app()->db->createCommand("SELECT facebook , twitter , text_berjalan FROM website")->queryRow();
    $facebook = $website['facebook'];
    $twitter = $website['twitter'];
    $teks_berjalan = $website['text_berjalan'];
    $ip = ar::ip();
    $tanggal = date("Y-m-d");
    $cek = Yii::app()->db->createCommand("SELECT id FROM visitor WHERE tanggal = '".$tanggal."' AND ip = '$ip'")->queryScalar();
    if(empty($cek))
    {
        $insert = Yii::app()->db->createCommand("INSERT INTO visitor VALUES('','$ip','$tanggal')")->execute();
    }
?>
<style type="text/css">
#pengunjung{height: 40px;width: 150px;}
    #pengunjung span{font-size: 20px;background: #000;padding: 5px;}
</style>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>APDP Kalimantan Barat</title>
<link rel="shortcut icon" href="<?php echo $base; ?>image/pln-logo.jpg">

<!-- CSS Style -->
<link href="<?php echo $base; ?>css/mystyle.css" type="text/css" rel="stylesheet" />
<link href="<?php echo $base; ?>css/menu.css" type="text/css" rel="stylesheet" />
<link href="<?php echo $base; ?>css/menu-base.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base; ?>css/font-awesome.css" type="text/css" rel="stylesheet" />
<link href="<?php echo $base; ?>css/minimalism.css" type="text/css" rel="stylesheet" />
<!-- End CSS Style -->

<!-- Javascript -->
<?php /* <script type="text/javascript" src="<?php echo $base; ?>js/minimalism.js"></script> */ ?>
<script type="text/javascript" src="<?php echo $base; ?>js/menu.js"></script>   

<?php /*
<script type="text/javascript" src="<?php echo $base; ?>js/fade-image.js"></script>

*/ ?>

<?php $this->renderpartial('//layouts/web/banner'); ?>
<script type="text/javascript" src="<?php echo $base; ?>js/pqm.js"></script>
<script type="text/javascript" src="<?php echo $base; ?>js/marquee.js"></script>

    <?php
    /*  
    <script type="text/javascript" src="<?php echo $base; ?>js/jquery.min.js"></script>
    */
    Yii::app()->clientScript->registerCoreScript('jquery');
    ?>
<script type="text/javascript" src="<?php echo $base; ?>js/scroll.js"></script>
<?php
/*
    <script type="text/javascript" src="<?php echo $base; ?>js/jquery.magnifier.js"></script>
*/
?>
<script type="text/javascript" language="javascript">
    function bannerPop(letak,nama,lebar,tinggi) 
    {
        var height = screen.height;
        var width = screen.width;
        var leftpos = width / 2 - 350;
        var toppos = height / 2 - 130;
        hwnd = window.open(letak,nama,"statusbar=no,location=no,scrollbars=no,status=no,resizable=no,width="+
                           lebar+",height="+tinggi+",left=" + leftpos + ",top=" + toppos);
        hwnd.focus();
    }
</script>
<script>
    $(document).ready(function(){
        $('#pushstat').css({'display':'none'});
    });
</script>
<!-- Javascript -->
</head>

<body>
<!-- Wrapper -->
<div id="wrapper">
    <!-- Header -->
    <div id="header">
        <div><br/></div>
        <!-- Menu -->
        <div id="ddtopmenubar" class="mattblackmenu">
            <ul>
                <li><?php echo CHtml::link('Home' , array('site/index')); ?></li>
                <li><a href="#" rel="sekilas-apdp" class="">
                        Sekilas APDP
                    </a>
                </li>
                <li><a href="#" rel="kegiatan" class="">
                        Kegiatan
                    </a>
                </li>
                <li><a href="#" rel="operating" class="">
                        Operasi Sistem
                    </a>
                </li>
                <li><a href="#" rel="penyaluran" class="">
                        Penyaluran
                    </a>
                </li>
                <li><a style="margin-right: 15px;" href="#" rel="scadatel" class="">
                        Scadatel
                    </a>
                </li>
                <li style="font-size: 15px;"><a class="ahref" target="_blank" href="<?php echo $twitter; ?>"><span class="fa fa-twitter"></span></a></li>
                <li style="font-size: 15px;"><a class="ahref" style="padding: 11px 11px !important;" target="_blank" href="<?php echo $facebook; ?>"><span class="fa fa-facebook"></span></a></li>
                 <?php echo CHtml::image(Yii::app()->baseUrl."/images/loading.gif" ,"" , array('style' => 'display:none;' , 'id' => 'loadingSearch')); ?>
           
                    <input id = 'tcari' name="textsearch" placeholder="Search..." type="text" size="15" style="font-family: Tahoma; font-size: 10pt;margin-top: 7px;margin-left: 2px;outline:none;">
                    <?php
                        $gambarSearch = CHtml::image($base."image/icn-search.png" , "" , array('style' => 'width: 15px;margin-top: 11px;position: absolute;outline:none;'));
                        
                        echo CHtml::ajaxLink($gambarSearch , array("site/pencarian") , array(
                                    'type' => 'POST',
                                    'update' => '#rightContent',
                                    'data' => array('keyword' => 'js:tcari.value'),
                                    'beforeSend' => 'function(){$("#loadingSearch").show()}',
                                    'complete' => 'function(){$("#loadingSearch").hide()}',
                                )
                        );  
                     ?>
                  
                
                <div> 
                    
                    
                </div>
            </ul>
            <script type="text/javascript">
                ddlevelsmenu.setup("ddtopmenubar", "topbar") //ddlevelsmenu.setup("mainmenuid", "topbar|sidebar")
            </script>
        </div>
        <!-- End Menu -->
    </div>
    <!-- End Header -->
    
    <!-- Banner -->
    <div id="banner">
        <script type="text/javascript">
            //new fadeshow(IMAGES_ARRAY_NAME, slideshow_width, slideshow_height, borderwidth, delay, pause 
            //(0=no, 1=yes), optionalRandomOrder)
            new fadeshow(fadeimages, 780, 163, 0, 3000, 1, "R")      
        </script>
    </div>
    <!-- End Banner -->
    
    <!-- Konten -->
    <div id="allContent" style="background: url(<?php echo $base; ?>image/biru.png) left repeat-y !important;">
        <!-- Left Konten -->    
        <div id="leftContent" style="height: 1450px;">
        <div id="site-wrapper" class="show-nav">
        <div id="site-canvas" class="none">
        <div id="site-menu">
            <!-- Beban Realtime -->
            <div class="judulKiri"><strong style="padding-left: 30px;">Beban</strong></div>                    
            <div class="containerKiri">
                <div id="bbnRealtime">
                    <!--font size="1" color="#C0C0C0">
                        <p>Update time : <br>02-Jun-2014 11:26:37</p> 
                    </font-->
                    <font size="4" color="#FF8000" face="Tahoma">
                        <p></p>
                        
                        <script>
                            
                            
                            
                            /*$(document).ready(function(){

                                setInterval(test, 3000);

                                function test()
                                    { 
                                    
                                        $('#bebannya').load('<?php echo Yii::app()->createUrl("/site/getBeban"); ?>')
                                    
                                    }

                            }); */
                        </script>
                        
                        <div align="right">
                            <div id = 'bebannya' >
                                <table style = 'font-size:10px !important;color:white;'>
                                    <?php $this->renderpartial('//layouts/web/realtime'); ?>
                                </table>
                            
                            </div> 
                        </div>
                        <p></p> 
                    </font>
                </div>                 
               
               <a style="padding-left: 135px;">
                    &nbsp;
                </a>
                      </div>
            <!-- End Beban Realtime -->
            
            <!-- Berita -->
            <div class="judulKiri">
                <strong style="padding-left: 30px;">Berita Terkini</strong>
            </div>
            <div id="marqueecontainer" onMouseOver="copyspeed=pausespeed" onMouseOut="copyspeed=marqueespeed">
                <div id="vmarquee" style="position: absolute; width: 98%; top: -288px;">
                    <!-- Pertama -->
                                
                    <?php
                        $criteriaBerita = new CDbCriteria;
                        $criteriaBerita->limit = 3;
                        $criteriaBerita->order = 'rand()';
                        $berita = Berita::model()->findAll($criteriaBerita);
                        foreach($berita as $rowBerita)
                        {
                                $isi = substr(strip_tags($rowBerita->deskripsi) ,  0 , 100);
                    ?>
                                <div class="containerKiri" style = 'text-align:left;float:left;'>
                                    <b><font size="1" color="#C0C0C0"><?php echo ar::formatWaktu($rowBerita->date_post , "long" , ""); ?> </font></b><br>       
                                    <font color="#FADB05">
                                        <b><?php echo $rowBerita->judul; ?></b><br>
                                    </font>         
                                    <a href="#" style="font-size:12px; text-decoration:none;color:#FFF"><?php echo $isi; ?><br /><br/>
                                    </a>    
                                    <a href="<?php echo Yii::app()->createUrl("/kegiatan/beritaDetail/read/".$rowBerita->seo); ?>" style="font-size:9px; text-decoration:none;color:#FFF">
                                        <strong style="text-decoration:underline">Read More&gt;&gt;</strong>    
                                    </a>        
                                    <a href="<?php echo Yii::app()->createUrl("/kegiatan/berita"); ?>" style="font-size:9px; text-decoration:none;color:#FFF;padding-left: 30px;">
                                        <strong style="text-decoration:underline">Index Berita</strong> 
                                    </a>
                                    <p>&nbsp;</p>
                                    <img src="<?php echo $base; ?>image/line.jpg" alt=""><br>
                                </div>
                    <?php
                        }
                    ?>
                    <!-- End Pertama -->
                </div>
            </div>
            <div class="containerKiri" style="padding-top:15px; padding-bottom:10px;">
                <a href="<?php echo Yii::app()->createUrl("/kegiatan/berita"); ?>" style="font-size:9px; text-decoration:none;color:#FFF">
                    <strong style="text-decoration:underline">Selengkapnya&gt;&gt;</strong>    
                </a> 
            </div>  
            <!-- End Berita -->

            <!-- Pengumuman -->
            <div class="judulKiri">
                <strong style="padding-left: 30px;">Pengumuman</strong>
            </div>
            <marquee behavior="scroll" direction="up" scrollamount="3" onmouseover="this.setAttribute('scrollamount', 0, 0);" onmouseout="this.setAttribute('scrollamount', 3, 0);">
                    <!-- Pertama -->
                              
                    <?php
                        $criteriaBerita = new CDbCriteria;
                        $criteriaBerita->limit = 3;
                        $criteriaBerita->order = 'rand()';
                        $pengumuman = Pengumuman::model()->findAll($criteriaBerita);
                        foreach($pengumuman as $rowPengumuman)
                        {
                                $isi = substr(strip_tags($rowPengumuman->deskripsi) ,  0 , 100);
                    ?>
                                <div class="containerKiri">
                                    <b><font size="1" color="#C0C0C0"><?php echo ar::formatWaktu($rowPengumuman->date_post , "long" , ""); ?> </font></b><br>       
                                    <font color="#FADB05">
                                        <b><?php echo $rowPengumuman->judul; ?></b><br>
                                    </font>         
                                    <a href="#" style="font-size:12px; text-decoration:none;color:#FFF"><?php echo $isi; ?><br /><br/>
                                    </a>    
                                    <a href="<?php echo Yii::app()->createUrl("/kegiatan/pengumumanDetail/read/".$rowPengumuman->seo); ?>" style="font-size:9px; text-decoration:none;color:#FFF">
                                        <strong style="text-decoration:underline">Read More&gt;&gt;</strong>    
                                    </a>        
                                    <a href="<?php echo Yii::app()->createUrl("/kegiatan/berita"); ?>" style="font-size:9px; text-decoration:none;color:#FFF;padding-left: 30px;">
                                        <strong style="text-decoration:underline">Index Berita</strong> 
                                    </a>
                                    <p>&nbsp;</p>
                                    <img src="<?php echo $base; ?>image/line.jpg" alt=""><br>
                                </div>
                    <?php
                        }
                    ?>
                    
                    <!-- End Pertama -->
            </marquee>
            <div class="containerKiri" style="padding-top:15px; padding-bottom:10px;">
                <a href="<?php echo Yii::app()->createUrl("/kegiatan/pengumuman"); ?>" style="font-size:9px; text-decoration:none;color:#FFF">
                    <strong style="text-decoration:underline">Selengkapnya&gt;&gt;</strong>    
                </a> 
            </div> 
            <!-- End Pengumuman -->
            
            <!-- Link -->
            <div class="judulKiri">
                <strong style="padding-left:30px;">Link</strong>
            </div>
            <div class="containerKiri" style="padding-bottom: 30px;">                         
                <div style="padding-right:15px;padding-left:0px;padding-bottom:10px;">
                <?php
                    $sqlLink = "SELECT link , nama FROM master_link ORDER BY id DESC limit 10";
                    $query = Yii::app()->db->createCommand($sqlLink)->query();
                    while(($rowLink = $query->read()) !== false)
                    {
                        echo "<a href = '".$rowLink['link']."' target = '_blank'>$rowLink[nama]</a> <br/>";
                    }
                ?>
                </div>
                <br>
                <a href="<?php echo $base; ?>" style="font-size:9px; text-decoration:none;color:#FFF">
                    <strong style="text-decoration:underline">Read More&gt;&gt;</strong>
                </a>
            </div>
            <!-- End Link -->
            
            <!-- Counter Pengunjung -->
            <div class="judulKiri">
                <strong style="padding-left:30px;">Counter Pengunjung</strong>
            </div>
            <div class="containerKiri">     
                <div style="padding-right:15px;padding-left:15px;">
                  <div id="pengunjung" style = 'margin-top:20px;text-align:center;'>
                        <?php
                        $counter = Yii::app()->db->createCommand("SELECT COUNT(id) FROM visitor")->queryScalar();
                        ?>
                            <span><?php  echo $counter; ?></span>
                            
                    </div>
                  
                </div>
            </div>
            <!-- End Counter Pengunjung -->
            
            <!-- Profil Pegawai 
            <div class="judulKiri">
                <strong style="padding-left:30px;">Profil Pegawai</strong>
            </div>
            <div class="containerKiri">     
                <div style="padding-right:15px;padding-left:15px;">
                    <img src="<?php echo $base; ?>image/kt sapta.jpg" width="130" height="170" alt="I NYOMAN BUDIARTA"> 
                    <br><br>
                    <b>I NYOMAN BUDIARTA </b><br><br>
                    <b>JABATAN  :</b><br>JUNIOR ENGINEER SCADA DAN TEKNOLOGI INFORMASI <br><br> 
                    <a href="<?php echo $base; ?>#" style="font-size:9px; text-decoration:none;color:#FFF">
                        <strong style="text-decoration:underline">Read More&gt;&gt;</strong>
                    </a> &nbsp;
                    <a href="<?php echo $base; ?>#" style="font-size:9px; text-decoration:none;color:#FFF">
                        <strong style="text-decoration:underline;">Refresh&gt;&gt;</strong>
                    </a>
                </div>
            </div>
            <!-- End Profil Pegawai -->
            
            <div class="judulKiri">
                <strong style="padding-left:30px;">Login</strong>
            </div>                
            <div id="login" style="text-align:left;">
                <form method="post" action = "<?php echo Yii::app()->createUrl("/administrator/default/login"); ?>">
                    <table width="170" border="0">
                        <tbody>
                            <tr>
                                <td width="63"><span style = 'font-size:14px;'>Username</span></td> 
                                <td width="97">
                                    <input name="username" type="text" style="font-family: Tahoma;font-size: 10pt; width:97px" maxlength="20">
                                </td>
                            </tr>
                            <tr>
                                <td><span style = 'font-size:14px;'>Password</span></td>
                                <td><input name="password" type="password" style="width:97px" maxlength="20"></td>
                            </tr>
                        </tbody>
                    </table>
                    <button name = "cmd_login" type = "submit" class = 'bnt btn-primary'>Login</button><p></p>
                </form>
                        
            </div>
             
            
            <!-- Knowledge     
            <div class="judulKiri">
                <strong style="padding-left:30px;">Knowledge Center </strong>
            </div>                
            <div class="containerKiri">
                <img src="<?php echo $base; ?>image/kc.png" width="170" height="110" alt="Knowledge Center APD Bali">   
                <div align="center"> Please Login For Detail Knowledge Center APD Bali <br><br></div>               
            </div>  
            <!-- End Knowledge -->            
            
            <!-- Work Order 
            <div class="judulKiri">
                <strong style="padding-left:30px;">Work Order </strong>
            </div>                
            <div class="containerKiri">
                <img src="<?php echo $base; ?>image/wo.png" width="170" height="110" alt="Work Order APD Bali">                                 
            </div>
            <!-- End Work Order -->              
            
            <!-- Site Validator    
            <div class="judulKiri">
                <strong style="padding-left:30px;">Site Validator </strong>
            </div>                
            <div class="containerKiri">                         
                <img src="<?php echo $base; ?>image/valid-xhtml10.png" width="80" height="30">  
                 <img src="<?php echo $base; ?>image/valid-css.png" width="80" height="30"> 
            </div>
            <!-- End Site Validator -->
        </div>
        </div>  
        </div>
        </div>
        <!-- End Left Konten -->
        
        <!-- Right Konten -->
        <?php 
            echo $content;
        ?>
        <!-- End Right Konten -->
    </div>
    <!-- End Konten -->
    
    <!-- Footer -->
    <div id="footerWrapper">
        <div id="footer">
            <center>
                <span><a href="http://simkpnas.pln.co.id" target = '_blank'>SIMKPNAS</a></span>
                <span>|</span>
                <span><a href="https://webmail.pln.co.id/owa/auth/logon.aspx?url=https://webmail.pln.co.id/owa/&reason=0" target = '_blank'>Webmail</a></span>
                <span>|</span>
                <span><a href="http://erpappw1.pusat.corp.pln.co.id/irj/portal" target = '_blank'>ERP Portal</a></span>
                <span>|</span>
                <span><a href="http://202.162.213.66:36/pro/login.php" target = '_blank' >AMS</a></span>
                <span>|</span>
                <span><a href="https://simdiklat.pln-pusdiklat.co.id/portal" target = '_blank'>SIMDIKLAT</a></span>
            </center>
            <div style="border-bottom:1px dotted #D87F7F;padding-top:5px;padding-bottom:5px;margin-bottom: 5px;"></div>
            <center>Copyright � 2014, PT. PLN (Persero) APDP Kalbar. All rights reserved. </center>
       </div>
    </div>
    <!-- End Footer -->
</div>
<!-- End Wrapper -->

<!-- Running Text -->
<div class="running-text">
    <marquee style="padding: 6px 0px 0px 0px;" behavior="scroll" scrollamount="5" direction="left">
        <span><?php echo $teks_berjalan; ?></span>
    </marquee>
</div>
<!-- End Running Text -->

<!-- Scroll -->
<div id="topcontrol" title="Scroll Back to Top" style="position: fixed; right: 5px; opacity: 0; cursor: pointer;margin-bottom: 30px;">
    <img src="<?php echo Yii::app()->baseUrl; ?>/web/image/scup.png" style="width:48px; height:48px">
</div>
<!-- End Scroll -->

<!-- Dropdown Menu -->
     <?php
        $this->renderpartial('//layouts/web/menu');
     ?>
<!-- End Dropdown Menu -->

<script>
    function rotate()
    {
        document.getElementById('putar').style.margin='0px';
        document.getElementById("putar").style.WebkitTransform="rotate(-95deg)";
        document.getElementById("putar").style.MozTransform="rotate(-95deg)";
        document.getElementById('putar').setAttribute('onclick','changeid()');
        var e = document.getElementById("putar");
        e.id = "putars";
        //document.getElemenById("perjam").style.display='none';
    }
    function changeid ()
    {
        document.getElementById('putars').style.margin='7px';
        document.getElementById("putars").style.WebkitTransform="rotate(95deg)";
        document.getElementById("putars").style.MozTransform="rotate(95deg)";
        document.getElementById('putars').setAttribute('onclick','rotate()')
        var e = document.getElementById("putars");
        e.id = "putar";
    }
</script>
</body>
</html>