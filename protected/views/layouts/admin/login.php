<?php
  $base = Yii::app()->baseUrl."/admin/";
  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login Admin <?php echo Yii::app()->name; ?></title>
<link rel="shortcut icon" href="<?php echo $base; ?>image/pln-logo.jpg">

<!-- Styles -->
    <link href="<?php echo $base; ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $base; ?>css/lock.css" rel="stylesheet">
    <link href="<?php echo $base; ?>css/animate.css" rel="stylesheet">
    <link href="<?php echo $base; ?>css/font-awesome.css" rel="stylesheet">
<!-- End Styles -->
<script>
	$(document).ready(function(){
		$('#pushstat').css({'display':'none'});
	});
</script>
</head>

<body>
	<!-- Logo -->
	<div class="logo">
      <h4><a href="#" alt=""></a><?php echo CHtml::image(Yii::app()->baseUrl."/images/login-terbaru.png"); ?></h4>
    </div>
    <!-- End Logo -->
    
    <!-- Lock Holder -->
    <?php
      echo $content;

     ?>
    <!-- End Submit -->
</body>
<script src="<?php echo $base; ?>js/jquery.js"></script>
<script src="<?php echo $base; ?>js/bootstrap.min.js"></script>
<script src="<?php echo $base; ?>js/bootstrap-progressbar.js"></script>
<script type="text/javascript">
  $(document).ready(function(){

          $('.btn-submit').click(function(e){
            $('.input-username,dot-left').addClass('animated fadeOutRight')
            $('.input-password,dot-right').addClass('animated fadeOutLeft')
            $('.btn-submit').addClass('animated fadeOutUp')
            setTimeout(function () {
                      $('.avatar').addClass('avatar-top');
                      //$('.submit').html('<i class="fa fa-spinner fa-spin text-white"></i>');
                      $('.submit').html('<div class="progress"><div class="progress-bar progress-bar-success" aria-valuetransitiongoal="100"></div></div>');
                      $('.progress .progress-bar').progressbar(); 
              },
          500);

            setTimeout(function () {
                 // window.location.href = 'index2.html#redirect'; 

              },
          1500);

          });

        
});
</script>
</html>