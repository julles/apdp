<div id = 'render2'></div>
<?php
									
								$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelRange,'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listRange,
											 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 ),
										 // 'min' => 0 ,
									      //    'max' => 4
										  ),
										  'chart' => array(
												'renderTo' => 'render2',
											   
											),
										  'scrollbar' => array(
									      	'enabled' => true
									       ),
										  'yAxis' => array(
											//	'min' => 0,
											 'title' => array('text' => 'kWh')
										  ),
										   
										  'series' => array(
											 array('name' => 'PS GH', 'data' => $dataRange , 'type' => 'column'),
											 
										  )
									   )
									));
								 

							?>