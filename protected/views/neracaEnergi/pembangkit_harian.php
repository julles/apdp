<?php
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelHarian,'style' => 'font-size:16px;'),
										  'credits' => false ,
										  'xAxis' => array(
											 'categories' => $dataHarian,
											 
											 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 ),
											 //  'min' => 0 ,
									          //'max' => 12
										  ),
										  'scrollbar' => array(
									      	//'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
										  'yAxis' => array(
											//'min' => 0,
											 'title' => array('text' => 'kWh')
										  ),
										  'series' => array(
											 array('name' => 'Produksi', 'data' => $hasilHarian , 'type' => 'column'),
											
											
										  )
									   )
									));
									
								?>