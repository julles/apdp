


<script>
$(document).ready(function() {
    var table = $('#gridYii').dataTable({
		"ordering": false,
		 "sDom": "<'row-fluid'r><'row-fluid't><'row-fluid'<'span6'i><'span6'p>>", 
		"bPaginate" :false,
        "sScrollY": 500,
        "sScrollX": "40%",
        "sScrollXInner": "0%",
        "bScrollCollapse": true,
		aLengthMenu: [
        [25, 100],
        [25, 100]
		
		//[25, 50, 100, 200, -1],
        //[25, 50, 100, 200, "All"]
		],
		// "bProcessing": true,
		// "sAjaxSource": '<?php echo Yii::app()->createUrl('/neracaEnergi/populasi'); ?>'
	});

     new $.fn.dataTable.FixedColumns( table, {
        "iLeftColumns":3,
        "sHeightMatch" : "auto",
        //"iRightColumns": 0
    } );

} );
</script>
<table id="gridYii" class="display" cellspacing="0" width="99%" style = 'font-size:10px;'>
						    <thead>
						         <tr>
											
									<th>Gardu Induk</th>
									<th>Item</th>
									<th>&nbsp;</th>
									<?php
										for($a=1;$a<=$lastDay;$a++)
										{
											echo "<th>$a</th>";
										}
									?>
									
								</tr>
						    </thead>
						 
						    <tbody>
							<?php
								while(($row = $model->read())!==false)
								{
									
								
									echo "
									<tr>
											<td>$row[gardu_induk]</td>
											<td>$row[item]</td>
											<td>EXP</td>
									";
												for($a=1;$a<=$lastDay;$a++)
												{
													echo "<td>".floatval($this->field($bln , $tahun , 'expor' ,$a , $row['item_id']))."</td>";
												}
											
									echo"
									</tr>
									<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>IMP</td>
									
											";
												for($a=1;$a<=$lastDay;$a++)
												{
													echo "<td>".floatval($this->field($bln , $tahun , 'impor' ,$a,$row['item_id']))."</td>";
												}
											
									echo"
									</tr>
									";
								}
							?>
									
						     </tbody>
						     </table>					