<div id = 'render1'></div>

<?php
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelTahunan,'style' => 'font-size:16px;'),
										  'credits' => false ,
										  'xAxis' => array(
											 'categories' => $dataTahunan,
											 
											 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 ),
											   //'min' => 0 ,
									          //'max' => 6
										  ),
										  'chart' => array(
												'renderTo' => 'render1',
											   
											),
										  'scrollbar' => array(
									      	//'enabled' => false
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
										  'yAxis' => array(
											//'min' => 0,
											 'title' => array('text' => 'kWh')
										  ),
										  'series' => array(
											 array('name' => 'PS GH', 'data' => $hasilTahunan , 'type' => 'column'),
											
											
										  )
									   )
									));
								
								?>