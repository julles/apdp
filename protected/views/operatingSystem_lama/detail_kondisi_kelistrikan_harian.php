<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
								   		  'gradient' => array('enabled'=> true),

							   	   		  'credits' => array('enabled' => false),
 										  'exporting' => array('enabled' => true),
									      'title' => array('text' => $hariIniHarian ,'style' => 'font-size:16px;'),
									      
									      	 'theme' => 'epcGray',
						                    'chart' => array(
						                        'renderTo' => 'harian',
						                       
						                    ),
									     /* 'scrollbar' => array(
									      	'enabled' => true
									       ),
										 */
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
									      
									      'xAxis' => array(
									         'categories' => $tgl,

									          'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 )
									      ),
									      'yAxis' => array(
									        'title' => array('text' => ''),
									         'min' => 0 ,
									         'max' => 2 ,
											 'labels' => array('format' => '{kondisi}' ,'formatter' => 'js:function(){
											 	var yAxisLabels = ["Defisit","Siaga","Normal"];
											 	return yAxisLabels[this.value];
											 }'),
									      ),	
									      
									     
									      'tooltip' => array(
											'headerFormat' => 'Tanggal {point.key}<br/>',
											'pointFormat' => 'Kondisi : {point.kondisi}<br/>',
											'footerFormat'=> '</table>',
											'shared' => true,
											'useHtml' => true
									      ),
										  //'colors'=>array('green' , 'yellow'),
									      'series' => array(
									      	
									         array('name' => 'Kondisi', 'data' => $modelHarian  ,'type' => 'line'),
											
									         //array('name' => 'Jam', 'data' => $modelHarian  ,'type' => 'line'),	
									        
									         
									      )
									   )
									));

							?>