<div id = 'tahunan'></div>
<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelTahunan,'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listTahun,
											 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 )
										  ),
										  'yAxis' => array(
											 'title' => array('text' => 'Beban (MW)')
										  ),
										   'chart' => array(
												'renderTo' => 'tahunan',
											   
											),
										   'colors'=>array('blue'),
										   'tooltip' => array(
												'headerFormat' => 'Tahun {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/>Tanggal:{point.tanggal}',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
											  ),
										   
										  'series' => array(
											 array('name' => 'Beban (MW)', 'data' => $valueTahun , 'type' => 'column'),
											
											
										  )
									   )
									));

							?>