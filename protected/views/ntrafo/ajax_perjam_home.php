<div id = 'trafonachar'></div>
<?php
											
											$this->Widget('ext.highcharts.HighchartsWidget', array(
											   'options'=>array(
												  'title' => array('text' => ar::formatWaktu($date , "long" ,""),'style' => 'font-size:16px;'),
												  'credits' => false , 
												  'xAxis' => array(
													 'categories' => $listJam,
													 'type' => 'category',
													 'labels' => array(
														'rotation' => 270,
														'y' => 40,
														//'style' => array('font-size' => '5px;')
													 )
												  ),
												  'yAxis' => array(
													 'title' => array('text' => 'Beban (MW)')
												  ),
												  'plotOptions' => array(
                                                    'series' => array(
                                                            'marker' => array(
                                                                'enabled' => false,

                                                            )
                                                    )
                                                   ),
												  'chart' => array(
														'renderTo' => 'trafonachar',
													   
													),
										 
												  // 'colors'=>array('red' , 'blue','pink'),
												  'series' => $chartTrafo
											   )
											));

									?>