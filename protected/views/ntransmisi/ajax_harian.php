<?php
									$labelBulanTahun = explode(" ",ar::formatWaktu($date , "long" ,""));
									$labelBulanTahunFix = $labelBulanTahun[1]." - ".$labelBulanTahun[2];
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelBulanTahunFix,'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listHari,
											  'type' => 'category',
												 'labels' => array(
													'rotation' => 270,
													'y' => 40,
													//'style' => array('font-size' => '5px;')
												 )
										  ),
										   'chart' => array(
												'renderTo' => 'perharian',
											   
											),
										  'yAxis' => array(
											 'title' => array('text' => 'Beban (MW)')
										  ),
										   'colors'=>array('green'),
										   'tooltip' => array(
												'headerFormat' => 'Tanggal {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/>',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
											  ),
										   
										  'series' => array(
											 array('name' => 'Beban(MW)', 'data' => $valueHari , 'type' => 'column'),
											
											
										  )
									   )
									));
									?>