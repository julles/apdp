<div id = 'destak'></div>
<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $year,'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listBulan,
											 'type' => 'category',
												 'labels' => array(
													'rotation' => 270,
													'y' => 40,
													//'style' => array('font-size' => '5px;')
												 )
										  ),
										  'yAxis' => array(
											 'title' => array('text' => 'Beban (MW)')
										  ),
										  'chart' => array(
												'renderTo' => 'destak',
											   
											),
										   'colors'=>array('red'),
										   'tooltip' => array(
												'headerFormat' => 'Bulan {point.key}<br/>',
												'pointFormat' => '{series.name}:{point.y}<br/> Jam: {point.jam} <br/> Tanggal: {point.tanggal}',
												'footerFormat'=> '</table>',
												'shared' => true,
												'useHtml' => true
											  ),
										   
										  'series' => array(
											 array('name' => 'Beban(MW)', 'data' => $valueBulan , 'type' => 'column'),
											
											
										  )
									   )
									));
									?>