<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>


<script>

	$(document).ready(
		function()
		{
			$("#cmd_utama").click(
				function()
				{
						$("#cmd1").click();
						$("#cmd2").click();
			
				}
			);
		}
	);
</script>


<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
			
            	<div id="hearts">
      				
					<?php
						//echo CHtml::link("" , '#' , array('id' => 'putars' , 'onclick' => '' , 'tittle' => 'Minimize and Maximize'));
					?>
                </div>
				
				
				
                <div class="judul" align="center">
                    <h2><b>Kinerja Operasi</b></h2>
                </div>
            	<div id="textnya">
				<div style="height: 60px;" id="pembebanan-trafo-top">
                        <div class="trafo-panel-top">
                            <div class="trafo-panel-input-top">
                                <label style="padding-right:20px;">Kinerja</label>
								<?php
									$data = CHtml::listData(MasterKinerjaOperasi::model()->findAll(array('order' => "id ASC")) ,'id' , 'kinerja_operasi'); 
									echo CHtml::dropDownList("kinerja" , "" , $data , array('style' => 'height:25px;' , 'id' => 'kinerja')); 
								?>
							</div>
                        </div>
                        <div class="trafo-panel-top">
                            <div class="trafo-panel-input-top">
                              <?php echo CHtml::link("<button style = 'margin-bottom:10px;'>Tampil</button>" , '#' , array('class' => '' , 'id' => 'cmd_utama' ,'style' => 'text-decoration:none;')); ?>
                            </div>
                        </div>
                       
                    </div>
					<br/>
                	<div id="portlet">
                    	<p class="portlet-title"><u>Kinerja Operasi Bulanan</u></p>
                    </div>
                	<div id="beban-puncak">
                	<?php
                	//echo CHtml::beginForm();
                	?>
                       <label style="padding-right:20px;">Tahun</label>
					    <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'tahun'>
							<option value="">Select</option>
							<?php
							$year = date('Y');
							for($a=2010;$a<=$year;$a++)
							{
								echo "<option value = '$a'>$a</option>";
							}
							?>

						</select>

                        <?php
                        	echo CHtml::ajaxLink('<button id = "cmd1" style = "height:27px;margin-top:1px;">Tampil</button>' , 
                        		 array('kinerjaOperasi/ajaxBulanan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perbulan',	
                        		 	'data' => array('year' => 'js:tahun.value' , 'kinerja' => 'js:kinerja.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading1").show();
									}',
									'complete' => 'function(){
										$("#loading1").hide();
									}',
                        		 )
								 

                        	);
                         ?>
					
					   <div id = 'loading1' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                       
                        <div  class = 'perbulan' class="export2" id = 'perbulan'>
                            <!-- Line Chart -->
							<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
										   'options'=>array(
											  'title' => array('text' => $year,'style' => 'font-size:16px;'),
											  'credits' => false , 
											  'xAxis' => array(
												 'categories' => $listBulan,
												 'labels' => array(
														'rotation' => 270,
														'y' => 40,
														//'style' => array('font-size' => '5px;')
													 ),
											  ),
											  'yAxis' => array(
												 'title' => array('text' => $satuan)
											  ),
											  'colors'=>array('blue'),
											  'series' => array(
												
												array('name' => 'Real', 'data' => $dataReal , 'type' => 'column'),
											
											  )
										   )
										));

							?>
                            <!-- End Line Chart -->
                    	
						</div>
                    	<?php

                    //	echo CHtml::endForm();
                    	?>
                	</div>
					
					
					
                </div>
				
				
				
				
				
            </div>
            <!-- End Pertama -->
        </div>

        <script>
	$(function() {
  		var special = ['reveal', 'top', 'boring', 'perspective', 'extra-pop'];
  		// Toggle Nav on Click
  		$('#hearts a').click(function() {
			var delay_ = 300;
			checkWidth = $('#leftContent').css( "width");
			var widthnya="594";
			var widthnyaChart="594";
			heightnya = "400";
			widthbaru = "594"
			widthnya5 = "267";
			widthnya6 = "565";
			LineCharts = "564";
			if( checkWidth < '186'){
			  $('#leftContent').animate( {"width":"186", "height":"100" + "%"}, delay_, "linear");
			  $('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			  $('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			  $('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			  $('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
			}else if(checkWidth > "0"){
			  $('#leftContent').animate({ "width":"0", "height":"0"}, delay_, "linear" );	
				widthnya = "780";
				widthnyaChart = "1000";
				heightnya = "500";
				widthnya5 = "325";
				widthnya6 = "750";
				LineCharts = "750";
			}
			$('#rightContent').animate({ "width": widthnya}, delay_, "linear" );
			$('.judul').animate({ "width": widthnya}, delay_, "linear" );
			$('.containerKanan').animate({ "width": widthnya2}, delay_, "linear" );
			$('#perjam').animate({ "width": widthnya2}, delay_, "linear" );
			$('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			$('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			$('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			$('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
  		});
	});
</script>