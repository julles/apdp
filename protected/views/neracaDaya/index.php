<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>




<script>

	$(document).ready(
		function()
		{
			$("#cmd_utama").click(
				function()
				{
						$("#cmd1").click();
						$("#cmd2").click();
			
				}
			);
		}
	);
</script>

<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
			
            	<div id="hearts">
      				
					<?php
						//echo CHtml::link("" , '#' , array('id' => 'putars' , 'onclick' => '' , 'tittle' => 'Minimize and Maximize'));
					?>
                </div>
				
				
				
                <div class="judul" align="center">
                    <h2><b>Neraca Daya</b></h2>
                </div>
            	<div id="textnya">
					
					<br/>
                	<div id="portlet">
                    	<p class="portlet-title"><u>Neraca Daya Perhari</u></p>
                    </div>
                	<div id="beban-puncak">
                	<?php
                	//echo CHtml::beginForm();
                	?>
					
                       <label style="padding-right:0px;">Bulan & Tahun</label>
						  <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'bulanHarian'>
                            <option value="">Select</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                         <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'tahunHarian'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>

                        <?php
                        	echo CHtml::ajaxLink('<button id = "cmd1">Tampil</button>' , 
                        		 array('neracaDaya/ajaxHarian'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#harian',
                        		 	'data' => array('year' => 'js:tahunHarian.value' , 'bulan' => 'js:bulanHarian.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading1").show();
									}',
									'complete' => 'function(){
										$("#loading1").hide();
									}',
                        		 )
								 

                        	);
                         ?>
						
					   <div id = 'loading1' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                       
                        <div  class = 'perbulan' class="export2" id = 'harian'>
                            <!-- Line Chart -->
							<?php
									$plot =  array('marker' => array('enabled' => false));


									$tanggal = substr(ar::waktuYii(date("Y-m-1") , "long" ,'') ,  2, 20);
									$this->Widget('ext.highcharts.HighchartsWidget', array(
								   'options'=>array(
									  'title' => array('text' => $tanggal ,'style' => 'font-size:16px;'),
									  'credits' => false , 
									  'xAxis' => array(
										 'categories' => $listHarian,
										/*'min' => 0, 
										'max' => 3,
										*/
										'labels' => array(
												'rotation' => 270,
												'y' => 40,)
									  ),
									  'yAxis' => array(
										 'title' => array('text' => 'MW'),
										'stackLabels' => array(
											//'enabled' => true
										),
										),
									  'plotOptions' => array(
										'column' => array(
												'stacking' => 'normal',
												//'dataLabels' => array('enabled' => true)
										)
									 
									  ),
									 
									  'colors' => array('#2EF287' ,'#A3C558' , '#CD6F22','#FFFFB4','#FFFF05','#B34EE9','#3972B7' , '#050505', 'green' , 'red','black'),
									  'series' => array(
										
										 array('name' => 'PO', 'data' => $po, 'type' => 'column'),
										  array('name' => 'MO', 'data' => $mo, 'type' => 'column'),
										 array('name' => 'FO', 'data' => $fo , 'type' => 'column'),
										 array('name' => 'Derating', 'data' => $derating , 'type' => 'column'),
										 array('name' => 'Cad.Operasi', 'data' => $cad_operasi , 'type' => 'column'),
										  array('name' => 'GangFdr', 'data' => $fdr, 'type' => 'column'),
										 array('name' => 'Beban', 'data' => $beban , 'type' => 'column'),
										
										array('name' => 'Padam', 'data' => $padam, 'type' => 'column'),
										
										 
										 array('name' => 'DM', 'data' => $dm, 'type' => 'line', 'marker' =>array('enabled' => false)),
										 array('name' => 'Beban Puncak', 'data' => $beban_puncak, 'type' => 'line' , 'marker' =>array('enabled' => false)),
										  array('name' => 'Kap Terpasang', 'data' => $terpasang , 'lineWidth' => 3 , 'marker' =>array('enabled' => false)
												
										),
										
										
									  )
								   )
								));

							?>
                            <!-- End Line Chart -->
                    	
						</div>
                    	<?php

                    //	echo CHtml::endForm();
                    	?>
                	</div>
					<div id="textnya">
                	<div id="portlet">
                    	<p class="portlet-title"><u>Neraca Daya Perbulan</u></p>
                    </div>
                	<div id="beban-puncak">
                	<?php
                	echo CHtml::beginForm();
                	?>
                        <label style="padding-right:0px;">Tahun</label>
						 
                         <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'tahunBulanan'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
						 

                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn_jam">Tampil</button>' , 
                        		 array('neracaDaya/ajaxBulanan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perbulanan',
                        		 	'data' => array('year' => 'js:tahunBulanan.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading3").show();
									}',
									'complete' => 'function(){
										$("#loading3").hide();
									}',
                        		 )
								 

                        	);
                         ?>
                        <div id = 'loading3' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                       
                        <div  class = 'perbulanan' class="export2" id = 'perbulanan'>
                            <!-- Line Chart -->
							<!-- Line Chart -->
							<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
								   'options'=>array(
									  'title' => array('text' => $year ,'style' => 'font-size:16px;'),
									  'credits' => false , 
									  'xAxis' => array(
										 'categories' => $listBulan,
										/*'min' => 0, 
										'max' => 3, */
										'labels' => array(
												'rotation' => 270,
												'y' => 40,)
									  ),
									  'yAxis' => array(
										 'title' => array('text' => 'MW'),
										'stackLabels' => array(
										//	'enabled' => true
										),
										),
									  'plotOptions' => array(
										'column' => array(
												'stacking' => 'normal',
												//'dataLabels' => array('enabled' => true)
										)
									  
									  ),
									  /* 'tooltip' => array(
										'headerFormat' => 'Bulan {point.key}<br/>',
										'pointFormat' => '{series.name}:{point.y}<br/>  <br/> Tanggal: {point.tanggal}',
										'footerFormat'=> '</table>',
										'shared' => true,
										'useHtml' => true
									  ),*/
									  'colors' => array('#2EF287' ,'#A3C558' , '#CD6F22','#FFFFB4','#FFFF05','#B34EE9','#3972B7' , '#050505', 'green' , 'red','black'),
									  'series' => array(
										
										 array('name' => 'PO', 'data' => $poBulan, 'type' => 'column'),
										  array('name' => 'MO', 'data' => $moBulan, 'type' => 'column'),
										 array('name' => 'FO', 'data' => $foBulan , 'type' => 'column'),
										 array('name' => 'Derating', 'data' => $deratingBulan , 'type' => 'column'),
										
										  array('name' => 'Cad.Operasi', 'data' => $cad_operasiBulan , 'type' => 'column'),
										   array('name' => 'GangFdr', 'data' => $fdrBulan, 'type' => 'column'),
										 array('name' => 'Beban', 'data' => $bebanBulan , 'type' => 'column'),
										
										array('name' => 'Padam', 'data' => $padamBulan, 'type' => 'column'),
										
										 
										 array('name' => 'DM', 'data' => $dmBulan, 'type' => 'line' , 'marker' =>array('enabled' => false)),
										 array('name' => 'Beban Puncak', 'data' => $beban_puncakBulan, 'type' => 'line' , 'marker' =>array('enabled' => false)),
										  array('name' => 'Kap Terpasang', 'data' => $terpasangBulan , 'type' => 'line' , 'marker' =>array('enabled' => false)
										),
										
										
									  )
								   )
								));

							?>
                            <!-- End Line Chart -->
                    	
						</div>
                    	<?php

                    	echo CHtml::endForm();
                    	?>
                	</div>
					
					<div id="textnya">
                	<div id="portlet">
                    	<p class="portlet-title"><u>Neraca Daya Pertahun</u></p>
                    </div>
                	<div id="beban-puncak">
                	<?php
                	echo CHtml::beginForm();
                	?>
                         <label style="padding-right:0px;">Tahun</label>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'awal'>
                            <option value="">Select</option>
                            <?php
	                        	$year = date('Y');
	                        	for($a=2010;$a<=$year;$a++)
	                        	{
	                        		echo "<option value = '$a'>$a</option>";
	                        	}
                        	?>
                        </select>
                        <label style="padding-right:0px;">s.d</label>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'akhir'>
                        	<option value="">Select</option>
                            <?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>
                        </select>
						 

                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn_jam">Tampil</button>' , 
                        		 array('neracaDaya/ajaxTahunan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#pertahunan',
                        		 	'data' => array('awal' => 'js:awal.value' , 'akhir' => 'js:akhir.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading4").show();
									}',
									'complete' => 'function(){
										$("#loading4").hide();
									}',
                        		 )
								 

                        	);
                         ?>
                        <div id = 'loading4' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                       
                        <div  class = 'pertahunan' class="export2" id = 'pertahunan'>
                            <!-- Line Chart -->
								<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
								   'options'=>array(
									  'title' => array('text' => $labelTahunan ,'style' => 'font-size:16px;'),
									  'credits' => false , 
									  'xAxis' => array(
										 'categories' => $listTahun,
										/*'min' => 0, 
										'max' => 3, */
										'labels' => array(
												'rotation' => 270,
												'y' => 40,)
									  ),
									  'yAxis' => array(
										 'title' => array('text' => 'MW'),
										'stackLabels' => array(
											//'enabled' => true
										),
										),
									  'plotOptions' => array(
										'column' => array(
												'stacking' => 'normal',
												//'dataLabels' => array('enabled' => true)
										)
									  
									  ),
									  /* 'tooltip' => array(
										'headerFormat' => 'Bulan {point.key}<br/>',
										'pointFormat' => '{series.name}:{point.y}<br/>  <br/> Tanggal: {point.tanggal}',
										'footerFormat'=> '</table>',
										'shared' => true,
										'useHtml' => true
									  ),*/
									  'colors' => array('#2EF287' ,'#A3C558' , '#CD6F22','#FFFFB4','#FFFF05','#B34EE9','#3972B7' , '#050505', 'green' , 'red','black'),
									  'series' => array(
										
										 array('name' => 'PO', 'data' => $poTahun, 'type' => 'column'),
										  array('name' => 'MO', 'data' => $moTahun, 'type' => 'column'),
										 array('name' => 'FO', 'data' => $foTahun , 'type' => 'column'),
										 array('name' => 'Derating', 'data' => $deratingTahun , 'type' => 'column'),
										
										  array('name' => 'Cad.Operasi', 'data' => $cad_operasiTahun , 'type' => 'column'),
										   array('name' => 'GangFdr', 'data' => $fdrTahun, 'type' => 'column'),
										 array('name' => 'Beban', 'data' => $bebanTahun , 'type' => 'column'),
										
										array('name' => 'Padam', 'data' => $padamTahun, 'type' => 'column'),
										
										 
										 array('name' => 'DM', 'data' => $dmTahun, 'marker' =>array('enabled' => false)),
										 array('name' => 'Beban Puncak', 'data' => $beban_puncakTahun, 'type' => 'line', 'marker' =>array('enabled' => false)),
										  array('name' => 'Kap Terpasang', 'data' => $terpasangTahun , 'type' => 'line' , 'marker' =>array('enabled' => false)
										),
										
										
									  )
								   )
								));

							?>
                            <!-- End Line Chart -->
                    	
						</div>
                    	<?php

                    	echo CHtml::endForm();
                    	?>
                	</div>
                    
                </div>
					
                    
                </div>
					
					
                </div>
				
				
				
            </div>
            <!-- End Pertama -->
        </div>

        <script>
	$(function() {
  		var special = ['reveal', 'top', 'boring', 'perspective', 'extra-pop'];
  		// Toggle Nav on Click
  		$('#hearts a').click(function() {
			var delay_ = 300;
			checkWidth = $('#leftContent').css( "width");
			var widthnya="594";
			var widthnyaChart="594";
			heightnya = "400";
			widthbaru = "594"
			widthnya5 = "267";
			widthnya6 = "565";
			LineCharts = "564";
			if( checkWidth < '186'){
			  $('#leftContent').animate( {"width":"186", "height":"100" + "%"}, delay_, "linear");
			  $('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			  $('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			  $('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			  $('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
			}else if(checkWidth > "0"){
			  $('#leftContent').animate({ "width":"0", "height":"0"}, delay_, "linear" );	
				widthnya = "780";
				widthnyaChart = "1000";
				heightnya = "500";
				widthnya5 = "325";
				widthnya6 = "750";
				LineCharts = "750";
			}
			$('#rightContent').animate({ "width": widthnya}, delay_, "linear" );
			$('.judul').animate({ "width": widthnya}, delay_, "linear" );
			$('.containerKanan').animate({ "width": widthnya2}, delay_, "linear" );
			$('#perjam').animate({ "width": widthnya2}, delay_, "linear" );
			$('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			$('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			$('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			$('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
  		});
	});
</script>