<?php
									$plot =  array('marker' => array('enabled' => false));


									$tanggal = substr(ar::waktuYii(date("Y-m-1") , "long" ,'') ,  2, 20);
									$this->Widget('ext.highcharts.HighchartsWidget', array(
								   'options'=>array(
									  'title' => array('text' => $tanggal ,'style' => 'font-size:16px;'),
									  'credits' => false , 
									  'xAxis' => array(
										 'categories' => $listHarian,
										/*'min' => 0, 
										'max' => 3,
										*/
										'labels' => array(
												'rotation' => 270,
												'y' => 40,)
									  ),
									  'yAxis' => array(
										 'title' => array('text' => 'MW'),
										'stackLabels' => array(
											//'enabled' => true
										),
										),
									  'plotOptions' => array(
										'column' => array(
												'stacking' => 'normal',
												//'dataLabels' => array('enabled' => true)
										)
									 
									  ),
									 
									  'colors' => array('#2EF287' ,'#A3C558' , '#CD6F22','#FFFFB4','#FFFF05','#B34EE9','#3972B7' , '#050505', 'green' , 'red','black'),
									  'series' => array(
										
										 array('name' => 'PO', 'data' => $po, 'type' => 'column'),
										  array('name' => 'MO', 'data' => $mo, 'type' => 'column'),
										 array('name' => 'FO', 'data' => $fo , 'type' => 'column'),
										 array('name' => 'Derating', 'data' => $derating , 'type' => 'column'),
										 array('name' => 'Cad.Operasi', 'data' => $cad_operasi , 'type' => 'column'),
										  array('name' => 'GangFdr', 'data' => $fdr, 'type' => 'column'),
										 array('name' => 'Beban', 'data' => $beban , 'type' => 'column'),
										
										array('name' => 'Padam', 'data' => $padam, 'type' => 'column'),
										
										 
										 array('name' => 'DM', 'data' => $dm, 'type' => 'line', 'marker' =>array('enabled' => false)),
										 array('name' => 'Beban Puncak', 'data' => $beban_puncak, 'type' => 'line' , 'marker' =>array('enabled' => false)),
										  array('name' => 'Kap Terpasang', 'data' => $terpasang , 'lineWidth' => 3 , 'marker' =>array('enabled' => false)
												
										),
										
										
									  )
								   )
								));

							?>