<div id = 'render2'></div>
<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelRange),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listRange,
											 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 ),
										  'min' => 0 ,
									          'max' => 3
										  ),
										  'scrollbar' => array(
									      	'enabled' => true
									       ),
										   'chart' => array(
												'renderTo' => 'render2',
											   
											),
										  'yAxis' => array(
												'min' => 0,
											 'title' => array('text' => 'Liter')
										  ),
										   
										  'series' => array(
											 array('name' => 'HSD', 'data' => $hsdRange , 'type' => 'column'),
											 array('name' => 'MFO', 'data' => $mfoRange , 'type' => 'column'),
											 array('name' => 'Olein', 'data' => $oleinRange , 'type' => 'column'),
											  array('name' => 'Bahan Bakar', 'data' => $bahan_bakarRange , 'type' => 'column'),
										  )
									   )
									));

							?>
