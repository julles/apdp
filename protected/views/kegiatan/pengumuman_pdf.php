<?php
    // get the HTML
    ob_start();
?>
	
					
					<div style = 'text-align:center;'>
						<h3>
							<?php 
								echo $model->judul;
							?>
						</h3>
					</div>
	
<?php
	echo "<p>
	
		".$model->deskripsi."
		</p>";

?>

<?php
    $content = ob_get_clean();
 
    // convert in PDF
    Yii::import('application.extensions.tcpdf.HTML2PDF');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'en');
//      $html2pdf->setModeDebug();
        $html2pdf->setDefaultFont('Arial');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output("name.pdf");
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
?>
