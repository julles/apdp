<?php
$absoluteUrl = $_SERVER['HTTP_HOST'].Yii::app()->request->getUrl();
?>
<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
            	<div id="hearts">
      				<!--a id="putar"href="#" onclick="rotate()" title="Minimize and Maximize"></a-->
                </div>
                <div class="judul" align="center">
                    <h2><b>
                    	
                    	<?php
                    		echo $model->judul;
                    	?>
                    </b></h2>
                </div>
            	<div id="textnya" style="padding-bottom: 25px;">
                    <?php echo $gambar; ?>
                    <p class="justify">
                    	
                    	<?php
                    		echo $model->deskripsi;
                    	?>
                    </p>
                    <div id="share">
                    	<ul>
                        	<li><a target = '_blank' href="<?php echo ar::createUrl("/kegiatan/pengumumanDetail/read/$model->seo/aksi/print");?>"><span class="print"></span>Print this page</a></li>
                            <li>
                            	<a href="#"><span class="share1"></span>Share this page</a>
                            	<div class="pop" style="display:none;">
                                	<div class="flyout five-item">
                                    	<div class="sharepage" style = 'margin-left:28px;'>
                                            <?php
												$this->widget('ext.sosmed.SocialShareWidget', array(
												    'url' => $absoluteUrl,          //required
												    'services' => array('facebook'),   //optional
												    //'htmlOptions' => array('class' => 'someClass'), //optional
												    'popup' => true,               //optional
												));

											?> 
                                        </div>
                                        <div class="sharepage">
                                           <?php
												$this->widget('ext.sosmed.SocialShareWidget', array(
												    'url' => $absoluteUrl,          //required
												    'services' => array('twitter'),   //optional
												    //'htmlOptions' => array('class' => 'someClass'), //optional
												    'popup' => true,               //optional
												));

											?> 
                                        </div>
                                       
                                    </div>
                                    <span class="arrow"><span class="inner"></span></span>
                                </div>
                            </li>
                            <li><a target = '_blank' href="<?php echo ar::createUrl("/kegiatan/pengumumanDetail/read/$model->seo/aksi/pdf");?>"><span class="pdf"></span>Export to pdf</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End Pertama -->
        </div>
       
<script>
	$(function() {
  		var special = ['reveal', 'top', 'boring', 'perspective', 'extra-pop'];
  		// Toggle Nav on Click
  		$('#hearts a').click(function() {
			var delay_ = 300;
			checkWidth = $('#leftContent').css( "width");
			var widthnya="594";
			heightnya = "400";
			widthbaru = "594"
			if( checkWidth < '186'){
			  $('#leftContent').animate( {"width":"186" ,}, delay_, "linear");
			/*  $('.containerKanan img').animate({ "width": widthbaru , "height": heightnya}, delay_, "linear" ); */
			}else if(checkWidth > "0"){
			  $('#leftContent').animate({ "width":"0"}, delay_, "linear" );	
				widthnya = "780";
				heightnya = "500";
			}
			$('#rightContent').animate({ "width": widthnya}, delay_, "linear" );
			$('.judul').animate({ "width": widthnya}, delay_, "linear" );
			$('.containerKanan').animate({ "width": widthnya}, delay_, "linear" );
			/* $('.containerKanan img').animate({ "width": widthnya, "height": heightnya}, delay_, "linear" ); */ 
  		});
	});
</script>