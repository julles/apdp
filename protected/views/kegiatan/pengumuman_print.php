<?php
    $base = Yii::app()->baseUrl."/web/";
?>   
<script type="text/javascript">
window.print()
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php  echo Yii::app()->name; ?></title>
<!-- CSS -->

<link rel="stylesheet" type="text/css" media="print" href="<?php echo $base; ?>css/print.css">
<!-- End CSS -->

</head>

<body>
<p>
&nbsp;
</p>
<div style="border:1px solid black;width:90%;height:90%;margin:0px auto;">
<p>
&nbsp;
</p>
<div style="width:80%;margin:0px auto;">
					<div style = 'text-align:center;'>
						<h3>
							<?php echo $model->judul; ?>
						</h3>
					</div>
					<?php echo $gambar; ?>
					
					<?php echo $model->deskripsi; ?>
</div>
</div>
</body>
</html>