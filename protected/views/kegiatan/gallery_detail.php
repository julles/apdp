 <?php
$this->widget('ext.SliderPopImage.SliderPopImage', array(
'selectorImgPop' => '.thumbsgen',
'popupwithpaginate' => true,
'maxpopuwidth' => '$(window).width()*0.8',

'postfixThumb' => '_thumb',
    'relPathThumbs' => 'thumbs' 
    //'relPathThumbs' => array('thumbsTiny','thumbsMedium') //only version 1.1
));
?>
<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
            	<div id="hearts">
      				<!--a id="putar"href="#" onclick="rotate()" title="Minimize and Maximize"></a-->
                </div>
                <div class="judul" align="center">
                    <h2><b><?php echo $model->nama_album; ?></b></h2>
                </div>
            	<div id="gallery">
                    
            	<?php

            	foreach($listFoto as $row)
            	{
            		$alamat = Yii::app()->baseUrl."/images/gallery/thumb/$row->gambar";
            	?>
	                    <!-- Pertama -->
	                    <div class="col-3">
	                    	
	                        	<div class="box">
	                                <img style = 'cursor:pointer;' src="<?php echo $alamat; ?>" class = 'thumbsgen' />
	                            </div>
	                        
	                    </div>
	                    <!-- End Pertama -->
                    
                <?php
                }
                ?>    
                </div>
            </div>
            <!-- End Pertama -->
        </div>