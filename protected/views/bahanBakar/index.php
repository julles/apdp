
<?php Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;?>
<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>

<script>

	$(document).ready(
		function()
		{
			$("#cmd_utama").click(
				function()
				{
						$("#btn1").click();
						$("#btn2").click();
						$("#btn3").click();
			
				}
			);
		}
	);
</script>
<?php
$ajax = array(
			'type' => 'POST',
			'url' => Yii::app()->createUrl("/bahanBakar/getUnitPembangkit"),
			'data' => array(
				'pembangkit_id' => 'js:this.value',

			),
			'update' => '#unit_pembangkit_id'
		);
$optionsPembangkit = array(
		//'class' => 'form-control',
    'style' => 'width:200;',
		'empty' => 'all',
		'id' => 'pembangkit_id',
		'onChange' => CHtml::Ajax($ajax)
		);
	

	$optionsUnitPembangkit = array(
		//'class' => 'form-control',
        'style' => 'width:180;',
		'empty' => 'all',
		'id' => 'unit_pembangkit_id',
	);
?>
<div id="rightContent">
        	<!-- Pertama -->						 
            <div class="containerKanan">
            	<div id="hearts">
      				<!--a id="putar"href="#" onclick="rotate()" title="Minimize and Maximize"></a-->
                </div>
                <div class="judul" align="center">
                    <h2><b>Pemakaian Bahan Bakar</b></h2>
                </div>
            	<div id="textnya">
                	
            		<div id="pembebanan-trafo-top" style="height: 80;">
                        <div class="trafo-panel-top" style = "width:237px;">
                            <div class="trafo-panel-input-top">
                               <table width = '200'>
                                    <tr>
                                        <td style = 'font-size:12px;'>
                                            
                                            Jenis Pembangkit
                                              <?php
                                                echo CHtml::dropDownList('JenisPembangkit' , '' , CHtml::listData(MasterJenisPembangkit::model()->findAll(array('order' => 'id ASC')) , 'id' , 'jenis_pembangkit') , 
                                                    array(
                                                        'class' => '',
                                                        'empty' => 'all',
                                                       'style' => 'width:100;',
                                                        'id' => 'jenis_pembangkit_id',
                                                        //'class' => 'form-control',
                                                        'ajax' => array(
                                                            'update' => '#pembangkit_id',
                                                            'type' => 'POST',
                                                            'url' => Yii::app()->createUrl("/bahanBakar/getPembangkit"),
                                                            'data' => array(
                                                                'jenis_pembangkit_id' => 'js:this.value'
                                                            )

                                                        )
                                                    )
                                                );
                                              ?>
                                        </td>

                                        <td style = 'font-size:12px;'>
                                            Pembangkit
                                            <?php
                                                echo CHtml::dropDownList('pembangkit' , '' , $comboPembangkit  , $optionsPembangkit);
                                            ?>

                                        </td>


                                        <td style = 'font-size:12px;'>
                                           Unit Pembangkit
                                            <?php
                                                echo CHtml::dropDownList('unit_pembangkit' , '' , $comboUnitPembangkit, $optionsUnitPembangkit);
                                                
                                            ?>

                                        </td>
                                         <td style = 'font-size:12px;' valign="bottom">
                                          <?php echo CHtml::link("<button style = 'width:50px;'>Tampil</button>" , "#" , array('id' => 'cmd_utama' , 'style' => 'text-decoration:none;margin-top:10px;')); ?>
                            
                                        </td>
                                    </tr>
                               </table> 
                              
                                
                            </div>
                        </div>
                       
						
					
						
                        <!--div class="trafo-panel-top" style="text-align: left;">
                            <div class="trafo-panel-input-top">
                                <input type="submit" value="Tampil" class="btn btn-primary">
                            </div>
                        </div-->
                    </div>


                    <div id="portlet">
                    	<p class="portlet-title"><u>Bahan Bakar Perhari</u></p>
                    </div>
                	<div id="beban-puncak">
                        <label style="padding-right:0px;">Bulan & Tahun</label>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'bulanHarian'>
                            <option value="">Select</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'tahunHarian'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn1" style = "height:27px;margin-top:1px;">Tampil</button>' , 
                        		 array('bahanBakar/harian'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perharian',
                        		 	'data' => array('bulan' => 'js:bulanHarian.value' , 'tahun' => 'js:tahunHarian.value'  , 'unit_pembangkit_id' => 'js:unit_pembangkit_id.value' , 'jenis_pembangkit_id' => 'js:jenis_pembangkit_id.value' , 'pembangkit_id' => 'js:pembangkit_id.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading2").show();
									}',
									'complete' => 'function(){
										$("#loading2").hide();
									}',
                        		 )

                        	);
                         ?>
                        <div id = 'loading2' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                        <div id = 'harian'></div>
                        <div class="export2" id = 'perharian'>
                    		<!-- Bar Chart -->
								<?php
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelHarian,'style' => 'font-size:16px;'),
										  'credits' => false ,
										  'xAxis' => array(
											 'categories' => $dataHarian,
											 
											 'labels' => array(
												'rotation' => 0,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 ),
											   'min' => 0 ,
									          'max' => 12
										  ),
										  'scrollbar' => array(
									      	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
										  'yAxis' => array(
											'min' => 0,
											 'title' => array('text' => 'Liter')
										  ),
										  'series' => array(
											 array('name' => 'HSD', 'data' => $hsd , 'type' => 'column'),
											 array('name' => 'MFO', 'data' => $mfo , 'type' => 'column'),
											 array('name' => 'OLEIN', 'data' => $olein , 'type' => 'column'),
											 array('name' => 'BATU BARA', 'data' => $batu_bara , 'type' => 'column'),
											
										  )
									   )
									));
								
								?>
                            
                            <!-- End Bar Chart -->
                    	</div>
                	</div>
                    
                	

                </div>
				
				<div id="textnya">
                	<div id="portlet">
                    	<p class="portlet-title"><u>Bahan Bakar Perbulan</u></p>
                    </div>
                	<div id="beban-puncak">
                	<?php
                	echo CHtml::beginForm();
                	?>
                        <label style="padding-right:0px;">Tahun</label>
						 
                         <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'tahunBulanan'>
                            <option value="">Select</option>
                        	<?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>

                        </select>
						 

                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn2" style = "height:27px;margin-top:1px;">Tampil</button>' , 
                        		 array('bahanBakar/bulanan'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#perbulanan',
                        		 	'data' => array('tahun' => 'js:tahunBulanan.value' , 'unit_pembangkit_id' => 'js:unit_pembangkit_id.value' , 'jenis_pembangkit_id' => 'js:jenis_pembangkit_id.value' , 'pembangkit_id' => 'js:pembangkit_id.value'),
                        		 	'beforeSend' => 'function(){
										$("#loading3").show();
									}',
									'complete' => 'function(){
										$("#loading3").hide();
									}',
                        		 )
								 

                        	);
                         ?>
                        <div id = 'loading3' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                       
                        <div  class = 'perbulanan' class="export2" id = 'perbulanan'>
                            <!-- Line Chart -->
							<?php
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelTahunan,'style' => 'font-size:16px;'),
										  'credits' => false ,
										  'xAxis' => array(
											 'categories' => $dataTahunan,
											 
											 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 ),
											//   'min' => 0 ,
									         // 'max' => 6
										  ),
										  'scrollbar' => array(
									      //	'enabled' => true
									       ),
									      
									      'rangeSelector' => array(
									      		'selected' => 1
									       ),
										  'yAxis' => array(
											'min' => 0,
											 'title' => array('text' => 'Liter')
										  ),
										  'series' => array(
											 array('name' => 'HSD', 'data' => $hsdTahunan , 'type' => 'column'),
											 array('name' => 'MFO', 'data' => $mfoTahunan , 'type' => 'column'),
											 array('name' => 'OLEIN', 'data' => $oleinTahunan , 'type' => 'column'),
											 array('name' => 'BATU BARA', 'data' => $batu_bara_tahunan , 'type' => 'column'),
											
										  )
									   )
									));
								
								?>
                            <!-- End Line Chart -->
                    	
						</div>
                    	<?php

                    	echo CHtml::endForm();
                    	?>
                	</div>
                    
                </div>



                <div id="textnya">
                	<div id="portlet">
                    	<p class="portlet-title"><u>Bahan Bakar Pertahun</u></p>
                    </div>
                	<div id="beban-puncak">
                	<?php
                	echo CHtml::beginForm();
                	?>
                         <label style="padding-right:0px;">Tahun</label>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'awal'>
                            <option value="">Select</option>
                            <?php
	                        	$year = date('Y');
	                        	for($a=2010;$a<=$year;$a++)
	                        	{
	                        		echo "<option value = '$a'>$a</option>";
	                        	}
                        	?>
                        </select>
                        <label style="padding-right:0px;">s.d</label>
                        <select class="form-control" style="height:27px;width:105px;font-size:12px;" id = 'akhir'>
                        	<option value="">Select</option>
                            <?php
                        	$year = date('Y');
                        	for($a=2010;$a<=$year;$a++)
                        	{
                        		echo "<option value = '$a'>$a</option>";
                        	}
                        	?>
                        </select>
						 

                        <?php
                        	echo CHtml::ajaxLink('<button id = "btn3" style = "height:27px;margin-top:1px;">Tampil</button>' , 
                        		 array('bahanBakar/range'),
                        		 array(
                        		 	'type' => 'POST',
                        		 	'update' => '#pertahunan',
                        		 	'data' => array('awal' => 'js:awal.value' , 'akhir' => 'js:akhir.value' , 'unit_pembangkit_id' => 'js:unit_pembangkit_id.value' , 'jenis_pembangkit_id' => 'js:jenis_pembangkit_id.value' , 'pembangkit_id' => 'js:pembangkit_id.value'),
                        		 	
                        		 	'beforeSend' => 'function(){
										$("#loading4").show();
									}',
									'complete' => 'function(){
										$("#loading4").hide();
									}',
                        		 )
								 

                        	);
                         ?>
                        <div id = 'loading4' style='display:none;'><img  src = "<?php echo Yii::app()->baseUrl."/images/loading.gif"; ?>" /></div>
                       
                        <div  class = 'pertahunan' class="export2" id = 'pertahunan'>
                            <!-- Line Chart -->
							<?php
									
									$this->Widget('ext.highcharts.HighchartsWidget', array(
									   'options'=>array(
										  'title' => array('text' => $labelRange,'style' => 'font-size:16px;'),
										  'credits' => false , 
										  'xAxis' => array(
											 'categories' => $listRange,
											 'labels' => array(
												'rotation' => 270,
												'y' => 40,
												//'style' => array('font-size' => '5px;')
											 ),
										//  'min' => 0 ,
									      //    'max' => 5
										  ),
										  'scrollbar' => array(
									      	'enabled' => true
									       ),
										  'yAxis' => array(
												'min' => 0,
											 'title' => array('text' => 'Liter')
										  ),
										   
										  'series' => array(
											 array('name' => 'HSD', 'data' => $hsdRange , 'type' => 'column'),
											 array('name' => 'MFO', 'data' => $mfoRange , 'type' => 'column'),
											 array('name' => 'OLEIN', 'data' => $oleinRange , 'type' => 'column'),
											  array('name' => 'BAHAN BAKAR', 'data' => $bahan_bakarRange , 'type' => 'column'),
										  )
									   )
									));

							?>
                            <!-- End Line Chart -->
                    	
						</div>
                    	<?php

                    	echo CHtml::endForm();
                    	?>
                	</div>
                    
                </div>
				
            </div>
            <!-- End Pertama -->
        </div>

        <script>
	$(function() {
  		var special = ['reveal', 'top', 'boring', 'perspective', 'extra-pop'];
  		// Toggle Nav on Click
  		$('#hearts a').click(function() {
			var delay_ = 300;
			checkWidth = $('#leftContent').css( "width");
			var widthnya="594";
			heightnya = "400";
			widthbaru = "594"
			widthnya5 = "267";
			widthnya6 = "565";
			LineCharts = "564";
			if( checkWidth < '186'){
			  $('#leftContent').animate( {"width":"186", "height":"100" + "%"}, delay_, "linear");
			  $('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			  $('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			  $('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			  $('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
			}else if(checkWidth > "0"){
			  $('#leftContent').animate({ "width":"0", "height":"0"}, delay_, "linear" );	
				widthnya = "780";
				heightnya = "500";
				widthnya5 = "325";
				widthnya6 = "750";
				LineCharts = "750";
			}
			$('#rightContent').animate({ "width": widthnya}, delay_, "linear" );
			$('.judul').animate({ "width": widthnya}, delay_, "linear" );
			$('.containerKanan').animate({ "width": widthnya}, delay_, "linear" );
			$('.trafo-panel-top').animate({ "width": widthnya5}, delay_, "linear" );
			$('.export2 img').animate({ "width": widthnya6}, delay_, "linear" );
			$('.line-charts').animate({ "width": LineCharts}, delay_, "linear" );
			$('.highcharts-container').animate({ "width": LineCharts}, delay_, "linear" );
  		});
	});
</script>