<?php
	class BahanBakarController extends Controller
	{
		public $layout = '//layouts/web/utama';

		public function idJenisPembangkit()
		{
			$db = Yii::app()->db;
			$sql = "SELECT id FROM master_jenis_pembangkit ORDER BY id ASC limit 1";
			$query = $db->createCommand($sql)->queryScalar();
			return $query;
		}


		public function actionGetPembangkit()
		{
			if(!empty($_POST['jenis_pembangkit_id']))
			{
				$criteria = new CDbCriteria;
				$criteria->condition = 'jenis_pembangkit_id=:id';
				$criteria->params = array(':id' => $_POST['jenis_pembangkit_id']);
			
				$data = MasterPembangkit::model()->findAll($criteria);
				
				$hasil = CHtml::listData($data , 'id' , 'pembangkit');
				echo "<option value = ''>all</optio>";
				foreach($hasil as $key => $value)
				{
					echo CHtml::tag('option' , array('value' => $key) , CHtml::encode($value));
				}
			
			}else{
				echo "<option value = ''>all</option>";
			}
		
		}

		public function actionGetUnitPembangkit()
		{
			if(!empty($_POST['pembangkit_id']))
			{
				$criteria = new CDbCriteria;
				$criteria->condition = 'pembangkit_id=:id';
				$criteria->params = array(':id' => $_POST['pembangkit_id']);
			
				$data = MasterUnitPembangkit::model()->findAll($criteria);
				
				$hasil = CHtml::listData($data , 'id' , 'unit_pembangkit');
				echo "<option value = ''>all</option>";
				foreach($hasil as $key => $value)
				{
					echo CHtml::tag('option' , array('value' => $key) , CHtml::encode($value));
				}
			
			
			}

		
		}

		/*public function comboPembangkit($params = "")
		{
			$db = Yii::app()->db;
			if(empty($params))
			{
				$criteria = new CDbCriteria;
				$criteria->condition = 'jenis_pembangkit_id=:jenis';
				$criteria->params = array(':jenis' => $this->idJenisPembangkit());
				$criteria->order = 'id ASC';
				$model = CHtml::listData(MasterPembangkit::model()->findAll($criteria), 'id' , 'pembangkit');
				return $model;
			}elseif($params == 'one'){
				$sql = "SELECT id FROM master_pembangkit WHERE jenis_pembangkit_id = '".$this->idJenisPembangkit()."' ORDER BY id ASC LIMIT 1";
				$query = $db->createCommand($sql)->queryScalar();
				return $query;
			}
		}


		public function comboUnitPembangkit($params = "")
		{
			$db = Yii::app()->db;
			if(empty($params))
			{
				$criteria = new CDbCriteria;
				$criteria->condition = 'pembangkit_id=:pembangkit';
				$criteria->params = array(':pembangkit' => $this->comboPembangkit('one'));
				$criteria->order = 'id ASC';
				$model = CHtml::listData(MasterUnitPembangkit::model()->findAll($criteria), 'id' , 'unit_pembangkit');
				return $model;
			}elseif($params == 'one'){
				$sql = "SELECT id FROM master_unit_pembangkit WHERE pembangkit_id = '".$this->comboPembangkit('one')."' ORDER BY id ASC LIMIT 1";
				$query = $db->createCommand($sql)->queryScalar();
				return $query;
			}
		}
		*/
		public function actionIndex()
		{

			$date = date("Y-m-d");
			$day = date("d");
			$lastDay = date('t');
			$month = date("m");
			$year = date("Y");
			$awal = 2010;
			$labelRange = "$awal - $year";
			$jenis_pembangkit_id = "";
			$pembangkit_id = "";
			$unit_pembangkit = "";

			$labelHarian = substr(ar::waktuYii("$year-$month-1" , "long" ,"") , 1, 100);
			
			$this->render('index' ,
				array(
					'comboPembangkit' => array(),
					'comboUnitPembangkit' => array(),
					'labelHarian' => $labelHarian,
					
					'dataHarian' => ar::getDataHarian($lastDay),
					'hsd' => BahanBakar::model()->getDataHarianValue($year , $month , $lastDay , $jenis_pembangkit_id , $pembangkit_id , $unit_pembangkit , 'hsd'),
					'mfo' => BahanBakar::model()->getDataHarianValue($year , $month , $lastDay , $jenis_pembangkit_id , $pembangkit_id , $unit_pembangkit , 'mfo'),
					'olein' => BahanBakar::model()->getDataHarianValue($year , $month , $lastDay , $jenis_pembangkit_id , $pembangkit_id , $unit_pembangkit , 'olein'),
					'batu_bara' => BahanBakar::model()->getDataHarianValue($year , $month , $lastDay , $jenis_pembangkit_id , $pembangkit_id , $unit_pembangkit , 'batu_bara'),
					
					'labelTahunan' => $year,
					'dataTahunan' => ar::getDataBulananAja(3),
					'hsdTahunan' => BahanBakar::model()->getDataBulananValue($year  , $jenis_pembangkit_id , $pembangkit_id ,  $unit_pembangkit , 'hsd'),
					'mfoTahunan' => BahanBakar::model()->getDataBulananValue($year , $jenis_pembangkit_id , $pembangkit_id ,  $unit_pembangkit , 'mfo'),
					'oleinTahunan' => BahanBakar::model()->getDataBulananValue($year , $jenis_pembangkit_id , $pembangkit_id , $unit_pembangkit , 'olein'),
					'batu_bara_tahunan' => BahanBakar::model()->getDataBulananValue($year , $jenis_pembangkit_id , $pembangkit_id , $unit_pembangkit , 'batu_bara'),
					
					'labelRange' => $labelRange,
					'listRange' => ar::getDataTahunan($awal , $year),
					'hsdRange' => BahanBakar::model()->getDataRangeValue($awal , $year, $jenis_pembangkit_id , $pembangkit_id ,  $unit_pembangkit , 'hsd'),
					'mfoRange' => BahanBakar::model()->getDataRangeValue($awal , $year, $jenis_pembangkit_id , $pembangkit_id ,  $unit_pembangkit , 'mfo'),
					'oleinRange' => BahanBakar::model()->getDataRangeValue($awal , $year, $jenis_pembangkit_id , $pembangkit_id ,  $unit_pembangkit , 'olein'),
					'bahan_bakarRange' => BahanBakar::model()->getDataRangeValue($awal , $year, $jenis_pembangkit_id , $pembangkit_id ,  $unit_pembangkit , 'batu_bara'),
				
				)
			);
		}

		public function actionHarian()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
			(!empty($_POST['bulan'])) ? $month = $_POST['bulan'] : $month = date("m");

			$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$labelHarian = substr(ar::waktuYii("$year-$month-$lastDay" , "long" ,"") , 2, 100);
			$jenis_pembangkit_id = $_POST['jenis_pembangkit_id'];
			$pembangkit_id = $_POST['pembangkit_id'];
			$unit_pembangkit = $_POST['unit_pembangkit_id'];

			$this->renderpartial(
				'harian' , 
					array(
					'labelHarian' => $labelHarian,
					'dataHarian' => ar::getDataHarian($lastDay),
					'hsd' => BahanBakar::model()->getDataHarianValue($year , $month , $lastDay , $jenis_pembangkit_id , $pembangkit_id , $unit_pembangkit , 'hsd'),
					'mfo' => BahanBakar::model()->getDataHarianValue($year , $month , $lastDay , $jenis_pembangkit_id , $pembangkit_id , $unit_pembangkit , 'mfo'),
					'olein' => BahanBakar::model()->getDataHarianValue($year , $month , $lastDay , $jenis_pembangkit_id , $pembangkit_id , $unit_pembangkit , 'olein'),
					'batu_bara' => BahanBakar::model()->getDataHarianValue($year , $month , $lastDay , $jenis_pembangkit_id , $pembangkit_id , $unit_pembangkit , 'batu_bara'),
					) , false , true
			);
		}

		public function actionBulanan()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
			$jenis_pembangkit_id = $_POST['jenis_pembangkit_id'];
			$pembangkit_id = $_POST['pembangkit_id'];
			$unit_pembangkit = $_POST['unit_pembangkit_id'];
			$this->renderpartial(
				'bulanan',
				array(
					'labelTahunan' => $year,
					'dataTahunan' => ar::getDataBulananAja(3),
					'hsdTahunan' => BahanBakar::model()->getDataBulananValue($year  , $jenis_pembangkit_id , $pembangkit_id ,  $unit_pembangkit , 'hsd'),
					'mfoTahunan' => BahanBakar::model()->getDataBulananValue($year , $jenis_pembangkit_id , $pembangkit_id ,  $unit_pembangkit , 'mfo'),
					'oleinTahunan' => BahanBakar::model()->getDataBulananValue($year , $jenis_pembangkit_id , $pembangkit_id , $unit_pembangkit , 'olein'),
					'batu_bara_tahunan' => BahanBakar::model()->getDataBulananValue($year , $jenis_pembangkit_id , $pembangkit_id , $unit_pembangkit , 'batu_bara'),
				),false , true
			);
		}

		public function actionRange()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			(!empty($_POST['awal'])) ? $awal = $_POST['awal'] : $awal = 2010;
			(!empty($_POST['akhir'])) ? $year = $_POST['akhir'] : $year = date("Y");
			
			$jenis_pembangkit_id = $_POST['jenis_pembangkit_id'];
			$pembangkit_id = $_POST['pembangkit_id'];
			$unit_pembangkit = $_POST['unit_pembangkit_id'];
			$labelRange = "$awal - $year";
			$this->renderpartial(
				'range' , 
				array(
					'labelRange' => $labelRange,
					'listRange' => ar::getDataTahunan($awal , $year),
					'hsdRange' => BahanBakar::model()->getDataRangeValue($awal , $year, $jenis_pembangkit_id , $pembangkit_id ,  $unit_pembangkit , 'hsd'),
					'mfoRange' => BahanBakar::model()->getDataRangeValue($awal , $year, $jenis_pembangkit_id , $pembangkit_id ,  $unit_pembangkit , 'mfo'),
					'oleinRange' => BahanBakar::model()->getDataRangeValue($awal , $year, $jenis_pembangkit_id , $pembangkit_id ,  $unit_pembangkit , 'olein'),
					'bahan_bakarRange' => BahanBakar::model()->getDataRangeValue($awal , $year, $jenis_pembangkit_id , $pembangkit_id ,  $unit_pembangkit , 'batu_bara'),
				
				),false,true
			);
		}

	}