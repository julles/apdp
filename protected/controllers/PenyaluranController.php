<?php
	class PenyaluranController extends Controller
	{
		public $layout = '//layouts/web/utama';
		
		public function actionPembebananTrafo()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			$date = date("Y-m-d");
			$month = date("m");
			$year = date("Y");
			$hariIni = ar::formatWaktu($date , "full" , "");
			//beban trafo perjam
				
				$hariIni = ar::formatWaktu($date , "full" , "");
				$tampil_jam = ar::tampilJam();
				$hasilJam = PembebananTrafo::model()->getDataJam($date, "" , "");
			//

			//beban trafo harian
				$exDate = explode(" " ,  $hariIni);
				$labelBulanTahun = $exDate[2]." ".$exDate[3];
				$last = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
				$tampilHari = ar::getDataHarian($last);
				$hasilHari = PembebananTrafo::model()->getDataHarian($month , $year , "" ,"");
				//print_r($hasilHari);
			//
			

			//beban trafo bulanan
				$labelBulan = $year;
				$tampilBulan = ar::getDataBulanan($year);
			    $hasilBulan = PembebananTrafo::model()->getDataBulanan($year , "" , "");
			
			//

			 //beban trafo tahunan
			    $awal = $year-5;
			    $akhir = $year;
			    $labelTahun = $awal." S/D ".$akhir;
			    $tampilTahun = ar::getDataTahunan($awal , $akhir);
			    $hasilTahunan = PembebananTrafo::model()->getDataTahunan($awal , $akhir , "" , "");
			   // print_r($hasilTahunan);
			  //

			
			$this->render('index_pembebanan_trafo' , 
				array(
					'hariIni' => $hariIni,
					'tampil_jam' => $tampil_jam,
					'hasilJam' => $hasilJam,

					'labelBulanTahun' => $labelBulanTahun,
					'tampilHari' => $tampilHari,
					'hasilHari' => $hasilHari,

					'labelBulan' => $labelBulan,
					'tampilBulan' =>$tampilBulan ,
					'hasilBulan' =>$hasilBulan ,

					'labelTahun' => $labelTahun,
					'tampilTahun' => $tampilTahun,
					'hasilTahunan' => $hasilTahunan
					
				) , 
				false , 
				true
			
			);
		}
		
		public function actionPembebananTrafoDetailJam()
		{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				//beban trafo perjam
				if(empty($_POST['tanggalJam'])) 
				{
					$date = date("Y-m-d"); 
				}else{
					$date = $_POST['tanggalJam'];
				}

				$hariIni = ar::formatWaktu($date , "full" , "");
				$tampil_jam = ar::tampilJam();
				//print_r($hasilJam = PembebananTrafo::model()->getDataJam($date , $_POST['gardu_induk_id'] ,$_POST['trafo_id']));
				$hasilJam = PembebananTrafo::model()->getDataJam($date , $_POST['gardu_induk_id'] ,$_POST['trafo_id']);
				
			//
			
			
			$this->renderpartial('detail_pembebanan_trafo_jam' , 
				array(
					'hariIni' => $hariIni,
					'tampil_jam' => $tampil_jam,
					'hasilJam' => $hasilJam,
						
					
				) , 
				false , 
				true
			
			);
				
				
		}

		public function actionPembebananTrafoDetailHari()
		{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				
				(empty($_POST['bulan'])) ? $month = date("m") : $month = $_POST['bulan'];
				(empty($_POST['tahun'])) ? $year = date("Y") : $year = $_POST['tahun'];
				//echo $_POST['gardu_induk_id'];
				
				 $hariIni =  date("F", mktime(0, 0, 0, $month, 10))." ".$year;

				//beban trafo harian
				
				$labelBulanTahun = $hariIni;
				$last = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
				$tampilHari = ar::getDataHarian($last);
				$hasilHari = PembebananTrafo::model()->getDataHarian($month , $year , $_POST['gardu_induk_id'] ,$_POST['trafo_id']);
				
				$this->renderpartial('detail_pembebanan_trafo_hari' , 
				array(
					'labelBulanTahun' => $labelBulanTahun,
					'tampilHari' => $tampilHari,
					'hasilHari' => $hasilHari,

					
					
				) , 
				false , 
				true
			
			);

			//
		}

		public function actionPembebananTrafoDetailBulan()
		{	
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			(empty($_POST['tahun'])) ? $year = date("Y") : $year = $_POST['tahun'];

			//beban trafo bulanan
				$labelBulan = $year;
				$tampilBulan = ar::getDataBulanan($year);
			   $hasilBulan = PembebananTrafo::model()->getDataBulanan($year , $_POST['gardu_induk_id'] , $_POST['trafo_id']);
			//

			  $this->renderpartial('detail_pembebanan_trafo_bulan' , array(
			  		'labelBulan' => $labelBulan,
					'tampilBulan' =>$tampilBulan ,
					'hasilBulan' =>$hasilBulan ,
			  )
			  , false ,true
			  );
		}

		public function actionPembebananTrafoDetailTahun()
		{
			 //beban trafo tahunan
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				(empty($_POST['awal'])) ? $awal = date("Y") - 5 : $awal = $_POST['awal'];
				(empty($_POST['akhir'])) ? $akhir = date("Y") : $akhir = $_POST['akhir'];
			    $labelTahun = $awal." S/D ".$akhir;
			    $tampilTahun = ar::getDataTahunan($awal , $akhir);
			    $hasilTahunan = PembebananTrafo::model()->getDataTahunan($awal , $akhir , $_POST['gardu_induk_id'] , $_POST['trafo_id']);
			 //   print_r($hasilTahunan);
			  

			    $this->renderpartial('detail_pembebanan_trafo_tahun' , array(
			  		'labelTahun' => $labelTahun,
					'tampilTahun' =>$tampilTahun ,
					'hasilTahunan' =>$hasilTahunan ,
			  )
			  , false ,true
			  );
		}

		public function actionGetTrafo()
		{
			$data = MasterTrafo::model()->findAll("gardu_induk_id=:gardu_induk_id" , array(':gardu_induk_id' => $_POST['gardu_induk_id']));
			$data = CHtml::listdata($data , 'id' ,'trafo');
			echo "<option value=''>All</option>";
			foreach($data as $key => $value)
			{
				echo CHtml::tag('option' , array('value' => $key) , CHtml::encode($value) , true);
			}
		}

		public function actionGetTransmisi()
		{
			$data = MasterTransmisi::model()->findAll("gardu_induk_id=:gardu_induk_id" , array(':gardu_induk_id' => $_POST['gardu_induk_id']));
			$data = CHtml::listdata($data , 'id' ,'transmisi');
			echo "<option value=''>All</option>";
			foreach($data as $key => $value)
			{
				echo CHtml::tag('option' , array('value' => $key) , CHtml::encode($value) , true);
			}
		}
		
		public function actionPembebananTransmisi()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			$date = date("Y-m-d");
			$month = date("m");
			$year = date("Y");
			$hariIni = ar::formatWaktu($date , "full" , "");
			//beban trafo perjam
				
				$hariIni = ar::formatWaktu($date , "full" , "");
				$tampil_jam = ar::tampilJam();
				$hasilJam = PembebananTransmisi::model()->getDataJam($date, "" , "");
			//

			//beban trafo harian
				$exDate = explode(" " ,  $hariIni);
				$labelBulanTahun = $exDate[2]." ".$exDate[3];
				$last = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
				$tampilHari = ar::getDataHarian($last);
				$hasilHari = PembebananTransmisi::model()->getDataHarian($month , $year , "" ,"");
				//print_r($hasilHari);
			//
			

			//beban trafo bulanan
				$labelBulan = $year;
				$tampilBulan = ar::getDataBulanan($year);
			    $hasilBulan = PembebananTransmisi::model()->getDataBulanan($year , "" , "");
			
			//

			 //beban trafo tahunan
			    $awal = $year-5;
			    $akhir = $year;
			    $labelTahun = $awal." S/D ".$akhir;
			    $tampilTahun = ar::getDataTahunan($awal , $akhir);
			    $hasilTahunan = PembebananTransmisi::model()->getDataTahunan($awal , $akhir , "" , "");
			   // print_r($hasilTahunan);
			  //

			
			$this->render('index_pembebanan_transmisi' , 
				array(
					'hariIni' => $hariIni,
					'tampil_jam' => $tampil_jam,
					'hasilJam' => $hasilJam,

					'labelBulanTahun' => $labelBulanTahun,
					'tampilHari' => $tampilHari,
					'hasilHari' => $hasilHari,

					'labelBulan' => $labelBulan,
					'tampilBulan' =>$tampilBulan ,
					'hasilBulan' =>$hasilBulan ,

					'labelTahun' => $labelTahun,
					'tampilTahun' => $tampilTahun,
					'hasilTahunan' => $hasilTahunan
					
				) , 
				false , 
				true
			
			);
		}


		public function actionPembebananTransmisiDetailJam()
		{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				//beban trafo perjam
				if(empty($_POST['tanggalJam'])) 
					{
						$date = date("Y-m-d"); 
					}else{
						$date = $_POST['tanggalJam'];
					}

				$hariIni = ar::formatWaktu($date , "full" , "");
				$tampil_jam = ar::tampilJam();
				//print_r($hasilJam = PembebananTrafo::model()->getDataJam($date , $_POST['gardu_induk_id'] ,$_POST['trafo_id']));
				$hasilJam = PembebananTransmisi::model()->getDataJam($date , $_POST['gardu_induk_id'] ,$_POST['transmisi_id']);
				
			//
			
			
			$this->renderpartial('detail_pembebanan_transmisi_jam' , 
				array(
					'hariIni' => $hariIni,
					'tampil_jam' => $tampil_jam,
					'hasilJam' => $hasilJam,
						
					
				) , 
				false , 
				true
			
			);
				
				
		}


		public function actionPembebananTransmisiDetailHari()
		{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				
				(empty($_POST['bulan'])) ? $month = date("m") : $month = $_POST['bulan'];
				(empty($_POST['tahun'])) ? $year = date("Y") : $year = $_POST['tahun'];
				//echo $_POST['gardu_induk_id'];
				
				 $hariIni =  date("F", mktime(0, 0, 0, $month, 10))." ".$year;

				//beban trafo harian
				
				$labelBulanTahun = $hariIni;
				$last = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
				$tampilHari = ar::getDataHarian($last);
				$hasilHari = PembebananTransmisi::model()->getDataHarian($month , $year , $_POST['gardu_induk_id'] ,$_POST['transmisi_id']);
				
				$this->renderpartial('detail_pembebanan_transmisi_hari' , 
				array(
					'labelBulanTahun' => $labelBulanTahun,
					'tampilHari' => $tampilHari,
					'hasilHari' => $hasilHari,

					
					
				) , 
				false , 
				true
			
			);

			//
		}

		public function actionPembebananTransmisiDetailBulan()
		{	
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			(empty($_POST['tahun'])) ? $year = date("Y") : $year = $_POST['tahun'];

			//beban trafo bulanan
				$labelBulan = $year;
				$tampilBulan = ar::getDataBulanan($year);
			   $hasilBulan = PembebananTransmisi::model()->getDataBulanan($year , $_POST['gardu_induk_id'] , $_POST['transmisi_id']);
			//

			  $this->renderpartial('detail_pembebanan_transmisi_bulan' , array(
			  		'labelBulan' => $labelBulan,
					'tampilBulan' =>$tampilBulan ,
					'hasilBulan' =>$hasilBulan ,
			  )
			  , false ,true
			  );
		}


		public function actionPembebananTransmisiDetailTahun()
		{
			 //beban trafo tahunan
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				(empty($_POST['awal'])) ? $awal = date("Y") - 5 : $awal = $_POST['awal'];
					(empty($_POST['akhir'])) ? $akhir = date("Y") : $akhir = $_POST['akhir'];
			    $labelTahun = $awal." S/D ".$akhir;
			    $tampilTahun = ar::getDataTahunan($awal , $akhir);
			    $hasilTahunan = PembebananTransmisi::model()->getDataTahunan($awal , $akhir , $_POST['gardu_induk_id'] , $_POST['transmisi_id']);
			  

			    $this->renderpartial('detail_pembebanan_transmisi_tahun' , array(
			  		'labelTahun' => $labelTahun,
					'tampilTahun' =>$tampilTahun ,
					'hasilTahunan' =>$hasilTahunan ,
			  )
			  , false ,true
			  );
		}
		
	}