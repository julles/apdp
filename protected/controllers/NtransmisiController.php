<?php
	class NtransmisiController extends Controller
	{
		public $layout = '//layouts/web/utama';

		public function actionIndex()
		{
			$date = date("Y-m-d");
			$day = date("d");
			$lastDay = date('t');
			$month = date("m");
			$year = date("Y");
			
			$awal = $year-4;
			$labelTahunan = "$awal - $year";
			$transmisi = $this->transmisiPatok($this->garduPatok());
			//perjam

			//

			$this->render('index' , 
				array(
					'date' => $date,
					'listJam' =>  TransPembebananTransmisiDetail::model()->listJam($date),
					'nilaiPerjam' => TransPembebananTransmisiDetail::model()->valueJam($date , $this->garduPatok() , $transmisi),
				
					'listHari' => TransPembebananTransmisiDetail::model()->getDataHarian($lastDay),
					'valueHari' => TransPembebananTransmisiDetail::model()->getDataHarianValue($month , $year , $lastDay , $this->garduPatok() , $transmisi),
				
					'year' => $year,
					'listBulan' => ar::getDataBulananAja(3),
					'valueBulan' => TransPembebananTransmisiDetail::model()->getDataBulananValue($year , $this->garduPatok() , $transmisi),
					
					'labelTahunan' => $labelTahunan,
					'listTahun' => ar::getDataTahunan($awal , $year),
					'valueTahun' => TransPembebananTransmisiDetail::model()->getDataTahunanValue($awal , $year , $this->garduPatok() , $transmisi)

				)
			);
		}


		public function actionAjaxJam()
		{	
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				(!empty($_POST['tanggalJam'])) ? $date = $_POST['tanggalJam'] : $date = date("Y-m-d");
				$db = Yii::app()->db;
				$gardu = $_POST['gardu_induk_id'];
				$transmisi = $_POST['transmisi_id']; 

				
					$this->renderpartial('ajax_perjam' , array(
						'date' => $date,
						'listJam' =>  TransPembebananTransmisiDetail::model()->listJam($date),
						'nilaiPerjam' => TransPembebananTransmisiDetail::model()->valueJam($date , $gardu , $transmisi),
						'listJam' =>  TransPembebananTransmisiDetail::model()->listJam($date)
					), false ,true);
				
		}

		public function actionAjaxHarian()
		{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			
				(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
				(!empty($_POST['bulan'])) ? $month = $_POST['bulan'] : $month = date("m");
				$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				$gardu = $_POST['gardu_induk_id'];
				$transmisi = $_POST['transmisi_id']; 
				$this->renderpartial('ajax_harian' ,
					array(
						'date' => "$year-$month-1",
						'listHari' => TransPembebananTransmisiDetail::model()->getDataHarian($lastDay),
						'valueHari' => TransPembebananTransmisiDetail::model()->getDataHarianValue($month , $year , $lastDay , $gardu , $transmisi),
					
					) , false , true
				);
		}

		public function actionAjaxBulanan()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			
			(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
			$gardu = $_POST['gardu_induk_id'];
			$transmisi = $_POST['transmisi_id']; 
			$this->renderpartial("ajax_bulanan" , array(
					'year' => $year,
					'listBulan' => ar::getDataBulananAja(3),
					'valueBulan' => TransPembebananTransmisiDetail::model()->getDataBulananValue($year , $gardu , $transmisi),
					
				) , false , true
			);
		}

		public function actionAjaxTahunan()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			(!empty($_POST['tahun'])) ? $akhir = $_POST['tahun'] : $akhir = date("Y");
			$awal = $akhir-4;
			$labelTahunan = "$awal - $akhir";
			$gardu = $_POST['gardu_induk_id'];
			$transmisi = $_POST['transmisi_id']; 

			$this->renderpartial('ajax_tahunan' , 
				array(
					'labelTahunan' => $labelTahunan,
					'listTahun' => ar::getDataTahunan($awal , $akhir),
					'valueTahun' => TransPembebananTransmisiDetail::model()->getDataTahunanValue($awal , $akhir , $gardu , $transmisi)

				) , false , true
			);
		}

		public function garduPatok()
		{
			$db = Yii::app()->db;
			return $db->createCommand("SELECT id FROM master_gardu_induk order by id asc limit 1")->queryScalar(); 
		}

		public function transmisiPatok($id)
		{
			$db = Yii::app()->db;
			return $db->createCommand("SELECT id FROM master_transmisi WHERE gardu_induk_id = '$id' ORDER BY id asc LIMIT 1")->queryScalar();
		}

		public function actionGetTransmisi()
		{
			$data = MasterTransmisi::model()->findAll("gardu_induk_id=:gardu_induk_id" , array(':gardu_induk_id' => $_POST['gardu_induk_id']));
			$data = CHtml::listdata($data , 'id' ,'transmisi');
			foreach($data as $key => $value)
			{
				echo CHtml::tag('option' , array('value' => $key) , CHtml::encode($value) , true);
			}
		}

		/*public function actionChart()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			$date =  $_POST['tanggalJam'];
			(empty($date)) ? $date = date("Y-m-d") : $date = $date;
			$ex = explode("-",$date);
			$day = $ex[2];
		
			$month = $ex[1];
			$year = $ex[0];
			$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$awal = $year-10;
			$labelTahunan = "$awal - $year";
			$gardu = $_POST['gardu_induk_id'];
			$transmisi = $_POST['transmisi_id']; 




			$this->renderpartial('ajax' , 
				array(
					'date' => $date,
					'listJam' =>  TransPembebananTrafoDetail::model()->listJam($date),
					'nilaiPerjam' => TransPembebananTrafoDetail::model()->valueJam($date , $gardu , $transmisi),

					'listHari' => TransPembebananTrafoDetail::model()->getDataHarian($lastDay),
					'valueHari' => TransPembebananTrafoDetail::model()->getDataHarianValue($month , $year , $lastDay , $this->garduPatok() , $transmisi),
					
					'year' => $year,
					'listBulan' => ar::getDataBulananAja(),
					'valueBulan' => TransPembebananTrafoDetail::model()->getDataBulananValue($year , $gardu , $transmisi),
					
					'labelTahunan' => $labelTahunan,
					'listTahun' => ar::getDataTahunan($awal , $year),
					'valueTahun' => TransPembebananTrafoDetail::model()->getDataTahunanValue($awal , $year , $this->garduPatok() , $transmisi)

				) , false , true
			);
		}*/
	}