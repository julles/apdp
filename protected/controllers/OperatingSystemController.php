<?php
	class OperatingSystemController extends Controller
	{
			public $layout = '//layouts/web/utama';


			public function actionGetBusbar()
			{
			
				$data = MasterBusbar::model()->findAll("gardu_hubung_id=:gardu_hubung_id" , array(':gardu_hubung_id' => $_POST['gardu_hubung_id']));
				$data = CHtml::listdata($data , 'id' ,'busbar');
				echo "<option value=''></option>";
				foreach($data as $key => $value)
				{
					echo CHtml::tag('option' , array('value' => $key) , CHtml::encode($value) , true);
				}
			}


			public function actionBebanPuncak()
			{

				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				
				//set variable to view
				$date = date("Y-m-d");
				$tampil_jam = ar::tampilJam();
				$hasilJam = BebanPuncak::model()->getDataJam(date("d-m-Y"));
				$hariIni = ar::formatWaktu($date , "full" , "");
				
					//harian
						
						$tgl = BebanPuncak::model()->getDataHarian(date('t'));
						$modelHarian = BebanPuncak::model()->getDataHarianValue("" , date('m') , date('Y') , date('t'));
						$hariIniHarian = explode(" " ,ar::formatWaktu(date("Y-m") , "full" , ""));
						$hariIniHarian = $hariIniHarian[2]." ".$hariIniHarian[3];
					//
					
					//bulanan
						$bulan = BebanPuncak::model()->getDataBulanan(date('Y')); 
						$hasilBulan = BebanPuncak::model()->getDataBulananValue(date('Y'));
						$titleBulan = "Beban Puncak ".date('Y');
					//
						
						
					//tahunan
						$akhir = date('Y');
						$awal = $akhir-5;
						$tahun = BebanPuncak::model()->getDataTahunan($awal , $akhir);
						$hasilTahun = BebanPuncak::model()->getDataTahunanValue($awal,$akhir);
						
					//
				
				//

				//render view
				$this->render('index_beban_puncak' , 
						array(
								'tampil_jam' => $tampil_jam,
								'hasilJam' => $hasilJam,
								'hariIni' => $hariIni,
								
								'hariIniHarian' => $hariIniHarian,
								'tgl' => $tgl,	
								'modelHarian' => $modelHarian,
								
								'bulan' => $bulan,
								'titleBulan' => $titleBulan,
								'hasilBulan' => $hasilBulan,
								
								'tahun' => $tahun,
								'hasilTahun' => $hasilTahun
						)
				);
				//
			}



			public function actionBebanPuncakDetailJam()
			{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				if(empty($_POST['tanggalJam'])) 
				{
					$date = date("Y-m-d"); 
				}else{
					$date = $_POST['tanggalJam'];
				}
				//$date = $_POST['tanggalJam'];
				
				//set variable to view
				$tampil_jam = ar::tampilJam();
				$hasilJam = BebanPuncak::model()->getDataJam($date);
				$hariIni = ar::formatWaktu($date , "full" , "");
				//

				//render view
				$this->renderpartial('detail_beban_puncak_jam' , 
						array(
								'tampil_jam' => $tampil_jam,
								'hasilJam' => $hasilJam,
								'hariIni' => $hariIni
						) , 
						false ,
						true
				);
				//
			}

			public function actionBebanPuncakDetailHarian()
			{

					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(empty($_POST['bulan'])) ? $bulan = date("m") : $bulan = $_POST['bulan'];
					(empty($_POST['tahun'])) ? $tahun = date("Y") : $tahun = $_POST['tahun'];
					//	
						$date = $tahun."-".$bulan;
						
						$last = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun); 
						
						$hariIniHarian = explode(" " ,ar::formatWaktu($date , "full" , ""));
						$hariIniHarian = $hariIniHarian[2]." ".$hariIniHarian[3];
						$tgl = BebanPuncak::model()->getDataHarian($last);
						
						$modelHarian = BebanPuncak::model()->getDataHarianValue("" , $bulan, $tahun , date($last));
					//

						$this->renderpartial('detail_beban_puncak_harian' , 
								array(
										'hariIniHarian' => $hariIniHarian,
										'tgl' => $tgl,
										'modelHarian' => $modelHarian
								) ,
								false,
								true
						);


			}
			
			public function actionBebanPuncakDetailBulanan()
			{
				
					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(empty($_POST['tahun'])) ? $tahun = date("Y") : $tahun = $_POST['tahun'];
					//bulanan
						$bulan = BebanPuncak::model()->getDataBulanan($tahun); 
						$hasilBulan = BebanPuncak::model()->getDataBulananValue($tahun);
						$titleBulan = "Beban Puncak ".$tahun;
					//
					
					$this->renderpartial('detail_beban_puncak_bulanan' , 
								array(
										'bulan' => $bulan,
										'hasilBulan' => $hasilBulan,
										'titleBulan' => $titleBulan
								) ,
								false,
								true
						);
			}
			
			
			
			public function actionBebanPuncakDetailTahunan()
			{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				(empty($_POST['awal'])) ? $awal = date("Y") - 5 : $awal = $_POST['awal'];
				(empty($_POST['akhir'])) ? $akhir = date("Y") : $akhir = $_POST['akhir'];
				//tahunan
				//	$awal = $_POST['awal'];
				//	$akhir = $_POST['akhir'];



					$tahun = BebanPuncak::model()->getDataTahunan($awal , $akhir);
					$hasilTahun = BebanPuncak::model()->getDataTahunanValue($awal,$akhir);
					$min = $akhir-$awal;
					($min >= 5) ? $max =5 : $max = 	$min;
				
				//
				$this->renderpartial('detail_beban_puncak_tahunan' , 
								array(
										'tahun' => $tahun,
										'hasilTahun' => $hasilTahun,
										'max' => $max
										
								) ,
								false,
								true
						);
			}


			public function actionKondisiKelistrikan()
			{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				$date = date("Y-m-d");
				$hariIni = ar::formatWaktu($date , "full" , "");
				//harian
					
					$tgl = KondisiKelistrikan::model()->getDataHarian(date('t'));
					$hariIniHarian = explode(" " ,ar::formatWaktu(date("Y-m") , "full" , ""));
					$hariIniHarian = $hariIniHarian[2]." ".$hariIniHarian[3];
					$modelHarian = KondisiKelistrikan::model()->getDataHarianValue("" , date('m') , date('Y') , date('t'));
				//

				 	//bulanan
						$bulan = KondisiKelistrikan::model()->getDataBulanan(date('Y')); 
						$hasilBulanNormal = KondisiKelistrikan::model()->getDataBulananValue(date('Y') , 'Normal');
						$hasilBulanSiaga = KondisiKelistrikan::model()->getDataBulananValue(date('Y') , 'Siaga');
						$hasilBulanDefisit = KondisiKelistrikan::model()->getDataBulananValue(date('Y') , 'Defisit');
						$titleBulan = "Kondisi Kelistrikan ".date('Y');
					//

					 //tahunan
						$akhir = date('Y');
						$awal = $akhir-4;
						$tahun = KondisiKelistrikan::model()->getDataTahunan($awal , $akhir);
						$Normal = KondisiKelistrikan::model()->getDataTahunanValue($awal,$akhir , "Normal");
						$Siaga = KondisiKelistrikan::model()->getDataTahunanValue($awal,$akhir , "Siaga");
						$Defisit = KondisiKelistrikan::model()->getDataTahunanValue($awal,$akhir , "Defisit");
						
						$hasilTahun = array(
								array('name' => 'Normal', 'data' => $Normal  ,'type' => 'column'),
								array('name' => 'Siaga', 'data' => $Siaga  ,'type' => 'column'),
								array('name' => 'Defisit', 'data' => $Defisit  ,'type' => 'column'),
						);
					
						
					//

				$this->render('index_kondisi_kelistrikan' , 
						array(

							'hariIniHarian' => $hariIniHarian,
							'tgl' => $tgl,
							'modelHarian' => $modelHarian,

							'bulan' => $bulan,
							'titleBulan' => $titleBulan,
							'hasilBulanNormal' => $hasilBulanNormal,
							'hasilBulanSiaga' => $hasilBulanSiaga,
							'hasilBulanDefisit' => $hasilBulanDefisit,

							'tahun' => $tahun,
							'hasilTahun' => $hasilTahun

						), false , true
				);
			}



			public function actionKondisiKelistrikanDetailHarian()
			{

					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(empty($_POST['bulan'])) ? $bulan = date("m") : $bulan = $_POST['bulan'];
					(empty($_POST['tahun'])) ? $tahun = date("Y") : $tahun = $_POST['tahun'];
					//	
						$date = $tahun."-".$bulan;
						
						$last = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun); 
						
						$hariIniHarian = explode(" " ,ar::formatWaktu($date , "full" , ""));
						$hariIniHarian = $hariIniHarian[2]." ".$hariIniHarian[3];
						$tgl = BebanPuncak::model()->getDataHarian($last);
						
						$modelHarian = KondisiKelistrikan::model()->getDataHarianValue("" , $bulan , $tahun , date($last));
					//

						$this->renderpartial('detail_kondisi_kelistrikan_harian' , 
								array(
										'hariIniHarian' => $hariIniHarian,
										'tgl' => $tgl,
										'modelHarian' => $modelHarian
								) ,
								false,
								true
						);


			}

			public function actionKondisiKelistrikanDetailBulanan()
			{
					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(empty($_POST['tahun'])) ? $y = date("Y") : $y = $_POST['tahun'];
					//bulanan
						$bulan = KondisiKelistrikan::model()->getDataBulanan($y); 
						$hasilBulanNormal = KondisiKelistrikan::model()->getDataBulananValue(date($y) , 'Normal');
						$hasilBulanSiaga = KondisiKelistrikan::model()->getDataBulananValue(date($y) , 'Siaga');
						$hasilBulanDefisit = KondisiKelistrikan::model()->getDataBulananValue(date($y) , 'Defisit');
						$titleBulan = "Kondisi Kelistrikan ".date('Y');
					//

					    $this->renderpartial('detail_kondisi_kelistrikan_bulanan' , 
								array(
										'bulan' => $bulan,
										'titleBulan' => $titleBulan,
										'hasilBulanNormal' => $hasilBulanNormal,
										'hasilBulanSiaga' => $hasilBulanSiaga,
										'hasilBulanDefisit' => $hasilBulanDefisit,
								) ,
								false,
								true
						);

			}


			public function actionKondisiKelistrikanDetailTahunan()
			{
					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(empty($_POST['awal'])) ? $awal = date("Y") - 5 : $awal = $_POST['awal'];
					(empty($_POST['akhir'])) ? $akhir = date("Y") : $akhir = $_POST['akhir'];
					$min = $akhir-$awal;
					($min >= 5) ? $end =5 : $end = 	$min;
					//tahunan
						
						$tahun = KondisiKelistrikan::model()->getDataTahunan($awal , $akhir);
						$Normal = KondisiKelistrikan::model()->getDataTahunanValue($awal,$akhir , "Normal");
						$Siaga = KondisiKelistrikan::model()->getDataTahunanValue($awal,$akhir , "Siaga");
						$Defisit = KondisiKelistrikan::model()->getDataTahunanValue($awal,$akhir , "Defisit");
						
						$hasilTahun = array(
								array('name' => 'Normal', 'data' => $Normal  ,'type' => 'column'),
								array('name' => 'Siaga', 'data' => $Siaga  ,'type' => 'column'),
								array('name' => 'Defisit', 'data' => $Defisit  ,'type' => 'column'),
						);
					
						
					//


						$this->renderpartial('detail_kondisi_kelistrikan_tahunan' , 
								array(
									'tahun' => $tahun,
									'hasilTahun' => $hasilTahun,
									'end' => $end,
								) ,
								false,
								true
						);
			}


			public function actionTegangan()
			{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;

				$date = date("Y-m-d");
				$month = date("m");
				$year = date("Y");
				$hariIni = ar::formatWaktu($date , "full" , "");
				//Tegangan perjam
				
						$hariIni = ar::formatWaktu($date , "full" , "");
						$tampil_jam = ar::tampilJam();
						$hasilJam = Tegangan::model()->getDataJam($date, "" , "");
				//

				//beban trafo harian
					$exDate = explode(" " ,  $hariIni);
					$labelBulanTahun = $exDate[2]." ".$exDate[3];
					$last = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
					$tampilHari = ar::getDataHarian($last);
					$hasilHari = Tegangan::model()->getDataHarian($month , $year , "" ,"");
					//print_r($hasilHari);
				//
			

				//beban trafo bulanan
					$labelBulan = $year;
					$tampilBulan = ar::getDataBulanan($year);
				    $hasilBulan = Tegangan::model()->getDataBulanan($year , "" , "");
				
				//

				 //beban trafo tahunan
				    $awal = $year-5;
				    $akhir = $year;
				    $labelTahun = $awal." S/D ".$akhir;
				    $tampilTahun = ar::getDataTahunan($awal , $akhir);
				    $hasilTahunan = Tegangan::model()->getDataTahunan($awal , $akhir , "" , "");
				   // print_r($hasilTahunan);
				  //


						$this->render('index_tegangan' , 
						array(
							'hariIni' => $hariIni,
							'tampil_jam' => $tampil_jam,
							'hasilJam' => $hasilJam,

							'labelBulanTahun' => $labelBulanTahun,
							'tampilHari' => $tampilHari,
							'hasilHari' => $hasilHari,

							'labelBulan' => $labelBulan,
							'tampilBulan' =>$tampilBulan ,
							'hasilBulan' =>$hasilBulan ,

							'labelTahun' => $labelTahun,
							'tampilTahun' => $tampilTahun,
							'hasilTahunan' => $hasilTahunan
							
						) , 
						false , 
						true
					
					);


			}

			public function actionTeganganDetailJam()
			{
					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					//beban trafo perjam
					if(empty($_POST['tanggalJam'])) 
					{
						$date = date("Y-m-d"); 
					}else{
						$date = $_POST['tanggalJam'];
					}

					$hariIni = ar::formatWaktu($date , "full" , "");
					$tampil_jam = ar::tampilJam();
					//print_r($hasilJam = PembebananTrafo::model()->getDataJam($date , $_POST['gardu_induk_id'] ,$_POST['trafo_id']));
					$hasilJam = Tegangan::model()->getDataJam($date , $_POST['gardu_hubung_id'] ,$_POST['busbar_id']);
					
				//
				
				
				$this->renderpartial('detail_tegangan_jam' , 
					array(
						'hariIni' => $hariIni,
						'tampil_jam' => $tampil_jam,
						'hasilJam' => $hasilJam,
							
						
					) , 
					false , 
					true
				
				);
					
					
			}


			public function actionTeganganDetailHari()
			{
					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					
					(empty($_POST['bulan'])) ? $month = date("m") : $month = $_POST['bulan'];
					(empty($_POST['tahun'])) ? $year = date("Y") : $year = $_POST['tahun'];

				
					
					 $hariIni =  date("F", mktime(0, 0, 0, $month, 10))." ".$year;

					//beban trafo harian
					
					$labelBulanTahun = $hariIni;
					$last = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
					$tampilHari = ar::getDataHarian($last);
					$hasilHari = Tegangan::model()->getDataHarian($month , $year , $_POST['gardu_hubung_id'] ,$_POST['busbar_id']);
					
					$this->renderpartial('detail_tegangan_hari' , 
					array(
						'labelBulanTahun' => $labelBulanTahun,
						'tampilHari' => $tampilHari,
						'hasilHari' => $hasilHari,

						
						
					) , 
					false , 
					true
				
				);

				//
			}


			public function actionTeganganDetailBulan()
			{	
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				(empty($_POST['tahun'])) ? $year = date("Y") : $year = $_POST['tahun'];

				//beban trafo bulanan
					$labelBulan = $year;
					$tampilBulan = ar::getDataBulanan($year);
				   $hasilBulan = Tegangan::model()->getDataBulanan($year , $_POST['gardu_hubung_id'] , $_POST['busbar_id']);
				//

				  $this->renderpartial('detail_tegangan_bulan' , array(
				  		'labelBulan' => $labelBulan,
						'tampilBulan' =>$tampilBulan ,
						'hasilBulan' =>$hasilBulan ,
				  )
				  , false ,true
				  );
			}


			public function actionTeganganDetailTahun()
			{
				 //beban trafo tahunan
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(empty($_POST['awal'])) ? $awal = date("Y") - 5 : $awal = $_POST['awal'];
					(empty($_POST['akhir'])) ? $akhir = date("Y") : $akhir = $_POST['akhir'];
				    $labelTahun = $awal." S/D ".$akhir;
				    $tampilTahun = ar::getDataTahunan($awal , $akhir);
				    $hasilTahunan = Tegangan::model()->getDataTahunan($awal , $akhir , $_POST['gardu_hubung_id'] , $_POST['busbar_id']);
				  

				    $this->renderpartial('detail_tegangan_tahun' , array(
				  		'labelTahun' => $labelTahun,
						'tampilTahun' =>$tampilTahun ,
						'hasilTahunan' =>$hasilTahunan ,
				  )
				  , false ,true
				  );
			}
			

			public function actionGetPembangkit()
			{
				if(!empty($_POST['jenis_pembangkit_id']))
				{
					$criteria = new CDbCriteria;
					$criteria->condition = 'jenis_pembangkit_id=:id';
					$criteria->params = array(':id' => $_POST['jenis_pembangkit_id']);
				
					$data = MasterPembangkit::model()->findAll($criteria);
					
					$hasil = CHtml::listData($data , 'id' , 'pembangkit');
					
					foreach($hasil as $key => $value)
					{
						echo CHtml::tag('option' , array('value' => $key) , CHtml::encode($value));
					}
				
				}
			
			}



			public function actionBahanBakar()
			{
				$db = Yii::app()->db;
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;

				$date = date("Y-m-d");
				$month = date("m");
				$year = date("Y");
				$hariIni = ar::formatWaktu($date , "full" , "");
				//patok jenis dan pembangkit
				$row = $db->createCommand(" SELECT master_jenis_pembangkit.id AS id_jenis ,
											master_jenis_pembangkit.jenis_pembangkit AS jenis_pembangkit,
											master_pembangkit.id AS id_pembangkit ,
											master_pembangkit.pembangkit AS pembangkit 
											FROM master_jenis_pembangkit
											INNER JOIN master_pembangkit ON master_pembangkit.jenis_pembangkit_id = master_jenis_pembangkit.id
											WHERE master_jenis_pembangkit.date_post IN (SELECT MIN(date_post) FROM master_jenis_pembangkit); ")
											->queryRow();
				
				$jenis_pembangkit_id = $row['id_jenis'];
				$pembangkit_id = $row['id_pembangkit'];
				$pembangkit = $row['pembangkit'];
				$jenis_pembangkit = $row['jenis_pembangkit'];
				$comboPembangkit = CHtml::listData(MasterPembangkit::model()->findAll('jenis_pembangkit_id=:id' , array(':id' => $jenis_pembangkit_id)) , 'id' , 'pembangkit');
				//
				
				
				
				//bahan bakar harian
					$exDate = explode(" " ,  $hariIni);
					$labelBulanTahun = $exDate[2]." ".$exDate[3]." / ($jenis_pembangkit - $pembangkit)";
					$last = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
					$tampilHari = ar::getDataHarian($last);
					$hasilHarianHsd = BahanBakar::model()->getDataHarian($month , $year , $jenis_pembangkit_id,$pembangkit_id ,'hsd');
					$hasilHarianMfo = BahanBakar::model()->getDataHarian($month , $year , $jenis_pembangkit_id,$pembangkit_id ,'mfo');
					$hasilHarianOlein = BahanBakar::model()->getDataHarian($month , $year , $jenis_pembangkit_id,$pembangkit_id ,'olein');
					$hasilHarianBatuBara = BahanBakar::model()->getDataHarian($month , $year , $jenis_pembangkit_id,$pembangkit_id ,'batu_bara');
					
				//


				//bahan bakar bulanan

					$labelBulan = $year." / ($jenis_pembangkit - $pembangkit)";
					$tampilBulan = ar::getDataBulanan($year);
				  
				  	$hasilBulanHsd = BahanBakar::model()->getDataBulanan($year , $jenis_pembangkit_id,$pembangkit_id ,'hsd');
				  	$hasilBulanMfo = BahanBakar::model()->getDataBulanan($year , $jenis_pembangkit_id,$pembangkit_id ,'mfo');
				  	$hasilBulanOlein = BahanBakar::model()->getDataBulanan($year , $jenis_pembangkit_id,$pembangkit_id ,'olein');
				  	$hasilBulanBatuBara = BahanBakar::model()->getDataBulanan($year , $jenis_pembangkit_id,$pembangkit_id ,'batu_bara');
				//


				//beban trafo tahunan
				    $awal = $year-5;
				    $akhir = $year;
				    $labelTahun = $awal." S/D ".$akhir." / ($jenis_pembangkit - $pembangkit)";
				    $tampilTahun = ar::getDataTahunan($awal , $akhir);
				    $hasilTahunanHsd = BahanBakar::model()->getDataTahunan($awal , $akhir , $jenis_pembangkit_id,$pembangkit_id ,'hsd');
				    $hasilTahunanMfo = BahanBakar::model()->getDataTahunan($awal , $akhir , $jenis_pembangkit_id,$pembangkit_id ,'mfo');
				    $hasilTahunanOlein = BahanBakar::model()->getDataTahunan($awal , $akhir , $jenis_pembangkit_id,$pembangkit_id ,'olein');
				    $hasilTahunanBatuBara = BahanBakar::model()->getDataTahunan($awal , $akhir , $jenis_pembangkit_id,$pembangkit_id ,'batu_bara');
				  //
				
				$this->render('index_bahan_bakar' , array(
					'comboPembangkit' => $comboPembangkit,
					'labelBulanTahun' => $labelBulanTahun,
					'tampilHari' => $tampilHari,
					'hasilHarianHsd' => $hasilHarianHsd,
					'hasilHarianMfo' => $hasilHarianMfo,
					'hasilHarianOlein' => $hasilHarianOlein,
					'hasilHarianBatuBara' => $hasilHarianBatuBara,
					
					'labelBulan' => $labelBulan,
					'tampilBulan' => $tampilBulan,
					'hasilBulananHsd' => $hasilBulanHsd,
					'hasilBulananMfo' => $hasilBulanMfo,
					'hasilBulananOlein' => $hasilBulanOlein,
					'hasilBulananBatuBara' => $hasilBulanBatuBara,

					'labelTahun' => $labelTahun,
					'tampilTahun' => $tampilTahun,
					'hasilTahunHsd' => $hasilTahunanHsd,
					'hasilTahunMfo' => $hasilTahunanMfo,
					'hasilTahunOlein' => $hasilTahunanOlein,
					'hasilTahunBatuBara' => $hasilTahunanBatuBara,
				

				));
			
			}

			public function actionBahanBakarDetailHarian()
			{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				
				(empty($_POST['bulan'])) ? $month = date("m") : $month = $_POST['bulan'];
				(empty($_POST['tahun'])) ? $year = date("Y") : $year = $_POST['tahun'];

				$hariIni =  date("F", mktime(0, 0, 0, $month, 10))." ".$year;
				$jenis_pembangkit_id = $_POST['jenis_pembangkit_id'];
				$pembangkit_id = $_POST['pembangkit_id'];
				$jenis_pembangkit = BahanBakar::model()->getJenisPembangkit($_POST['jenis_pembangkit_id']);
				$pembangkit = BahanBakar::model()->getPembangkit($_POST['pembangkit_id']);


				//bahan bakar harian
					
					$labelBulanTahun = $hariIni." / ($jenis_pembangkit - $pembangkit)";
					$last = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
					$tampilHari = ar::getDataHarian($last);
					$hasilHarianHsd = BahanBakar::model()->getDataHarian($month , $year , $jenis_pembangkit_id,$pembangkit_id ,'hsd');
					$hasilHarianMfo = BahanBakar::model()->getDataHarian($month , $year , $jenis_pembangkit_id,$pembangkit_id ,'mfo');
					$hasilHarianOlein = BahanBakar::model()->getDataHarian($month , $year , $jenis_pembangkit_id,$pembangkit_id ,'olein');
					$hasilHarianBatuBara = BahanBakar::model()->getDataHarian($month , $year , $jenis_pembangkit_id,$pembangkit_id ,'batu_bara');
					
				//

					$this->renderpartial('detail_bahan_bakar_harian' , array(
						//'comboPembangkit' => $comboPembangkit,
						'labelBulanTahun' => $labelBulanTahun,
						'tampilHari' => $tampilHari,
						'hasilHarianHsd' => $hasilHarianHsd,
						'hasilHarianMfo' => $hasilHarianMfo,
						'hasilHarianOlein' => $hasilHarianOlein,
						'hasilHarianBatuBara' => $hasilHarianBatuBara,
					
				),false,true);
			}



			public function actionBahanBakarDetailBulanan()
			{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				
				(empty($_POST['tahun'])) ? $year = date("Y") : $year = $_POST['tahun'];
				
				$jenis_pembangkit_id = $_POST['jenis_pembangkit_id'];
				$pembangkit_id = $_POST['pembangkit_id'];
				$jenis_pembangkit = BahanBakar::model()->getJenisPembangkit($_POST['jenis_pembangkit_id']);
				$pembangkit = BahanBakar::model()->getPembangkit($_POST['pembangkit_id']);


				//bahan bakar bulanan

					$labelBulan = $year." / ($jenis_pembangkit - $pembangkit)";
					$tampilBulan = ar::getDataBulanan($year);
				  
				  	$hasilBulanHsd = BahanBakar::model()->getDataBulanan($year , $jenis_pembangkit_id,$pembangkit_id ,'hsd');
				  	
				  	$hasilBulanMfo = BahanBakar::model()->getDataBulanan($year , $jenis_pembangkit_id,$pembangkit_id ,'mfo');
				  	$hasilBulanOlein = BahanBakar::model()->getDataBulanan($year , $jenis_pembangkit_id,$pembangkit_id ,'olein');
				  	$hasilBulanBatuBara = BahanBakar::model()->getDataBulanan($year , $jenis_pembangkit_id,$pembangkit_id ,'batu_bara');
				//

					$this->renderpartial('detail_bahan_bakar_bulanan' , array(
						//'comboPembangkit' => $comboPembangkit,
						'labelBulan' => $labelBulan,
					'tampilBulan' => $tampilBulan,
					'hasilBulananHsd' => $hasilBulanHsd,
					'hasilBulananMfo' => $hasilBulanMfo,
					'hasilBulananOlein' => $hasilBulanOlein,
					'hasilBulananBatuBara' => $hasilBulanBatuBara,
					
				),false,true);
			}


			public function actionBahanBakarDetailTahunan()
			{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				
				
				
				$jenis_pembangkit_id = $_POST['jenis_pembangkit_id'];
				$pembangkit_id = $_POST['pembangkit_id'];
				$jenis_pembangkit = BahanBakar::model()->getJenisPembangkit($_POST['jenis_pembangkit_id']);
				$pembangkit = BahanBakar::model()->getPembangkit($_POST['pembangkit_id']);


				//beban trafo tahunan
				  (empty($_POST['awal'])) ? $awal = date("Y") - 5 : $awal = $_POST['awal'];
					(empty($_POST['akhir'])) ? $akhir = date("Y") : $akhir = $_POST['akhir'];
				    $labelTahun = $awal." S/D ".$akhir." / ($jenis_pembangkit - $pembangkit)";
				    $tampilTahun = ar::getDataTahunan($awal , $akhir);
				    $hasilTahunanHsd = BahanBakar::model()->getDataTahunan($awal , $akhir , $jenis_pembangkit_id,$pembangkit_id ,'hsd');
				    $hasilTahunanMfo = BahanBakar::model()->getDataTahunan($awal , $akhir , $jenis_pembangkit_id,$pembangkit_id ,'mfo');
				    $hasilTahunanOlein = BahanBakar::model()->getDataTahunan($awal , $akhir , $jenis_pembangkit_id,$pembangkit_id ,'olein');
				    $hasilTahunanBatuBara = BahanBakar::model()->getDataTahunan($awal , $akhir , $jenis_pembangkit_id,$pembangkit_id ,'batu_bara');
				  //

					$this->renderpartial('detail_bahan_bakar_tahunan' , array(
						//'comboPembangkit' => $comboPembangkit,
					'labelTahun' => $labelTahun,
					'tampilTahun' => $tampilTahun,
					'hasilTahunHsd' => $hasilTahunanHsd,
					'hasilTahunMfo' => $hasilTahunanMfo,
					'hasilTahunOlein' => $hasilTahunanOlein,
					'hasilTahunBatuBara' => $hasilTahunanBatuBara,
					
				),false,true);
			}
	
			
			public function actionNeracaDaya()
			{

				$db = Yii::app()->db;
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				$date = date("Y-m-d");
				$month = date("m");
				$year = date("Y");
				
				//harian
					$hariIniHarian =  date("F", mktime(0, 0, 0, $month, 10))." ".$year;

					$last = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
					$tampilHari = ar::getDataHarian($last);
					$modelHarian = NeracaDaya::model()->getDataHarianValue($year , $month);

				//

				//bulanan
					$hariIniBulanan = $year;
					$tampilBulan = ar::getDataBulanan($year);
					$modelBulanan = NeracaDaya::model()->getDataBulananValue($year); 
				//


				//tahunan
					$akhir = $year;
					$awal = $year-5;
					$hariIniTahunan = $awal." - ".$akhir;
					$tampilTahunan = ar::getDataTahunan($awal , $akhir);
					$modelTahunan = NeracaDaya::model()->getDataTahunanValue($awal , $akhir); 
				//

					

				$this->render('index_neraca_daya' , array(
					'hariIniHarian' => $hariIniHarian,
					'tampilHari' => $tampilHari , 
					'modelHarian' => $modelHarian,

					'hariIniBulanan' => $hariIniBulanan,
					'tampilBulan' => $tampilBulan,
					'modelBulanan' => $modelBulanan,

					'hariIniTahunan' => $hariIniTahunan,
					'tampilTahunan' => $tampilTahunan,
					'modelTahunan' => $modelTahunan,

				
				) , false , true);
			}

			public function actionNeracaDayaHarian()
			{
				$db = Yii::app()->db;
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				//$date = date("Y-m-d");
				(empty($_POST['bulan'])) ? $month = date("m") : $month = $_POST['bulan'];
				(empty($_POST['tahun'])) ? $year = date("Y") : $year = $_POST['tahun'];

				//harian
					$hariIniHarian =  date("F", mktime(0, 0, 0, $month, 10))." ".$year;

					$last = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
					$tampilHari = ar::getDataHarian($last);
					$modelHarian = NeracaDaya::model()->getDataHarianValue($year , $month);

				//



					$this->renderpartial('detail_neraca_daya_harian' , array(
					'hariIniHarian' => $hariIniHarian,
					'tampilHari' => $tampilHari , 
					'modelHarian' => $modelHarian,
				
					) , false , true);
			}

			public function actionNeracaDayaBulanan()
			{

				$db = Yii::app()->db;
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				
				(empty($_POST['tahun'])) ? $year = date("Y") : $year = $_POST['tahun'];
				//bulanan
					$hariIniBulanan = $year;
					$tampilBulan = ar::getDataBulanan($year);
					$modelBulanan = NeracaDaya::model()->getDataBulananValue($year); 
				//

					$this->renderpartial('detail_neraca_daya_bulanan' , array(
					'hariIniBulanan' => $hariIniBulanan,
					'tampilBulan' => $tampilBulan,
					'modelBulanan' => $modelBulanan,
				
					) , false , true);
			}


			public function actionNeracaDayaTahunan()
			{

				$db = Yii::app()->db;
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				
				
				//tahunan
					(empty($_POST['awal'])) ? $awal = date("Y") - 5 : $awal = $_POST['awal'];
					(empty($_POST['akhir'])) ? $akhir = date("Y") : $akhir = $_POST['akhir'];
					$hariIniTahunan = $awal." - ".$akhir;
					$tampilTahunan = ar::getDataTahunan($awal , $akhir);
					$modelTahunan = NeracaDaya::model()->getDataTahunanValue($awal , $akhir); 
				//

					$this->renderpartial('detail_neraca_daya_tahunan' , array(
					'hariIniTahunan' => $hariIniTahunan,
					'tampilTahunan' => $tampilTahunan,
					'modelTahunan' => $modelTahunan,
				
					) , false , true);
			}
		
	}