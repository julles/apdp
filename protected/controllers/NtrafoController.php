<?php
	class NtrafoController extends Controller
	{
		public $layout = '//layouts/web/utama';

		public function actionIndex()
		{
			$date = date("Y-m-d");
			$day = date("d");
			$lastDay = date('t');
			$month = date("m");
			$year = date("Y");
			
			$awal = $year-4;
			$labelTahunan = "$awal - $year";
			$trafo = $this->trafoPatok($this->garduPatok());
			//perjam

			//

			$this->render('index' , 
				array(
					'date' => $date,
					'listJam' =>  TransPembebananTrafoDetail::model()->listJam($date),
					'nilaiPerjam' => TransPembebananTrafoDetail::model()->valueJam($date , $this->garduPatok() , $trafo),
				
					'listHari' => TransPembebananTrafoDetail::model()->getDataHarian($lastDay),
					'valueHari' => TransPembebananTrafoDetail::model()->getDataHarianValue($month , $year , $lastDay , $this->garduPatok() , $trafo),
				
					'year' => $year,
					'listBulan' => ar::getDataBulananAja(3),
					'valueBulan' => TransPembebananTrafoDetail::model()->getDataBulananValue($year , $this->garduPatok() , $trafo),
					
					'labelTahunan' => $labelTahunan,
					'listTahun' => ar::getDataTahunan($awal , $year),
					'valueTahun' => TransPembebananTrafoDetail::model()->getDataTahunanValue($awal , $year , $this->garduPatok() , $trafo)

				)
			);
		}


		public function actionAjaxJam()
		{	
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				(!empty($_POST['tanggalJam'])) ? $date = $_POST['tanggalJam'] : $date = date("Y-m-d");
				$db = Yii::app()->db;
				$gardu = $_POST['gardu_induk_id'];
				$trafo = $_POST['trafo_id']; 

				
					$this->renderpartial('ajax_perjam' , array(
						'date' => $date,
						'listJam' =>  TransPembebananTrafoDetail::model()->listJam($date),
						'nilaiPerjam' => TransPembebananTrafoDetail::model()->valueJam($date , $gardu , $trafo),
						'listJam' =>  TransPembebananTrafoDetail::model()->listJam($date)
					), false ,true);
				
		}

		public function actionAjaxJamHome()
		{	
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				(!empty($_POST['tanggalJam'])) ? $date = $_POST['tanggalJam'] : $date = date("Y-m-d");
				$db = Yii::app()->db;
				

				
					$this->renderpartial('ajax_perjam_home' , array(
						'date' => $date,
						'listJam' =>  TransPembebananTrafoDetail::model()->listJam($date),
						'chartTrafo' => TransPembebananTrafoDetail::model()->valueJamKurva($date),
						'listJam' =>  TransPembebananTrafoDetail::model()->listJam($date)
					), false ,true);
				
		}

		public function actionAjaxHarian()
		{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			
				(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
				(!empty($_POST['bulan'])) ? $month = $_POST['bulan'] : $month = date("m");
				$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				; 
				$this->renderpartial('ajax_harian' ,
					array(
						'date' => "$year-$month-1",
						
					'listHari' => TransPembebananTrafoDetail::model()->getDataHarian($lastDay),
					'valueHari' => TransPembebananTrafoDetail::model()->getDataHarianValue($month , $year , $lastDay , $_POST['gardu_induk_id'] , $_POST['trafo_id']),
					) , false , true
				);
		}

		public function actionAjaxBulanan()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			
			(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
			$gardu = $_POST['gardu_induk_id'];
			$trafo = $_POST['trafo_id']; 
			$this->renderpartial("ajax_bulanan" , array(
					'year' => $year,
					'listBulan' => ar::getDataBulananAja(3),
					'valueBulan' => TransPembebananTrafoDetail::model()->getDataBulananValue($year , $gardu , $trafo),
					
				) , false , true
			);
		}

		public function actionAjaxTahunan()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			(!empty($_POST['tahun'])) ? $akhir = $_POST['tahun'] : $akhir = date("Y");
			$awal = $akhir-4;
			$labelTahunan = "$awal - $akhir";
			$gardu = $_POST['gardu_induk_id'];
			$trafo = $_POST['trafo_id']; 

			$this->renderpartial('ajax_tahunan' , 
				array(
					'labelTahunan' => $labelTahunan,
					'listTahun' => ar::getDataTahunan($awal , $akhir),
					'valueTahun' => TransPembebananTrafoDetail::model()->getDataTahunanValue($awal , $akhir , $gardu , $trafo)

				) , false , true
			);
		}

		public function garduPatok()
		{
			$db = Yii::app()->db;
			return $db->createCommand("SELECT id FROM master_gardu_induk order by id asc limit 1")->queryScalar(); 
		}

		public function trafoPatok($id)
		{
			$db = Yii::app()->db;
			return $db->createCommand("SELECT id FROM master_trafo WHERE gardu_induk_id = '$id' ORDER BY id asc LIMIT 1")->queryScalar();
		}

		public function actionGetTrafo()
		{
			$data = MasterTrafo::model()->findAll("gardu_induk_id=:gardu_induk_id" , array(':gardu_induk_id' => $_POST['gardu_induk_id']));
			$data = CHtml::listdata($data , 'id' ,'trafo');
			foreach($data as $key => $value)
			{
				echo CHtml::tag('option' , array('value' => $key) , CHtml::encode($value) , true);
			}
		}

		/*public function actionChart()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			$date =  $_POST['tanggalJam'];
			(empty($date)) ? $date = date("Y-m-d") : $date = $date;
			$ex = explode("-",$date);
			$day = $ex[2];
		
			$month = $ex[1];
			$year = $ex[0];
			$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$awal = $year-10;
			$labelTahunan = "$awal - $year";
			$gardu = $_POST['gardu_induk_id'];
			$trafo = $_POST['trafo_id']; 




			$this->renderpartial('ajax' , 
				array(
					'date' => $date,
					'listJam' =>  TransPembebananTrafoDetail::model()->listJam($date),
					'nilaiPerjam' => TransPembebananTrafoDetail::model()->valueJam($date , $gardu , $trafo),

					'listHari' => TransPembebananTrafoDetail::model()->getDataHarian($lastDay),
					'valueHari' => TransPembebananTrafoDetail::model()->getDataHarianValue($month , $year , $lastDay , $this->garduPatok() , $trafo),
					
					'year' => $year,
					'listBulan' => ar::getDataBulananAja(),
					'valueBulan' => TransPembebananTrafoDetail::model()->getDataBulananValue($year , $gardu , $trafo),
					
					'labelTahunan' => $labelTahunan,
					'listTahun' => ar::getDataTahunan($awal , $year),
					'valueTahun' => TransPembebananTrafoDetail::model()->getDataTahunanValue($awal , $year , $this->garduPatok() , $trafo)

				) , false , true
			);
		}*/
	}