<?php
	class NeracaEnergiController extends Controller
	{
		public $layout = '//layouts/web/utama';
		
		/* kepake semua */

		public function allKategori($gardu_induk , $jenis)
		{
			$model = MasterItemNe::model()->findAll('gardu_induk_id=:gardu AND jenis_id=:jenis' , array(':gardu' => $gardu_induk, ':jenis' => $jenis));
			return $model;
		}

		public function actionGetItem()
		{
			$model = $this->allKategori($_POST['gardu_induk_id'] , $_POST['jenis_id']);
			$data = CHtml::listData($model , 'id' , 'item');
			echo"<option value = ''>All</option>";
			foreach($data as $key => $val)
			{
				echo CHtml::tag('option' , array('value' => $key) , CHtml::encode($val));
			}
		}

		/* */


		/* bagian stand */
		public function field($bulan , $tahun , $field , $tgl , $item)
		{
			$db = Yii::app()->db;
			$model = $db->createCommand()
			->select("d.$field")
			->from('neraca_energi_detail d')
			->join('neraca_energi u' , 'd.neraca_energi_id = u.id')
			->join('master_item_ne i' , 'd.item_id=i.id')
			->where('MONTH(u.tanggal)=:m AND YEAR(u.tanggal)=:t AND DAY(u.tanggal)=:d AND item_id=:item' 
				, array(':m' => $bulan , ':t' => $tahun ,':d' => $tgl , ':item' => $item)
			)
			->order('u.tanggal DESC')
					->queryScalar();
			return $model;
		
		}

		public function actionFilter()
		{
				$db = Yii::app()->db;
				(!empty($_POST['tahun'])) ? $tahun = $_POST['tahun'] : $tahun = date("Y");
				(!empty($_POST['bulan'])) ? $bln = $_POST['bulan'] : $bln = date("m");
				$lastDay = cal_days_in_month(CAL_GREGORIAN, $bln, $tahun);
				$gi = $_POST['gardu_induk_id'];
				//$item_id = $_POST['item_id'];
				if(empty($gi))
				{
					$where = 'MONTH(u.tanggal)=:m AND YEAR(u.tanggal)=:t';  
					$param = array(':m' => $bln ,':t' => $tahun);
				}elseif(!empty($gi) && empty($_POST['item_id'])){
						$where = 'MONTH(u.tanggal)=:m AND YEAR(u.tanggal)=:t AND u.gardu_induk_id=:gardu';  
						$param = array(':m' => $bln ,':t' => $tahun,':gardu' => $_POST['gardu_induk_id']);
				//}elseif(!empty($gi) && !empty($_POST['item_id'])){
				//		$where = 'MONTH(u.tanggal)=:m AND YEAR(u.tanggal)=:t AND u.gardu_induk_id=:gardu AND d.item_id=:item ';  
				//		$param = array(':m' => $bln ,':t' => $tahun,':gardu' => $_POST['gardu_induk_id'] , ':item' => $item_id);
				}

				$model = $db->createCommand()
				->select('i.item , d.item_id ,  g.gardu_induk')
				->from('neraca_energi_detail d')
				->join('neraca_energi u' , 'd.neraca_energi_id = u.id')
				->join('master_item_ne i' , 'd.item_id=i.id')
				->join('master_gardu_induk g' , 'u.gardu_induk_id = g.id')
				->where($where, 
						$param
				)

				->order('i.item ASC')
				->group('i.item')
				->query();

			$this->renderpartial('filter' , array(
					'model' => $model,
					'bln' => $bln,
					'tahun' => $tahun,
					'lastDay' => $lastDay
				), false , true);
		}

		

		public function actionStand()
		{
				$db = Yii::app()->db;
				$bln = date('m');
				$tahun = date("Y");
				
				$model = $db->createCommand()
				->select('i.item , d.item_id ,  g.gardu_induk')
				->from('neraca_energi_detail d')
				->join('neraca_energi u' , 'd.neraca_energi_id = u.id')
				->join('master_item_ne i' , 'd.item_id=i.id')
				->join('master_gardu_induk g' , 'u.gardu_induk_id = g.id')
				->where('MONTH(u.tanggal)=:m AND YEAR(u.tanggal)=:t AND u.gardu_induk_id=2' , array(':m' => $bln , ':t' => $tahun ))
				->order('i.item ASC')
				->group('i.item')
				->query();

				$this->render('stand' , array(
					'model' => $model,
					'bln' => $bln,
					'tahun' => $tahun
				));
		}

		/* end stand*/

		/* bagian PSGI */

				public function actionPsgi()
				{
					$date = date("Y-m-d");
					$day = date("d");
					$lastDay = date('t');
					$month = date("m");
					$year = date("Y");
					$awal = 2010;
					$labelRange = "$awal - $year";
					$gardu_induk_id = "";
					$item_id = "";
					$jenis_id = 5;



					$this->render('index_psgi' , array(
							'labelHarian' => substr(ar::waktuYii("$year-$month-1" , "long" ,"") , 1, 100),
							'dataHarian' => ar::getDataHarian($lastDay),
							'hasilHarian' => NeracaEnergiDetail::model()->getDataHarian($year , $month , $lastDay , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor'),
							
							'labelTahunan' => $year,
							'dataTahunan' => ar::getDataBulananAja(3),
							'hasilTahunan' => NeracaEnergiDetail::model()->getDataTahunan($year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor'),
							
							'labelRange' => $labelRange,
							'listRange' => ar::getDataTahunan($awal , $year),
							'dataRange' => NeracaEnergiDetail::model()->getDataRange($awal , $year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor'),
						)
					);
				}

				public function actionPsgiHarian()
				{
					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
					(!empty($_POST['bulan'])) ? $month = $_POST['bulan'] : $month = date("m");
					$gardu_induk_id = $_POST['gardu_induk_id'];
					$item_id = $_POST['item_id'];
					$jenis_id = 5;
					$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					$labelHarian = substr(ar::waktuYii("$year-$month-$lastDay" , "long" ,"") , 2, 100);
					
					$this->renderpartial('psgi_harian' , array(
							'labelHarian' => $labelHarian,
							'dataHarian' => ar::getDataHarian($lastDay),
							'hasilHarian' => NeracaEnergiDetail::model()->getDataHarian($year , $month , $lastDay , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor')
						), false , true
					);

				}

				public function actionPsgBulanan()
				{

					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
					$gardu_induk_id = $_POST['gardu_induk_id'];
					$item_id = $_POST['item_id'];
					$jenis_id = 5;
					$this->renderpartial('psgi_bulanan' , array(
							'labelTahunan' => $year,
							'dataTahunan' => ar::getDataBulananAja(3),
							'hasilTahunan' => NeracaEnergiDetail::model()->getDataTahunan($year   , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor'),
						), false , true
					);
				}

				public function actionPsgiRange()
				{
					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(!empty($_POST['awal'])) ? $awal = $_POST['awal'] : $awal = 2010;
					(!empty($_POST['akhir'])) ? $year = $_POST['akhir'] : $year = date("Y");
					$labelRange = "$awal - $year";
					$gardu_induk_id = $_POST['gardu_induk_id'];
					$item_id = $_POST['item_id'];
					$jenis_id = 5;
					$this->renderpartial('psgi_range' , array(
							'labelRange' => $labelRange,
							'listRange' => ar::getDataTahunan($awal , $year),
							'dataRange' => NeracaEnergiDetail::model()->getDataRange($awal , $year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor'),
						), false , true
					);

				}

		/* END PSGI */

		/* START PSGH */

		public function actionPsgh()
		{
			$date = date("Y-m-d");
			$day = date("d");
			$lastDay = date('t');
			$month = date("m");
			$year = date("Y");
			$awal = 2010;
			$labelRange = "$awal - $year";
			$gardu_induk_id = "";
			$item_id = "";
			$jenis_id = 6;



			$this->render('index_psgh' , array(
					'labelHarian' => substr(ar::waktuYii("$year-$month-1" , "long" ,"") , 1, 100),
					'dataHarian' => ar::getDataHarian($lastDay),
					'hasilHarian' => NeracaEnergiDetail::model()->getDataHarian($year , $month , $lastDay , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor'),
					
					'labelTahunan' => $year,
					'dataTahunan' => ar::getDataBulananAja(3),
					'hasilTahunan' => NeracaEnergiDetail::model()->getDataTahunan($year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor'),
					
					'labelRange' => $labelRange,
					'listRange' => ar::getDataTahunan($awal , $year),
					'dataRange' => NeracaEnergiDetail::model()->getDataRange($awal , $year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor'),
				)
			);
		}

				public function actionPsghHarian()
				{
					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
					(!empty($_POST['bulan'])) ? $month = $_POST['bulan'] : $month = date("m");
					$gardu_induk_id = $_POST['gardu_induk_id'];
					$item_id = $_POST['item_id'];
					$jenis_id = 6;
					$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					$labelHarian = substr(ar::waktuYii("$year-$month-$lastDay" , "long" ,"") , 2, 100);
					
					$this->renderpartial('psgh_harian' , array(
							'labelHarian' => $labelHarian,
							'dataHarian' => ar::getDataHarian($lastDay),
							'hasilHarian' => NeracaEnergiDetail::model()->getDataHarian($year , $month , $lastDay , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor')
						), false , true
					);

				}

				public function actionPsghBulanan()
				{

					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
					$gardu_induk_id = $_POST['gardu_induk_id'];
					$item_id = $_POST['item_id'];
					$jenis_id = 6;
					$this->renderpartial('psgh_bulanan' , array(
							'labelTahunan' => $year,
							'dataTahunan' => ar::getDataBulananAja(3),
							'hasilTahunan' => NeracaEnergiDetail::model()->getDataTahunan($year   , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor'),
						), false , true
					);
				}

				public function actionPsghRange()
				{
					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(!empty($_POST['awal'])) ? $awal = $_POST['awal'] : $awal = 2010;
					(!empty($_POST['akhir'])) ? $year = $_POST['akhir'] : $year = date("Y");
					$labelRange = "$awal - $year";
					$gardu_induk_id = $_POST['gardu_induk_id'];
					$item_id = $_POST['item_id'];
					$jenis_id = 6;
					$this->renderpartial('psgh_range' , array(
							'labelRange' => $labelRange,
							'listRange' => ar::getDataTahunan($awal , $year),
							'dataRange' => NeracaEnergiDetail::model()->getDataRange($awal , $year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor'),
						), false , true
					);

				}
		/* END PSGH */

		/* START PEMBANGKIT */

			public function actionPembangkit()
			{
				$date = date("Y-m-d");
				$day = date("d");
				$lastDay = date('t');
				$month = date("m");
				$year = date("Y");
				$awal = $year - 4;
				$labelRange = "$awal - $year";
				$gardu_induk_id = "";
				$item_id = "";
				$jenis_id = 1;



				$this->render('index_pembangkit' , array(
						'labelHarian' => substr(ar::waktuYii("$year-$month-1" , "long" ,"") , 1, 100),
						'dataHarian' => ar::getDataHarian($lastDay),
						'hasilHarian' => NeracaEnergiDetail::model()->getDataHarianCalculate($year , $month , $lastDay , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor' , 'kwh_impor'),
						
						'labelTahunan' => $year,
						'dataTahunan' => ar::getDataBulananAja(3),
						'hasilTahunan' => NeracaEnergiDetail::model()->getDataTahunanCalculate($year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor','kwh_impor'),
						
						'labelRange' => $labelRange,
						'listRange' => ar::getDataTahunan($awal , $year),
						'dataRange' => NeracaEnergiDetail::model()->getDataRangeCalculate($awal , $year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor','kwh_impor'),
					)
				);
			}


			public function actionPembangkitHarian()
				{
					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
					(!empty($_POST['bulan'])) ? $month = $_POST['bulan'] : $month = date("m");
					$gardu_induk_id = $_POST['gardu_induk_id'];
					$item_id = $_POST['item_id'];
					$jenis_id = 1;
					$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					$labelHarian = substr(ar::waktuYii("$year-$month-$lastDay" , "long" ,"") , 1, 100);

					$this->renderpartial('pembangkit_harian' , array(
							'labelHarian' => $labelHarian,
							'dataHarian' => ar::getDataHarian($lastDay),
							'hasilHarian' => NeracaEnergiDetail::model()->getDataHarianCalculate($year , $month , $lastDay , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor' , 'kwh_impor')
						), false , true
					);

				}

				public function actionPembangkitBulanan()
				{

					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
					$gardu_induk_id = $_POST['gardu_induk_id'];
					$item_id = $_POST['item_id'];
					$jenis_id = 1;
					$this->renderpartial('pembangkit_bulanan' , array(
							'labelTahunan' => $year,
							'dataTahunan' =>ar::getDataBulananAja(3),
							'hasilTahunan' => NeracaEnergiDetail::model()->getDataTahunanCalculate($year   , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor','kwh_impor'),
						), false , true
					);
				}

				public function actionPembangkitRange()
				{
					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(!empty($_POST['awal'])) ? $awal = $_POST['awal'] : $awal = 2011;
					(!empty($_POST['akhir'])) ? $year = $_POST['akhir'] : $year = date("Y");
					$labelRange = "$awal - $year";
					$gardu_induk_id = $_POST['gardu_induk_id'];
					$item_id = $_POST['item_id'];
					$jenis_id = 1;
					$this->renderpartial('pembangkit_range' , array(
							'labelRange' => $labelRange,
							'listRange' => ar::getDataTahunan($awal , $year),
							'dataRange' => NeracaEnergiDetail::model()->getDataRangeCalculate($awal , $year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_expor' , 'kwh_impor'),
						), false , true
					);

				}

		/* END PEMBANGKIT */

		/* START DISTRIBUSI */
			public function actionDistribusi()
			{
				$date = date("Y-m-d");
				$day = date("d");
				$lastDay = date('t');
				$month = date("m");
				$year = date("Y");
				$awal = 2010;
				$labelRange = "$awal - $year";
				$gardu_induk_id = "";
				$item_id = "";
				$jenis_id = 3;



				$this->render('index_distribusi' , array(
						'labelHarian' => substr(ar::waktuYii("$year-$month-1" , "long" ,"") , 2, 100),
						'dataHarian' => ar::getDataHarian($lastDay),
						'hasilHarian' => NeracaEnergiDetail::model()->getDataHarianCalculate($year , $month , $lastDay , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_impor' , 'kwh_expor'),
						
						'labelTahunan' => $year,
						'dataTahunan' => ar::getDataBulananAja(3),
						'hasilTahunan' => NeracaEnergiDetail::model()->getDataTahunanCalculate($year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_impor' , 'kwh_expor'),
						
						'labelRange' => $labelRange,
						'listRange' => ar::getDataTahunan($awal , $year),
						'dataRange' => NeracaEnergiDetail::model()->getDataRangeCalculate($awal , $year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_impor' , 'kwh_expor'),
					)
				);
			}

			public function actionDistribusiHarian()
			{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
				(!empty($_POST['bulan'])) ? $month = $_POST['bulan'] : $month = date("m");
				$gardu_induk_id = $_POST['gardu_induk_id'];
				$item_id = $_POST['item_id'];
				$jenis_id = 3;
				$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				$labelHarian = substr(ar::waktuYii("$year-$month-$lastDay" , "long" ,"") , 2, 100);
				$where = "";
				
				$this->renderpartial('distribusi_harian' , array(
						'labelHarian' => $labelHarian,
						'dataHarian' => ar::getDataHarian($lastDay),
						'hasilHarian' => NeracaEnergiDetail::model()->getDataHarianCalculate($year , $month , $lastDay , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_impor' , 'kwh_expor'),
					), false , true
				);
		
			}

				public function actionDistribusiBulanan()
				{

					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
					$gardu_induk_id = $_POST['gardu_induk_id'];
					$item_id = $_POST['item_id'];
					$jenis_id = 3;
					$this->renderpartial('distribusi_bulanan' , array(
							'labelTahunan' => $year,
							'dataTahunan' => ar::getDataBulananAja(3),
							'hasilTahunan' => NeracaEnergiDetail::model()->getDataTahunanCalculate($year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_impor' , 'kwh_expor'),
						), false , true
					);
				}

				public function actionDistribusiRange()
				{
					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(!empty($_POST['awal'])) ? $awal = $_POST['awal'] : $awal = 2010;
					(!empty($_POST['akhir'])) ? $year = $_POST['akhir'] : $year = date("Y");
					$labelRange = "$awal - $year";
					$gardu_induk_id = $_POST['gardu_induk_id'];
					$item_id = $_POST['item_id'];
					$jenis_id = 3;
					$this->renderpartial('distribusi_range' , array(
							'labelRange' => $labelRange,
							'name' => 'kwh',
							'listRange' => ar::getDataTahunan($awal , $year),
							'dataRange' => NeracaEnergiDetail::model()->getDataRangeCalculate($awal , $year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_impor' , 'kwh_expor'),
						), false , true
					);

				}

		/* END DISTRIBUSI */

		/* START TRANSISI */
			public function actionTransmisi()
			{
				$db = Yii::app()->db;
				$date = date("Y-m-d");
				$day = date("d");
				$lastDay = date('t');
				$month = date("m");
				$year = date("Y");
				$awal = $year-4;
				$labelRange = "$awal - $year";
				$gardu_induk_id = "";
				$item_id = "";
				$jenis_id = 4;
				$pilihan = 1;
				$jenisSusut = 1;
				$text = 'kWh';
				$name = 'Susut Transmisi';
						$hasilHarian = NeracaEnergiDetail::model()->getDataHarianTransmisi($year , $month , $lastDay , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_impor-kwh_expor');
						
						$hasilTahunan = NeracaEnergiDetail::model()->getDataTahunanTransmisi($year   , $gardu_induk_id , $item_id , $jenis_id  , "kwh_impor - kwh_expor");
						
						$dataRange =NeracaEnergiDetail::model()->getDataRangeTransmisi($awal , $year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_impor - kwh_expor');

				$this->render('index_transmisi' , array(

						'name' => $name,
						'text' => $text,
						'labelHarian' => substr(ar::waktuYii("$year-$month-1" , "long" ,"") , 1, 100),
						'dataHarian' => ar::getDataHarian($lastDay),
						'hasilHarian' => $hasilHarian, 
						
						'labelTahunan' => $year,
						'dataTahunan' => ar::getDataBulananAja(3),
						'hasilTahunan' => $hasilTahunan,
						
						'labelRange' => $labelRange,
						'listRange' => ar::getDataTahunan($awal , $year),
						'dataRange' => $dataRange,
					)
				);
			}


			public function actionTransmisiHarian()
			{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
				(!empty($_POST['bulan'])) ? $month = $_POST['bulan'] : $month = date("m");
				$gardu_induk_id = $_POST['gardu_induk_id'];
				$item_id = $_POST['item_id'];
				$jenis_id = 4;
				$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				
				
				$pilihan = $_POST['pilihan'];
				$jenisSusut = $_POST['jenisSusut'];
				$text = 'kWh';


				$labelHarian = substr(ar::waktuYii("$year-$month-$lastDay" , "long" ,"") , 1, 100);
				if($pilihan == 2)
				{
					$name = 'Energi Masuk';
					$hasilHarian = NeracaEnergiDetail::model()->getDataHarianBaru($year , $month , $lastDay , $gardu_induk_id , $item_id , $jenis_id  , 'impor');
					
					
				}elseif($pilihan == 3){
					$name = 'Energi Keluar';
					$hasilHarian = NeracaEnergiDetail::model()->getDataHarianBaruKeluar($year , $month , $lastDay , $gardu_induk_id , $item_id , $jenis_id  , 'expor');
					
				}elseif($pilihan == 1){
					$name = 'Susut Transmisi';
					
					if($jenisSusut == 1)
					{
						$text = 'kWh';

						//$hasilHarian = NeracaEnergiDetail::model()->getDataHarianTransmisi($year , $month , $lastDay , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_impor-kwh_expor');
						
						$hasilHarian = NeracaEnergiDetail::model()->getDataHarianTransmisi($year , $month , $lastDay , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_impor-kwh_expor');
					}elseif($jenisSusut == 2){
						$text = '%';
						$hasilHarian = NeracaEnergiDetail::model()->getDataHarianTransmisiPersen($year , $month , $lastDay , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_impor-kwh_expor');
					
					}
				}
				
				$this->renderpartial('transmisi_harian' , array(
						'name' => $name,
						'text' => $text,
						'labelHarian' => $labelHarian,
						'dataHarian' => ar::getDataHarian($lastDay),
						'hasilHarian' => $hasilHarian,

					), false , true
				);
		
			}



			public function actionTransmisiBulanan()
				{

				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
				$gardu_induk_id = $_POST['gardu_induk_id'];
				$item_id = $_POST['item_id'];
				$jenis_id = 4;

				$pilihan = $_POST['pilihan'];
				$jenisSusut = $_POST['jenisSusut'];
				$text = 'kWh';

				if($pilihan == 2)
				{
					$name = 'Energi Masuk';
					$hasilTahunan = NeracaEnergiDetail::model()->getDataTahunanBaru($year   , $gardu_induk_id , $item_id , $jenis_id  , 'impor');
				}elseif($pilihan == 3){
					$name = 'Energi Keluar';
					$hasilTahunan = NeracaEnergiDetail::model()->getDataTahunanBaruKeluar($year   , $gardu_induk_id , $item_id , $jenis_id  , 'expor');
				}elseif($pilihan == 1){
					$name = 'Susut Transmisi';
					if($jenisSusut == 1)
					{
						$text = 'kWh';

						$hasilTahunan = NeracaEnergiDetail::model()->getDataTahunanTransmisi($year   , $gardu_induk_id , $item_id , $jenis_id  , "kwh_impor - kwh_expor");
						
					}elseif($jenisSusut == 2){
						$text = '%';
						$hasilTahunan  = NeracaEnergiDetail::model()->getDataTahunanTransmisiPersen($year   , $gardu_induk_id , $item_id , $jenis_id  , "kwh_impor - kwh_expor");
					}
				}


					$this->renderpartial('transmisi_bulanan' , array(
							'name' => $name,
							'text' => $text,
							'labelTahunan' => $year,
							'dataTahunan' => ar::getDataBulananAja(3),
							'hasilTahunan' => $hasilTahunan,
						), false , true
					);
				}


				public function actionTransmisiRange()
				{
					Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
					(!empty($_POST['awal'])) ? $awal = $_POST['awal'] : $awal = 2011;
					(!empty($_POST['akhir'])) ? $year = $_POST['akhir'] : $year = date("Y");
					$labelRange = "$awal - $year";
					$gardu_induk_id = $_POST['gardu_induk_id'];
					$item_id = $_POST['item_id'];
					$jenis_id = 4;

					$pilihan = $_POST['pilihan'];
					$jenisSusut = $_POST['jenisSusut'];
					$text = 'kWh';

					if($pilihan == 2)
					{
						$name = 'Energi Masuk';
						$dataRange = NeracaEnergiDetail::model()->getDataRangeBaru($awal , $year  , $gardu_induk_id , $item_id , $jenis_id  , 'impor');
						
					}elseif($pilihan == 3){
						$name = 'Energi Keluar';
						$dataRange = NeracaEnergiDetail::model()->getDataRangeBaruKeluar($awal , $year  , $gardu_induk_id , $item_id , $jenis_id  , 'expor');
					}elseif($pilihan == 1){
						$name = 'Susut Transmisi';
						if($jenisSusut == 1)
						{
							$text = 'kWh';

							$dataRange =NeracaEnergiDetail::model()->getDataRangeTransmisi($awal , $year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_impor - kwh_expor');

						}elseif($jenisSusut == 2){
							$text = '%';
							$dataRange =NeracaEnergiDetail::model()->getDataRangeTransmisiPersen($awal , $year  , $gardu_induk_id , $item_id , $jenis_id  , 'kwh_impor - kwh_expor'); 
						}
					}
					$this->renderpartial('distribusi_range' , array(
							'labelRange' => $labelRange,
							'listRange' => ar::getDataTahunan($awal , $year),
							'dataRange' => $dataRange,
							'name' => $name
						), false , true
					);

				}


		/* END TRANSMISI */
	}