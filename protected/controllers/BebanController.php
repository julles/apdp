<?php

	class BebanController extends Controller
	{
		public $layout = '//layouts/web/utama';

		public function actionIndex()
		{
			$date = date("Y-m-d");
			$day = date("d");
			$lastDay = date('t');
			$month = date("m");
			$year = date("Y");
			
			$awal = $year-4;
			$labelTahunan = "$awal - $year";


			$this->render('index' , array(
				'date' => $date,
				'beban_perjam' => BebanDetail::model()->valueJam($date , "beban_perjam"),
				'prakiraan_beban' => BebanDetail::model()->valueJam($date , "prakiraan_beban"),
				'prakiraan_daya_mampu' => BebanDetail::model()->valueJam($date , "prakiraan_daya_mampu"),
				'listJam' =>  BebanDetail::model()->listJam($date),

				'listHari' => BebanDetail::model()->getDataHarian($lastDay),
				'valueHari' => BebanDetail::model()->getDataHarianValue($month , $year , $lastDay),

				'year' => $year,
				'listBulan' => ar::getDataBulananAja(3),
				'valueBulan' => BebanDetail::model()->getDataBulananValue($year),

				'labelTahunan' => $labelTahunan,
				'listTahun' => ar::getDataTahunan($awal , $year),
				'valueTahun' => BebanDetail::model()->getDataTahunanValue($awal , $year)

			));
		}

		public function actionAjaxJam()
		{	
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				(!empty($_POST['tanggalJam'])) ? $date = $_POST['tanggalJam'] : $date = date("Y-m-d");
				$db = Yii::app()->db;
				$cek = 	$db->createCommand("SELECT tanggal FROM beban WHERE tanggal = '$date'")->queryScalar();
				if(!empty($cek))
				{
					$this->renderpartial('ajax_jam' , array(
						'date' => $date,
						'beban_perjam' => BebanDetail::model()->valueJam($date , "beban_perjam"),
						'prakiraan_beban' => BebanDetail::model()->valueJam($date , "prakiraan_beban"),
						'prakiraan_daya_mampu' => BebanDetail::model()->valueJam($date , "prakiraan_daya_mampu"),
						'listJam' =>  BebanDetail::model()->listJam($date)
					), false ,true);
				}else{
					echo "tidak ada data   untuk tanggal ".ar::formatWaktu($date , "long" , "");
				}
		}

		public function actionAjaxHarian()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			
			(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
			(!empty($_POST['bulan'])) ? $month = $_POST['bulan'] : $month = date("m");
			$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		
				$db = Yii::app()->db;
				$cek = 	$db->createCommand("SELECT tanggal FROM beban WHERE MONTH(tanggal) = '$month' AND YEAR(tanggal) = '$year'")->queryScalar();
				$labelBulanTahun = explode(" ",ar::formatWaktu("$year-$month-1" , "long" ,""));
				$labelBulanTahunFix = $labelBulanTahun[1]." - ".$labelBulanTahun[2];
				
				if(!empty($cek))
				{
					$this->renderpartial('ajax_harian' , array(
						'listHari' => BebanDetail::model()->getDataHarian($lastDay),
						'valueHari' => BebanDetail::model()->getDataHarianValue($month , $year , $lastDay),
						'labelBulanTahunFixs' => $labelBulanTahunFix
					), false ,true);
				}else{
					echo "tidak ada data   untuk bulan $labelBulanTahunFix";
				}

		}		

		public function actionAjaxBulanan()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
			$db = Yii::app()->db;
				$cek = 	$db->createCommand("SELECT tanggal FROM beban WHERE YEAR(tanggal) = '$year'")->queryScalar();
				if(!empty($cek))
				{
					$this->renderpartial('ajax_bulanan' , array(
							'year' => $year,
							'listBulan' => ar::getDataBulananAja(3),
							'valueBulan' => BebanDetail::model()->getDataBulananValue($year)
					), false ,true);
				}else{
					echo "tidak ada data  untuk Tahun $year";
				}
		}

		public function actionAjaxTahunan()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			(!empty($_POST['akhir'])) ? $akhir = $_POST['akhir'] : $akhir = date("Y");
			(!empty($_POST['awal'])) ? $awal = $_POST['awal'] : $awal = $akhir - 4;

				$db = Yii::app()->db;
				$cek = 	$db->createCommand("SELECT tanggal FROM beban WHERE YEAR(tanggal) >= '$awal' AND YEAR(tanggal) <= '$akhir'")->queryScalar();
				if(!empty($cek))
				{	
					$labelTahunan = "$awal - $akhir";
					$this->renderpartial('ajax_tahunan' , array(
							'labelTahunan' => $labelTahunan,
							'listTahun' => ar::getDataTahunan($awal , $akhir),
							'valueTahun' => BebanDetail::model()->getDataTahunanValue($awal , $akhir)
					), false ,true);
				}else{
					echo "tidak ada data  untuk range $awal s/d $akhir";
				}
		}

	}