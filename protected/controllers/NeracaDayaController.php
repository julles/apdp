<?php
	class NeracaDayaController extends Controller
	{
		public $layout = '//layouts/web/utama';
	
		public function actionIndex()
		{
			$date = date("Y-m-d");
			$day = date("d");
			$lastDay = date('t');
			$month = date("m");
			$year = date("Y");
			
			$awal = $year-4;
			$akhir = $year;
			$labelTahunan = "$awal - $year";
			//harian
			$listHarian = TransNeracaDaya::model()->getDataHarian($lastDay);
			$derating = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "derating");
			$beban = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "beban_terlayani");
			$beban_puncak = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "beban_puncak");
			$padam = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "padam");
			$cad_operasi = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "cad_ops");
			$fo = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "fo");
			$terpasang = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "terpasang");
			$po = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "po");
			$dm = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "daya_mampu");
			$fdr = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "fdr");
			$mo = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "mo");
			//
			


			$this->render("index" , array(
				'listHarian' => $listHarian,
				'beban' => $beban,
				'padam' => $padam,
				'derating' => $derating,
				'cad_operasi' => $cad_operasi,
				'fo' => $fo,
				'terpasang' => $terpasang,
				'po' => $po,
				'beban' => $beban,
				'dm' => $dm,
				'beban_puncak' => $beban_puncak,
				'fdr' => $fdr,
				'mo' => $mo,

				'listBulan' => ar::getDataBulananAja(3),
				'year' => $year,
				'bebanBulan' => TransNeracaDaya::model()->getDataBulananValue($year,"beban_terlayani"),
				'padamBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"padam"),
				'deratingBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"derating"),
				'cad_operasiBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"cad_ops"),
				'foBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"fo"),
				'terpasangBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"terpasang"),
				'poBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"po"),
				//'bebanBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,""),
				'dmBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"daya_mampu"),
				'beban_puncakBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"beban_puncak"),
				'fdrBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"fdr"),
				'moBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"mo"),

				'labelTahunan' => $labelTahunan,
				'listTahun' => ar::getDataTahunan($awal , $year),

				'bebanTahun' => TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"beban_terlayani"),
				'padamTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"padam"),
				'deratingTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"derating"),
				'cad_operasiTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"cad_ops"),
				'foTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"fo"),
				'terpasangTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"terpasang"),
				'poTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"po"),
				//'bebanTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,""),
				'dmTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"daya_mampu"),
				'beban_puncakTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"beban_puncak"),
				'fdrTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"fdr"),
				'moTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"mo"),

			));
		}

		public function actionAjaxHarian()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			
			(!empty($_POST['tahun'])) ? $year = $_POST['tahun'] : $year = date("Y");
			(!empty($_POST['bulan'])) ? $month = $_POST['bulan'] : $month = date("m");
			$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);

			$listHarian = TransNeracaDaya::model()->getDataHarian($lastDay);
			$derating = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "derating");
			$beban = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "beban_terlayani");
			$beban_puncak = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "beban_puncak");
			$padam = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "padam");
			$cad_operasi = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "cad_ops");
			$fo = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "fo");
			$terpasang = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "terpasang");
			$po = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "po");
			$dm = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "daya_mampu");
			$fdr = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "fdr");
			$mo = TransNeracaDaya::model()->getDataHarianValue($month , $year , $lastDay , "mo");

			$this->renderpartial('ajax_harian' , 
				array(
						'listHarian' => $listHarian,
						'beban' => $beban,
						'padam' => $padam,
						'derating' => $derating,
						'cad_operasi' => $cad_operasi,
						'fo' => $fo,
						'terpasang' => $terpasang,
						'po' => $po,
						'beban' => $beban,
						'dm' => $dm,
						'beban_puncak' => $beban_puncak,
						'fdr' => $fdr,
						'mo' => $mo
						) , false , true
			);
		}

		public function actionAjaxBulanan()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			(!empty($_POST['year'])) ? $year = $_POST['year'] : $year = date("Y");
			$this->renderpartial('ajax_bulanan' , array(
				
				'listBulan' => ar::getDataBulananAja(3),
				'year' => $year,
				'bebanBulan' => TransNeracaDaya::model()->getDataBulananValue($year,"beban_terlayani"),
				'padamBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"padam"),
				'deratingBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"derating"),
				'cad_operasiBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"cad_ops"),
				'foBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"fo"),
				'terpasangBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"terpasang"),
				'poBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"po"),
				//'bebanBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,""),
				'dmBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"daya_mampu"),
				'beban_puncakBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"beban_puncak"),
				'fdrBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"fdr"),
				'moBulan' =>TransNeracaDaya::model()->getDataBulananValue($year,"mo"),

				) , false , true
			);
		}

		public function actionAjaxTahunan()
		{
			Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
			(!empty($_POST['akhir'])) ? $akhir = $_POST['akhir'] : $akhir = date("Y");
			(!empty($_POST['awal'])) ? $awal = $_POST['awal'] : $awal = $_POST['akhir'] - 4;

			$labelTahunan = "$awal - $akhir";

			$this->renderpartial('ajax_tahunan' , array(
				'labelTahunan' => $labelTahunan,
				'listTahun' => ar::getDataTahunan($awal , $akhir),

				'bebanTahun' => TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"beban_terlayani"),
				'padamTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"padam"),
				'deratingTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"derating"),
				'cad_operasiTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"cad_ops"),
				'foTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"fo"),
				'terpasangTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"terpasang"),
				'poTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"po"),
				//'bebanTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,""),
				'dmTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"daya_mampu"),
				'beban_puncakTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"beban_puncak"),
				'fdrTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"fdr"),
				'moTahun' =>TransNeracaDaya::model()->getDataTahunanValue($awal , $akhir,"mo"),
				) , false , true
			);
		}
	}