<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */

	public $layout = '//layouts/web/utama';
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */


	public function actionPencarian()
	{
		$key =  $_POST['keyword'];
		$criteria = new CDbCriteria;
		$criteria->condition = "judul like :key";
		$criteria->params = array(":key" => "%$key%");

		$countBerita = Berita::model()->count($criteria);
		$countPengumuman = Pengumuman::model()->count($criteria);
		$plus = $countBerita + $countPengumuman;
		

		
		if($plus > 0)
		{
			//$pages = new CPagination($countBerita);
			//$pages->pageSize = 1;
			//$pages->applyLimit($criteria);

			$modelBerita = Berita::model()->findAll($criteria);
			$modelPengumuman = Pengumuman::model()->findAll($criteria);
			$this->renderpartial("pencarian" , array('modelBerita' => $modelBerita , 'modelPengumuman' => $modelPengumuman));
		}else{
			$this->renderpartial("pencarian_gagal");
		}
			
		
	}

	public function actionProfilPerusahaan()
	{
		$model = Profil::model()->find(array('order' => 'id DESC','limit' => 1));
		$this->render('profil' , array('model' => $model));
	}

	public function actionVisiMisi()
	{
		$model = Visi::model()->find(array('order' => 'id DESC','limit' => 1));
		$this->render('visi' , array('model' => $model));
	}


	public function actionWilayahKerja()
	{
		$model = WilayahKerja::model()->find(array('order' => 'id DESC','limit' => 1));
		$this->render('wilayah' , array('model' => $model));
	}


	public function actionStrukturOrganisasi()
	{
		$model = Struktur::model()->find(array('order' => 'id DESC','limit' => 1));
		
		$alamat = Yii::app()->baseUrl."/images/";

		(!empty($model->gambar)) ? $image = $alamat."struktur/thumb/".$model->gambar : $image = $alamat."no.jpg";
		
		$gambar = '<img style="width:565px;" src="'.$image.'" />';
		

		$this->render('struktur' , array('model' => $model , 'gambar' => $gambar));
	}

	public function actionProsesBisnis()
	{
		$model = ProsesBisnis::model()->find(array('order' => 'id DESC','limit' => 1));
		$this->render('proses' , array('model' => $model));
	}

	public function rowBahanBakar()
	{
		$db = Yii::app()->db;
		$row = $db->createCommand(" SELECT master_jenis_pembangkit.id AS id_jenis ,
		master_jenis_pembangkit.jenis_pembangkit AS jenis_pembangkit,
		master_pembangkit.id AS id_pembangkit ,
		master_pembangkit.pembangkit AS pembangkit 
		FROM master_jenis_pembangkit
		INNER JOIN master_pembangkit ON master_pembangkit.jenis_pembangkit_id = master_jenis_pembangkit.id
		WHERE master_jenis_pembangkit.date_post IN (SELECT MIN(date_post) FROM master_jenis_pembangkit); ")
		->queryRow();
		return $row;
	}
	
	

	public function actionIndex()
	{
		

		$date = date("Y-m-d");
		$dates = date("Y-m-d H:i:s");
		$day = date("d");
		$lastDay = date('t');
		$month = date("m");
		$year = date("Y");

		$hariIni = ar::formatWaktu($date , "full" , "");
		$labelHarian = substr(ar::waktuYii("$year-$month-1" , "long" ,"") , 1, 100);
		//beban trafo perjam
		$chartTrafo = TransPembebananTrafoDetail::model()->valueJamKurva($date);
		//

		//susut transmisi
		$text = '%';
		$hasilHarian = NeracaEnergiDetail::model()->getDataHarianTransmisiPersen($year , $month , $lastDay , "" , "" , 4  , 'kwh_impor-kwh_expor');
		//

		$this->render('index' , 
				array(
					'date' => $date,
					'beban_perjam' => BebanDetail::model()->valueJam($date , "beban_perjam"),
					'prakiraan_beban' => BebanDetail::model()->valueJam($date , "prakiraan_beban"),
					'prakiraan_daya_mampu' => BebanDetail::model()->valueJam($date , "prakiraan_daya_mampu"),
					'listJam' =>  BebanDetail::model()->listJam($date),
					
					'chartTrafo' => $chartTrafo,

					'text' => $text,
					'hasilHarian' => $hasilHarian,
					'labelHarian' => $labelHarian,
					'dataHarian' => ar::getDataHarian($lastDay),
					'name' => 'Susut Transmisi',

					
					'hsd' => BahanBakar::model()->getDataHarianValue($year , $month , $lastDay , "" , "" , "" , 'hsd'),
					'mfo' => BahanBakar::model()->getDataHarianValue($year , $month , $lastDay , "" , "" , "" , 'mfo'),
					'olein' => BahanBakar::model()->getDataHarianValue($year , $month , $lastDay , "" , "" , "" , 'olein'),
					'batu_bara' => BahanBakar::model()->getDataHarianValue($year , $month , $lastDay , "" , "" , "" , 'batu_bara'),
				)
		);
	
	}


	public function actionBahanBakarDetailHarian()
			{
				Yii::app()->clientScript->scriptMap['highcharts.src.js'] = false;
				
				(empty($_POST['bulan'])) ? $month = date("m") : $month = $_POST['bulan'];
				(empty($_POST['tahun'])) ? $year = date("Y") : $year = $_POST['tahun'];

				$hariIni =  date("F", mktime(0, 0, 0, $month, 10))." ".$year;
				$jenis_pembangkit_id = $_POST['jenis_pembangkit_id'];
				$pembangkit_id = $_POST['pembangkit_id'];
				$jenis_pembangkit = BahanBakar::model()->getJenisPembangkit($_POST['jenis_pembangkit_id']);
				$pembangkit = BahanBakar::model()->getPembangkit($_POST['pembangkit_id']);


				//bahan bakar harian
					
					$labelBulanTahun = $hariIni." / ($jenis_pembangkit - $pembangkit)";
					$last = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
					$tampilHari = ar::getDataHarian($last);
					$hasilHarianHsd = BahanBakar::model()->getDataHarian($month , $year , $jenis_pembangkit_id,$pembangkit_id ,'hsd');
					$hasilHarianMfo = BahanBakar::model()->getDataHarian($month , $year , $jenis_pembangkit_id,$pembangkit_id ,'mfo');
					$hasilHarianOlein = BahanBakar::model()->getDataHarian($month , $year , $jenis_pembangkit_id,$pembangkit_id ,'olein');
					$hasilHarianBatuBara = BahanBakar::model()->getDataHarian($month , $year , $jenis_pembangkit_id,$pembangkit_id ,'batu_bara');
					
				//

					$this->renderpartial('ajax_bahan_bakar' , array(
						//'comboPembangkit' => $comboPembangkit,
						'labelBulanTahun' => $labelBulanTahun,
						'tampilHari' => $tampilHari,
						'hasilHarianHsd' => $hasilHarianHsd,
						'hasilHarianMfo' => $hasilHarianMfo,
						'hasilHarianOlein' => $hasilHarianOlein,
						'hasilHarianBatuBara' => $hasilHarianBatuBara,
					
				),false,true);
			}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		$this->layout = 'empat';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}


	public function actionGetBeban()
	{
		date_default_timezone_set('Asia/Jakarta');
		 $date = date("Y-m-d");
		 $waktu = date('H');
		 $sql =   "SELECT beban_perjam FROM beban_detail INNER JOIN beban ON beban.id = beban_detail.beban_id WHERE  tanggal = '$date' AND jam like '%$waktu%'  ORDER BY jam DESC LIMIT 1";
	
		$cari = Yii::app()->db->createCommand($sql)->queryScalar();
		echo $cari;
	}

	/**
	 * Displays the contact page
	 */
	/*public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	/*public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	/*public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}*/
}