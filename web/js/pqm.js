/* Method untuk membuat objek XMLHttpRequest */



function createXMLHttpRequest2() 
{
	var xmlHttp;
	
	if (window.XMLHttpRequest) 
	{
		xmlHttp = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) 
	{
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	return xmlHttp;
}

/* Method untuk menyiapkan timer yang merefresh data */
function initTimer(realtime_id, id_GI, id_inc)
{
	var f = function()
	{
		getData(realtime_id, id_GI, id_inc);
	}

	// siapkan timer dengan interval 10 detik
	setInterval(f, 10000);
}

/* Method untuk mengambil data dari database
dengan metode AJAX */
function getData(realtime_id, id_GI, id_inc)
{
	// instansiasi objek xmHttpRequest baru
	var xmlHttp = createXMLHttpRequest2();
	
	var randomID = Math.random();
	
	// atur beberapa properti dari objek xmlHttp
	xmlHttp.onreadystatechange = function()
	{
		// jika response sudah didapat
		if (xmlHttp.readyState == 4)
		{
			if (xmlHttp.status == 200)
			{	
				document.getElementById(realtime_id).innerHTML = xmlHttp.responseText;
			}
		}
	}
	
	xmlHttp.open("GET", "pqm/getLatest_powerQuality.php?gi=" + id_GI + "&inc=" + id_inc + "&rid=" + randomID, true);
	xmlHttp.send(null);
}
// JavaScript Document
