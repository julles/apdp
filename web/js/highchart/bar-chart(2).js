$(function () {
	$('#container3').highcharts({
		chart: {
			type: 'column'
		},
		title: {
			text: 'Beban Puncak 2014'
		},
		xAxis: {
			categories: [
				'Jan 2014',
				'Feb 2014',
				'Mar 2014',
				'Apr 2014',
				'May 2014',
				'Jun 2014',
				'Jul 2014',
				'Aug 2014',
				'Sep 2014',
				'Oct 2014',
				'Nov 2014',
				'Dec 2014'
			]
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Daya (MW)'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y:.1f} mw</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: [{
			name: '',
			data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
	
		}]
	});
});